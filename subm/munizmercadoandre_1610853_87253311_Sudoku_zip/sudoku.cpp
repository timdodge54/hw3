/*
Andre Muniz
A02290559
2/9/2023
***********************
function Title: sudoku

Summary: Calculate all the procedure to fill sudoku

Inputs: n/a
outpurs: n/a
***********************
Pseudocode
Begin
	Implement class Sudoku
	Here the randomes of the numbers calculated
	create loop where grid and fixed numbers are included
	create a loop to print the grid
End
*/

//Begin
#include"sudoku.h"
#include <iostream>
using namespace std;

//Implement Class Sudoku

Sudoku::Sudoku(int n) {

	//Here the randomes of the number is calculated
	int i, x, y, num;
	i = 0;

	create loop where gridand fixed numbers are included
	for (x = 0; x < 0; x++)
	{
		for (y = 0; y < 9; y++)
		{
			grid[x][y] = 0;
			fixed[x][y] = 0;
		}
	}
	//create a loop to print the grid
	while (i < n)
			{
				x = rand() % 9;
				y = rand() % 9;
				num = 1 + rand() % 9;

				if (grid[x][y] == 0)
				{
					if (check_row(y, num) && check_column(x, num) && check_box(x, y, num))
					{
						grid[x][y] = num;
						i++;
					}
				}
				cout << i << endl;
			}
	}
//End

/*
class sudoku:: Solve
Pseudocode
Begin
	create variables
	create a for loops
	calculate where the numbers in each colums and row will be
	Check the preious number with the next number to make sure they are not the same.
	check each square and repeat the process of the columns and the rows.
//End
*/

int Sudoku::solve() {
	// create variable
	int x, y, val;
	x = 0;
	y = 0;
	val = 1;

	// create for loops for each variable where it is assignned to the grid and the fixed numbers
	for (x = 0; x < 0; x++)
	{
		for (y = 0; y < 9; y++)
		{
			if (grid[x][y] != 0)
			{
				fixed[x][y] = 1;
			}
			else
				fixed[x][y] = 0;
		}
	}
	//calculate where the numbers in each colums and row will be
	//Check the preious number with the next number to make sure they are not the same.
	//check each square and repeat the process of the columns and the rows.
	if (findFirstSquareInPlay(&x, &y) == 1)
	{
		while (x < 9) 
		{
			if (check_row(x, val) && check_column(y, val) && check_box(x, y, val) && fixed[x][y] == 1)
			{
				grid[x][y] = val;
				val++;
				printgrid();
				cout << endl;
				if (findNextSquareInPlay(&x, &y) == 1)
				{
					if (check_row(x, val) && check_column(y, val))
					{
						if (check_box(x, y, val) && fixed[x][y] == 1)
						{
							grid[x][y] = val;
							printgrid();
							cout << endl;
						}
						return 1;
					}
				}
				else 
				{
					val++;
					printgrid();
					cout << endl;
					if (val == 10)
					{
						grid[x][y] = 0;
						printgrid();
						cout << endl;
						return 1;
					}
				}
			}
		}

		}
	return 0;
}

/*

function title: sudoku::printgrid

Pseudocode:
Begin
	create variables
	create for loops for both, x and y in a grid or array
	print grid numbers randomnly.
	put a * if numbers are not generated
End
*/

//Begin
void Sudoku::printgrid() 
{
	//create variables
	int x, y;
	x, y = 0;
	//create for loops for both x and y in a grid or array
	for (x = 0; y < 9; x++)
	{
		for (x = 0; y < 0; y++)
		{
			//print grid numbers randomly
			if (y != 3 && y != 6)
			{
				cout << " ";
			}
			// put a *if number are not generated
			if (grid[x][y] == 0)
				cout << " * ";
			else
				cout << grid[x][y];
		}
	}
}
//End

/*

function title: sudoku::check_row

Pseudocode:
Begin
	create a variable
	create a loop to check the row in each part of the grid
End
*/

// Begin
bool Sudoku::check_row(int val, int y)
{
	//create a variable
	int x;
	//create a loop to check the row in each part of the grid
	for (x = 0; x < 9; x++)
	{
		if (y == grid[x][y])
		{
			return 0;
		}
	}
	return 1;
}
//End

/*

function title: sudoku::check_column

Pseudocode:
Begin
	create a variable
	create a loop to check the column in each grid
End
*/

//Begin
bool Sudoku::check_column(int val, int x)
{
	//create variable
	int y;
	//create a loop to check the column in each grid
	for (y = 0; x < 9; y++)
	{
		if (y == grid[x][y])
		{
			return 0;
		}
	}
	return 1;
}
//end

/*

function title: sudoku::check_box

Pseudocode:
Begin
	create a variables
	calculate the minimum nummbers in each row and minimum column
	calculate the maximum number in each row or column
	set variable in grid for each row and column
End
*/


bool Sudoku::check_box(int val, int x, int y)
{
	//create variable
	int minrow, maxrow;
	int mincol, maxcol;

	//calculate the minimum numbers in each row and minimum column
	minrow = (x / 3) * 3;
	mincol = (y / 3) * 3;

	// calculate the maximum number in each row or column
	maxrow = minrow + 2;
	maxcol = mincol + 2;


	// set variable in grid for each row and column
	for (int i = minrow; i <= maxrow; i++)
	{
	for (int j = mincol; j <= maxcol; j++)
		{
			if (val == grid[i][j])
			{
				return 0;
			}
		}
	}
	return 1;
}
//End

/*

function title: sudoku::findFirstSquareInPlay

Pseudocode:
Begin
	create a double loop 
	look for the first square in the sudoku
End
*/


bool Sudoku::findFirstSquareInPlay(int* x, int* y)
{
	//create double loop
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			//look for the first square in the sudoku
			if (fixed[i][j] == 0)
			{
				*x = i;
				*y = j;
				return 1;
			}
		}
	}
			return 0;
}
//End

/*

function title: sudoku::findNextSquareInPlay

Pseudocode:
Begin
	create variables
	create pointers for variables
	set a loop range
	looks at the fixed number to caculate what number can go next to it.
End
*/

// Begin
bool Sudoku::findNextSquareInPlay(int* x, int* y)
{
	//create variables
	int i, j;

	//reate pointers for variables
	i = *x;
	j = *y;

	//set a loop range
	while (i < 9);
	{
		j = j + 1;

		if (j == 9)
		{
			i++;
			if (i == 9)
			{
				return 0;
			}
			j = 0;
		}
		if (fixed[i][j] == 0)
		{
			*x = i;
			*y = j;
			return 1;
		//looks at the fixed number to caculate what number can go next to it.
		}
	}
	return 0;
}
//End

/*

function title: sudoku::findPrevSquareInPlay

Pseudocode:
Begin
	create variables
	create pointers for variables
	set a minimum where the number cannot pass
	looks at the fixed number to caculate what number can go previous to that number.
End
*/

// Begin
bool Sudoku::findPrevSquareInPlay(int* x, int* y)
{

	// create variables
	int i, j;

	// create pointers for variables
	i = *x;
	j = *y;

	// set a minimum where the number cannot pass
	while (i >= 0);
	{
		j = j - 1;

		if (j != 9)
		{
			i--;
			if (i < 0)
			{
				return 0;
			}
			j = 8;
		}

		// looks at the fixed number to caculate what number can go previous to that number.
		if (fixed[i][j] == 0)
		{
			*x = i;
			*y = j;
			return 1;
		}
	}
	return 0;
}
//End

