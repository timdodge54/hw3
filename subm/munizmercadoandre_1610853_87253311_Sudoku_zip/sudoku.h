/*
Andre Muniz
A02290559
2/9/2023
***********************
Function title: Sudoku

Summary: Create classes for functions in sudoku.cpp

*****************************
Pseudocode
Begin
	Create public class for Sudoku, solve, and printgrid
*/

#ifndef SUDOKU

#define SUDOKU

class Sudoku{
//Create public class for Sudoku, solve, and printgrid
public:
	Sudoku(int start_num);
	int solve();
	void printgrid();

// Create private class for check_row, check column, check_box
// create private class findFirstSquareInPlay, findNextSquareInPlay, findPrevSquareInPlay
private:
	bool check_row(int val, int y);
	bool check_column(int val, int x);
	bool check_box(int val, int x, int y);
	bool findFirstSquareInPlay(int *x, int *y);
	bool findNextSquareInPlay(int *x, int *y);
	bool findPrevSquareInPlay(int *x, int *y);

	int grid[9][9];
	int fixed[9][9];
};
#endif