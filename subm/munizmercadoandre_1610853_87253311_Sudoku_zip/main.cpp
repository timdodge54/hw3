/*
Andre Muniz
A02290559
2/9/2023
************************
Function Title: Main

Summary: welcomes user to SudokuSolver, and ask them to enter preopulation

Input: user enter number of preopulation
Output: Sudoku is created
***************************
Pseudocode
"Using example provided by Proffesor
Begin
	create main function
	print "welcome to sodoku Solver!!!
	print "Enter number of squares to preppopulate
	print out the grid
	if it solvavable print solved
	if it is unsolvavable print sorry unsolvable
End
*/

//Begin
#include "sudoku.h" 
#include <iostream> 

using namespace std;

// create main function
int main()
{
	int n;
	// print "welcome to sodoku Solver!!!
	cout << "Welcome to SudokuSolver!!" << endl;

	//print "Enter number of squares to preppopulate
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	//print "Enter number of squares to preppopulate
	Sudoku s(n);

	//print out the grid
	s.printgrid();

	//if it solvavable print solved
	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printgrid();
	}
	else {
		cout << "Sorry, unsolvable..." << endl; //if it is unsolvavable print sorry unsolvable
	}

	return 0;
}