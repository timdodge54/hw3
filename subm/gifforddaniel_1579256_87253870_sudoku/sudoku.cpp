﻿#include"sudoku.h"
using namespace std;
/*********************************************************************
* @name Sudoku::Sudoku
* @brief Generates 9x9 array with 0's then makes random amount of n 
* numbers through the array
* @param int n
* @retval  Sudoku
*********************************************************************/
Sudoku::Sudoku(int n)
{
	char x, y, i, j;
	int count, v;
	srand(time(NULL));
	for (i = 0; i < 9; i++) // Initialize grids to 0
	{
		for (j = 0; j < 9; j++)
		{
			grid[i][j] = 0;
			fixed[i][j] = 0;
		}
	}
	for (count = 0; count < n;) // Populate the grid with n random numbers 1-9
	{
		x = rand() % 9; // generates 0-8
		y = rand() % 9;
		//cout << "random value!\n";
		if (fixed[x][y] == 0) // check if square is empty
		{
			v = (rand() % 9 + 1);
			if (!(check_box(x, y, v)) && !check_col(x, v) && !check_row(y, v)) // check if valid
			{
				grid[x][y] = v;
				fixed[x][y] = 1;
				count++;
			}
			//cout << "value\n";
		}
	}

}
/*********************************************************************
* @name Sudoku::solve
* @brief solves the array with possible values to complete the sudoku
* @param none
* @retval bool solve
*********************************************************************/
bool Sudoku::solve()
{
	int n, * p;
	first_square();
	last_square();
	p = first;
	n = 1;
	do
	{
		// If no violations
		if (!(check_box(x, y, n)) && !check_col(x, n) && !check_row(y, n)) // If no violations
		{
			grid[x][y] = n; // Place n at (x,y)
			//fixed[x][y] = 1;
			if (x == 8 && y == 8)
	// cases that mean the grid is solved. 
		//Not all were needed, but at least one of them works
				return true;
			if (*last != 0)
				return true;
			if (!(next_square()))
				return true;
			
			n = 1; // Set n = 1
			
		}
		else
		{

			if (n < 9) // Don't increment n above 9 (bad things happen)
			{
				n++;
			}
			else
			{
				grid[x][y] = 0; // Clear square and move back
				if (!(prev_square()))
				{
					return false; // Check if off grid
				}
				// exit
				//cout << "back\n";
				if (grid[x][y] < 9)
				{
					n = grid[x][y] + 1; // Set n to (x,y) + 1
				}

			}
			
		}

		if (n == 9 && &fixed[x][y] == first) 
			return false;
		//cout << "solve loop\n";
		if (&fixed[x][y] == last && fixed[x][y] == 1)
			return true;
	} while (x < 9 || y < 9); // EndLoop when off the grid

	return true;
}
/*********************************************************************
* @name Sudoku::print_grid
* @brief prints array with grid lines for Sukoku, prints *'s for 0 values
* @param none
* @retval  none
*********************************************************************/
void Sudoku::print_grid()
{
	int x, y;
	cout << "\t";
	for (y = 0; y < 9; y++) // Step through grid and print
	{
		for (x = 0; x < 9; x++)
		{
			if (grid[x][y] == 0) // Print a * for empty spaces
			{
				if (x == 2 || x == 5)
				{
					cout << "* | ";
				}
				else
				cout << "*  ";
			}
			else
			{
				if (x == 2 || x == 5)
				{
					cout << grid[x][y] << " | ";
				}
				else
				{
					cout << grid[x][y] << "  "; // Print number
				}
			}
			if (x == 8)
			{
				if (y == 2 || y == 5)
				{
					cout << "\n\t-----------------------------\n\t";
				}
				else
				{
				cout << "\n\t"; // new row
				}
			}
		}
	}
}
/*********************************************************************
* @name Sudoku::check_row
* @brief checks to see if there are duplicates in the row of the grid
* @param int y, int v
* @retval  bool check_row
*********************************************************************/
bool Sudoku::check_row(int y, int v)
{
	int* p = &grid[0][y];
	for (int i = 0; i < 9; p++, i++)
	{
		if (grid[i][y] == v)
		{
			//cout << "row bad\n";
			return true; // duplicate found
		}
	}
	//cout << "row good\n";
	return false; // no duplicates
}
/*********************************************************************
* @name Sudoku::check_col
* @brief checks column for duplicates
* @param int x, int y
* @retval  bool check_col
*********************************************************************/
bool Sudoku::check_col(int x, int v)
{
	int* p = &grid[x][0];
	for (int i = 0; i < 9; i++)
	{
		if (grid[x][i] == v) // duplicate found
		{
			//cout << "col bad\n";
			return true;
		}
	}
	//cout << "col good\n";
	return false; // no duplicates
}
/*********************************************************************
* @name Sudoku::check_box
* @brief checks the individual 3x3 boxes to see if there are duplicates
* @param int x, int y, int v
* @retval  bool check_box
*********************************************************************/
bool Sudoku::check_box(int x, int y, int v)
{
	int xx = x - (x % 3); //Set position to top left of box
	int yy = y - (y % 3);
	for (int j = 0; j < 3; j++) // Loop through box
	{
		for (int i = 0; i < 3; i++)
		{
			if (grid[xx + i][yy + j] == v)
			{
				//cout << "box bad\n";
				return true; // duplciate found
			}
		}
	}
	//cout << "box good\n";
	return false; // no duplicates!!
}
/*********************************************************************
* @name Sudoku::first_square
* @brief finds first 0 value to begin comparing
* @param none
* @retval  none
*********************************************************************/
void Sudoku::first_square()
{
	// Set first pointer to first place where fixed grid is 0 (for future use)
	for (first = &fixed[0][0]; *first != 0; first++);

	for (y = 0; y < 9; y++) // Set (x,y) to first place where fixed grid is 0
	{
		for (x = 0; x < 9; x++)
		{
			if (fixed[x][y] == 0)
			{
				//cout << "first found\n";
				return;
			}

		}
	}


	return;
}
/*********************************************************************
* @name Sudoku::prev_square
* @brief goes back to previous square
* @param none
* @retval  bool prev_square
*********************************************************************/
bool Sudoku::prev_square()
{

	if (x == 0 && y > 0) // Move back one
	{
		x = 8; // move up a row
		y--;
	}
	else if (x > 0)
	{
		x--;
	}

	for (;y >= 0; y--)
	{
		for (;x >= 0;)
		{

			if (fixed[x][y] == 0 && &fixed[x][y] != first)
			{
				//cout << "prev found\n";
				return true; // previous found!
			}
			if (x == 0) // move up a row
			{
				x = 8;
				break;
			}
			else
			{
				x--;
			}
			if (*first == 9 || &fixed[x][y] == first) // Don't try to move off the grid
				return false; // unsolvable
		}

		// move to end of the row
	}
	//cout << "jumped off grid!\n Unsolvable!";


	return false;
}

/*********************************************************************
* @name Sudoku::next_square
* @brief moves to the next square and compare
* @param none
* @retval  none
*********************************************************************/
bool Sudoku::next_square()
{

	for (next = &fixed[x][y]; *next != 0; next++)// Ignore this part
	{
		if (next == &fixed[8][8])
		{
			//cout << "Jumped off grid!\n";
			//exit(1);
		}
	}
	if (x == 8 && y < 8) // Move forward 1
	{
		x = 0;
		y++;
	}
	else if (x < 8)
	{
		x++;
	}
	for (; y < 9; y++) // Loop until next is found
	{
		for (; x < 9;)
		{
			if (fixed[x][y] == 0)
			{
				//cout << "next found\n";
				return true; // next found
			}
			if (x == 8 && y < 8)
			{
				x = 0; // move to beginning of next row
				y++;
			}
			else if (x < 8)
			{
				x++; // move forward 1
			}
		}
	}
	//cout << "jumped off grid!\n";
	//exit(1);

	return false;
}
/*********************************************************************
* @name Sudoku::last_square
* @brief goes through the last square and compares
* @param none
* @retval  none
*********************************************************************/
void Sudoku::last_square()
{
	for (last = &fixed[8][8]; *last != 0; last--); // Find last square
}