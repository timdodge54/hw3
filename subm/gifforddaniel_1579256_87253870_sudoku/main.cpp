#include"sudoku.h"

using namespace std;
/*********************************************************************
* @name main
* @brief Fills in a 9x9 grid Sudoku with n inputed numbers and solves
* for missing values to complete the Sudoku
* @param none
* @retval  none
*********************************************************************/
int main()
{
	int n;
	cout << "Welcome to SudokuSolver!!" << endl; // Print prompt
	cout << "Enter number of squares to prepopulate: ";
	cin >> n; // Get integer from keyboard
	Sudoku s(n); // Populate grid
	
	s.print_grid(); // Print grid
	
	if (!(s.solve()))
	{
		cout << "Sorry, unsolvable..." << endl; // unsolvable :(
	}
	else
	{
		cout << "\nSolved!" << endl;
		s.print_grid(); // Print grid
	}

	return 0;
}