#include "sudoku.h"
/*********************************************************************
* @name Constructor
* @brief constructs a sudoku puzzle with n pregenerated numbers
* @param integer n
* @retval  none
*********************************************************************/

Sudoku::Sudoku(int n)
{
	int i, j;
	srand(time(NULL));
	for (i = 0; i < 9; i++)
	{
		for (j = 0; j < 9; j++)
		{
			grid[i][j] = 0;
			pre[i][j] = 0;
		}
	}
	i = 0;
	while(i < n)
	{
		int x, y, v;
		x = rand() % 9;
		y = rand() % 9;
		v = rand() % 9 + 1;
		if (pre[x][y] == 0 && checkSquare(x, y, v) && checkRow(y, v) && checkCol(x, v))
		{
			pre[x][y] = 1;
			grid[x][y] = v;
			i++;
		}
		else {
			
		}
	}
}

/*
n = 1, x y are at first
loop
	if no violations
	then
		place n at grid[x,y]
		n = 1, x y are at next
		if can't find next
		then
			return success
		endif
	else
		increment n
		if n > 9
		then
			place 0 at grid[x,y]
			x y at prev
			if can't find prev
			then
				return fail
			else
				increment n at [x,y]
			endif
		else

		endif
	endif
endloop

*/
/*********************************************************************
* @name solve
* @brief solves the previously generated Sudoku puzzle
* @param none
* @retval  int
*********************************************************************/

int Sudoku::solve()
{
	int x, y, n, check;
	x = 0;
	y = 0;
	n = 1;
	int* i, * j;
	i = &x;
	j = &y;
	findFirst(i, j);
	for (;;)
	{
		check = 0;
		if (checkSquare(x, y, n) && checkRow(y, n) && checkCol(x, n) && n < 10 && n > 0)
		{
			grid[x][y] = n;
			n = 1;
			check = findNext(i, j);
			if (!check)
			{
				return 1;
			}
		}
		else {
			n++;
			if (n > 9)
			{
				grid[x][y] = 0;
				check = findPrev(i, j);
				if (check)
				{
					n = grid[x][y] + 1;
				}
				else {
					return 0;
				}
			}
		}
	}
	
	return 0;
}
/*********************************************************************
* @name printGrid
* @brief Prints the Sudoku puzzle with correct formatting 
* @param none
* @retval  none
*********************************************************************/

void Sudoku::printGrid()
{
	int i, j;
	for (i = 0; i < 9; i++)
	{
		if (i==3 || i==6)
		{
			std::cout << "-----------------------------------------" << std::endl << std::endl;
		}
		for (j = 0; j < 9; j++)
		{
			if (grid[j][i] == 0)
			{
				std::cout << "*" << "   ";
			}
			else {
				std::cout << grid[j][i] << "   ";
			}
			if (j == 2 || j == 5)
			{
				std::cout << "|   ";
			}
		}
		std::cout << std::endl << std::endl;
		
	}
 }
/*********************************************************************
* @name checkSquare
* @brief checks if the entered number n is already in the 3x3 grid
* @param int x, y, and n
* @retval  int
*********************************************************************/

int Sudoku::checkSquare(int x, int y, int n)
{
	int i, j;
	int minc, maxc, minr, maxr;
	minc = (x / 3) * 3;
	maxc = minc + 2;
	minr = (y / 3) * 3;
	maxr = minr + 2;
	for (i = minc; i <= maxc; i++)
	{
		for (j = minr; j <= maxr; j++)
		{
			if (grid[i][j] == n)
			{
				return 0;
			}
		}
	}
	return 1;
}
/*********************************************************************
* @name checkRow
* @brief checks if the entered number n is already in the row
* @param int y, n
* @retval  int
*********************************************************************/

int Sudoku::checkRow(int y, int n)
{
	int i;
	for (i = 0; i < 9; i++)
	{
		if (grid[i][y] == n)
		{
			return 0;
		}
	}
	return 1;
}
/*********************************************************************
* @name checkCol
* @brief checks if the entered number n is already in the column
* @param int x, n
* @retval  int
*********************************************************************/

int Sudoku::checkCol(int x, int n)
{
	int i;
	for (i = 0; i < 9; i++)
	{
		if (grid[x][i] == n)
		{
			return 0;
		}
	}
	return 1;
}
/*********************************************************************
* @name findFirst
* @brief finds x and y position of first non-pregenerated number 
* @param pointer to int x, y
* @retval  int
*********************************************************************/

int Sudoku::findFirst(int* x, int* y)
{
	int i, j;
	for (j = 0; j < 9; j++)
	{
		for (i = 0; i < 9; i++)
		{
			if (pre[i][j] == 0)
			{
				*x = i;
				*y = j;
				return 1;
			}
		}
	}		
	return 0;
}
/*********************************************************************
* @name findNext
* @brief finds x and y position of next non-pregenerated number
* @param pointers to int x, y
* @retval  int
*********************************************************************/

int Sudoku::findNext(int* x, int* y)
{
	int i, j;
	i = *x;
	j = *y;
	for (; j < 9; j++)
	{
		for (; i < 9; i++)
		{
			if (pre[i][j] == 0 && (i != *x || j != *y))
			{
				*x = i;
				*y = j;
				return 1;
			}
		}
		if (i == 9)
		{
			i = 0;
		}
	}
	return 0;
}
/*********************************************************************
* @name findPrev
* @brief finds x and y positions of the previous non-pregenerated number
* @param pointers to int x, y
* @retval  int
*********************************************************************/

int Sudoku::findPrev(int* x, int* y)
{
	int i, j;
	i = *x;
	j = *y;
	for (; j > 0; j--)
	{
		for (; i > 0; i--)
		{
			if (pre[i][j] == 0 && (i != *x || j != *y))
			{
				*x = i;
				*y = j;
				return 1;
			}
		}
		if (i == 0)
		{
			i = 8;
		}
	}
	return 0;
}
