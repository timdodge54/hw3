#ifndef SUDOKU_H
#define SUDOKU_H

#include <iostream>
#include <ctime>
#include <cstdlib>
class Sudoku {
private:
	int grid[9][9];
	int pre[9][9];
	int checkSquare(int x, int y, int n);
	int checkRow(int y, int n);
	int checkCol(int x, int n);
	int findFirst(int* x, int* y);
	int findNext(int* x, int* y);
	int findPrev(int* x, int* y);
public:
	Sudoku(int n);
	int solve();
	void printGrid();
};

#endif