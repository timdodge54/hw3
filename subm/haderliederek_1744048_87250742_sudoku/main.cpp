#include "sudoku.h"

using namespace std;
/*********************************************************************
* @name main
* @brief Creates and solves Sudoku puzzles
* @param none
* @retval  none
*********************************************************************/

int main()
{
	int n;
	int check;

	cout << "Welcome to SudokuSolver!!" << endl; 
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;
	Sudoku s(n);
	s.printGrid();
	check = s.solve();
	if (check) {
		cout << "Solved!" << endl;
		s.printGrid();
	}
	else {
		cout << " Sorry, unsolvable..." << endl;
	}
	return 0;
}
