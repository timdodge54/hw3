/*********************************************************************
* @name main
* @brief Asks the user for the number of squares to prepopulate, and 
*           then calls the sudoku class to try and solve puzzle. If
*           the puzzle is solved then print the solved puzzle to 
*           screen. If it is unsolvable, then print that it is not
*           solvable to screen
* @param none
* @retval none
*********************************************************************/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include "sudoku.h"

using namespace std;

int main() {
    //begin
    //srand function to generate random numbers
    srand(time(NULL));
    int n;
    //ask user for the number of squares to prepopulate
    cout << "Welcome to SudokuSolver!!" << endl;
    cout << "Enter the number of squares to prepopulate: ";
    //get the number of squares to prepopulate from user
    cin >> n;
    //call the sudoku class
    Sudoku sudoku(n);
    //print the initial grid with the desired number of prepopulated squares
    sudoku.printGrid();
    int success = sudoku.solve();
    //if the puzzle is solved
    if (success) {
        cout << endl << endl << "Solved!" << endl << endl;
        //print the new finishe grid to screen
        sudoku.printGrid();
    }
    //if the puzzle cannot be solved
    else {
        //print that the puzzle is unsolvable to screen
        cout << endl << endl << "Sorry, unsolvable..." << endl;
    }
    //end
    return 0;
}
