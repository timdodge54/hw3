#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <iomanip>
#include "sudoku.h"

using namespace std;

/*********************************************************************
* @name Sudoku
* @brief initializes the grid with zeros
* @param none
* @retval none
*********************************************************************/

Sudoku::Sudoku(int n) {
    int count = 0;
    int x, y;

    //srand function to generate random numbers
    srand(time(NULL));

    //initialize the grid with zeros
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            grid[i][j] = 0;

    while (count < n) {
        x = rand() % 9;
        y = rand() % 9;
        if (grid[x][y] == 0) {
            grid[x][y] = (rand() % 9) + 1;
            if (isValid(x, y))
                count++;
            else
                grid[x][y] = 0;
        }
    }
}

/*********************************************************************
* @name isValid
* @brief Checks the row, column, and 3x3 box to see if a number is 
*           allowed to be placed in a specific square
* @param none
* @retval true/false
*********************************************************************/

bool Sudoku::isValid(int x, int y) {
    //check the row
    for (int i = 0; i < 9; i++)
        if (i != x && grid[i][y] == grid[x][y])
            return false;

    //check the column
    for (int j = 0; j < 9; j++)
        if (j != y && grid[x][j] == grid[x][y])
            return false;

    //check the 3x3 grid
    int x0 = (x / 3) * 3;
    int y0 = (y / 3) * 3;
    for (int i = x0; i < x0 + 3; i++)
        for (int j = y0; j < y0 + 3; j++)
            if ((i != x || j != y) && grid[i][j] == grid[x][y])
                return false;

    return true;
}

/*********************************************************************
* @name solve
* @brief solves the puzzle, if solvable
* @param none
* @retval none
*********************************************************************/

int Sudoku::solve() {
    int x = 0, y = 0;
    if (!findUnassignedLocation(x, y))
        return 1;

    for (int num = 1; num <= 9; num++) {
        grid[x][y] = num;
        if (isValid(x, y) && solve())
            return 1;
        grid[x][y] = 0;
    }
    return 0;
}

/*********************************************************************
* @name findUnassignedLocation
* @brief finds an unassigned location in the grid
* @param none
* @retval true/false
*********************************************************************/

bool Sudoku::findUnassignedLocation(int& x, int& y) {
    for (x = 0; x < 9; x++)
        for (y = 0; y < 9; y++)
            if (grid[x][y] == 0)
                return true;
    return false;
}

/*********************************************************************
* @name printGrid
* @brief prints the grid to screen
* @param none
* @retval none
*********************************************************************/

void Sudoku::printGrid() {
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            cout << setw(2) << left << (grid[i][j] == 0 ? "*" :
                to_string(grid[i][j])) << "";
            if (j % 3 == 2 && j < 8) {
                cout << "| ";
            }
        }
        cout << endl;
        if (i % 3 == 2 && i < 8) {
            cout << "------+-------+------" << endl;
        }
    }
}