#ifndef SUDOKU_H
#define SUDOKU_H

class Sudoku {
public:
    Sudoku(int n);
    int solve();
    void printGrid();

private:
    int grid[9][9];
    bool isValid(int x, int y);
    bool findUnassignedLocation(int& row, int& col);
};

#endif
