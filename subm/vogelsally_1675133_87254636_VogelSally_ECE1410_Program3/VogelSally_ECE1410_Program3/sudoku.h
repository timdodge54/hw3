#ifndef SUDOKU_H
#define SUDOKU_H

class Sudoku
{
	public:
		Sudoku(int number);
		int solve();
		void printGrid();
	private:
		int PuzzleRowCheck(int val, int row);
		int PuzzleColumnCheck(int val, int column);
		int PuzzleBoxCheck(int val, int row, int column);
		int FixedRowCheck(int val, int row);
		int FixedColumnCheck(int val, int column);
		int FixedBoxCheck(int val, int row, int column);
		int FirstSquare(int *row, int *column);
		int NextSquare(int *row, int *column);
		int PreviousSquare(int *row, int *column);

		int puzzle[9][9];
		int fixed[9][9];
};

#endif