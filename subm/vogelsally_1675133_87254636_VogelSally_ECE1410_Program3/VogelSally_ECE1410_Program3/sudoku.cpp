#include <iostream>
#include <cstdlib>
# include "sudoku.h"
using namespace std;

/****************************************************************************** 
* @name Sudoku
* @brief Prepopulates 9 x 9 grid with user-defined number of entries
* @param number
* @retval none
******************************************************************************/ 

Sudoku::Sudoku(int number)
{
	int random_numeral, row, column;
	random_numeral = 0;
	row = 0;
	column = 0;
	int i, j;
	i = 0;
	j = 0;
	
	// While still on the board in terms of rows
	while (j < 9)
	{
		// While still moving across one row
		while (i < 9)
		{
			// Set the value of the array element equal to 0
			fixed[i][j] = 0;
			// Move along the row by 1
			i = i + 1;
		}
		i = 0;
		// Move down to the next row
		j = j + 1;
	}
	
	//Initialize the random number generator.
	srand (time(NULL)); 
	
	// While there are random entries left to make
	while (number > 0)
	{
		// Set the random number to a value between 1 and 9
		random_numeral = (rand() % 9) + 1;
		// Set the row randomly to a spot on the 9 x 9 grid
		row = (rand() % 9);
		// Set column
		column = (rand() % 9);
		
		// If there is not already a non zero value to the element
		if (fixed[row][column] == 0)
		{		
			if (FixedRowCheck(random_numeral, row) == 1)
			{
				if (FixedColumnCheck(random_numeral, column) == 1)
				{
					// Set the random number in a position on the grid
					fixed[row][column] = random_numeral;
					// Decrease the random numbers left to enter by 1
					number = number - 1;
				}
			}	
		}	
	}
		
	// PRINT OUT FIXED GRID
	i = 0;
	j = 0;
	
	// While still on the board in terms of rows
	while (j < 9)
	{
		// While still moving along one row
		while (i < 9)
		{
			// If the array element is equal to 0
			if (fixed[i][j] == 0)
			// Then
			{
				// Print * to screen rather than 0
				cout << "*" << " ";
			}
			else
			{
				// Print the randomly generated number to screen
				cout << fixed[i][j] << " ";
			}
			// Move along the row by one
			i = i + 1;
		}
		cout << endl;
		i = 0;
		// Move to the next row
		j = j + 1;
	}
	// EndLoop
	

}

/****************************************************************************** 
* @name solve
* @brief Performs backtracking algorithm
* @param none
* @retval none
******************************************************************************/ 

int Sudoku::solve()
{
	// Initialize the malleable grid, "puzzle," to be all zeros.
	
	int i, j, n, num, x;
	int row, column;
	i = 0;
	j = 0;
	row = column = num = 0;
	n = 1;
	x = 0;
	
	// While still on the board in terms of rows
	while (j < 9)
	{
		// While still moving across one row
		while (i < 9)
		{
			// Set the value of the array element equal to 0
			num = fixed[i][j];
			puzzle[i][j] = num;
			// Move along the row by 1
			i = i + 1;
		} 
		// EndLoop
		i = 0;
		// Move down to the next row
		j = j + 1;
	}
	
	
	// EndLoop
	
	// START BACKTRACKING ALGORITHM
	
	// Find first square of play
	if (FirstSquare(&row, &column) == 1)
	{
		cout << "row element: " << row << endl;
		cout << "column element: " << column << endl;
	}
	
	for (x = 0; x < 5; x++)
	{
		// Put n (1) at first square of play
		//cout << "In the for loop." << endl;
		puzzle[row][column] = n;
			
		// If there are no violations
		if ((PuzzleColumnCheck(n, row) == 1) && (PuzzleRowCheck(n, column) == 1))
		{
			n = 1;
			// If there is an available next spot
			if (NextSquare(&row, &column) == 1)
			{
				cout << "row element: " << row << endl;
				cout << "column element: " << column << endl;
			}
			// Else if there is not an available next spot
			else
			{
				// Break out of function; puzzle is solved
				return 0;
			}
			// EndIf
		}
		// Else if there are violations
		else 
		{
			// Add one to n
			n = n + 1;
			// If n is equal to 10
			if (n == 10)
			{
				cout << "n = 10" << endl;
				puzzle[row][column] = 0;
				cout << "puzzle row: " << row << endl;
				cout << "puzzle column: " << column << endl;
				// If there is a previous square that can be played
				if (PreviousSquare(&row, &column) == 1)
				{
					cout << "can backtrack" << endl;
					n = puzzle[row][column] + 1;
				}
				// Else if we have backtracked off the grid
				else
				{
					cout << "can't backtrack" << endl;
					return 1;
				}
				// EndIf
			}
			// EndIf
		}
		//EndIf
		printGrid();
		cout << endl;
	}
	// EndLoop
				

	return 0;

}

/****************************************************************************** 
* @name FirstSquare
* @brief Finds the first square that can be played (not used in fixed grid)
* @param *int, *column
* @retval True/False
 
******************************************************************************/ 

int Sudoku::FirstSquare(int *row, int *column)
{
	int r, c;
	r = c = 0;
	
	// Loop until an element of "0" is found or no more spaces on board
	for (r = 0; r < 9; r++)
	{
		for (c = 0; c < 9; c++)
		{
			if (fixed[r][c] == 0)
			{
				*row = r;
				*column = c;
				return 1;
			}
		}
	}
		
	return 0;
}

/****************************************************************************** 
* @name NextSquare
* @brief Finds the next empty array element that can be played
* @param *row, *column
* @retval True/False
******************************************************************************/ 

int Sudoku::NextSquare(int *row, int *column)
{
	int r, c;
	r = c = 0;
	
	// Loop until an element of "0" is found or no more spaces on board
	for (r = 0; r < 9; r++)
	{
		for (c = 0; c < 9; c++)
		{
			
			if (puzzle[r][c] == 0)
			{
				*row = r;
				*column = c;
				return 1;
			}
		}
	}
		
	return 0;
}


/****************************************************************************** 
* @name PreviousSquare
* @brief Finds the last played array element, for backtracking purposes
* @param *row, *column
* @retval True/False
******************************************************************************/ 

int Sudoku::PreviousSquare(int *row, int *column)
{
	int r, c;
	r = *row;
	c = *column;

	
	// Loop backwards from position until a fixed-0 place is found
	for (r = *row; r >= 0; r--)
	{
		for (c = *column; c >= 0; c--)
		{
			// Check to see if fixed puzzle is 0 at array element
			if (fixed[r][c] == 0)
			{
				*row = r;
				*column = c;
				return 1;
			}
		}
	}
		
	return 0;
	
}

/****************************************************************************** 
* @name FixedRowCheck
* @brief Check whether the value is already in the row in the fixed array
* @param val, row
* @retval True/False
******************************************************************************/ 


int Sudoku::FixedRowCheck(int val, int row)
{
	// Return 1 if value is good to go with respect to row
	// Return 0 if value cannot be place in row
	
	int c, x;
	x = 1;
	c = 0;
	
	for (c = 0; c < 9; c++)
	{
		// If the puzzle grid already has a value equal to val
		if (fixed[row][c] == val)
		// Then
		{
			// Return 0, false, no good value
			x = 0;
		}
	}
	
	// Otherwise, return 1, true, acceptable value
	
	return x;
}

/****************************************************************************** 
* @name PuzzleRowCheck
* @brief Check whether the value is already in the row in the puzzle array
* @param val, row
* @retval True/False
******************************************************************************/ 


int Sudoku::PuzzleRowCheck(int val, int row)
{
	// Return 1 if value is good to go with respect to row
	// Return 0 if value cannot be place in row
	
	int c, x;
	x = 1;
	c = 0;
	
	for (c = 0; c < 9; c++)
	{
		// If the puzzle grid already has a value equal to val
		if (puzzle[row][c] == val)
		// Then
		{
			// Return 0, false, no good value
			x = 0;
			cout << "Row returning 0" << endl;
		}
	}
	
	// Otherwise, return 1, true, acceptable value
	
	return x;
}

/****************************************************************************** 
* @name FixedColumnCheck
* @brief Check whether the value is already in the column in the fixed array
* @param val, column
* @retval True/False
******************************************************************************/ 

int Sudoku::FixedColumnCheck(int val, int column)
{
	// Return 1 if value is good to go with respect to column
	// Return 0 if value cannot be place in column
	
	int r, x;
	x = 1;
	r = 0;
	
	for (r = 0; r < 9; r++)
	{
		// If the puzzle grid already has a value equal to val
		if (fixed[r][column] == val)
		// Then
		{
			// Return 0, false, no good value
			x = 0;
		}
	}
	
	// Otherwise, return 1, true, acceptable value
	return x;
}

/****************************************************************************** 
* @name PuzzleColumnCheck
* @brief Check whether the value is already in the column in the puzzle array
* @param val, column
* @retval True/False
******************************************************************************/ 

int Sudoku::PuzzleColumnCheck(int val, int column)
{
	// Return 1 if value is good to go with respect to column
	// Return 0 if value cannot be place in column
	
	int r, x;
	x = 1;
	r = 0;
	
	for (r = 0; r < 9; r++)
	{
		// If the puzzle grid already has a value equal to val
		if (puzzle[r][column] == val)
		// Then
		{
			// Return 0, false, no good value
			x = 0;
		}
	}
	
	// Otherwise, return 1, true, acceptable value
	return x;
}


/****************************************************************************** 
* @name BoxCheck
* @brief Check whether the value is already in the 3x3 box
* @param val
* @retval none
******************************************************************************/ 

/*
int BoxCheck(int val, int row, int column);
{
}
*/

/****************************************************************************** 
* @name printGrid
* @brief Prints 9 x 9 sudoku puzzle
* @param none
* @retval none
******************************************************************************/ 

void Sudoku::printGrid()
{
	int i, j, x;
	i = 0;
	j = 0;
	
	// While still on the board in terms of rows
	while (j < 9)
	{
		// While still moving along one row
		while (i < 9)
		{

			// Print the array element to screen
			cout << puzzle[i][j] << " ";
			// Move along the row by one
			i = i + 1;
		}
		// EndLoop
		
		cout << endl;
		i = 0;
		// Move to the next row
		j = j + 1;
	}
}



