/****************************************************************************** 
* @name main
* @brief Calls constructor, then calls solve function. Prints solved grid if
* possible, otherwise prints "unsolvable."
* @param none
* @retval none
******************************************************************************/ 

#include <iostream>
#include "sudoku.h"
using namespace std;

int main()
{
// Begin

	int number;
	
	// Print "Welcome to Sudoku Solver!" to screen
	cout << "Welcome to Sudoku Solver!" << endl;
	// Get number of entries to prepopulate from user
	cout << "Enter number of squares to prepopulate: ";
	cin >> number;
	
	// Call the constructor
	Sudoku s = Sudoku(number);
	

	// If the puzzle is solvable
	if (s.solve() == 0) 
	// Then
	{
		// Print "Solved!" and print the solved puzzle to screen
		cout << "Solved!" << endl;
		s.printGrid();
	}
	// Else
	else
	{
		// Print "Sorry, unsolvable..." to screen
		cout << "Sorry, unsolvable..." << endl;
	}
	
	return 0;
	
// End
}