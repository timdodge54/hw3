
#include <iostream> 
#include "sudoku.h" 

using namespace std;

/***********************************************
* @name		main
* @brief	create and solve a sudoku puzzle
* @param	none
* @retval	0
***********************************************/

//starter code
int main()
{
	//declare variable
	int n;

	//get value n from user
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	//initiate sudoku with n prepopulated values
	Sudoku s(n);

	cout << endl;

	//print unsolved sudoku
	s.printGrid();

	cout << endl;

	//check if solvable
	if (s.solve()) {

		//if solvable, print solved sudoku
		cout << "Solved!" << endl << endl;
		s.printSolvedGrid();

	}
	else {

		//if not solvable, end
		cout << "Sorry, unsolvable..." << endl;
	}

	return 0;
}