#include "sudoku.h"
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

/***********************************************
* @name		testRow
* @brief	check if given int is legal in its row
* @param	number n to be tested, location r, array to search
* @retval	1 if true, 0 if false
***********************************************/

int Sudoku::testRow(char n, int r, char g[9][9])
{
	//iterate through columns
	for (int i = 0; i < 9; i++) {

		//check if value == n
		if (g[r][i] == n) {
			return 0;

		}
	}

	//return true you make it this far
	return 1;
}


/***********************************************
* @name		testCol
* @brief	check if given int is legal in its column
* @param	number n to be tested, location c, array to search
* @retval	1 if true, 0 if false
***********************************************/

int Sudoku::testCol(char n, int c, char g[9][9])
{
	//iterate through row
	for (int i = 0; i < 9; i++) {

		//check if value == n
		if (g[i][c] == n) {

			return 0;
		}
	}

	//return true you make it this far
	return 1;
}


/***********************************************
* @name		testBox
* @brief	check if given int is legal in its 3x3 box
* @param	number n to be tested, location, array to search
* @retval	1 if true, 0 if false
***********************************************/

int Sudoku::testBox(char n, int r, int c, char g[9][9])
{
	//check if values in box == n

	//set r and c to top left value of their own box
	r = (r / 3) * 3;
	c = (c / 3) * 3;

	//check box values
	for (int i = 0; i < 3; i++, r++) {
		for (int k = 0; k < 3; k++, c++) {
			if (g[r][c] == n) {
				return 0;
			}
		}
		c -= 3;
	}

	//return true you make it this far
	return 1;
}





/***********************************************
* @name		incrementPosition
* @brief	increment position until empty position is found
* @param	row and column of location
* @retval	0 if succesful, 1 if end of grid is reached
***********************************************/

int Sudoku::incrementPosition(int& r, int& c)
{

	//loop
	while (1) {

		//iterate through columns
		if (c < 8) {
			c++;

			//check position
			if (grid[r][c] == '*') {
				return 0;
			}
		}

		//iterate through rows
		else {
			if (r < 8) {
				r++;
				c = 0;

				//check position
				if (grid[r][c] == '*') {
					return 0;
				}
			}
			else {

				//return 1 if end of grid is reached
				return 1;
			}
		}
	}
}






/***********************************************
* @name		decrementPosition
* @brief	decrement position until empty position is found
* @param	row and column of location
* @retval	0 if succesful, 1 if end of grid is reached
***********************************************/

int Sudoku::decrementPosition(int& r, int& c)
{
	//loop
	while (1) {

		//iterate through columns
		if (c > 0) {
			c--;

			//check position availability
			if (grid[r][c] == '*') {

				//check position value
				if (solvedGrid[r][c] < '9') {
					return 0;
				}
				else {

					//reset position value before moving on
					solvedGrid[r][c] = '*';
				}
			}
		}

		//iterate through rows
		else {
			if (r > 0) {
				r--;
				c = 8;

				//check position availability
				if (grid[r][c] == '*') {

					//check position value
					if (solvedGrid[r][c] < '9') {
						return 0;
					}
					else {

						//reset position value before moving on
						solvedGrid[r][c] = '*';
					}
				}
			}
			else {

				//return 1 if end of grid is reached
				return 1;
			}
		}
	}
}





/***********************************************
* @name		Sudoku
* @brief	Constructor for Sudoku class. Creates a sudoku puzzle
*			with a number of prepopulated squares given by user
* @param	num n of prepopulated squares
* @retval	none
***********************************************/

Sudoku::Sudoku(int n)
{
	//declare variables
	int row, col;
	char num;

	//initialize random seed
	srand(time(NULL));

	//loop n times
	for (int i = 0; i < n;) {

		//generate random number, row, and column
		num = '1' + rand() % 9;
		row = rand() % 9;
		col = rand() % 9;

		//check row, column, and box for legality
		if (testRow(num, row, grid) & testCol(num, col, grid) & 
			testBox(num, row, col, grid)) {
			//assign value
			grid[row][col] = num;

			//increment i
			i++;

		}
	}
}






/***********************************************
* @name		solve
* @brief	solves sudoku puzzle using a backtracking algorithm
* @param	none
* @retval	1 if solvable, 0 if not
***********************************************/

int Sudoku::solve()
{
	//declare variables
	char num;
	int row, col;

	//make solvedGrid identical to grid
	for (int r = 0; r < 9; r++) {
		for (int c = 0; c < 9; c++) {
			solvedGrid[r][c] = grid[r][c];
		}
	}

	//initialize n and grid position
	num = '1';
	row = 0;
	col = 0;
	if (grid[row][col] != '*') {
		if (incrementPosition(row, col)) {
			return 0;
		}
	}

	//loop
	while (1) {

		//if num is valid at position
		if (testRow(num, row, solvedGrid) & testCol(num, col, solvedGrid) & 
			testBox(num, row, col, solvedGrid)) {

			//assign num to position
			solvedGrid[row][col] = num;

			//reset num to 1
			num = '1';

			//increment position, check if off grid
			if (incrementPosition(row, col)) {

				//success
				return 1;

			}
		}

		//else
		else {

			//assign num to position
			solvedGrid[row][col] = num;

			//increment num
			num++;

			//if num is 10
			if (num > '9') {

				//place * at position
				solvedGrid[row][col] = '*';

				//move to previous available square, check if off grid
				if (decrementPosition(row, col)) {

					//fail
					return 0;

				}
				else {

					//num = placed num + 1
					num = solvedGrid[row][col] + 1;

				}
			}
		}
	}
}






/***********************************************
* @name		printgrid
* @brief	prints unsolved sudoku puzzle to screen
* @param	none
* @retval	none
***********************************************/

void Sudoku::printGrid()
{
	//loop 3 times
	for (int i = 0; i < 3; i++) {

		//iterate through 3 rows
		for (int r = 0; r < 3; r++) {

			//print row values
			cout << grid[r + (i * 3)][0]
				<< "\t" << grid[r + (i * 3)][1]
				<< "\t" << grid[r + (i * 3)][2]
				<< "\t|"
				<< "\t" << grid[r + (i * 3)][3]
				<< "\t" << grid[r + (i * 3)][4]
				<< "\t" << grid[r + (i * 3)][5]
				<< "\t|"
				<< "\t" << grid[r + (i * 3)][6]
				<< "\t" << grid[r + (i * 3)][7]
				<< "\t" << grid[r + (i * 3)][8]
				<< endl << endl;
		}

		//print line
		if (i < 2) {
			cout << "------------------------+-------------------------------"
				<< "+------------------------" << endl << endl;
		}
	}
}






/***********************************************
* @name		printSolvedGrid
* @brief	prints solved sudoku puzzle to screen
* @param	none
* @retval	none
***********************************************/

void Sudoku::printSolvedGrid()
{
	//loop 3 times
	for (int i = 0; i < 3; i++) {

		//iterate through 3 rows
		for (int r = 0; r < 3; r++) {

			//print row values
			cout << solvedGrid[r + (i * 3)][0]
				<< "\t" << solvedGrid[r + (i * 3)][1]
				<< "\t" << solvedGrid[r + (i * 3)][2]
				<< "\t|"
				<< "\t" << solvedGrid[r + (i * 3)][3]
				<< "\t" << solvedGrid[r + (i * 3)][4]
				<< "\t" << solvedGrid[r + (i * 3)][5]
				<< "\t|"
				<< "\t" << solvedGrid[r + (i * 3)][6]
				<< "\t" << solvedGrid[r + (i * 3)][7]
				<< "\t" << solvedGrid[r + (i * 3)][8]
				<< endl << endl;
		}

		//print line
		if (i < 2) {
			cout << "------------------------+-------------------------------+------------------------" << endl << endl;
		}
	}
}
