#pragma once

class Sudoku {
	char grid[9][9] = {
		{'*','*','*','*','*','*','*','*','*'},
		{'*','*','*','*','*','*','*','*','*'},
		{'*','*','*','*','*','*','*','*','*'},
		{'*','*','*','*','*','*','*','*','*'},
		{'*','*','*','*','*','*','*','*','*'},
		{'*','*','*','*','*','*','*','*','*'},
		{'*','*','*','*','*','*','*','*','*'},
		{'*','*','*','*','*','*','*','*','*'},
		{'*','*','*','*','*','*','*','*','*'}
	};
	char solvedGrid[9][9];
	int testRow(char n, int r, char g[9][9]);
	int testCol(char n, int c, char g[9][9]);
	int testBox(char n, int r, int c, char g[9][9]);
	int incrementPosition(int& r, int& c);
	int decrementPosition(int& r, int& c);
public:
	Sudoku(int n);
	int solve();
	void printGrid();
	void printSolvedGrid();
};