#pragma once
#ifndef SUDOKU_H
#define SUDOKU_H			//muliple inclusion check

#include <iostream>
#include <ctime>
#include <stdexcept>		//libraries

using namespace std;

//int SudokuArray[8][8] = { 0 };
class Sudoku {
	public:
		Sudoku(int n);	//populates n squares of 9x9 with legal values
		int solve(int row, int col, int SudokuArray[9][9]);	//make a valid sol
		//int solve();
		void printgrid(); //prints 9x9 grid	
		void printarray();	//debugging
		int SudokuArray[9][9] = { };
		
	private:
		int fixed[9][9] = { };			//global
		int N = 9;

		int CheckRow (int val, int y);
		int CheckCol(int val, int x);
		int CheckBox(int val, int x, int y);
		int FindFirstSquareInPlay(int &x, int &y); 	
		int FindNextSquareInPlay(int &x, int &y);
};
#endif
