/**
@name Sudoku Solver
@brief creates a random sudoku puzzle and attempts to solve
@param none
@retval 0
*/
#include "sudoku.h"
int main()
{
	int n;
												//ask entry
	cout << "Welcome to SudokueSolver!!" << endl;
	cout << "Enter a number of squares to prepopulate: ";
	cin >> n;									//get entry

	Sudoku s(n);								//create puzzle
	s.printgrid();								//display problem
	cout << endl;	//debug
	//s.printarray(); //debug

	if (s.solve(0, 0, s.SudokuArray))			//solve
	{
		cout << "Solved!" << endl;
		s.printgrid();							//show solution
	}
	else {
		cout << "Sorry, unsolvable..." << endl;	//sad
	}

	return 0;
}