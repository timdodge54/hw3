#include "sudoku.h"
/**
@name Sudoku Constructor
@brief creates a random sudoku puzzle
@param n: number of entries
@retval NA
*/
Sudoku::Sudoku(int n)
{
	srand(time(0));								//randomizer

	int i;
	for (i = 0; i < n; )
	{
		int row = rand() % 9;					//create rando member
		int col = rand() % 9;
		int number = ((rand() % 8) + 1);		//create rando entry

		if (SudokuArray[row][col] == 0)
		{
			if (CheckRow(number, row) == true)
			{									//check rules
				if (CheckCol(number, col) == true)
				{
					if(CheckBox(number, row, col) == true)
					SudokuArray[row][col] = number;	//set to number
					fixed[row][col] = 1;
					i++;
					//cout << row + 1 << '\t' << col + 1 << '\t' << number;
					//printgrid();					//debug
					//cout << endl << endl;			//debug
				}
			}
		}
	}
}

/**
@name Check Row
@brief Looks for matches inside row
@param val: integer of entry
@param y: row sifting through
@retval pass or fail
*/
int Sudoku::CheckRow(int val, int y)
{
	int i;
	for (i = 0; i < 9; i++)
	{
		//cout << endl << "checked" << '\t' << y + 1 << '\t' << i + 1;
		if (SudokuArray[y][i] == val)			//check row for same value
		{
			//cout << "ROW FAIL" <<  y + 1 << '\t' << i + 1 << '\t' << val;
			return false;

		}
	}
	return true;								//no matches found =)
}

/**
@name Check Col
@brief Looks for matches inside column
@param val: integer of entry
@param y: col sifting through
@retval pass or fail
*/
int Sudoku::CheckCol(int val, int x)
{
	int i;
	for (i = 0; i < 9; i++)
	{
		//cout << endl << "checked" << '\t' << i + 1 << '\t' << x + 1;
		if (SudokuArray[i][x] == val)			//check col for same value
		{
			//cout << " COL FAIL" << i + 1 << '\t' << x + 1 << '\t' << val;
			return false;

		}
	}
	return true;								//no matches found =)
}		

/**
@name Check Box
@brief Looks for matches inside 3x3
@param val: integer of entry
@param x: col sifting through
@param y: row sifting through
@retval pass or fail
*/
int Sudoku::CheckBox(int val, int x, int y)
{
	int rowstart, colstart, i, j;
	rowstart = (x / 3) * 3;						//set boundaries
	colstart = (y / 3) * 3;
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			if (SudokuArray[rowstart + i][colstart + j] == val)
			{
				return false;					//look for commonality
			}
		}
	}
	return true;								//no matches found =)
}

/**
@name Solve
@brief recursive algorithm for solving given sudoku puzzle
@param row: row of puzzle
@param col: column of puzzle
@oaram SudokuArray: the array being worked through
@retval pass or fail
*/
int Sudoku::solve(int row, int col, int SudokuArray[9][9])
{												//solve puzzle
	int i;

	if (FindFirstSquareInPlay(row, col) == false)
	{
		return true;							//checks to see if complete
	}
	
	if (col >= 9)
	{
		row = row + 1;							//next row when col full
		col = 0;								//reset col
	}
	
	if (SudokuArray[row][col] > 9)				//checks if space is used
	{
		solve(row, (col + 1), SudokuArray);		//moves to next emptyspace
	}
	//cout << i << endl;		//debug
	//cout << SudokuArray[row][col];

	for (i = 1; i <= 9; i++)					//iterate through sudoku nums
	{
		if (CheckRow(i, row) == true)
		{
			if (CheckCol(i, col) == true)
			{									//check if following rules
				if (CheckBox(i, row, col) == true)
				{
					SudokuArray[row][col] = i;	//give space a new number
					fixed[row][col] = 1;
					//printgrid();					//debug
					if (solve(row,col+1,SudokuArray) == true)
					{
						return true;
					}
					SudokuArray[row][col] = 0;	//set 0 b4 retreating a member
					fixed[row][col] = 0;
				}
			}
		}
	}
	return false;								//failed >.<
}

/**
@name Find First
@brief Crawls through array looking for a zero member
@param x: address of row
@param y: address of col
@retval pass or fail
*/
int Sudoku::FindFirstSquareInPlay(int &x, int &y) 
{
	for (x = 0; x < 9; x++)
	{											//cycle through rows & cols
		for (y = 0; y < 9; y++)
		{
			if (fixed[x][y] == 0)				//tells call:space is empty
				return true;
		}
	}
	return false;
}

/**
@name Find Next
@brief Crawls through array looking for a zero member
@param x: address of row
@param y: address of col
@retval pass or fail
*/
int Sudoku::FindNextSquareInPlay(int &x, int &y)
{
	for (x = 0; x < 9; x++)						//cycle throw rows and colums
	{
		for (y = 0; y < 9; y++)
		{
			if (fixed[x][y] == 0)				//tells call: space is empty
				return true;
		}
	}
	return false;
}

/**
@name Print Grid
@brief Crawls through array printing each member
@param 0
@retval 0
*/
void Sudoku::printgrid()						//prints sudoku page
{
	int i,j;
	for(i=0; i < 9; i++)
	{
		for (j = 0; j < 9; j++)
		{
			if (SudokuArray[i][j] == 0)			//prints * in empty members
			{
				cout << "* ";
			}
			else								//prints integers
			{
				cout << SudokuArray[i][j] << " ";
			}
			if (j == 2 || j == 5)				//prints formatting
			{
				cout << "|";
			}
		}
		if (i == 2 || i == 5 )					//prints formatting
		{
			cout << endl << "------+------+------";
		}
		cout << endl;
	}
}

/**
@name Print Array
@brief Crawls through checking array printing each member
@param 0
@retval 0
*/
void Sudoku::printarray()						//prints out the fixed array
{
	int i, j;
	for (i = 0; i < 9; i++)
	{
		for (j = 0; j < 9; j++)
		{
			if (fixed[i][j] == 0)				//prints 0
			{
				cout << "0 ";
			}
			else
			{
				cout << "1 ";		//shows which members have integers
			}
			if (j == 2 || j == 5)				//prints formatting
			{
				cout << "|";
			}
		}
		if (i == 2 || i == 5)					//prints formatting
		{
			cout << endl << "---------+---------+---------";
		}
		cout << endl;
	}
}