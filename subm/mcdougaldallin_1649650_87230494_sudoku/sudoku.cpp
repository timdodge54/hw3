

/*****************************************************************************
* @name Sudoku
* @brief Constructs a sudoku grid with n prepopulated values
* @param int n
* @retval none
*****************************************************************************/
#include "sudoku.h"
#include <iostream> 
#include <cstdlib>
using namespace std;


Sudoku::Sudoku(int x) {
	int i, j;
	int y;
	int rand1, rand2, val;

	srand(time(0));

	// initilize grid
	for (i = 0; i < 9; i++) {
		for (j = 0; j < 9; j++) {
			grid[i][j].value = 0;
			grid[i][j].prepopulated = false;
		}
	}

	// Loop x times for x prepopulated numbers
	y = 0;
	while (y < x) {
	//	choose random space and enter random number
		rand1 = rand() % 9;
		rand2 = rand() % 9;
	//	if not already prepopulated
		if (grid[rand1][rand2].prepopulated == false) {
			// loop until space is populated 
			while (grid[rand1][rand2].prepopulated == false) {
				val = rand() % 9 + 1;
				
				// If not violations set space to value
				if (noViolations(rand1, rand2, val)) {
					grid[rand1][rand2].value = val;
					grid[rand1][rand2].prepopulated = true;
				}
			}
			y++;
		}
	}

	// Print grid
	printGrid();
}


/*****************************************************************************
* @name solve
* @brief solves the sudoku puzzle using backtracking
* @param none
* @retval int 
*****************************************************************************/
int Sudoku::solve() {
	// n = 1, x,y = first open space
	int n = 1;
	int x, y;
	
	firstOpenSpace(x, y);
	// loop
	while (true) {
		// try n at space x, y 
		// if no violations	
		if (noViolations(x, y, n)) {
			// n=1, x,y = next open space
			grid[x][y].value = n;
			n = 1;
			// if next open space if off grid return 1
			if (!nextOpenSpace(x, y)) { return 1; };
		}
		else {
			n++;
			// if n is 10
			if (n == 10) {
				// change grid[x,y] to 0
				grid[x][y].value = 0;
				// x, y = prev open space
				// if x, y off grid return 0
				if (!lastOpenSpace(x, y)) { return 0;}
				else {
					// n = grid[x,y]
					n = grid[x][y].value;
				}
			}
		}
	}
}


/*****************************************************************************
* @name printGrid
* @brief prints the 9x9 sudoku grid to the screen
* @param none
* @retval none
*****************************************************************************/
void Sudoku::printGrid() {
	int i, j;
	// Loop through rows and cols
	for (i = 0; i < 9; i++) {
		for (j = 0; j < 9; j++) {
			if (grid[i][j].value == 0) {
				cout << " * ";
			}
			else {
				cout << " " << grid[i][j].value << " ";
			}
			
			// Make vertical line after cols 3 and 6
			if (j == 2 || j == 5) {
				cout << "|";
			}
		}
		cout << "\n";
		// Make Horizontal line after rows 3 and 6
		if (i == 2 || i == 5) {
			cout << "---------+---------+---------\n";
		}
	}
}


/*****************************************************************************
* @name checkViolations
* @brief checks if there are violations in the rows, cols, or box
* @param int i, int j
* @retval bool
*****************************************************************************/
bool Sudoku::noViolations(int i, int j, int val) {
	int x, y;

	// Check for violations in row
	for (x = 0; x < 9; x++) {
		// If violation in row return false
		if (grid[x][j].value == val) {
			return false;
		}
	}

	// Check for violations in col
	for (x = 0; x < 9; x++) {
		// If violation in cols return false
		if (grid[i][x].value == val) {
			return false;
		}
	}

	// Check for violations in box
	int minRow = (i / 3) * 3;
	int minCol = (j / 3) * 3;
	// Loop through rows of box
	for (x = minRow; x <= (minRow + 2); x++) {
		// Loop through cols of row
		for (y = minCol; y <= (minCol + 2); y++) {
			if (grid[x][y].value == val) { 
				return false; };
		}
	}

	// If not violations return true
	return true;

}


/*****************************************************************************
* @name firstOpenSpace
* @brief changes x and y to the first open space in the sudoku grid, returns 
* false if one could not be found
* @param int* x, int* y
* @retval bool 
*****************************************************************************/
bool Sudoku::firstOpenSpace(int &x, int &y) {
	int i, j;
	// Loop through spaces
	for (i = 0; i < 9; i++) {
		for (j = 0; j < 9; j++) {
			// if space is not prepoulated set x and y to that space and exit
			if (!grid[i][j].prepopulated) {
				x = i;
				y = j;
				return 1;
			}
		}
	}
	// if no unpopulated space is found return 0
	return 0;
}


/*****************************************************************************
* @name lastOpenSpace
* @brief changes x and y to the previous open space in the sudoku grid, 
* returns false if one could not be found
* @param int* x, int* y
* @retval bool
*****************************************************************************/
bool Sudoku::lastOpenSpace(int &x, int &y) {
	int i, j;

	// Start at current x and y
	i = x; j = y;
	// go to previous in row
	j--;
	// go to end of last row if at begining of row
	if (j == 0) { j = 8; i--; };

	// Loop through spaces until beginning of grid
	while (i >= 0) {
		// if space is not prepoulated set x and y to that space and exit
		if (!grid[i][j].prepopulated) {
			x = i;
			y = j;
			return 1;
		}

		// go to previous in row
		j--;
		// go to end of last row if at begining of row
		if (j == 0) { j = 8; i--; };
	}
	return 0;
}


/*****************************************************************************
* @name nextOpenSpace
* @brief changes x and y to the next open space in the sudoku grid, returns
* false if one could not be found
* @param int* x, int* y
* @retval bool
*****************************************************************************/
bool Sudoku::nextOpenSpace(int &x, int &y) {
	int i, j;

	// Start at current x and y
	i = x; j = y;
	// Loop through spaces until end of grid
	while (i < 9) {

		// if space is not prepoulated set x and y to that space and exit
		if (grid[i][j].value == 0) {
			x = i;
			y = j;
			return 1;
		}

		// go to next in row
		j++;
		// go to beginning of next row if at end of row
		if (j == 9) { j = 0; i++; };
	}
	return 0;
}