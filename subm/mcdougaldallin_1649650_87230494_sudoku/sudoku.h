#ifndef SUDOKU_H
#define SUDOKU_H

class Sudoku {
	// Private Functions
	bool noViolations(int i, int j, int val);
	bool firstOpenSpace(int &x, int &y);
	bool lastOpenSpace(int &x, int &y);
	bool nextOpenSpace(int &x, int &y);
public:
	// Data Members
	struct Value {
		int value;
		bool prepopulated;
	};
	Value grid[9][9];

	// Class Constructor
	Sudoku(int);

	// Public Functions
	int solve();
	void printGrid();
};

#endif