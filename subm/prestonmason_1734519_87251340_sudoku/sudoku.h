#pragma once
class Sudoku {
	int loaded[9][9] = { 0 };
	int value[9][9] = { 0 };
	int checkval(int x, int y, int val);
	int checkrow(int x, int val);
	int checkcol(int y, int val);
	int checkbox(int x, int y, int val);
	int nextip(int* x, int* y);
	int previp(int* x, int* y);
public:
	Sudoku();
	void solve();
	void printgrid();
};