#include "sudoku.h"

/**----------------------------------------------------------------------------
@name main

@brief generates and solves a sudoku

@param none

@retval none
-----------------------------------------------------------------------------*/

int main() {
	//generate then solve
	Sudoku s; s.solve();
}