#include <iostream>
#include <ctime>
#include <cstdlib>
#include "sudoku.h"

using namespace std;

#define VALID 0
#define INVALID 1

/**----------------------------------------------------------------------------
@name Sudoku

@brief generates random numbers between 1 and 9 in random spaces while
	following all aplicable rules of sudoku, then prints the generated sudoku

@param none

@retval none
-----------------------------------------------------------------------------*/

Sudoku::Sudoku() {
	int n;
	//ask for number of prepopulated squares
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	//get number to prepopulate, n
	cin >> n;
	//start random number generator
	srand(time(NULL));
	//loop n times
	for (int i = 0; i < n; i++) {
		//get random coordinates and a random value
		int x = rand() % 9, y = rand() % 9, val = (rand() % 9) + 1;
		//if the spot and value are invalid, loop again
		if (checkval(x, y, val) > 0) {
			i--;
		}
		//otherwise, store the value and mark it as prepopulated
		else {
			value[y][x] = val; loaded[y][x] = 1;
		}
	}
	//print the generated grid
	printgrid();
}

/**----------------------------------------------------------------------------
@name checkval

@brief 

@param none

@retval none
-----------------------------------------------------------------------------*/

int Sudoku::checkval(int x, int y, int val) {
	//check if current spot is prepopulated
	if (loaded[y][x] > 0) {
		return INVALID;
	}
	//check row against value
	if (checkrow(y, val) > 0) {
		return INVALID;
	}
	//check box against value
	if (checkcol(x, val) > 0) {
		return INVALID;
	}
	//check box against value
	if (checkbox(x, y, val) > 0) {
		return INVALID;
	}
	return VALID;
}

/**----------------------------------------------------------------------------
@name checkrow

@brief checks a row for duplicates of a value

@param y coordinate and a value

@retval valid = 0, invalid = 1
-----------------------------------------------------------------------------*/

int Sudoku::checkrow(int y, int val) {
	//check the entire row against the value
	for (int x = 0; x < 9; x++) {
		if (value[y][x] == val) {
			return INVALID;
		}
	}
	return VALID;
}

/**----------------------------------------------------------------------------
@name checkcol

@brief checks a column for duplicates of a value

@param x coordinate and a value

@retval valid = 0, invalid = 1
-----------------------------------------------------------------------------*/

int Sudoku::checkcol(int x, int val) {
	//check the entire column against the value
	for (int y = 0; y < 9; y++) {
		if (value[y][x] == val) {
			return INVALID;
		}
	}
	return VALID;
}

/**----------------------------------------------------------------------------
@name checkbox

@brief checks the coresponding box for any duplicates of a value

@param x and y coordinates and a value

@retval valid = 0, invalid = 1
-----------------------------------------------------------------------------*/

int Sudoku::checkbox(int x, int y, int val) {
	//get starting values for each box
	int xbox = x / 3, ybox = y / 3;
	xbox *= 3; ybox *= 3;
	//loop three times
	for (int i = 0; i < 3; i++) {
		//loop three times
		for (int n = 0; n < 3; n++) {
			//check current spot against the value
			if (value[ybox][xbox] == val) {
				return INVALID;
			}
			//go to next row
			xbox++;
		}
		//reset row and go to next column
		xbox = (x / 3) * 3; ybox++;
	}
	return VALID;
}

/**----------------------------------------------------------------------------
@name printgrid

@brief prints a sudoku grid

@param none

@retval none
-----------------------------------------------------------------------------*/

void Sudoku::printgrid() {
	//loop for each row
	for (int y = 0; y < 9;) {
		//loop for spot in the row
		for (int x = 0; x < 9;) {
			//replace 0 with *
			if (value[y][x] == 0) {
				cout << " *";
			}
			else {
				//place number value of current sspot
				cout << " " << value[y][x];
			}
			//go to next spot in the row
			x++;
			//place divideing lines at the appropriate palce
			if ((x % 3) == 0 && x < 9) {
				cout << " |";
			}
		}
		//go to next row
		y++;
		//place divideing lines at the appropriate palce
		if ((y % 3) == 0 && y < 9) {
			cout << endl << "-------+-------+-------";
		}
		cout << endl;
	}
	cout << endl;
}

/**----------------------------------------------------------------------------
@name solve

@brief solves a pregenerated sudoku

@param none

@retval none
-----------------------------------------------------------------------------*/

void Sudoku::solve() {
	int x = -1, y = 0, val = 1, check;
	//find the first usable spot
	nextip(&x, &y);
	//loop forever
	while (1) {
		//check value with the current spot
		check = checkval(x, y, val);
		//if check proved valid
		if (check != 1) {
			//set the current spot to the value
			value[y][x] = val; val = 0;
			//go to the next usable spot
			//and if it is at the end
			if (nextip(&x, &y)) {
				cout << "Solved!" << endl;
				//print the solved sudoku and exit
				printgrid();
				exit(0);
			}
		}
		//if the check proved valid, but the value is illegal
		if (check && val >= 9) {
			//change current spot to 0
			value[y][x] = 0;
			//go to previous usable spot, and if there is none
			if (previp(&x, &y)) {
				//print "unsolvable" and exit
				cout << "Sorry, unsolvable..." << endl;
				exit(0);
			}
			//otherwise, set the value to that of the current spot
			val = value[y][x];
			//if value is 9, subtract 1 to prevent illegal values
			if (val == 9) {
				val--;
			}
		}
		//add 1 to the value
		val++;
	}
}

/**----------------------------------------------------------------------------
@name nextip

@brief sets x and y to the next usable coordinate
	returns 1 if there are no usable coords

@param x pointer and y pointer

@retval 0 or 1
-----------------------------------------------------------------------------*/

int Sudoku::nextip(int* x, int* y) {
	//if x isn't at the end of the row, go to the next spot in the row
	if (*x < 8) {
		*x += 1;
	}
	//otherwise start at the begining of the next row
	else {
		*y += 1; *x = 0;
	}
	//loop while the current spot is a prepopulated spot
	while (loaded[*y][*x] == 1) {
		//if x isn't at the end of the row, go to the next spot in the row
		if (*x < 8) {
			*x += 1;
		}
		//otherwise start at the begining of the next row
		else {
			*y += 1; *x = 0;
		}
	}
	//if at the end of the sudoku, return 1
	if (*y > 8) {
		return 1;
	}
	return 0;
}

/**----------------------------------------------------------------------------
@name previp

@brief sets x and y to the previous usable coordinate.
	returns 1 if there are no usable coords

@param x pointer and y pointer

@retval 0 or 1
-----------------------------------------------------------------------------*/

int Sudoku::previp(int* x, int* y) {
	//if x isn't at the begining of the row, go to the previous spot
	if (*x > 0) {
		*x -= 1;
	}
	//otherwise go to the end of the previous row
	else {
		*y -= 1; *x = 8;
	}
	//loop while the current spot is a prepopulated spot
	while (loaded[*y][*x] == 1) {
		////if x isn't at the begining of the row, go to the previous spot
		if (*x > 0) {
			*x -= 1;
		}
		//otherwise go to the end of the previous row
		else {
			*y -= 1; *x = 8;
		}
	}
	//if at the begining of the sudoku, return 1
	if (*y < 0) {
		return 1;
	}
	return 0;
}