#include "sudoku.h"
#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

/************************
* @name		Sudoku
* @brief	generates sudoku board with n numbers pre-populated
* @param	n
* @retval	none
************************/
Sudoku::Sudoku(int n) {
	//initialize random
	srand(time(NULL));

	//loop unill valid board
	do {
		//wipe grid
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				grid[i][j] = 0;
			}
		}

		//loop n times
		for (int i = 0; i < n; i++) {
			//generate random number and position
			int x = rand() % 9, y = rand() % 9;
			if (grid[x][y] == 0) {
				grid[x][y] = rand() % 9 + 1;
			}
			else {
				i--;
			}
		}
		//check for valid board
	} while (!check());

	printGrid();
}

/************************
* @name		solve
* @brief	solves sudoku board
* @param	none
* @retval	int
************************/
int Sudoku::solve() {
	//get fixed grid
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			if (grid[i][j] != 0) {
				fixedGrid[i][j] = 1;
			}
			else {
				fixedGrid[i][j] = 0;
			}
		}
	}

	//find first valid value
	int x, y;
	
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
					if (fixedGrid[i][j] == 0) {
						x = i; y = j;
						i = 10, j = 10;				
				}
			}
		}
	
	//loop through grid
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			int num = 0;

				//find valid space
				while (num == 0) {
					num = 1;
					if (fixedGrid[i][j] == 0) {
						//loop untill valid number	
						do {
							int k = grid[i][j] + 1;

							grid[i][j] = k;

							if (grid[x][y] == 10) {
								return 0;
							}
							else if (k == 10) {
								grid[i][j] = 0;
								//go to last number
								do {
									if (j == 0) {
										i--;
										j = 8;
									}
									else {
										j--;
									}
								} while (fixedGrid[i][j] == 1);
								num = 0;
							}
							else {
								k++;
							}
						} while (!check());

					}
				}	
		}
	}

	if (check()) {
		return 1;
	}

}

/************************
* @name		printGrid
* @brief	prints sudoku board
* @param	none
* @retval	none
************************/
void Sudoku::printGrid() {
	char prntGrid[9][9];

	//change 0 to *
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			//copy grid to prntGrid
			if (grid[i][j] == 0) {
				prntGrid[i][j] = '*';
			}
			else {
				prntGrid[i][j] = (char(grid[i][j]) + 48);
			}

		}
	}

	//print the grid
	cout << " " << prntGrid[0][0] << " " << prntGrid[1][0] << " " 
		 << prntGrid[2][0] << " | " << prntGrid[3][0] << " " 
		 << prntGrid[4][0] << " " << prntGrid[5][0] << " | " 
		 << prntGrid[6][0] << " " << prntGrid[7][0] << " " 
		 << prntGrid[8][0] << endl;

	cout << " " << prntGrid[0][1] << " " << prntGrid[1][1] << " " 
		 << prntGrid[2][1] << " | " << prntGrid[3][1] << " " 
		 << prntGrid[4][1] << " " << prntGrid[5][1] << " | " 
		 << prntGrid[6][1] << " " << prntGrid[7][1] << " " 
		 << prntGrid[8][1] << endl;

	cout << " " << prntGrid[0][2] << " " << prntGrid[1][2] << " " 
		 << prntGrid[2][2] << " | " << prntGrid[3][2] << " " 
		 << prntGrid[4][2] << " " << prntGrid[5][2] << " | " 
		 << prntGrid[6][2] << " " << prntGrid[7][2] << " " 
		<< prntGrid[8][2] << endl;

	cout << " ------+-------+------" << endl;

	cout << " " << prntGrid[0][3] << " " << prntGrid[1][3] << " " 
		 << prntGrid[2][3] << " | " << prntGrid[3][3] << " " 
		 << prntGrid[4][3] << " " << prntGrid[5][3] << " | " 
		 << prntGrid[6][3] << " " << prntGrid[7][3] << " " 
		<< prntGrid[8][3] << endl;

	cout << " " << prntGrid[0][4] << " " << prntGrid[1][4] << " " 
		 << prntGrid[2][4] << " | " << prntGrid[3][4] << " " 
		 << prntGrid[4][4] << " " << prntGrid[5][4] << " | " 
		 << prntGrid[6][4] << " " << prntGrid[7][4] << " " 
		<< prntGrid[8][4] << endl;

	cout << " " << prntGrid[0][5] << " " << prntGrid[1][5] << " " 
		 << prntGrid[2][5] << " | " << prntGrid[3][5] << " " 
		 << prntGrid[4][5] << " " << prntGrid[5][5] << " | " 
		 << prntGrid[6][5] << " " << prntGrid[7][5] << " " 
		 << prntGrid[8][5] << endl;

	cout << " ------+-------+------" << endl;

	cout << " " << prntGrid[0][6] << " " << prntGrid[1][6] << " " 
		 << prntGrid[2][6] << " | " << prntGrid[3][6] << " " 
		 << prntGrid[4][6] << " " << prntGrid[5][6] << " | " 
		 << prntGrid[6][6] << " " << prntGrid[7][6] << " " 
		<< prntGrid[8][6] << endl;

	cout << " " << prntGrid[0][7] << " " << prntGrid[1][7] << " " 
		<< prntGrid[2][7] << " | " << prntGrid[3][7] << " " 
		<< prntGrid[4][7] << " " << prntGrid[5][7] << " | " 
		<< prntGrid[6][7] << " " << prntGrid[7][7] << " " 
		<< prntGrid[8][7] << endl;

	cout << " " << prntGrid[0][8] << " " << prntGrid[1][8] << " " 
		<< prntGrid[2][8] << " | " << prntGrid[3][8] << " " 
		<< prntGrid[4][8] << " " << prntGrid[5][8] << " | " 
		<< prntGrid[6][8] << " " << prntGrid[7][8] << " " 
		<< prntGrid[8][8] << endl;
}

/************************
* @name		checkRow
* @brief	checks rows to make sure board is valid
* @param	none
* @retval	valid rows
************************/
int Sudoku::checkRow() {
	int temp, count;

	//loop through grid
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {

			//find number
			count = 0;
			if (grid[i][j] != 0) {
				temp = grid[i][j];

				//loop through row to see if there's repeats
				for (int k = 0; k < 9; k++) {
					if (grid[k][j] == temp) {
						count += 1;
					}
				}
				if (count > 1) {
					return 0;					
				}
			}		
		}
	}
	return 1;
}

/************************
* @name		checkCol
* @brief	checks columns to make sure board is valid
* @param	none
* @retval	valid cols
************************/
int Sudoku::checkCol() {
	int temp, count;

	//loop through grid
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {

			//find number
			count = 0;
			if (grid[i][j] != 0) {
				temp = grid[i][j];

				//loop through col to see if there's repeats
				for (int k = 0; k < 9; k++) {
					if (grid[i][k] == temp) {
						count += 1;
					}
				}
				if (count > 1) {
					return 0;
				}
			}
		}
	}
	return 1;
}

/************************
* @name		checkBox
* @brief	checks box sections to make sure board is valid
* @param	none
* @retval	valid boxes
************************/
int Sudoku::checkBox() {
	int temp, count;
	int sudBox[9][9] = {
		{grid[0][0], grid[0][1], grid[0][2], 
		 grid[1][0], grid[1][1], grid[1][2], 
		 grid[2][0], grid[2][1], grid[2][2]},

		{grid[0][3], grid[0][4], grid[0][5], 
		 grid[1][3], grid[1][4], grid[1][5], 
		 grid[2][3], grid[2][4], grid[2][5]},

		{grid[0][6], grid[0][7], grid[0][8], 
		 grid[1][6], grid[1][7], grid[1][8], 
		 grid[2][6], grid[2][7], grid[2][8]},
		
		{grid[3][0], grid[3][1], grid[3][2], 
		 grid[4][0], grid[4][1], grid[4][2], 
		 grid[5][0], grid[5][1], grid[5][2]},

		{grid[3][3], grid[3][4], grid[3][5], 
		 grid[4][3], grid[4][4], grid[4][5], 
		 grid[5][3], grid[5][4], grid[5][5]},

		{grid[3][6], grid[3][7], grid[3][8], 
		 grid[4][6], grid[4][7], grid[4][8], 
		 grid[5][6], grid[5][7], grid[5][8]},

		{grid[6][0], grid[6][1], grid[6][2], 
		 grid[7][0], grid[7][1], grid[7][2], 
		 grid[8][0], grid[8][1], grid[8][2]},

		{grid[6][3], grid[6][4], grid[6][5], 
		 grid[7][3], grid[7][4], grid[7][5], 
		 grid[8][3], grid[8][4], grid[8][5]},

		{grid[6][6], grid[6][7], grid[6][8], 
		 grid[7][6], grid[7][7], grid[7][8], 
		 grid[8][6], grid[8][7], grid[8][8]},
	};
	
	//loop through grid
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {

			//find number
			count = 0;
			if (sudBox[i][j] != 0) {
				temp = sudBox[i][j];

				//loop through col to see if there's repeats
				for (int k = 0; k < 9; k++) {
					if (sudBox[i][k] == temp) {
						count += 1;
					}
				}
				if (count > 1) {
					return 0;				
				}
			}
		}
	}
	return 1;
}

/************************
* @name		check
* @brief	combines all check functions into one
* @param	none
* @retval	valid board
************************/
int Sudoku::check() {
	if (checkBox() && checkRow() && checkCol()) {
		return 1;
	}
	return 0;
}

