#include "sudoku.h" 
#include <iostream> 

using namespace std;

/************************
* @name		main
* @brief	generates sudoku board with user inputed number of pre-populated
* @brief	numbers and then tries to solve board
* @param	none
* @retval	none
************************/
int main()
{
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	Sudoku s(n);

	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printGrid();
		exit(1);
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
		exit(1);
	}

	return 0;
}