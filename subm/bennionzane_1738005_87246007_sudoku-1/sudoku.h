#ifndef SUDOKU
#define SUDOKU

class Sudoku {
private:
	int grid[9][9] = { 0 };
	int fixedGrid[9][9];
	int checkRow();
	int checkCol();
	int checkBox();
	int check();
	int nextGrid(int *x, int *y);
	int prevGrid(int *x, int *y);
	int firstGrid(int *x, int *y);

public:
	Sudoku(int n);
	int solve();
	void printGrid();

};

#endif SUDOKU
