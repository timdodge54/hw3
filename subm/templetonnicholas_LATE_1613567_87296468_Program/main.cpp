/******************************************************************************
*	@name	main
*	@brief	uses the sudoku class to solve a sudoku with n pregenerated values
*			dictated by the user. 
*	@param	none
*	@retval	none
******************************************************************************/
#include "Sudoku.h"
#include <iostream>

using namespace std;
int main()
{
	int n;
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	Sudoku s(n);
	s.printFinalGrid();

	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printFinalGrid();
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}
	return 0;
}