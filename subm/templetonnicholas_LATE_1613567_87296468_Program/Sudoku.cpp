#include "Sudoku.h"
#include <iostream>
#include <cstdlib>
using namespace std;

/******************************************************************************
*	@name	isValid
*	@brief	evaluates if a spot can be a candidate for an n number. 
*	@param	3 ints, a number to test and 2 location numbers
*	@retval	1 bool, true if it is a valid spot
******************************************************************************/
bool Sudoku::isValid(int n, int row, int column)
{
	int valid; 
	// n is never valid if it is greater than 9
	if (n > 9)
		return false;
	// evaluates to 0 if all of the called functions work successfully and it is a valid spot
	valid = checkColumn(n, column) + checkRow(n, row) + checkBox(n, row, column);
	//if at a pregenerated spot, it is automatically not a valid location
	if (generatedGrid[row][column] == 1)
		return false;
	else if (valid == 0)
		return true;
	else
		return false;
}

/******************************************************************************
*	@name	checkColumn
*	@brief	iterates through a column to see if a value is already present
*	@param	2 ints, 1 test number and 1 column to test
*	@retval	1 int, returns 0 if no number matching n is already present. 
******************************************************************************/
int Sudoku::checkColumn(int n, int column)
{
		// column stays constant and we check up and down the column
		for (int i = 0; i < 9; i++)
		{
			//if true then a value n is already present in the column
			if (testGrid[i][column] == n)
				return 1;
		}
		return 0;
}

/******************************************************************************
*	@name	checkRow
*	@brief	iterates through a row to see if a value is already present.
*	@param	2 ints, 1 test number and 1 row to test.
*	@retval	1 int, returns 0 if no number matching n is already present. 
******************************************************************************/
int Sudoku::checkRow(int n, int row)
{
		// the row stays constant and we move through the row values to check
		for (int i = 0; i < 9; i++)
		{
			//if true then a value n is already present in the row
			if (testGrid[row][i] == n)
				return 1;
		}
		return 0;
}

/******************************************************************************
*	@name	checkBox
*	@brief	iterates through a box of interest to see if a value is already present.
*	@param	3 ints, 1 test number and 2 location numbers to tell which box to test.
*	@retval	1 int, returns 0 if no number matching n is already present.
******************************************************************************/
int Sudoku::checkBox(int n, int x, int y)
{
		//if were checking quadrant 1
		if (x < 3 && y < 3)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					if (testGrid[i][j] == n)
						return 1;
				}
			}
			return 0;
		}
		//if were checking quadrant 2
		else if (x < 6 && x > 2 && y < 3)
		{
			for (int i = 3; i < 6; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					if (testGrid[i][j] == n)
						return 1;
				}
			}
			return 0;
		}
		//if were checking quadrant 3
		else if (x > 5 && y < 3)
		{
			for (int i = 6; i < 9; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					if (testGrid[i][j] == n)
						return 1;
				}
			}
			return 0;
		}
		//if were checking quadrant 4
		else if (x < 3 && y < 6 && y > 2)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 3; j < 6; j++)
				{
					if (testGrid[i][j] == n)
						return 1;
				}
			}
			return 0;
		}
		//if were checking quadrant 5
		else if (x < 6 && x > 2 && y < 6 && y > 2)
		{
			for (int i = 3; i < 6; i++)
			{
				for (int j = 3; j < 6; j++)
				{
					if (testGrid[i][j] == n)
						return 1;
				}
			}
			return 0;
		}
		//if were checking quadrant 6
		else if (x > 5 && y < 6 && y > 2)
		{
			for (int i = 6; i < 9; i++)
			{
				for (int j = 3; j < 6; j++)
				{
					if (testGrid[i][j] == n)
						return 1;
				}
			}
			return 0;
		}
		//if were checking quadrant 7
		else if (x < 3 && y > 5)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 6; j < 9; j++)
				{
					if (testGrid[i][j] == n)
						return 1;
				}
			}
			return 0;
		}
		//if were checking quadrant 8
		else if (x < 6 && x > 2 && y > 5)
		{
			for (int i = 3; i < 6; i++)
			{
				for (int j = 6; j < 9; j++)
				{
					if (testGrid[i][j] == n)
						return 1;
				}
			}
			return 0;
		}
		//if were checking quadrant 9
		else if (x > 5 && y > 5)
		{
			for (int i = 6; i < 9; i++)
			{
				for (int j = 6; j < 9; j++)
				{
					if (testGrid[i][j] == n)
						return 1;
				}
			}
			return 0;
		}
	
}

/******************************************************************************
*	@name	printGeneratedGrid
*	@brief	prints a grid showing which values were pregenerated
*	@param	none
*	@retval	none
******************************************************************************/
void Sudoku::printGeneratedGrid()
{
	
	cout << " " << generatedGrid[0][0] << "  " << generatedGrid[0][1] << "  " << generatedGrid[0][2] << " | " <<
		generatedGrid[0][3] << "  " << generatedGrid[0][4] << "  " << generatedGrid[0][5] << " | " <<
		generatedGrid[0][6] << "  " << generatedGrid[0][7] << "  " << generatedGrid[0][8] << " " << endl;

	cout << " " << generatedGrid[1][0] << "  " << generatedGrid[1][1] << "  " << generatedGrid[1][2] << " | " <<
		generatedGrid[1][3] << "  " << generatedGrid[1][4] << "  " << generatedGrid[1][5] << " | " <<
		generatedGrid[1][6] << "  " << generatedGrid[1][7] << "  " << generatedGrid[1][8] << " " << endl;

	cout << " " << generatedGrid[2][0] << "  " << generatedGrid[2][1] << "  " << generatedGrid[2][2] << " | " <<
		generatedGrid[2][3] << "  " << generatedGrid[2][4] << "  " << generatedGrid[2][5] << " | " <<
		generatedGrid[2][6] << "  " << generatedGrid[2][7] << "  " << generatedGrid[2][8] << " " << endl;

	cout << "---------+---------+---------" << endl;

	cout << " " << generatedGrid[3][0] << "  " << generatedGrid[3][1] << "  " << generatedGrid[3][2] << " | " <<
		generatedGrid[3][3] << "  " << generatedGrid[3][4] << "  " << generatedGrid[3][5] << " | " <<
		generatedGrid[3][6] << "  " << generatedGrid[3][7] << "  " << generatedGrid[3][8] << " " << endl;

	cout << " " << generatedGrid[4][0] << "  " << generatedGrid[4][1] << "  " << generatedGrid[4][2] << " | " <<
		generatedGrid[4][3] << "  " << generatedGrid[4][4] << "  " << generatedGrid[4][5] << " | " <<
		generatedGrid[4][6] << "  " << generatedGrid[4][7] << "  " << generatedGrid[4][8] << " " << endl;

	cout << " " << generatedGrid[5][0] << "  " << generatedGrid[5][1] << "  " << generatedGrid[5][2] << " | " <<
		generatedGrid[5][3] << "  " << generatedGrid[5][4] << "  " << generatedGrid[5][5] << " | " <<
		generatedGrid[5][6] << "  " << generatedGrid[5][7] << "  " << generatedGrid[5][8] << " " << endl;

	cout << "---------+---------+---------" << endl;

	cout << " " << generatedGrid[6][0] << "  " << generatedGrid[6][1] << "  " << generatedGrid[6][2] << " | " <<
		generatedGrid[6][3] << "  " << generatedGrid[6][4] << "  " << generatedGrid[6][5] << " | " <<
		generatedGrid[6][6] << "  " << generatedGrid[6][7] << "  " << generatedGrid[6][8] << " " << endl;

	cout << " " << generatedGrid[7][0] << "  " << generatedGrid[7][1] << "  " << generatedGrid[7][2] << " | " <<
		generatedGrid[7][3] << "  " << generatedGrid[7][4] << "  " << generatedGrid[7][5] << " | " <<
		generatedGrid[7][6] << "  " << generatedGrid[7][7] << "  " << generatedGrid[7][8] << " " << endl;

	cout << " " << generatedGrid[8][0] << "  " << generatedGrid[8][1] << "  " << generatedGrid[8][2] << " | " <<
		generatedGrid[8][3] << "  " << generatedGrid[8][4] << "  " << generatedGrid[8][5] << " | " <<
		generatedGrid[8][6] << "  " << generatedGrid[8][7] << "  " << generatedGrid[8][8] << " " << endl;

	cout << endl;
}

//This prints the final answer grid in a nice looking format
void Sudoku::printFinalGrid()
{

	cout << " " << testGrid[0][0] << "  " << testGrid[0][1] << "  " << testGrid[0][2] << " | " <<
		testGrid[0][3] << "  " << testGrid[0][4] << "  " << testGrid[0][5] << " | " <<
		testGrid[0][6] << "  " << testGrid[0][7] << "  " << testGrid[0][8] << " " << endl;

	cout << " " << testGrid[1][0] << "  " << testGrid[1][1] << "  " << testGrid[1][2] << " | " <<
		testGrid[1][3] << "  " << testGrid[1][4] << "  " << testGrid[1][5] << " | " <<
		testGrid[1][6] << "  " << testGrid[1][7] << "  " << testGrid[1][8] << " " << endl;

	cout << " " << testGrid[2][0] << "  " << testGrid[2][1] << "  " << testGrid[2][2] << " | " <<
		testGrid[2][3] << "  " << testGrid[2][4] << "  " << testGrid[2][5] << " | " <<
		testGrid[2][6] << "  " << testGrid[2][7] << "  " << testGrid[2][8] << " " << endl;

	cout << "---------+---------+---------" << endl;

	cout << " " << testGrid[3][0] << "  " << testGrid[3][1] << "  " << testGrid[3][2] << " | " <<
		testGrid[3][3] << "  " << testGrid[3][4] << "  " << testGrid[3][5] << " | " <<
		testGrid[3][6] << "  " << testGrid[3][7] << "  " << testGrid[3][8] << " " << endl;

	cout << " " << testGrid[4][0] << "  " << testGrid[4][1] << "  " << testGrid[4][2] << " | " <<
		testGrid[4][3] << "  " << testGrid[4][4] << "  " << testGrid[4][5] << " | " <<
		testGrid[4][6] << "  " << testGrid[4][7] << "  " << testGrid[4][8] << " " << endl;

	cout << " " << testGrid[5][0] << "  " << testGrid[5][1] << "  " << testGrid[5][2] << " | " <<
		testGrid[5][3] << "  " << testGrid[5][4] << "  " << testGrid[5][5] << " | " <<
		testGrid[5][6] << "  " << testGrid[5][7] << "  " << testGrid[5][8] << " " << endl;

	cout << "---------+---------+---------" << endl;

	cout << " " << testGrid[6][0] << "  " << testGrid[6][1] << "  " << testGrid[6][2] << " | " <<
		testGrid[6][3] << "  " << testGrid[6][4] << "  " << testGrid[6][5] << " | " <<
		testGrid[6][6] << "  " << testGrid[6][7] << "  " << testGrid[6][8] << " " << endl;

	cout << " " << testGrid[7][0] << "  " << testGrid[7][1] << "  " << testGrid[7][2] << " | " <<
		testGrid[7][3] << "  " << testGrid[7][4] << "  " << testGrid[7][5] << " | " <<
		testGrid[7][6] << "  " << testGrid[7][7] << "  " << testGrid[7][8] << " " << endl;

	cout << " " << testGrid[8][0] << "  " << testGrid[8][1] << "  " << testGrid[8][2] << " | " <<
		testGrid[8][3] << "  " << testGrid[8][4] << "  " << testGrid[8][5] << " | " <<
		testGrid[8][6] << "  " << testGrid[8][7] << "  " << testGrid[8][8] << " " << endl;

	cout << endl;
	/*
	for (int y = 0; y < 11; y++)
	{
		for (int row = 0; row < 11; row++)
		{
			if (row == 3 || row == 7)
				cout << "|";
			else
				cout << " " << testGrid[row][y] << "  ";
			cout << endl;
		}
		if (y == 3 || y == 7)
			cout << "------------+------------+--------------" << endl;
	}
	*/
}

/******************************************************************************
*	@name	nextSquare
*	@brief	moves the spot of interest to the next square valid for testing
*	@param	2 int addresses
*	@retval	1 int , returns 1 if next spot is valid, returns 0 if the next 
*			square is off the grid
******************************************************************************/
int Sudoku::nextSquare(int &row, int &column)
{
	
	for (row; row <= 8;)
	{
		for ( column; column < 8;)
		{
			//everytime this function is called it moves to the next square
			column++;
			//it exits the function if the new square is valid for change
			if (generatedGrid[row][column] == 0)
				return 1;
		}
		//increments the row if we reach the end of a row and resets column to zero
		row++;
		column = 0;
		//it exits the function if the new square is valid for change
		if (generatedGrid[row][column] == 0)
			return 1;
	}
	return 0;
}

/******************************************************************************
*	@name	previousSquare
*	@brief	moves to the last square open for change
*	@param	2 int addresses
*	@retval	1 int, returns 0 if the previous square is off the grid
******************************************************************************/
int Sudoku::previousSquare(int& row, int& column)
{
	//if were at the first spot on the grid, we can't move backwards
	if ((row == 0) && (column == 0))
		{
			return 0;
		}
	for (row; row >= 0;)
	{
		for (column; column >  0;)
		{
			//moves column back each time function is called
			column--;
			//checks to see if it moves back to a valid spot
			if (generatedGrid[row][column] == 0)
				return 1;
			
		}
		//reduces the row if im not on the first row. 
		if (row != 0)
		{
			row--;
			column = 8;
			//checks to see if it moves back to a valid spot
			if (generatedGrid[row][column] == 0)
				return 1;
		}
		else
			return 0;
		
		
	}
	return 0;
}

/******************************************************************************
*	@name	solve
*	@brief	actaully solves the sudoku
*	@param	none
*	@retval	1 int, 1 if it succeeds and zero if it fails
******************************************************************************/
int Sudoku::solve()
{
	// starts at spot [0][0] checking number 1 
	int number = 1;
	bool notFinished = true;
	int columnBegin = 0;
	int rowBegin = 0;
	while (notFinished)
	{
		// if the number is valid in that spot, place the number, reset test number to 
		// zero and then move on to the next square
		if (isValid(number, rowBegin, columnBegin))
		{
			testGrid[rowBegin][columnBegin] = number;
			number = 1;
			//checks to see if nextSquare moves us off the grid or not. 
			if (nextSquare(rowBegin, columnBegin) == 0)
			{
				notFinished = false;
				return 1;
			}
		}
		//if the next spot it moved too isnt valid, move to the next spot
		else if (generatedGrid[rowBegin][columnBegin] == 1)
		{
			if (columnBegin < 8)
				columnBegin += 1;
			else
			{
				rowBegin += 1;
				columnBegin = 0;
			}
				
		}
		else
		{
			//if the previous number doesn't work, try the next one. 
			number += 1;
			//if numbers 1-9 dont work, move backwards to find a solution. 
			if (number >= 10)
			{
				testGrid[rowBegin][columnBegin] = 0;
				if (previousSquare(rowBegin, columnBegin) == 0)
				{
					return 0;
				}
				else
				{
					number = testGrid[rowBegin][columnBegin] + 1;
				}
			}
		}
	}
}

/******************************************************************************
*	@name	sudoku
*	@brief	this is the constructor for the sudoku class
*	@param	1 int, the number of pregenerated values wanted.
*	@retval none
******************************************************************************/
Sudoku::Sudoku(int n)
{
	//these 2 for loops are initializing all the entries in the grid to 0
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			generatedGrid[i][j] = 0;
			testGrid[i][j] = 0;
		}
	}
	int entry = 0;
	int row = 0;
	int column = 0;


	srand(time(NULL));

	//add a random number to the grid at a random location as many times as
	//dictaed by the user 
	for (int i = 0; i < n;)
	{
		entry = rand() % 9 + 1;
		row = rand() % 9;
		column = rand() % 9;
		


		//if the number and spot chosen are valid, then the test and generated grids
		// are assigned their needed values. if not then it loops around and 
		//generates a new number and location
		if (isValid(entry, row, column))
		{
			//The generated grid will have a 0 or a 1 depending on if a number is placed there
			generatedGrid[row][column] = 1;
			//The test grid will have all zeros and then the actaul generated values.
			testGrid[row][column] = entry;
			
			
			i++;
		}
	}
}