#pragma once


#ifndef _HEADER_
#define _HEADER_
#define MAX_ROW = 9;
#define MAX_COLUMN = 9;


class Sudoku
{
private:


	int generatedGrid[9][9];
    int testGrid[9][9];
    bool useGeneratedGrid = true;
    bool useTestGrid = false;
    bool isValid(int n, int row, int column);
    int checkColumn(int n, int column);
    int checkRow(int n, int row);
    int checkBox(int n, int row, int column);
    int nextSquare(int &row, int &column);
    int previousSquare(int &row, int&column);
    


public:
    void printGeneratedGrid();
	void printFinalGrid();
	int solve();

	Sudoku(int n);
};

#endif
