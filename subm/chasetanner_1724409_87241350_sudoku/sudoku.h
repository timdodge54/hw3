#pragma once
#include <iostream>

using namespace std;

class Sudoku {
public:
	Sudoku(int n);
	int solve();
	void printGrid();
private:
	char grid[9][9] = {0};
	bool fixed[9][9] = {0};

	bool posGood(char val, char x, char y);
	bool getFirstSquare(char& x, char& y);
	bool getNextSquare(char& x, char& y);
	bool getPrevSquare(char& x, char& y);
};