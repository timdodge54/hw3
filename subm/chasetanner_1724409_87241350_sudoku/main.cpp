#include "sudoku.h"

/******************************************************************************
* @name main
* @brief Creates a sudoku puzzle based on user entered number of prepopulated \
*		 squares and solves it if possible.
* @param none
* @retval none
******************************************************************************/
int main(void)
{
	// Initialize input variable
	int in;

	// Print prompt and get input
	cout << "Welcome to SudokuSolver!!" << endl
		<< "Enter number of squares to prepopulate: ";
	cin >> in;

	// Create puzzle
	Sudoku puzzle(in);
	// If the puzzle is solvable
	if (puzzle.solve()) {
		// Print the solved puzzle
		cout << endl << endl << "Solved!" << endl;
		puzzle.printGrid();
	}
	// Else print impossible
	else {
		cout << endl << endl << "Sorry, unsolvable..." << endl;
	}

	// Exit with no errors
	return 0;
}