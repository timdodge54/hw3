#include "sudoku.h"
#include <cstdlib>

/******************************************************************************
* @name Sudoku
* @brief Constructor for Sudoku class that initializes puzzle randomly.
* @param n number of squared to prepopulate
* @retval none
******************************************************************************/
Sudoku::Sudoku(int n)
{
	// Seed random number function
	srand(time(NULL));

	// Initialize grid
	for (char i = 0; i < n; i++)
	{
		// Loop until a valid random combination is found
		bool flag = false;
		while (!flag)
		{
			// Initialize random position and value
			char val = rand() % 9 + 1;
			char x = rand() % 8;
			char y = rand() % 8;
			// Set value if not already and is valid
			if (!fixed[y][x] && posGood(val, x, y))
			{
				// Set the grid value / fixed status then continue
				grid[y][x] = val;
				fixed[y][x] = true;
				flag = true;
			}
		}
	// End the loop when n squares have been populated
	}
	// Print the intial status of the grid
	printGrid();
}

/******************************************************************************
* @name solve
* @brief Solves the class-owned Sudoku puzzle if possible and prints it if so.
* @param none
* @retval 1 if solvable, 0 if not
******************************************************************************/
int Sudoku::solve()
{
	// Initialize variables
	char x, y, n;
	// Get first square and return success if already solved
	if (!getFirstSquare(x, y))
	{
		return 1;
	}

	// Loop until solved
	do
	{
		// Loop through all values of n for each square
		for (n = grid[y][x] + 1; n < 10; n++)
		{
			// If the value works
			if (posGood(n, x, y))
			{
				// Set the grid to n and continue
				grid[y][x] = n;
				break;
			}
		}

		// If none of the valid numbers worked
		if (n == 10)
		{
			// Reset the current value to 0
			grid[y][x] = 0;
			// Get the previous square
			if (!getPrevSquare(x, y))
			{
				// If there is none return unsolvable
				return 0;
			}
		}
		// Get the next square
		else if (!getNextSquare(x, y))
		{
			// If there is none return solved
			return 1;
		}
	} while (true);
}

/******************************************************************************
* @name printGrid
* @brief Prints the current state of the Sudoku puzzle to the CLI.
* @param none
* @retval none
******************************************************************************/
void Sudoku::printGrid()
{
	// Loop through all of the grid rows
	for (char i = 0; i < 9; i++)
	{
		// If it row 3 or 6
		if (i && i % 3 == 0)
		{
			// Print row lines
			cout << "-----------+-----------+-----------" << endl;
		}

		// Loop through all columns of row
		for (char j = 0; j < 9; j++)
		{
			// If is column 3 or 6
			if (j && j % 3 == 0)
			{
				// Add a column line
				cout << "  |";
			}

			// Print space between each number
			cout << "  ";
			// Print a * if the value is 0
			if (grid[i][j] == 0)
			{
				cout << '*';
			}
			// If not then print the integer value of the number
			else
			{
				cout << (int)grid[i][j];
			}
		}
		// Print a newline after each row
		cout << endl;
		// If the rows are not the ends / middle
		if ((i % 3) < 2)
		{
			// Print more dividing lines
			cout << "           |           |           " << endl;
		}
	}
}

/******************************************************************************
* @name posGood
* @brief determines if a value is valid at a given position in the puzzle.
* @param val value to test
* @param x column cordinate to test
* @param y row cordinate to test
* @retval true if valid, false otherwise
******************************************************************************/
bool Sudoku::posGood(char val, char x, char y)
{
	// Initialize variables
	char minrow = 3 * (y / 3);
	char mincol = 3 * (x / 3);

	// Loop through the rows
	for (char i = 0; i < 9; i++)
	{
		// Validate val for the row
		if (grid[y][i] == val)
		{
			return false;
		}
	}
	// Loop through the columns
	for (char i = 0; i < 9; i++)
	{
		// Validate val for the column
		if (grid[i][x] == val)
		{
			return false;
		}
	}
	// Loop through the rows of 3x3 grid
	for (char i = minrow; i < (minrow + 3); i++)
	{
		// Loop through the col of 3x3 grid
		for (char j = mincol; j < (mincol + 3); j++)
		{
			// If the value exists
			if (grid[i][j] == val)
			{
				// Return invalid
				return false;
			}
		}
	}
	// Return true if validations passed
	return true;
}

/******************************************************************************
* @name getFirstSquare
* @brief Gets the first square of the puzzle with a nonfixed value.
* @param x column cordinate reference of first square
* @param y row cordinate reference of first square
* @retval true if found, false otherwise
******************************************************************************/
bool Sudoku::getFirstSquare(char& x, char& y)
{
	// Loop through the rows of grid
	for (char i = 0; i < 9; i++)
	{
		// Loop through the columns of grid
		for (char j = 0; j < 9; j++)
		{
			// If the value is not fixed
			if (!fixed[i][j])
			{
				// Set the pos variables to these values
				y = i;
				x = j;
				// Return found
				return true;
			}
		}
	}
	// Return not found if not
	return false;
}

/******************************************************************************
* @name getNextSquare
* @brief Gets the first square of the puzzle with a zero value.
* @param x column cordinate reference of next square
* @param y row cordinate reference of next square
* @retval true if found, false otherwise
******************************************************************************/
bool Sudoku::getNextSquare(char& x, char& y)
{
	// Loop through the rows of grid
	for (char i = 0; i < 9; i++)
	{
		// Loop through the columns of grid
		for (char j = 0; j < 9; j++)
		{
			// If the value is zero
			if (!grid[i][j])
			{
				// Set the pos variables to these values
				y = i;
				x = j;
				// Return found
				return true;
			}
		}
	}
	// Return not found if not
	return false;
}

/******************************************************************************
* @name getPrevSquare
* @brief Gets the last square of the puzzle with an unfixed nonzero value.
* @param x column cordinate reference of previous square
* @param y row cordinate reference of previous square
* @retval true if found, false otherwise
******************************************************************************/
bool Sudoku::getPrevSquare(char& x, char& y)
{
	// Loop through the rows of grid backwards
	for (char i = 8; i >= 0; i--)
	{
		// Loop through the columns of grid backwards
		for (char j = 8; j >= 0; j--)
		{
			// If the value is not fixed and nonzero
			if (!fixed[i][j] && grid[i][j])
			{
				// Set the pos variables to these values
				y = i;
				x = j;
				// Return found
				return true;
			}
		}
	}
	// Return not found if not
	return false;
}