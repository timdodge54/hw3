/*************************************************
* @name: main
* @brief: This is a program that will play a sudoku game
*
*Compile instructions : gcc Sudoku.c �o Sudoku.exe
*************************************************/

#include <iostream>
#include <ctime>
#include <iomanip>
#include <string.h>
#include <cmath>
using namespace std;

/*@name: Sudoku
*@brief: declarations for functions, variables, and sets up
* the sudokuBoard
* @param:
* @retval: none
*/
class Sudoku
{
private:
    int sudokuBoard[9][9];
    static int UNC;
public:
    //declarations
    int solve();
    int isAllowed(int, int, int);
    void printGrid();
    int containsInRow(int row, int number);
    int containsInCol(int col, int number);
    int containsInBox(int row, int col, int number);
    Sudoku(int n)
    {
        //setting up sudokuBoard
        int*** sudokuBoard = new int** [n];
        for (int j = 0; j < 9; j++) {
            sudokuBoard[j] = new int* [9];
            for (int i = 0; i < 9; i++) {
                sudokuBoard[j][i] = new int[9];
            }
        }

        for (int j = 0; j < n; j++) {
            for (int i = 0; i < 9; i++)
            {
                for (int k = 0; k < 9; k++)
                {
                    sudokuBoard[j][i][k] = 0;
                }
            }
        }
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < 9; i++)
            {
                for (int k = 0; k < 9; k++)
                {
                    cout << sudokuBoard[j][i][k] << " ";
                }
                cout << endl;
            }
        }
    }
};
//while istream & operator>>(istream & input, Sudoku & s)
/*
TEST(Sudoku, Easy)
{
  ifstream puzzleIn("easy.txt");
  ifstream answerIn("easy.answer");
  Sudoku puzzle;
  Sudoku answer;

  puzzleIn >> puzzle;
  answerIn >> answer;

  EXPECT_EQ(true, puzzle.solve());

  for (int i = 0; i < 9; ++i)
  {
    for (int j = 0; k < 9; ++k)
    {
      EXPECT_EQ(puzzle.getValue(i, k), answer.getValue(i, k));
    }
  }
}
*/
/*
TEST(Sudoku, Impossible)
{
    std::ifstream puzzleIn("impossible.txt");
    std::ifstream answerIn("easy.answer");
    Sudoku puzzle;
    Sudoku answer;

    puzzleIn >> puzzle;
    answerIn >> answer;
   // bool value = puzzle.solve();
   // if (value) puzzle.printGrid();
    EXPECT_EQ(false, puzzle.solve());


}


TEST(Sudoku, Medium)
{
  std::ifstream puzzleIn("medium.txt");
  std::ifstream answerIn("medium.answer");
  Sudoku puzzle;
  Sudoku answer;

  puzzleIn >> puzzle;
  answerIn >> answer;

  EXPECT_EQ(true, puzzle.solve());

  for (int i = 0; i < 9; ++i)
  {
    for (int k = 0; k < 9; ++k)
    {
      EXPECT_EQ(puzzle.getValue(i, k), answer.getValue(i, k));
    }
  }
}

TEST(Sudoku, Hard)
{
  std::ifstream puzzleIn("hard.txt");
  std::ifstream answerIn("hard.answer");
  Sudoku puzzle;
  Sudoku answer;

  puzzleIn >> puzzle;
  answerIn >> answer;

  EXPECT_EQ(true, puzzle.solve());

  for (int i = 0; i < 9; ++i)
  {
    for (int k = 0; k < 9; ++k)
    {
      EXPECT_EQ(puzzle.getValue(i, k), answer.getValue(i, k));
    }
  }
}

TEST(Sudoku, Evil)
{
  std::ifstream puzzleIn("evil.txt");
  std::ifstream answerIn("evil.answer");
  Sudoku puzzle;
  Sudoku answer;

  puzzleIn >> puzzle;
  answerIn >> answer;

  EXPECT_EQ(true,puzzle.solve());

  for (int i = 0; i < 9; ++i)
  {
    for (int k = 0; k < 9; ++k)
    {
      EXPECT_EQ(puzzle.getValue(i, k), answer.getValue(i, k));
    }
  }
}*/

/*@name: containsInRow
*@brief: makes sure rows are correct
*@param: none
*@retval: true when conditions are met and false after loop finishes
*/
int Sudoku::containsInRow(int row, int number)
{
    //sets up amount of rows and randomizes
    srand(time(0));
    for (int i = 0; i < 9; i++)
    {
        i = rand() % 9;
        if (sudokuBoard[row][i] == number)
        {
            return true;
        }
    }
    return false;
}
/*@name: containsInCol
*@brief: makes sure columns are correct
*@param: none
*@retval: true when conditions are met and false after loop finishes
*/
int Sudoku::containsInCol(int col, int number)
{
    //sets up amount of columns and randomizes
    srand(time(0));
    for (int j = 0; j < 9; j++)
    {
        j = rand() % 9;
        if (sudokuBoard[j][col] == number)
        {
            return true;
        }
    }
    return false;
}
/*@name: containsInBox
*@brief: makes sure sudoku boxes are correct dimensions
*@param: i, r, j, c, row, col, number
*@retval: true when conditions are met and false after loop finishes
*/
int Sudoku::containsInBox(int row, int col, int number)
{
    // making sure each single sudoku box is a 3x3
    int r = row - row % 3;
    int c = col - col % 3;
    for (int i = r; i < r + 3; i++)
    {
        for (int j = c; j < c + 3; j++)
        {
            if (sudokuBoard[i][j] == number)
            {
                return true;
            }
        }
        return false;
    }
}
/*@name: isAllowed
*@brief:
*@param: row, col, numbers
*@retval: not
*/
int Sudoku::isAllowed(int row, int col, int number)
{
    return !(containsInRow(row, number) ||
        containsInCol(col, number) || containsInBox(row, col, number));
}
/*@name: solve
*@brief:
*@param: none
*@retval:
*/
int Sudoku::solve()
{
    UNC = 0;
    for (int row = 0; row < 9; row++)
    {
        for (int col = 0; col < 9; col++)
        {
            if (sudokuBoard[row][col] == UNC)
            {
                for (int number = 1; number <= 9; number++)
                {
                    if (isAllowed(row, col, number))
                    {
                        sudokuBoard[row][col] = number;
                        if (solve())
                        {
                            return true;
                        }
                        else
                        {
                            sudokuBoard[row][col] = UNC;
                        }
                    }
                }
                return false;
            }
        }
    }
    return true;
}
/*@name: printGrid
*@brief:
*@param: i, j
*@retval: none
*/
void Sudoku::printGrid() {
    for (int i = 0; i < 9; i++)
    {
        if (i % 3 == 0 && i != 0)
        {
            cout << "----------------------------------\n";
        }
        for (int j = 0; j < 9; j++)
        {
            if (j % 3 == 0 && j != 0)
            {
                cout << " | ";
            }
            cout << " " << sudokuBoard[i][j] << " ";

        }

        cout << endl;
    }
    cout << "\n\n__________________________________________\n\n";
}
/*@name: main
*@brief: takes user input and also tells user their options.
* @param: z
* @retval: none
*/
int main()
{
    int z;
    Sudoku result(z);

    cout << "Welcome to SudokuSolver!!" << endl;
    cout << "Enter number of squares to prepopulate: ";
    cin >> z;

    if (result.solve()) {
        cout << "Solved!" << endl;
        result.printGrid();
    }
    else {
        cout << "Sorry, unsolvable..." << endl;
    }

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
