#ifndef	SUDOKU_H
#define SUDOKU_H
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <cstdio>
using namespace std;

class Sudoku {
private:
	int checkrow(int val, int y);
	int checkcol(int val, int x);
	int checkgrid(int cal, int x, int y);
public:
	int i, j;
	int grid[9][9] = {{0,0,0,0,0,0,0,0,0},
					{0,0,0,0,0,0,0,0,0},
					{0,0,0,0,0,0,0,0,0},
					{0,0,0,0,0,0,0,0,0},
					{0,0,0,0,0,0,0,0,0},
					{0,0,0,0,0,0,0,0,0},
					{0,0,0,0,0,0,0,0,0},
					{0,0,0,0,0,0,0,0,0},
					{0,0,0,0,0,0,0,0,0},};
	Sudoku(int n);
	int solve();
	void printgrid();
	void printnew();
};

#endif