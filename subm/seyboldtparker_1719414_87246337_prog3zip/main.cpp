/******************************************************************************
* @name 	main
* @brief	program that asks user how many sudoku numbers to fill in,
*			then solves the puzzle if possible
* @param	none
* @retval	none
******************************************************************************/
#include "sudoku.h"

int main(void) {
	//initialize array, and ints
	int n;
	// random number set
	srand(time(NULL));
	// welcome message
	cout << "Welcome to Sudoku Solver!" << endl;
	// ask user for a number of squares to pre-populate
	cout << "Enter number of squares to pre-populate: ";
	// cin number to int n
	cin >> n;
	// send number to Sudoku s(n);
	Sudoku s(n);
	// if solvable, send to print function
	if (s.solve()) {
		s.printgrid();
		cout << endl << "Solved!" << endl;
		s.printnew();
	}
	// if not solvable, output error message
	else {
		cout << "Sorry! This puzzle could not be solved." << endl;
	}
	
	return 0;
}