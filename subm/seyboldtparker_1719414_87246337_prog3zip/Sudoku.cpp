#include "sudoku.h"

/******************************************************************************
* @name 	checkrow
* @brief	checks if there is a matching number in the row
* @param	int val, int y
* @retval	none
******************************************************************************/
int Sudoku::checkrow(int val, int y){
	
}

/******************************************************************************
* @name 	checkcol
* @brief	checks if there is a matching number in the column
* @param	int val, int x
* @retval	none
******************************************************************************/
int Sudoku::checkcol(int val, int x) {

}

/******************************************************************************
* @name 	checkgrid
* @brief	checks if there is a matching number in the 3x3 grid
* @param	int val, int x, int y
* @retval	none
******************************************************************************/
int Sudoku::checkgrid(int val, int x, int y) {

}

/******************************************************************************
* @name 	Sudoku
* @brief	initializes int n to int val
* @param	int n
* @retval	none
******************************************************************************/
Sudoku::Sudoku(int n) {
	
}

/******************************************************************************
* @name 	solve
* @brief	solves the sudoku puzzle
* @param	none
* @retval	none
******************************************************************************/
int Sudoku::solve() {

} 

/******************************************************************************
* @name 	printgrid
* @brief	prints grid to the screen
* @param	none
* @retval	none
******************************************************************************/
void Sudoku::printgrid() {
	cout << "Original Puzzle" << endl;
	for (i = 0; i < 9; i++) {
		for (j = 0; j < 9; j++) {
			if (grid[i][j] == 0) {
				cout << "*";
			}
		}
	}
}

/******************************************************************************
* @name 	printnew
* @brief	prints new grid to the screen
* @param	none
* @retval	none
******************************************************************************/
void Sudoku::printnew() {
for (i = 0; i < 9; i++) {
		for (j = 0; j < 9; j++) {
			if (grid[i][j] == 0) {
				cout << grid[i][j];
			}
		}
	}
}