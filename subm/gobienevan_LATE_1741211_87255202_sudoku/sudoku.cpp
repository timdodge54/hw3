#include "sudoku.h"
#include <iostream>

using namespace std;


/*********************************************************************
* @name Sudoku
* @brief creates the puzzle 
* @param none
* @retval  none
*********************************************************************/
Sudoku::Sudoku(int n)
{
    //random number generator
    srand(time(NULL));
    //fill grid
    grid = std::vector<std::vector<int>>(9, std::vector<int>(9, 0));


    //counter for the number of random numbers added to the grid
    int count = 0;
    while (count < n)
    {
        //rand row
        int row = rand() % 9;

        // rand column 
        int col = rand() % 9;

        // rand number 1-9
        int num = rand() % 9 + 1;

        
        if (grid[row][col] == 0 && checkRow(row, num) && checkCol(col, num) && checkBox(row, col, num))
        {

            //random number to the selected cell
            grid[row][col] = num;
            count++;
        }
    }
}

/*********************************************************************
* @name solve
* @brief solves the sudoku
* @param none
* @retval  none
*********************************************************************/
// Solving sudoku
int Sudoku::solve()
{
    // variables to store unassigned 
    int row, col;
    if (!isUnassigned(row, col))
    {
        //If no unassigned cell found, return 1 for solved
        return 1;
    }

    for (int num = 1; num <= 9; num++)
    {

        // If number can be placed without rule breaking
        if (checkRow(row, num) && checkCol(col, num) && checkBox(row, col, num))
        {
            // assign the number 
            grid[row][col] = num;
            if (solve())
            {
                //return 1 if  sudoku cant be solved
                return 1;
            }
            // backtrack 
            grid[row][col] = 0;
        }
    }

    // Return 0 if sudoku can't be solved
    return 0;
}

/*********************************************************************
* @name printGrid
* @brief Print the sudoku grid
* @param none
* @retval  none
*********************************************************************/
// Print the sudoku grid
void Sudoku::printGrid()
{
    cout << " -----------------------" << endl;

    for (int i = 0; i < 9; i++)
    {
        cout << "| ";
        for (int j = 0; j < 9; j++)
        {
            if (grid[i][j] == 0)
            {
                cout << "- ";
            }
            else
            {
                cout << grid[i][j] << " ";
            }
            if (j == 2 || j == 5)
            {
                cout << "| ";
            }
        }
        cout << "|" << endl;
        if (i == 2 || i == 5)
        {
            cout << " -------+-------+-------" << endl;
        }
    }
    cout << " -----------------------" << endl;
}

/*********************************************************************
* @name checkRow
* @brief Check if the number is present in the current row
* @param none
* @retval  none
*********************************************************************/

bool Sudoku::checkRow(int row, int num)
{
    for (int col = 0; col < 9; col++)
    {
        //Check if the number is present in the current row
        if (grid[row][col] == num)
        {
            return false;
        }
    }
    return true;
}

/*********************************************************************
* @name checkCol
* @brief Check if the number is present in the current column
* @param none
* @retval  none
*********************************************************************/

bool Sudoku::checkCol(int col, int num)
{
    for (int row = 0; row < 9; row++)
    {
        //Check if the number is present in the current column
        if (grid[row][col] == num)
        {
            return false;
        }
    }
    return true;
}

/*********************************************************************
* @name checkBox
* @brief Check if the number is present in the current 3x3 box
* @param none
* @retval  none
*********************************************************************/
bool Sudoku::checkBox(int row, int col, int num)
{
    int row_start = row - row % 3;
    int col_start = col - col % 3;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            //Check if the number is present in the current 3x3 box
            if (grid[row_start + i][col_start + j] == num)
            {
                return false;
            }
        }
    }
    return true;
}

/*********************************************************************
* @name isUnassigned
* @brief Checks if unassigned
* @param none
* @retval  none
*********************************************************************/
bool Sudoku::isUnassigned(int& row, int& col)
{
    for (row = 0; row < 9; row++)
    {
        for (col = 0; col < 9; col++)
        {
            //Check if unassigned
            if (grid[row][col] == 0)
            {
                return true;
            }
        }
    }
    return false;
}



