#pragma once
#include <iostream>
#include <vector>

class Sudoku
{
public:
	Sudoku(int n);
	int solve();
	void printGrid();

private:
	std::vector<std::vector<int>> grid;
	bool checkRow(int row, int num);
	bool checkCol(int col, int num);
	bool checkBox(int row, int col, int num);
	bool isUnassigned(int& row, int& col);
};