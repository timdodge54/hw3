#include "sudoku.h"
#include <iostream>

using namespace std;

/*********************************************************************
* @name main
* @brief Runs the whole code for sudoku
* @param none
* @retval  none
*********************************************************************/

int main()
{
    int n;

    cout << "Welcome to SudokuSolver!!" << endl;
    cout << "Enter number of squares to prepopulate: ";
    cin >> n;

    //prepopulating the grid
    Sudoku sudoku(n);
    sudoku.printGrid();
    cout << endl;

    //solving sudoku and printing results
    int result = sudoku.solve();
    if (result == 1)
    {
        cout << "Solved! " << endl;
        sudoku.printGrid();
    }
    else
    {
        cout << "Sorry, unsolvable..." << endl;
    }

    return 0;
}