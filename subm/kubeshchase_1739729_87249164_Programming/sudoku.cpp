#include <iostream>
#include <cstdlib>
#include "sudoku.h"
using namespace std;

/*****************************************************************************
* @name		Sudoku
* @brief	Initializes the Sudoku puzzle, placing numbers around the board
* @param	number of spaces to fill
* @retval	none
*****************************************************************************/

Sudoku::Sudoku(int n) {
	int x, y, i, upNext, error = 0;
	time_t t;
	srand((unsigned) time(&t));
	
	x = rand() % 9;
	y = rand() % 9;
	upNext = (rand() % 9) + 1;

	//Initialize sudoku board
	for (i = 0; i < n; i++) {
		error = 0;
		//If we cant place in that spot, reroll
		while (checkMove(x, y, upNext)) {
			x = rand() % 9;
			y = rand() % 9;
			upNext = (rand() % 9) + 1;
			//Check if we're stuck and wipe everything and restart
			if (error > 1000) {
				wipeBoard();
				error = 0;
				i = 0;
			}
			error++;
		}
		//Place number
		board[y][x] = upNext;
	}
}

/*****************************************************************************
* @name		solve
* @brief	Attempts to solve the Sudoku puzzle
* @param	none
* @retval	0 on failure, 1 on success
*****************************************************************************/

int Sudoku::solve() {
	int pos = -1;
	int doOnce = 0;
	int didFail = 0;
	int numToTry = 0;
	//Find the first valid spot
	findNext(&pos);
	
	try {
	//Main solver loop. Exits once its been solved, or failed
	while (1) {
		doOnce = 0;
		didFail = 0;
		//Decides what number to place
		do {
			//Starts by adding 1 to the number already placed
			if (doOnce == 0) {
				doOnce = 1;
				numToTry = solveBoard[pos/9][pos%9] + 1;
			}
			else {
				numToTry++;
			}

			if (numToTry > 9) {

				//Back up and check if we should fail
				if (backtrack(&pos) == 0) {
					throw 0;
				}
				//Restart loop
				didFail = 1;
			}
		//Check validity of new number, exits if valid
		} while ((checkMove(pos%9, pos/9, numToTry) > 0) && (didFail == 0));

		//If we didnt fail out of the do loop
		if (didFail == 0) {
			solveBoard[pos / 9][pos % 9] = numToTry;
			//Move on, and see if solve succeeded
			if (findNext(&pos) == 0) {
				throw 1;
			}
		}
	}
	}
	catch (int i) {
		if (i == 1) {
			//Merge boards
			for (pos = 0; pos <= 80; pos++) {
				if (board[pos/9][pos%9] == 0){
					board[pos/9][pos%9] = solveBoard[pos/9][pos%9];
				}
			}
		}
		return i;
	}

	//if we hit this return, we messed up
	return 808;
}

/*****************************************************************************
* @name		printGrid
* @brief	Prints the Sudoku board in the correct format
* @param	none
* @retval	none
*****************************************************************************/

void Sudoku::printGrid() {
	int row, column, block, temp; 
	//Goes across the rows, printing in the correct format
	for (row = 0; row <= 8; row++) {
		//Begin row with 2 spaces
		cout << "  ";

		for (block = 0; block <= 2; block++) {
			for (column = 0; column <= 2; column++) {
				//Print number or *
				temp = board[row][column + (block * 3)];
				if (temp == 0) {
					cout << "*";
				}
				else {
					cout << temp;
				}

				//Inter-number spacing
				if (column != 2) {
					cout << "   ";
				}
			}
			//Inter-block spacing
			if (block < 2) {
				cout << " | ";
			}
		}
		
		//Vertical spacing and box-making
		if (row < 8) {
			if (((row + 1) % 3 == 0)) {
				//Makes the horizontal box line
			cout << endl << "------------+-----------+------------" << endl;
			}
			else {
				//Makes vertical box line
			cout << endl << "            |           |            " << endl;
			}
		}
	}
	cout << endl << endl;
	return;
}

/*****************************************************************************
* @name		checkMove
* @brief	Checks if a potential number placement is valid
* @param	x, y, number to try
* @retval	0 if success, > 0 if fail
*****************************************************************************/

int Sudoku::checkMove(int x, int y, int number) {
	int i, boxx, boxy, row, column;
	
	//If spot is already taken, return failed
	if (board[y][x] != 0) {
		return 1;
	}
	//Check vertical column
	for (i = 0; i <= 8; i++) {
		if (board[i][x] == number) {
			return 2;
		}
	}
	//Check horizontal row
	for (i = 0; i <= 8; i++) {
		if (board[y][i] == number) {
			return 3;
		}
	}
	//Check in box
	boxx = x / 3;
	boxy = y / 3;
	for (row = 0; row <= 2; row++) {
		for (column = 0; column <= 2; column++) {
			if (board[row + (boxy * 3)][column + (boxx * 3)] == number) {
				return 4;
			}
		}
	}
	//Check vertical column Solver
	for (i = 0; i <= 8; i++) {
		if (solveBoard[i][x] == number) {
			return 5;
		}
	}
	//Check horizontal row Solver
	for (i = 0; i <= 8; i++) {
		if (solveBoard[y][i] == number) {
			return 6;
		}
	}
	//Check in box Solver
	boxx = x / 3;
	boxy = y / 3;
	for (row = 0; row <= 2; row++) {
		for (column = 0; column <= 2; column++) {
			if (solveBoard[row + (boxy * 3)][column + (boxx * 3)] == number) {
				return 7;
			}
		}
	}

	return 0;
}

/*****************************************************************************
* @name		wipeBoard
* @brief	Resets all values of the board to 0
* @param	none
* @retval	none
*****************************************************************************/

void Sudoku::wipeBoard() {
	int i, k;
	for (i = 0; i <= 8; i++) {
		for (k = 0; k <= 8; board[i][k] = 0, k++);
	}
}

/*****************************************************************************
* @name		backtrack
* @brief	Finds the last spot where a number was placed, backtracking
* @param	position index
* @retval	0 if solve should fail out, 1 if backing successful
*****************************************************************************/

int Sudoku::backtrack(int* pos) {
	try {
		//Cleanse us of our sins
		solveBoard[(*pos)/9][(*pos)%9] = 0;
		//Do until we hit an empty spot
		do {
			(*pos)--;
			//Solve failed
			if (*pos < 0) {
				throw 90;
			}
		} while (board[(*pos)/9][(*pos)%9] != 0);
	}
	catch (int i) {
		return 0;
	}

	return 1;
}

/*****************************************************************************
* @name		findNext
* @brief	Finds the next spot where a number can be placed
* @param	position index
* @retval	0 if solver finished the last piece, 1 if spot available
*****************************************************************************/

int Sudoku::findNext(int *pos) {
	try {
		//Do until we hit an empty spot
		do {
			(*pos)++;
			//Solve succeeded
			if (*pos > 80) {
				throw 90;
			}
		} while (board[(*pos)/9][(*pos)%9] != 0);
	}
	catch (int i) {
		return 0;
	}

	return 1;
}