#include <iostream>
#include "sudoku.h"
using namespace std;

/*****************************************************************************
* @name		main
* @brief	Creates and attempts to solve a Sudoku board
* @param	none
* @retval	none
*****************************************************************************/

int main() {
	int userInput;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> userInput;
	//Create the puzzle
	Sudoku puzzle(userInput);
	puzzle.printGrid();
	//Solve that sucker
	if (puzzle.solve() == 1) {
		cout << endl << "Solved!" << endl;
		puzzle.printGrid();
	}
	else {
		//Unfortunate
		cout << endl << "Sorry, unsolvable...";
	}

	return 0;
}