#ifndef SUDOKU_H
#define SUDOKU_H

class Sudoku {
private:
	int board[9][9] = { 0 };
	int solveBoard[9][9] = {0};
	int checkMove(int x, int y, int number);
	void wipeBoard();
	int findNext(int *pos);
	int backtrack(int *pos);
public:
	Sudoku(int n);
	int solve();
	void printGrid();
};

#endif