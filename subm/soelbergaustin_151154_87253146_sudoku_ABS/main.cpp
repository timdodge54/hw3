#include "sudoku.h"





/*****************************************************************************
* @name		main
* @brief	asks user how many squares they would like to have initially 
*			populated, and sends this amount to the sudoku function. then
*			prints a solved problem
* @param	none
* @retval	none
*****************************************************************************/

using namespace std;

int main()
{
	int n;

	//Prompt user for n number of squares to pre-populate
	cout << "Welcome to Simple Sudoko." << endl;
	cout << "Please enter the number of squares to pre-populate: ";
	cin >> n;

	//Call Sudoku constructor with n
	Sudoku puzzle(n);

	//Call solve function
	if (puzzle.solve() == 0)
	{
		cout << "Solved!" << endl;
		puzzle.printGrid();
	}
	else
	{
		cout << "Sorry, unsolvable..." << endl;
	}
	
	return 0;
}