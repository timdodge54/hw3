#ifndef SUDOKU_H
#define SUDOKU_H
#include <iostream>

class Sudoku {
public:
	Sudoku(int n);
	int solve();
	void printGrid();

private:
	int findfirst(int *row, int *col);
	int findnext(int *row, int *col);
	int findprev(int *row, int *col);
	int checkgrid(int checkval, int row, int col);
	int checkrow(int checkval, int row);
	int checkcol(int checkval, int col);
		
	int x, y, temp;
	struct box {
		int value;
		int inplay;
	};
	struct box grid[9][9];


};
#endif
