#include "sudoku.h"
#include <cstdlib>
#include <ctime>


/*****************************************************************************
* @name		Sudoku
* @brief	constructor for Sudoku class, initializes grid and then places 
*			n random values at random locations within the grid, with n coming
*			from the main function. finally, prints the initial puzzle
* @param	n value to to fill n grid spaces
* @retval	none
*****************************************************************************/

Sudoku::Sudoku(int n)
{
	int i = 0, temp, row, col, violation = 0;
	
	//initialize all grid values to 0
	for (row = 0; row < 9; row++)
	{
		for (col = 0; col < 9; col++)
		{
			grid[row][col].value = 0;
			grid[row][col].inplay = 0;
		}

	}
	row = 0;
	col = 0;
		
	//initialize random number generator
	srand(time(NULL));
	//loop n times, placing random numbers in random places
	for (i = 0; i < n;)
	{
		//get a random row value between 0 and 8
		row = rand() % 9;
		//get a random col value between 0 and 8
		col = rand() % 9;
		//get a random square value between 1 and 9
		temp = rand() % 9 + 1;
		//check for violations
		if (checkrow(temp, row) == 1)
		{
			violation = 1;
		}
		if (checkcol(temp, col) == 1)
		{
			violation = 1;
		}
		if (checkgrid(temp, row, col) == 1)
		{
			violation = 1;
		}
		if (violation == 0)
		{
			//if there are no violations, then set value to the checkvalue
			grid[row][col].value = temp;
			//and set the inplay value to 1 (false)
			grid[row][col].inplay = 1;
			i++;
		}
	}
	//print the initial puzzle
	printGrid();
}

/*****************************************************************************
* @name		checkrow
* @brief	checks the value of each box in a specified row against the 
*			given checkvalue, if there is a match, returns a 1 to tell the 
*			calling function that the value being checked cannot be used
* @param	tentative value to be placed in box, row value to represent row to
*			to be checked
* @retval	'conflict': 0 for good, 1 for bad
*****************************************************************************/

int Sudoku::checkrow(int checkval, int row)
{
	int conflict = 0, i = 0;

	//loop until every position in row is checked
	for (i = 0; i < 9; i++)
	{
		if (grid[row][i].value == checkval)
		{
			conflict = 1;
		}
	}
	return conflict;
}

/*****************************************************************************
* @name		checkcol
* @brief	checks the value of each box in a specified column against the 
*			given checkvalue, if there is a match, returns a 1 to tell the 
*			calling function that the value being checked cannot be used
* @param	tentative value to be placed in box, col value to represent 
			column to to be checked
* @retval	'conflict': 0 for good, 1 for bad
*****************************************************************************/

int Sudoku::checkcol(int checkval, int col)
{
	int conflict = 0, i = 0;

	//loop until every position in column is checked
	for (i = 0; i < 9; i++)
	{
		if (grid[i][col].value == checkval)
		{
			conflict = 1;
		}
	}
	return conflict;
}

/*****************************************************************************
* @name		checkgrid
* @brief	checks the value of each box in the subgrid that the input 
*			row and col coordinates falls within. if any values in the subgrid
*			match the value being checked, then returns a 1 to tell the
*			calling function that the checked value cannot be used
* @param	tentative value to be placed in box, col value and row value to
*			determine subgrid to check
* @retval	'conflict': 0 for good, 1 for bad
*****************************************************************************/

int Sudoku::checkgrid(int checkval, int row, int col)
{
	int minrow, maxrow, mincol, maxcol, i, j, conflict = 0;
	//Courtesy of Dr. Phillips
	minrow = (row / 3) * 3;
	maxrow = minrow + 2;
	mincol = (col / 3) * 3;
	maxcol = mincol + 2;

	for (i = mincol; i <= maxcol; i++)
	{
		for (j = minrow; j <= maxrow; j++)
		{
			if (grid[j][i].value == checkval)
			{
				conflict = 1;
			}

		}
	}
	return conflict;
}

/*****************************************************************************
* @name		printGrid
* @brief	prints the grid array
* @param	none
* @retval	none
*****************************************************************************/

void Sudoku::printGrid()
{
	int prow, pcol, row = 0, col = 0;
	
	for (row = 0; row < 9; row++)
	{
		for (col = 0; col < 9; col++)
		{
			if (grid[row][col].value == 0)
			{
				std::cout << " * ";
				if (col == 8)
				{
					std::cout << std::endl;
				}
			}
			else
			{
				std::cout << " " << grid[row][col].value << " ";
				if (col == 8)
				{
					std::cout << std::endl;
				}
			}
			if (col == 2 || col == 5)
			{
				std::cout << "|";
			}
		}
		if (row == 2 || row == 5)
		{
			std::cout << "---------+---------+--------" << std::endl;
		}
	}
	
}

/*****************************************************************************
* @name		solve
* @brief	solves the puzzle that the constructor has created
* @param	none
* @retval	integer - 0 if succeeds, 1 if fails
*****************************************************************************/

int Sudoku::solve()
{
	int  row = 0, col = 0;
	int* pointrow = &row;
	int* pointcol = &col;
	int n = 1, temp, violation = 0;

	//set gridrow and gridcol to coordinates of first playable box
	findfirst(pointrow, pointcol);
	for (;;)
	{ 
		temp = n;
		//check for violations
		if (checkrow(temp, row) == 1)
		{
			violation = 1;
		}
		if (checkcol(temp, col) == 1)
		{
			violation = 1;
		}
		if (checkgrid(temp, row, col) == 1)
		{
			violation = 1;
		}
		if (violation == 0)
		{
			//if there are no violations, then set value to the checkvalue
			grid[row][col].value = temp;
			//and set the inplay value to 1 (false)
			grid[row][col].inplay = 1;
			n++;
			//find next square
			if (findnext(pointrow, pointcol) == 1)
			{
				//if findnext returned a 1, then puzzle is solved
				return 0;
			}
			else
			{
				n++;
				if (n >= 10)
				{
					grid[row][col].value = 0;
					if (findprev(pointrow, pointcol) == 1)
					{
						//if findprev returned a 1, then puzzle failed
						return 1;
					}
					else
					{
						n = grid[row][col].value;
						n++;
					}
				}
			}
		}
		
	}
}


/*****************************************************************************
* @name		findfirst
* @brief	finds the first position in the array that is playable
* @param	pointers to row and column
* @retval	integer 0 for success and 1 for failure
*****************************************************************************/

int Sudoku::findfirst(int *row, int *col)
{
	int  x = 0, y = 0;

	for (;;)
	{
		if (grid[x][y].inplay == 0)
		{
			*row = x;
			*col = y;
			return 0;
		}
		else
		{
			y++;
			if (y == 9)
			{
				x++;
				y = 0;
			}
			if (x == 9);
			//found no playable tile, return 1
			{
				return 1;
			}
		}
	}
	
}

/*****************************************************************************
* @name		findnext
* @brief	finds the next position in the array that is playable
* @param	pointers to row and column
* @retval	integer 0 for success and 1 for falling out of array
*****************************************************************************/

int Sudoku::findnext(int *row, int *col)
{
	int x = *row, y = *col;

	for (;;)
	{
		y++;
		if (y == 9)
		{
			x++;
			y = 0;
			if (x == 9)
			{
				return 1;
			}
		}
		if (grid[x][y].inplay == 0)
		{
			*row = x;
			*col = y;
			return 0;
		}
	}
}

/*****************************************************************************
* @name		findprev
* @brief	finds the previous position in the array that is playable
* @param	pointers to row and column
* @retval	integer 0 for success and 1 for falling out of array
*****************************************************************************/

int Sudoku::findprev(int *row, int *col)
{
	int x = *row, y = *col;

	for (;;)
	{
		y--;
		if (y == -1)
		{
			x--;
			y = 0;
			if (x == -1)
			{
				return 1;
			}
		}
		if (grid[x][y].inplay == 0)
		{
			*row = x;
			*col = y;
			return 0;
		}
	}
}

