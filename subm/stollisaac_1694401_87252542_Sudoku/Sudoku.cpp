#include <iostream>
#include <cstdlib>
#include "Sudoku.h"

using namespace std;

/******************************************************************************
* @name 	Sudoku
* @brief 	creates class object
* @param 	int n
* @retval 	none
******************************************************************************/
Sudoku::Sudoku(int n)
{
	//creates sudoku class object
	srand(time(NULL));
	int randX, randY, randNum, i;
	i = 0;
	num = n;

	//loop while i < num
	while (i < num)
	{
		//makes random numbers
		randX = rand() % 8;
		randY = rand() % 8;
		randNum = rand() % 9 + 1;

		// if no violations
		if (testRow(randX, randNum) && testCol(randY, randNum) && testBox(randY, randX, randNum) && (game[randX][randY] == 0))
		{
			// put num on the board
			game[randX][randY] = randNum;
			i++;
		}
	}
}

/******************************************************************************
* @name 	solve
* @brief 	solves a prepopulated puzzle
* @param 	none
* @retval 	int 1 or 0
******************************************************************************/
int Sudoku::solve()
{
	//intialize fixed array with 0's
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (game[i][j] != 0)
			{
				fixed[i][j] = 1;
			}
			else
			{
				fixed[i][j] = 0;
			}
		}
	}

	int x = 0;
	int y = 0;
	int n = 1;
	int forever = 1;

	//back tracking algorithim
	if (findFirstSquareInPlay(&x, &y))
	{
		cout << "(" << x << ", " << y << ")" << endl;
		//loop forever
		while (forever == 1)
		{
			//if no violations
			if (testRow(x, n) && testCol(y, n) && testBox(y, x, n) && (game[x][y] == 0) && n <= 9)
			{
				//board = n
				game[x][y] = n;
				cout << "play is " << game[x][y] << endl;
				n = 1;

				//if find next sqaure is true
				if (!findNextSquareInPlay(&x, &y))
				{
					return 1;
				}
			}
			else
			{
				n = n + 1;
				//cout << "play is " << game[x][y] << endl;
				// if n = 10
				if (n == 10)
				{
					//set place on board to 0
					game[x][y] = 0;

					//if find prev square is true increment whatever is on the board 
					if (findPrevSquareInPlay(&x, &y) && testRow(x, n) && testCol(y, n) && testBox(y, x, n))
					{
						n = game[x][y] + 1;
						
					}
				}
			}
		}
	}
}

/******************************************************************************
* @name		testCol
* @brief 	test for violations in the column
* @param 	int y, int n
* @retval 	int 1 or 0
******************************************************************************/
int Sudoku::testCol(int y, int n)
{
	int i;
	
	//loop for 9 times
	for (i = 0; i < 9; i++)
	{
		//if num is equal to anything in col
		if (n == game[i][y])
		{
			return 0;
		}
	}
	return 1;
}

/******************************************************************************
* @name		testRow
* @brief 	test for violations in the row
* @param 	int x, int n
* @retval 	int 1 or 0
******************************************************************************/
int Sudoku::testRow(int x, int n)
{
	int j;

	//loop 9 time
	for (j = 0; j < 9; j++)
	{
		//if num is equal to anything in the row
		if (n == game[x][j])
		{
			return 0;
		}
	}
	return 1;
}

/******************************************************************************
* @name		testbox
* @brief 	test for violations in the 3 X 3 box
* @param 	int y, int n
* @retval 	int 1 or 0
******************************************************************************/
int Sudoku::testBox(int x, int y, int n)
{
	int i, j, minRow, minCol, maxCol, maxRow;

	//scales the board to check 3 X 3 grid
	minRow = (x / 3) * 3;
	minCol = (y / 3) * 3;
	maxRow = minRow + 2;
	maxCol = minCol + 2;

	//checks for any of the same numbers in the grid
	for (i = minCol; i <= maxCol; i++)
	{
		for (j = minRow; j <= maxRow; j++)
		{
			if (n == game[i][j])
			{
				return 0;
			}
		}
	}
	return 1;
}

/******************************************************************************
* @name 	printGrid
* @brief	prints the sudoku grid
* @param 	none
* @retval 	none
******************************************************************************/
void Sudoku::printGrid()
{
	int row, col;

	for (row = 0; row < 9; row++)
	{
		if (row % 3 != 0)
		{
			cout << endl;
			cout << "          |           |";
		}
		if (row >= 3 && row % 3 == 0)
		{
			cout << endl;
			cout << "----------+-----------+-----------";
		}
		cout << endl;

		for (col = 0; col < 9; col++)
		{
			if (col % 3 != 0)
			{
				cout << "   ";
			}
			if (col >= 3 && col % 3 == 0)
			{
				cout << " | ";
			}
			if (game[row][col] == 0)
			{
				cout << '*';
			}
			else
			{
				cout << game[row][col];
			}
		}
	}
	cout << endl;
}

/******************************************************************************
* @name 	findFirstSqaureInPlay
* @brief 	will find the first mutable square
* @param 	none
* @retval 	int 1 or 0
******************************************************************************/
int Sudoku::findFirstSquareInPlay(int *x, int *y)
{
	//loops until it finds the first 0 in fixed array
	cout << "start of first loop" << endl;
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			//sets the fixed coordinates the the game coordinates
			if (fixed[i][j] == 0)
			{
				*x = i;
				*y = j;
				cout << "(" << *x << ", " << *y <<")" << endl;
				cout << "end of first loop" << endl << endl;
				return 1;
			}
		}
	}
	return 0;
}

/******************************************************************************
* @name 	findNextSquareInPlay
* @brief 	looks for the next mutable sqaure
* @param 	none
* @retval 	int 1 or 0
******************************************************************************/
int Sudoku::findNextSquareInPlay(int *x, int *y)
{
	int i = *x;
	int j = *y;

	//loops until it finds the first 0 in fixed array
	cout << endl << endl;
	cout << "start next loop" << endl;
	while (i < 9)
	{
		j = j + 1;
		if(j == 9)
		{
			i++;
			j = 0;
		}
		//sets the fixed coordinates the the game coordinates
		if (fixed[i][j] == 0)
		{
			*x = i;
			*y = j;
			cout << "(" << *x << ", " << *y << ")" << endl;
			cout << "end of next loop" << endl;
			return 1;
		}
	}
	return 0;
}

/******************************************************************************
* @name 	findPrevSqaureinPlay
* @brief 	looks for the last mutable square
* @param 	none
* @retval 	int 1 or 0
******************************************************************************/
int Sudoku::findPrevSquareInPlay(int* x, int* y)
{
	int i = *x;
	int j = *y;

	cout << endl << endl;
	cout << "start prev loop" << endl;

	do
	{
		// when j is 0 move the row and reset j
		if (j == 0)
		{
			i--;
			j = 8;
			cout << "- row";
		}
		else
		{
			//decrement j
			j = j - 1;
			cout << "- col";
		}
		//set coordinates
		*x = i;
		*y = j;
		cout << "(" << *x << ", " << *y << ")" << endl;
		cout << "end of prev loop" << endl << endl;

	} while (fixed[i][j] == 1);
	
	return 1;
}