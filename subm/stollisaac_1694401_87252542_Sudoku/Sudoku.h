#pragma once

class Sudoku {
private:
	int game[9][9] = {};
	int fixed[9][9];
	int num;

	int testRow(int y, int n);
	int testCol(int x, int n);
	int testBox(int x, int y, int n);
	int findFirstSquareInPlay(int *x, int *y);
	int findNextSquareInPlay(int *x, int *y);
	int findPrevSquareInPlay(int *x, int *y);
public:
	Sudoku(int n);
	int solve();
	void printGrid();
};