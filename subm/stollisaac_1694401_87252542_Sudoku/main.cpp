#include <iostream>
#include "Sudoku.h"

using namespace std;

/******************************************************************************
* @name 	main
* @brief 	prints a sovled or unsolved sudoku puzzle
* @param 	none
* @retval 	none
******************************************************************************/
int main(void)
{
	int num;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> num;

	Sudoku s(num);
	s.printGrid();

	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printGrid();
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}
	return 0;
}