/*
@name: main
@brief: takes a number from user and creates and attempts to solve a sudoku puzzle
@param:input from user
@retval:none
*/


#include "sudoku.h"

using namespace std;

int main()
{
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";//ask user for input
	cin >> n;

	sudoku s(n);//send input to class

	s.sudoku::sudoku(n);//print original array

	if (s.sudoku::solve())
	{
		cout << "Solved!" << endl;
		s.printgrid();//print solved array
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}

	return 0;
}
