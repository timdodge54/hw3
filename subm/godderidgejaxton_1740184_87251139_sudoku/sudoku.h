#ifndef SUDOKU_H
#define SUDOKU_H
#include <iostream>
#include <cstdlib>
#include <time.h>
#define N 9
using namespace std;

class sudoku {
private:
	int checkrow(int val, int y);
	int checkcol(int x, int val);
	int checkgrid(int x, int y, int val);
	int findfirstsquare(int* x, int* y);
	int nextsquare(int* x, int* y);
	int previoussquare(int* x, int* y);
	int num;
	int arr[N][N] = {0};
	int fixed[N][N] = {0};
public:
	sudoku(int n);
	int solve();
	void printgrid();

};

#endif
