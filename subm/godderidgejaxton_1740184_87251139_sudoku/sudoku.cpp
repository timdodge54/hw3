/**
@name: sudoku
@brief: creates a random sudoku grid to solve
@param:
@retval:
**/
#define N 9
#include "sudoku.h"


sudoku::sudoku(int n)
{
	srand(time(NULL));
	int row = 0, col = 0;
	int num, i, j;

	while (n > 0)
	{
		row = rand() % 9;//selects a random row
		col = rand() % 9;//secets a random col
		if (arr[row][col] == 0)
		{
			num = 1 + rand() % 9;//set num = a random num between 1-9
			
			if (checkrow(row, num)&&checkcol(col, num)&&checkgrid(col, row, num) == 1)
			{
				fixed[row][col] = 1;//set fixed array to 1
				arr[row][col] = num;//store number in array
				n--;
			}

		}
		
	}
	
	for (i = 0; i < 9; i++)
	{
		for (j = 0; j < 9; j++)
		{
			cout << arr[i][j];//print beginning array
			/*
			if (fixed[i][j] == 0)
			{
				cout << "* ";

				cout << arr[i][j];
				if (j == 2 || j == 5)
				{
					cout << "|";
				}
			}
			*/
		}
		cout << endl;
		/*
		if (i == 2 || i == 5)
		{
			cout << "- - - + - - - + - - -" << endl;
		}
		*/
	}
	
	cout << endl;
	
}
/*
@name: checkrow
@brief: checks each row for repeat numbers
@param:
@retval:
*/
int sudoku::checkrow(int y, int val)
{
	int col;
	for (col = 0; col < 9; col++)//checks row of numbers
	{
		if (arr[y][col] == val)
		{
			return 0;
		}
	}
	return 1;
}
/*
@name: checkcol
@brief: checks each col for repeat numbers
@param:
@retval:
*/
int sudoku::checkcol(int x, int val)
{
	int row;
	for (row = 0; row < 9; row++)//checks column of numbers
	{
		if (arr[row][x] == val)
		{
			return 0;
		}
	}

	return 1;
}
/*
@name: checkgrid
@brief: checks each 3x3 grid for errors
@param:
@retval:
*/
int sudoku::checkgrid(int x, int y, int val)
{
	int minrow, mincol, maxrow, maxcol, row, col;

	minrow = (x / 3) * 3;
	maxrow = minrow + 2;
	mincol = (y / 3) * 3;
	maxcol = mincol + 2;

	for (row = minrow; row <= maxrow; row++)//checks row in 3x3 grid
	{
		for (col = mincol; col <= maxcol; col++)//checks col in 3x3 grid
		{
			if (arr[row][col] == val)
			{
				return 0;
			}
		}
	}
	return 1;
}
/*
@name: findfirstsquare
@brief: locates the first square
@param: pointers to arr
@retval: true or false
*/
int sudoku::findfirstsquare(int* x, int* y)
{
	int i;
	i = *x;

	while (i < 9)
	{
		int j = *y;
		j = j + 1;
		
		if (j == 0)//if j points to a zero
		{
			i++;
			if (i == 9)//if j points to a nine
			{
				return 0;
			}
			j = 0;
		}
		else
		{
			j++;
		}
		
		if (fixed[i][j] == 0)//if fixed array doesnt have a number
		{
			*x = i;
			*y = j;
			return 1;//return true
		}

	}

	return 0;//return false
}
/*
@name: nextsquare
@brief: locates the next available square
@param: pointers to arr
@retval: true or false
*/

int sudoku::nextsquare(int* x, int* y)
{
	int i;
	i = *x;

	while (i < 9)
	{
		int j = *y;
		j = j + 1;

		if (j == 9)
		{
			i++;
			if (i == 9)//if i is at the end
			{
				return 0;
			}
			j = 0;
		}
		else
		{
			j++;
		}
		if (fixed[i][j] == 0)//if fixed array doesnt have a number
		{
			*x = i;
			*y = j;
			return 1;
		}


	}

	return 0;
}
/*
@name: previoussquare
@brief: locates the last available square
@param: pointers to arr
@retval: true or false
*/
int sudoku::previoussquare(int* x, int* y)
{
	int i;
	i = *x;

	while (i < 9)
	{
		int j = *y;
		j = j + 1;

		if (j == 0)
		{
			i++;
			if (i == 9)//if i is at the end
			{
				return 0;
			}
			j = 0;
		}
		else
		{
			j--;
		}
		if (fixed[i][j] == 0)//if fixed array doesnt have a number
		{
			*x = i;
			*y = j;
			return 1;
		}

	}

	return 0;
}
/*
@name: solve
@brief: loops through the array to attempt to solve
@param: none
@retval: true or false
*/

int sudoku::solve()
{
	int val;
	int row;
	int col;
	int forever = 1;
	int n = 1;
	row = 0;
	col = 0;
	if (findfirstsquare(&row, &col))
	{

		//loop entire function
		while (forever == 1)
		{

			if (row == N - 1 && col == N)
			{
				return 1;//return true
			}

			else
			{
				for (int num = 1; num <= 9; num++)
				{
					//if all check functions are returned true
					if (checkrow(col, num) && checkcol(row, num) && checkgrid(row, col, num) == 1)
					{
						arr[row][col] = num;//set number to array
						if (nextsquare(&row, &col))//check if next square is available
						{
							;
						}

						else if (previoussquare(&row, &col))//check if last square is available
						{
							;
						}
						fixed[row][col] = 1;
						return 1;
					}

				}

			}

		}


		return 0;

	}

	else
	{
		return 0;
	}


}
/*
@name: printgrid
@brief: prints solved grid
@param: none
@retval: none
*/
void sudoku::printgrid()
{
	int i, j;

	for (i = 0; i < 9; i++)
	{
		for (j = 0; j < 9; j++)
		{
			cout << arr[i][j];//prints solved array
		}

		cout << endl;
		
	}

	
}