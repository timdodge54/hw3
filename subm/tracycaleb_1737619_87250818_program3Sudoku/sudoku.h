#ifndef _SUDOKU_H_
#define _SUDOKU_H_
#include <iostream>
#include <cstdlib>
#include <string>

const int N = 9;

class Sudoku {
private:
	struct matrix {
		int value;
		int fixed;
	};
	matrix grid[9][9];
	
	int checkCol(int x, int y, int val);
	int checkRow(int x, int y, int val);
	int check3x3(int x, int y, int val);
public:
	Sudoku(int n);
	int solve(int row, int col);
	void printGrid(std::string state);
};

#endif