#include "sudoku.h"
#include <ctime>
using std::cin;
using std::cout;
using std::endl;
using std::string;



/**
@name		Sudoku
@brief		creates a sudoku puzzle with n numbers prepopulated

@param		numbers to prepopulate
@retval		none
*/

Sudoku::Sudoku(int n) {

	// initialize random number generator
	int x, y, val;
	srand(time(NULL));

	// set all array values to 0
	for (int row = 0; row < N; row++) {
		for (int col = 0; col < N; col++) {
			grid[row][col].value = 0;
			grid[row][col].fixed = 0;
		}
	}

	// populate array with random numbers in random locations
	for (int i = 0; i < n; i++) {
		x = rand() % 9;
		y = rand() % 9;
		val = (rand() % 9) + 1;

		// ensure sudoku rules are met
		if (checkCol(x, y, val) && checkRow(x, y, val) &&
			check3x3(x, y, val) && (grid[x][y].fixed == 0)) {
			grid[x][y].fixed = 1;
			grid[x][y].value = val;
		}

		// if rules aren't met and a coordinate isn't populated,
		// increment n to ensure proper prepopulation
		else { n++; }
	}
}


/**
@name		solve
@brief		solves a Sukoku puzzle

@param		starting row and column
@retval		1 or 0, solved or unsolved
*/

int Sudoku::solve(int row, int col)
{
	// reached the end of the matrix
	if (row == N - 1 && col == N)
		return 1;

	// move to the next column
	if (col == N) {
		row++;
		col = 0;
	}

	// skip filled cells
	if (grid[row][col].fixed == 1)
		return solve(row, col + 1);

	// try numbers 1 through 9
	for (int val = 1; val <= N; val++) {
		if (checkRow(row,col,val) && checkCol(row,col,val) 
			&& check3x3(row,col,val)){
			grid[row][col].value = val;

			if (solve(row, col + 1))
				return 1;

			// Backtrack
			grid[row][col].value = 0;
		}
	}

	return 0;
}

/**
@name		printGrid
@brief		prints either an unsolved or solved Sudoku puzzle

@param		string for choosing between solved or unsolved states
@retval		none
*/

void Sudoku::printGrid(string state) {

	if (state == "solved") {
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				cout << grid[row][col].value << " ";

				// prints a seperating bar after third and sixth terms
				if (col == 2 || col == 5) {
					cout << "| ";
				}
			}

			// newline every 9 terms
			cout << endl;

			// horizontal bar after third and sixth rows
			if (row == 2 || row == 5) {
				cout << "- - - + - - - + - - -" << endl;
			}
		}
	}
	else if (state == "unsolved") {
		for (int row = 0; row < N; row++) {
			for (int col = 0; col < N; col++) {

				// all unpopulated locations are replaced by an '*'
				if (grid[row][col].fixed == 0) {
					cout << "* ";
					if (col == 2 || col == 5) {
						cout << "| ";
					}
				}
				else {
					cout << grid[row][col].value << " ";
					if (col == 2 || col == 5) {
						cout << "| ";
					}
				}
			}
			cout << endl;
			if (row == 2 || row == 5) {
				cout << "- - - + - - - + - - -" << endl;
			}
		}
	}
}


/**
@name		checkCol
@brief		finds matching values in a given column

@param		x and y coordinates and that location's value
@retval		1 or 0, 1 meaning no matches
*/

int Sudoku::checkCol(int x, int y, int val) {

	// compare values in column to the argument's value
	for (int j = 0; j < 9; j++) {

		// if values are equivalent return false
		if (grid[x][j].value == val)
			return 0;
	}
	return 1;
}


/**
@name		checkRow
@brief		finds matches in a given row

@param		x and y coordinates and that location's value
@retval		1 or 0, 1 meaning no matches found
*/

int Sudoku::checkRow(int x, int y, int val) {

	// compare values in row to the argument's value
	for (int i = 0; i < 9; i++) {

		// if values are equivalent return false
		if (grid[i][y].value == val)
			return 0;
	}
	return 1;
}


/**
@name		check3x3
@brief		finds matching values in a 3x3 Sudoku grid

@param		x and y coordinates and that location's value
@retval		1 or 0, 1 meaning no matches found
*/

int Sudoku::check3x3(int x, int y, int val) {
	int maxcol, mincol, maxrow, minrow;

	// locate correct collumn segment
	mincol = (y / 3) * 3;
	maxcol = mincol + 2;

	// locate correct row segment
	minrow = (x / 3) * 3;
	maxrow = minrow + 2;

	// compare values in 3x3 square to the argument's value
	for (int row = minrow; row <= maxrow; row++) {
		for (int col = mincol; col <= maxcol; col++) {

			// if values are equivalent return false
			if (grid[row][col].value == val)
				return 0;
		}
	}
	return 1;
}

