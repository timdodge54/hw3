/**
@name		main
@brief		generates and solves a Sudoku puzzle given n 
			numbers to prepopulate

@param		none
@retval		none
*/

#include "sudoku.h"
using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
	
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	// exception handling for out of range ints
	try {
		if (n > 81) {
			throw 'e';
		}
		else if (n < 0) {
			throw 1;
		}
	}
	catch (char e) {
		cout << "Inputted number exceeds array size";
		exit(1);
	}
	catch (int e) {
		cout << "Inputted number is invalid" << endl;
		cout << "Enter a number between 0 and 81" << endl;
		exit(1);
	}

	// construct the sudoku puzzle
	Sudoku s(n);

	s.printGrid("unsolved");

	// if solved print completed puzzle
	if (s.solve(0,0)) {
		cout << "Solved!" << endl;
		s.printGrid("solved");
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}
	return 0;
}
