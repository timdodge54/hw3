/*****************************************************************************
* @name main
* @brief solves Sudoku
* @param none
* @retval none
*****************************************************************************/
#include "sudoku.h"
#include <iostream>

using namespace std;

int main()
{ //Begin
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	//welcomes user
	cout << "Enter number of squares to prepopulate: ";
	//prompts user to enter number of squares to prepopulate
	cin >> n;
	//scans in number

	Sudoku s(n);
	s.printGrid();
	//prints grid
	if (s.solve())
	//if solvable
	{
		cout << "Solved!" << endl;
		//print "Solved!" to screen
		s.printGrid();
		//print solved grid
	}
	else
	//else
	{
		cout << "Sorry, unsolvable..." << endl;
		//print "Sorry, unsolvable..." to screen
	}

	return 0;
	//End
}