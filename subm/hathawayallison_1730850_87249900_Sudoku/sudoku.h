#ifndef SUDOKU_H
#define SUDOKU_H

class Sudoku
{
public:
	Sudoku(int n);
	int solve();
	void printGrid(void);
	void printGrid2(void);
	
private:
	int board[9][9];
	int board2[9][9];
	int checkRow(int row, int num);
	int checkCol(int col, int num);
	int checkBox(int row, int col, int num);
	int findSafeSpot(int& row, int& col);
	bool isSafe(int row, int col, int num);
};

#endif

