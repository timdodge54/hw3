#include <iostream>
#include "sudoku.h"
#include <ctime>
#include <cmath>
#define space " "

using namespace std;

/*****************************************************************************
* @name Sudoku
* @brief sets up board
* @param none
* @retval none
*****************************************************************************/
Sudoku::Sudoku(int n)
{ //Begin
	int row, col;
	row = 0;
	col = 0;
	while (row != 9)
	//while row is not 9
	{
		col = 0;
		//col = 0
		while (col < 9) 
		//while col is less than 9
		{
			board[row][col] = 0;
			//board[row][col] = 0
			col++;
			//col++
		}
		row++;
		//row++
	} 
	srand(time(NULL));
	while (n > 0)
	//while n is greater than 0
	{
		int randRow = rand() % 9;
		//randRow is a random number % 9
		int randCol = rand() % 9;
		//randCol is a random number % 9
		int randNum = rand() % 9 + 1;
		//randNum is a random number % 9 + 1
		if (checkRow(randRow, randNum) && checkCol(randCol, randNum)
			&& checkBox(randRow, randCol, randNum) == 1
			&& board[randRow][randCol] == 0)
		//if checkRow, checkCol, checkBox are true AND board[randRow][randCol]
		//is 0
		{
			board[randRow][randCol] = randNum;
			//put randNum in board[randRow][randCol]
			n = n - 1;
			//n = n - 1
		}
	}
} //End


/*****************************************************************************
* @name printGrid
* @brief prints Sudoku board
* @param none
* @retval none
*****************************************************************************/
void Sudoku::printGrid(void)
{ //Begin
	int row, col;

	for (row = 0; row < 9; row++)
	//for (row = 0; row < 9; row++)
	{
		if (row % 3 == 0 && row != 0)
		// if (row % 3 is 0 and row does not equal 0)
		{
			cout << " ------+-------+-------" << endl;
			//print grid lines
		}
		for (col = 0; col < 9; col++)
		//for (col = 0; col < 9; col++)
		{
			if (col % 3 == 0 && col != 0)
			//if (col % 3 is 0 and col does not equal 0)
			{
				cout << space << "|";
				//print grid lines
			}
			if (board[row][col] == 0)
			//if (board[row][col] is 0
			{
				cout << space << '*';
				//print stars in spaces
			}
			else
			//else
			{
				cout << space << board[row][col];
				//print values
			}
		}
		cout << endl;
	}
} //End


/*****************************************************************************
* @name checkRow
* @brief checks to see if number can go in a row
* @param none
* @retval none
*****************************************************************************/
int Sudoku::checkRow(int row, int num)
{ //Begin
	int i = 0;
	while (i != 9)
	//while i is not 9
	{
		if (board[row][i] == num)
		//if board[row][i] equals num
		{
			return 0;
			//return 0
		}
		i++;
		//i++
	}
	return 1;
	//End
}


/*****************************************************************************
* @name checkCol
* @brief checks to see if number can go in a column
* @param none
* @retval none
*****************************************************************************/
int Sudoku::checkCol(int col, int num)
{ //Begin
	int i = 0;
	while (i != 9)
	//while i is not 9
	{
		if (board[i][col] == num)
		//if board[i][col] equals num
		{
			return 0;
			//return 0
		}
		i++;
		//i++
	}
	return 1;
	//End
}


/*****************************************************************************
* @name checkBox
* @brief checks to see if number can go in a box
* @param none
* @retval none
*****************************************************************************/
int Sudoku::checkBox(int row, int col, int num)
{ //Begin
	int i, j;
	int minCol = (col / 3) * 3;
	//minCol is (col / 3) * 3
	int maxCol = (minCol + 2);
	//maxCol is (minCol + 2)
	int minRow = (row / 3) * 3;
	//minRow is (row / 3) * 3
	int maxRow = (minRow + 2);
	//maxRow is (minRow + 2)
	for (i = minRow; i <= maxRow; i++)
	//for (i = minRow; i <= maxRow; i++)
	{
		for (j = minCol; j <= maxCol; j++)
		//for (j = minCol; j <=maxCol; j++)
		{
			if (board[i][j] == num)
			//if board[i][j] is equal to num
			{
				return 0;
				//return 0
			}
		}
	}
	return 1;
	//End
}

/*****************************************************************************
* @name findSafeSpot
* @brief checks to see if a value can be placed
* @param none
* @retval none
*****************************************************************************/
int Sudoku::findSafeSpot(int& row, int& col)
{ //Begin
	for (row = 0; row < 9; row++)
	//for (row = 0; row < 9; row++)
	{
		for (col = 0; col < 9; col++)
		//for (col = 0; col < 9; col++)
		{
			if (board[row][col] == 0)
			//if board[row][col] is equal to 0
			{
				return 1;
				//return 1
			}
		}
	}
	return 0;
	//End
}

/*****************************************************************************
* @name solve
* @brief solves Sudoku board
* @param none
* @retval none
*****************************************************************************/
int Sudoku::solve()
{ //Begin
	int row, col;
	if (findSafeSpot(row, col) == 0)
	//if spot is safe
	{
		return 1;
		//return 1
	}
	for (int number = 1; number <= 9; number++)
	//for (number = 1; number <= 9; number++)
	{
		if (checkRow(row, number) && checkCol(col, number)
			&& checkBox(row, col, number) == 1 && board[row][col] == 0)
		//if checkRow, checkCol, checkBox are true AND board[randRow][randCol]
		//is 0
		{
			board[row][col] = number;
			//put number in grid
			if (solve())
			// if solve
			{
				return 1;
				//return 1
			}
			board[row][col] = 0;
			//put 0 in grid
		}
	}
	return 0;
	//End
}



