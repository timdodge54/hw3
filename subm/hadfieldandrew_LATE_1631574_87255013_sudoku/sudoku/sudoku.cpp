#include "sudoku.h"
#include <iostream>
using namespace std;


Sudoku::Sudoku(int n)
{
    int val;
    srand (time(NULL));
    for (int i = 0; i < n; i++)
    {
        i = rand() % 9 + 1;
        char val = i;
        xpos = rand() % 9;
        ypos = rand() % 9;
        grid[xpos][ypos] = val;
    }
}

/******************************************************************************
* @name gridFill
* @brief fills the grid with * characters
* @param none
* @return none
******************************************************************************/
void Sudoku::gridFill()
{
    //fills the grid with * character for anyone that isn't already filled
    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 9; j++)
        {
            grid[i][j] = '*';
        }
    }
}

/******************************************************************************
* @name solve
* @brief solves the sudoku puzzle
* @param none
* @return 1 or 0
******************************************************************************/
int Sudoku::solve()
{

    return 0;
}

/******************************************************************************
* @name rowcheck
* @brief checks to see if the number is already in the row
* @param none
* @return 1 or 0
******************************************************************************/
int Sudoku::rowcheck()
{

    return 0;
}

/******************************************************************************
* @name columncheck
* @brief checks to see if the number is already in the column
* @param none
* @return 1 or 0
******************************************************************************/
int Sudoku::columncheck()
{

    return 0;
}

/******************************************************************************
* @name squarecheck
* @brief checks to see if the number is already in the square
* @param none
* @return 1 or 0
******************************************************************************/
int squarecheck()
{

    return 0;
}

/******************************************************************************
* @name printGrid
* @brief prints the grid
* @param none
* @return none
******************************************************************************/
void Sudoku::printGrid()
{
    int r, c;
    cout << endl;
    //prints the array
    for (c = 0; c < 9; c++)
    {
        for (r = 0; r < 9; r++)
        {
            //puts the | lines in the desired spot
            if (r == 2 || r == 5)
            {
                cout << ' ' << grid[c][r] << ' ' << '|';
            }
            //creates a row of - and + characters
            else if (c == 2 && r == 8 || c == 5 && r == 8)
            {
                cout << ' ' << grid[c][r];
                cout << endl;
                for (int i = 0; i < 7; i++)
                {
                    cout << '-';
                }
                cout << '+';
                for (int i = 0; i < 7; i++)
                {
                    cout << '-';
                }
                cout << '+';
                for (int i = 0; i < 7; i++)
                {
                    cout << '-';
                }
            }
            //prints the current value
            else
            {
                cout << ' ' << grid[c][r];
            }
        }
        cout << endl;
    }
    cout << endl;
    

}