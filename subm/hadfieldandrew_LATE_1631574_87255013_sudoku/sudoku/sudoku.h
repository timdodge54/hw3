#include <cstdlib>

class Sudoku 
{
    char grid[9][9];
   
    private:
        int xpos, ypos;

    public:
        Sudoku(int n);
        int solve();
        int rowcheck();
        int columncheck();
        int squarecheck();
        void printGrid();
        void gridFill();
};