#include "sudoku.h"
#include <iostream>
using namespace std;

/******************************************************************************
* @name main
* @brief asks the user for a number of squares to fill in a sudoku puzzle,
*        and then tries to solve it
* @param none
* @return none
******************************************************************************/
int main()
{
    int n;

    cout << "Welcome to Sudoku Solver!!" << endl;
    cout << "Enter number of squares to populate: " << endl;
    cin >> n;


    Sudoku s(n);
    s.gridFill();
    s.printGrid();

    /*if (s.solve()) 
    {
        cout << "Solved!" << endl;
        s.printGrid();
    }
    else
    {
        cout << "Sorry, unsolvable..." << endl;
    }*/

    return 0;
}