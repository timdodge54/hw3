#pragma once

/*************************************************************************
* Class: Sudoku
*
*
*
*************************************************************************/

class Sudoku {
private:
	int board[9][9];
	//char board[9][9];
	bool og[9][9];

	int rowCheck(int y, int x, int value);
	int columnCheck(int y, int x, int value);
	int boxCheck(int y, int x, int value);
	int totalCheck(int y, int x, int value);

	int getFirstSquareInPlay(int* y, int* x);
	int getNextSqaureInPlay(int* y, int* x);
	int getPrevSqaureInPlay(int* y, int* x);



public:
	Sudoku(int n);
	int solve();
	void printGrid();





};