
#include "sudoku.h"
#include <iostream>
#include <cstdlib>

//#define RAND_MAX = 8;

using namespace std;

/*************************************************************************
* @name Sudoku
* @brief This is the constructor for the Sudoku class. It initializes a 
* nine by nine gride to all zeroes then places a number of random cells
* given by the user.
* @param n, the number of cells to prepopulate.
* @retval none
*************************************************************************/
Sudoku::Sudoku(int n)
{
	srand(time(NULL));
	int i = 0, j = 0, x, y, value, placeable;
	//initialize sudoku board to all zeroes
	for (i = 0; i < 9; i++) {
		for (j = 0; j < 9; j++) {
			board[i][j] = 0;
		}
	}
	//initialize second grid to all false, to keep track of the prepopulated
	for (i = 0; i < 9; i++) {
		for (j = 0; j < 9; j++) {
			og[i][j] = false;
		}
	}
	//i think this just looks nice
	cout << endl;
	//loop for n times
	for (i = 0; i < n;) {
		//generate 3 numbers, row, column, and value of a new square
		x = rand() % 9;
		y = rand() % 9;
		value = rand() % 9 + 1;
		//check if the new square is populated
		placeable = totalCheck(y, x, value);
		//if it is placeable and we haven't placed a square there yet
		if (placeable && og[y][x] == false) {
			//place the square
			board[y][x] = value;
			//mark it as a prepopulated square
			og[y][x] = true;
			//increment i to make sure we generate exactly n squares
			i++;
		}
	}
	//print the grid
	printGrid();
}


/*************************************************************************
* @name solve
* @brief solves the sudoku or dies trying
* @param none
* @retval 1 if successful, 0 if it died trying
*************************************************************************/
int Sudoku::solve()
{
	int n, x, y;
	int win;
	int i = 0;
	int check;
	//start at 0,0 with value 1
	x = 0;
	y = 0;
	n = 1;
	//get the first square in play
	getFirstSquareInPlay(&y, &x);
	//loop until the heat death of the universe. or we solve it then stop
	while (1)
	{
		//set the square to value n
		board[y][x] = n;
		//check if it is good
		check = totalCheck(y, x, n);
		//if it works
		if (check)
		{
			//set n to one
			n = 1;
			//get next square
			win = getNextSqaureInPlay(&y, &x);
			//if we are off the board,
			if (!win)
			{
				//we solved it so return 1
				return 1;
			}
		}
		//else
		else {
			//increment n
			n = n + 1;
			//if n is 10
			if (n == 10)
			{
				//backtrack by resetting the square
				board[y][x] = 0;
				//go back one square
				win = getPrevSqaureInPlay(&y, &x);
				//if we went off the board
				if (!win) {
					//it is unsolveable so return 0
					return 0;
				}
				//else
				else {
					//n = the previous square plus 1
					n = board[y][x] + 1;
				}
			}
		}
	}
	return 1;
}


//shoule be working. returns 1 for successful finding. returns 0 for failing
/*************************************************************************
* @name getFirstSquareInPlay
* @brief uses pointers to modify x and y to be the coordinates of the 
* first square that isn't prepopulated
* @param the addresses of x and y
* @retval 1 if successful in finding a square, 0 if fails to find a square
*************************************************************************/
int Sudoku::getFirstSquareInPlay(int* y, int* x)
{
	//loop until we find it or fail to find it
	while (1) {
		//if our square isn't prepopulated
		if (og[*y][*x] == false) {
			//return 1 because we did find it
			return 1;
		}
		//else
		else {
			//increment x
			(*x)++;
			//if x goes of the row, go to first square of the next row
			if (*x > 8) {
				*x -= 9;
				(*y)++;
			}
			//if we go off the whole grid
			if (*y > 8) {
				//return 0 because we failed to find the first square
				return 0;
			}
		}
	}
}


/*************************************************************************
* @name getNextSquareInPlay
* @brief gets the next square in play by changing the x and y values
* @param the addresses of x and y
* @retval 1 if successful in finding a square, 0 if fails to find a square
*************************************************************************/
int Sudoku::getNextSqaureInPlay(int* y, int* x)
{
	int done = 0;
	//if we are off the board, return 0 
	if (*y > 8) {
		return 0;
	}
	do {
		//increment x
		*x += 1;
		//if we go off the row, go to first square of the next row
		if (*x > 8) {
			*x -= 9;
			*y += 1;
			//if we are off the board, return 0
			if (*y > 8)
				return 0;
		}
		//if we made it to a non-prepopulated square
		if (og[*y][*x] == false)
		{
			//return 1
			done = 1;
			return 1;
		}
	} while (!done);
	return 1;
}


/*************************************************************************
* @name getPrevSquareInPlay
* @brief gets the previous square in play by changing the x and y values
* @param the addresses of x and y
* @retval 1 if successful in finding a square, 0 if fails to find a square
*************************************************************************/
int Sudoku::getPrevSqaureInPlay(int* y, int* x)
{
	int done = 0;
	//if we are off the board
	if (*y < 0)
	{
		//return 0
		return 0;
	}
	//loop
	do {
		//decrement x
		*x -= 1;
		//if we go off the row, go to last square of the previous row
		if (*x < 0) {
			*x += 9;
			(*y) -= 1;
			//if we are off the board
			if (*y < 0)
			{
				//return 0
				return 0;
			}
		}
		//if the current square isn't prepopulated
		if (og[*y][*x] == false)
		{
			//if square is a nine, reset it 
			if (board[*y][*x] == 9)
			{
				board[*y][*x] = 0;
			}
			//else return 1
			else {
				done = 1;
				return 1;
			}
		}
	} while (!done);
	return 1;
}



/*************************************************************************
* @name rowCheck
* @brief checks all squares in the row to see if a value is placeable
* in that row
* @param coordinates x and y, and the value to check for
* @retval 1 if placeable, 0 if not placeable
*************************************************************************/
int Sudoku::rowCheck(int y, int x, int value)
{
	int i = 0, placeable = 1;
	//assume it is placeable
	//loop 9 times, through each row place
	for (i = 0; i < 9; i++)
	{
		//if the cell is equal to the value and isn't itself,
		if (board[i][x] == value && (i != y))
		{
			//there is another of that value, so placeable is false
			placeable = 0;
		}
	}
	//return wether it is placeable
	return placeable;
}


/*************************************************************************
* @name columnCheck
* @brief check all squares in the column to see if the value is placeable
* @param coordinates x and y, and the value to check for
* @retval 1 if placeable, 0 if not placeable
*************************************************************************/
int Sudoku::columnCheck(int y, int x, int value)
{
	int i = 0, placeable = 1;
	//assume it is placeable
	//loop 9 times through each column place
	for (i = 0; i < 9; i++)
	{
		//if the square equals the value, and isn't itself
		if (board[y][i] == value && i != x)
		{
			//there is a square in the column so it isn't placeable
			placeable = 0;
		}
	}
	//return wether it is placeable
	return placeable;
}


/*************************************************************************
* @name boxCheck
* @brief checks all squares in the box to see if the value is placeable
* @param coordinates x,y and the value to check for
* @retval 1 if placeable, 0 if not placeable
*************************************************************************/
int Sudoku::boxCheck(int y, int x, int value)
{
	int i = 0, j = 0, placeable = 1;
	int row_start, col_start;
	//find the start coordinates of the box
	row_start = (x / 3) * 3;
	col_start = (y / 3) * 3;
	//loop through all 9 positions of the box
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			//if the square to check equals the value and isn't itself
			if (board[col_start + i][row_start + j] == value && ((col_start + i) != (y) && (row_start + j) != (x)))
			{
				//it isn't placeable
				placeable = 0;
			}
		}
	}
	//return whether it is placeable
	return placeable;
}


/*************************************************************************
* @name totalCheck
* @brief checks if the value is placeable in the whole grid in one 
* convenient function
* @param coordinates x,y and the value to check for
* @retval 1 if it is placeable, 0 if not
*************************************************************************/
int Sudoku::totalCheck(int y, int x, int value)
{
	int placeable;
	int column, row, box;
	//check column
	column = columnCheck(y, x, value);
	//check row
	row = rowCheck(y, x, value);
	//check box
	box = boxCheck(y, x, value);
	//placeable is the true if all are good
	placeable = (column & row & box);
	//return whether it is placeable
	return placeable;
}


/*************************************************************************
* @name printGrid
* @brief prints all the squares in the grid and lines to denote the boxes
* @param none
* @retval none
*************************************************************************/
void Sudoku::printGrid()
{
	int i = 0, j = 0;
	//do a ton of loops and stuff to print it all correctly
	
	for (j = 0; j < 3; j++)
	{
		for (i = 0; i < 3; i++)
		{
			cout << " ";
			if (board[j * 3 + i][0] == 0)
			{
				cout << ".";
			}
			else {
				//print the square
				cout << board[j * 3 + i][0];
			}
			cout << " ";

			cout << " ";
			if (board[j * 3 + i][1] == 0)
			{
				cout << ".";
			}
			else {
				//print the square
				cout << board[j * 3 + i][1];
			}
			cout << " ";

			cout << " ";
			if (board[j * 3 + i][2] == 0)
			{
				cout << ".";
			}
			else {
				//print the square
				cout << board[j * 3 + i][2];
			}
			cout << " ";
			cout << "|";
			cout << " ";
			if (board[j * 3 + i][3] == 0)
			{
				cout << ".";
			}
			else {
				//print the square
				cout << board[j * 3 + i][3];
			}
			cout << " ";
			cout << " ";
			if (board[j * 3 + i][4] == 0)
			{
				cout << ".";
			}
			else {
				//print the square
				cout << board[j * 3 + i][4];
			}
			cout << " ";
			cout << " ";
			if (board[j * 3 + i][5] == 0)
			{
				cout << ".";
			}
			else {
				//print the square
				cout << board[j * 3 + i][5];
			}
			cout << " ";
			cout << "|";
			cout << " ";
			if (board[j * 3 + i][6] == 0)
			{
				cout << ".";
			}
			else {
				//print the square
				cout << board[j * 3 + i][6];
			}
			cout << " ";
			cout << " ";
			if (board[j * 3 + i][7] == 0)
			{
				cout << ".";
			}
			else {
				//print the square
				cout << board[j * 3 + i][7];
			}
			cout << " ";
			cout << " ";
			if (board[j * 3 + i][8] == 0)
			{
				cout << ".";
			}
			else {
				//print the square
				cout << board[j * 3 + i][8];
			}
			cout << " ";
			cout << endl;
		}
		if (j == 0 || j == 1)
		{
			//print a row between the boxes
			cout << "---------+---------+---------" << endl;
		}
	}
}


/*************************************************************************
* @name
* @brief
* @param
* @retval
*************************************************************************/
