
#include "sudoku.h"
#include <iostream>

using namespace std;


/*************************************************************************
* @name main
* @brief creates a sudoku with a number of prepopulated squares given by
* the user. 
* @param none
* @retval none
*************************************************************************/

int main(void)
{
	int n;
	//welcome the user and get number of squares to populate
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;
	//make the sudoku
	Sudoku s(n);
	//try to solve the sudoku
	if (s.solve()) {
		//if it solves, say so and print the grid
		cout << "Solved!" << endl;
		s.printGrid();
	}
	else {
		//if it doesn't solve, print message saying it is unsolvable
		cout << "Sorry, unsolvable..." << endl;
	}
	return 0;
}


/*************************************************************************
* @name 
* @brief 
* @param 
* @retval 
*************************************************************************/
