#ifndef sudoku_h
#define sudoku_h
#include <iostream>
#include <cmath>
#include <ctime>

using namespace std;

class Sudoku{
	private:
		int colchk(int col,int v);
		int rowchk(int row,int v);
		int boxchk(int col,int row,int v);
		int board[9][9];
		int fixed[9][9];
		int nextsquare(int col, int row);
		int prevsquare(int col, int row);
		int*first, *last;
		void firstsquare();
		int row, col;
		void lastsquare();
	public:
		Sudoku(int n);
		int solve();
        void printGrid();
        
		

};
#endif