#include "sudoku.h" 

/****************************************************************************
* @name     Sudoku
* @brief    initializes board to 0 popultes with random numbers
* @param	none
* @retval   none
*****************************************************************************/
Sudoku::Sudoku(int n)
{
//declare necessary varibles
int x,y,k,j,count,a;
srand(time(NULL));
for (k=0;k<9;k++)
{//initiailize board to 0
	for(j=0;j<9;j++)
	{
		board[k][j]=0;
		fixed[k][j]=0;
	}
}
count=0;

while(count<n)
	{//put random numbers in random spots on the board
		x=rand() %9;
		y=rand() %9;
		if(board[x][y]==0)
		{
			a=rand() %9 + 1;
			if(colchk(x,a) && boxchk(x,y,a) && rowchk(y,a))
			{
				board[x][y] = a;
				fixed[x][y]= 1;
				count++;
				
			}
		}

	}
 printGrid();
}



/****************************************************************************
* @name     solve
* @brief    solves sudoku puzzles
* @param	none
* @retval   none
*****************************************************************************/
int Sudoku::solve()
{
	int n, x=0,y=0;
	n = 1;
cout<<"infinite loop"<<endl;
	while(1)
	{
		if ((boxchk(x, y, n)) && colchk(x, n) && rowchk(y, n)){ 
			board[x][y] = n; 
			if(nextsquare(x , y) == 0){
				return 1;	
			}
			
		
		
		}
		else if(prevsquare(x , y) == 0){
		return 0;
		}
			
		
	}
}


/****************************************************************************
* @name     printGrid
* @brief    prints sudoku board
* @param	none
* @retval   none
*****************************************************************************/
void Sudoku::printGrid()
{
	
int x, y;
 
for (y = 0; y < 9; y++) 
{
	for(x = 0; x < 9; x++)
	{
		if (board[x][y] == 0) 
		{
			cout << "*\t";
		}
		else
		{
		cout << board[x][y] << "\t"; 
		}		
			if (x == 8)
			{
				cout << "\n\n\n"; 
			}
}
}
}
/****************************************************************************
* @name     colchk
* @brief    checks column
* @param	none
* @retval   none
*****************************************************************************/
int Sudoku::colchk(int col,int v)
{
	int i=0;
	while(i<9)
	{
			if(board[i][col] == v)
			{
			return 0;
			
			}
		i++;
	}
	return 1;
}


/****************************************************************************
* @name     rowchk
* @brief    checks row
* @param	none
* @retval   none
*****************************************************************************/
int Sudoku::rowchk(int row, int v)
{
	int i=0;
	while(i<9)
	{
			if(board[i][row] == v)
			{
			return 0;
			
			}
		i++;
	}
	return 1;
}
	


/****************************************************************************
* @name     boxchk
* @brief    checks box
* @param	none
* @retval   none
*****************************************************************************/
int Sudoku::boxchk(int col, int row, int v)
{
	int i,j;
	int colmin, rowmin, rowmax, colmax;
	colmin = (col/3)*3;
	colmax = colmin+2;
	rowmin = (row/3)*3;
	rowmax = rowmin + 2;
	for( j = rowmin; j<=rowmax; j++)
	{
		
	
	for(i=colmin;i<=colmax;i++){
		if(board[j][i] ==  v){
			return 0;
		
		}
	}
	
}
return 1;
}
/****************************************************************************
* @name     next square
* @brief    checks the next square
* @param	none
* @retval   none
*****************************************************************************/
int Sudoku::nextsquare(int col, int row){
	

	if (col == 8 && row < 8) 
		{
		col = 0;
		row++;
		}
	else if (col < 8)
		{
		col++;
		}
	for (; row < 9; row++) 
	{cout<<"next square loop"<<endl;
		for (; col < 9;)
			
	{
		cout<<"next square 2"<<endl;
	if (fixed[col][row] == 0)
	{

		return 1; 
	}
	if (col == 8 && row < 8)
	{
	col = 0; 
	row++;
	}
	else if (col < 8)
	{
	col++; 
}
}
}
return 0;
}


/****************************************************************************
* @name     prevsquare
* @brief    checks previous square
* @param	none
* @retval   none
*****************************************************************************/
int Sudoku::prevsquare(int row, int col)
{

if (col == 0 && row > 0) 
	{
	col = 8; 
	row--;
	}
else if (col > 0)
	{
	col--;
	}

	for(;row > 0; row--)
	{
		cout<<"prev square"<<endl;
		for(;col >= 0;)
	{
cout<<"prev square 2"<<endl;
		if (fixed[col][row] == 0 && &fixed[col][row] != first)
		{

			return 1; 
		}
		if (col == 0) 
		{
			col = 8;
			break;
		}
		else
		{
			col--;
			}
			if (*first == 9 || &fixed[col][row] == first) 
		return 0; 
	}
	}

return 0;
}
/****************************************************************************
* @name     first square
* @brief    checks first square
* @param	none
* @retval   none
*****************************************************************************/
void Sudoku::firstsquare()
{
for (first = &fixed[0][0]; *first != 0; first++); 

for(row = 0; row < 9; row++) 
{
for (col = 0; col < 9; col++)
{
if (fixed[col][row] == 0)
{

return;
}

}
}
}