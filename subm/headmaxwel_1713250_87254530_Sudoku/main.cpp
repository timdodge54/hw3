#include "sudoku.h"
#include <iostream>

using namespace std;

/********************************************************************
 * @name main
 * @param n
 * @brief Program generates an array in a 9x9 square. User will enter
 *        the number of pre-populated spaces they want filled into
 *        the sudoku puzzle before the program attempts to solve it.
 *
 * @retval int
 *******************************************************************/
int main()
{
    int n;
    //asks for pre-populated spaces(n)
    cout << "Welcome to SudokuSolver!!" << endl;
    cout << "Enter number of squares to pre-populate: ";
    cin >> n;
    //constructs a object of class Sudoku
    Sudoku s(n);
    //prints solved puzzle if true. If false, prints unsolvable
    if (s.solve()) {
        cout << "Solved!" << endl;
        s.printGrid();
    }
    else {
        cout << "Sorry, unsolvable..." << endl;
    }

    return 0;
}
