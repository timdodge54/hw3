#ifndef SUDOKU_SUDOKU_H
#define SUDOKU_SUDOKU_H

class Sudoku{
public:
    Sudoku(int n);
    int solve();
    void printGrid();
    int a[9][9];
    int b[9][9];
private:

   bool checkAll(int x, int y, int val);
   bool checkRow(int val,int x);
   bool checkCol(int val, int y);
   bool checkGrid(int x,int y, int val);
   int findNext(int *x, int *y);
   int findPrevious(int *x, int *y);
};

#endif