#include <iostream>
#include<cstdlib>
#include "sudoku.h"

/********************************************************************
 * @name Sudoku
 * @brief function is the constructor of the class Sudoku. Constructs
 *        and prints the starting grid with the proper amount of
 *        prepopulated values given by the user
 * @param n
 * @retval void
 *******************************************************************/

// Empty = 0, Visited = 2, Original = 1

Sudoku::Sudoku(int n) {
    //initializes arrays to 0
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            b[i][j] = 0;
            a[i][j] = 0;
        }
    }
    int row, col, value, i;
    //RNG seed
    srand(time(nullptr));
    //counter variable
    i = 0;
    while(i<n){
        //grabs three random numbers
        row = rand() % 9;
        col = rand() % 9;
        value = rand() % 9 + 1;
        //attempts to place random number(val) in array at random location
        //checks if that spot has been used by looking at array b
        if(b[row][col]!=1){
            a[row][col] = value;
            //checks if move is legal
            if (checkAll(row, col, value)){
                //places value inside of b to mark that position as illegal
                //for future moves
                b[row][col] = 1;
                i++;
            } else {
                //resets val
                a[row][col] = 0;
            }
        }
    }
    //prints grid to the screen
    printGrid();

}

/********************************************************************
 * @name solve
 * @brief function uses a backtracking algorithm to find all of the of
 *        the legal answers available for puzzle. If function can't
 *        solve the puzzle, it returns false.
 * @retval true or false
 *******************************************************************/
int Sudoku::solve(){
    int x = 0;
    int y = 0;
    int found = findNext(&x,&y);
    //if the program can't find the next spot, returns true
    if (!found) {
        return 1;
    }

    int currentNumber = 1 ;
    //infinite loop so long as there are spaces left to change
    while (true) {

        a[x][y] = currentNumber;
        b[x][y] = 2;
        //checks if value in current space is legal
        if(checkAll(x, y, currentNumber)) {
            currentNumber = 1;
            //if it can't find next spot, return true
            if (!findNext(&x, &y)) {
                return 1;
            }
            //else, increase value and check for numbers higher than 10
            //reset numbers and arrays to 0
        } else {
            currentNumber += 1;
            if(a[x][y] > 10){
                b[x][y]=0;
                a[x][y]=0;
                findPrevious(&x,&y);
                //if program can't find a valid previous spot, return false
                if(!findPrevious(&x,&y)){
                    return 0;
                } else {
                    currentNumber = a[x][y] + 1;
                }

            }
        }
    }
}

/********************************************************************
 * @name printGrid
 * @brief function prints array in it's current state.
 * @param none
 * @retval void
 *******************************************************************/
void Sudoku::printGrid() {
    //goes through each cell and checks for a zero
    //if a zero is found, prints "*"
    //else, it prints out the value in a
    for(int i = 0; i <= 8; i++){
        for(int j = 0; j <= 8; j++){
            if(b[i][j]==0){
                std::cout << " * ";
            } else {
                std::cout << " "<< a[i][j] << " ";
            }
        }
        //makes array print nicely
        std::cout<<std::endl;
    }
}

/********************************************************************
 * @name checkAll
 * @brief function checks for all potential illegal values in the 3x3
 *        cell, the row, and the column.
 * @param x
 * @param y
 * @param val
 * @retval true/false
 *******************************************************************/
bool Sudoku::checkAll(int x, int y, int val){
    //makes checking for legal moves easier by calling all three functions
    //at once
    return checkRow(val, x) && checkGrid(x, y, val) && checkCol(val, y);
}

/********************************************************************
 * @name checkRow
 * @brief function checks for all potential illegal moves in the row.
 * @param x
 * @param val
 * @retval true/false
 *******************************************************************/
bool Sudoku::checkRow(int val,int x){
    int counter = 0;
    //allows us to see if there is more than one instance of the same value
    // in the same row. if there is, return false
    for(int j = 0; j <= 8; j++){
        if(a[x][j] == val){
            counter++;
        }
    }
    return counter < 2;
}

/********************************************************************
 * @name checkRow
 * @brief function checks for all potential illegal moves in the
 *        column.
 * @param y
 * @param val
 * @retval true/false
 *******************************************************************/
bool Sudoku::checkCol(int val, int y){
    int counter = 0;
    //allows us to see if there is more than one instance of the same value
    // in the same column. if there is, return false
    for(int i = 0; i <= 8; i++){
        if(a[i][y] == val){
            counter++;
        }
    }
    return counter < 2;
}

/********************************************************************
 * @name checkRow
 * @brief function checks for all potential illegal moves in the 3x3
 *        cell.
 * @param x
 * @param y
 * @param val
 * @retval true/false
 *******************************************************************/
bool Sudoku::checkGrid(int x, int y, int val) {
    //gives us the correct starting position for our 3x3 grids
    int counter = 0;
    int minCol = (y/3) * 3;
    int maxCol = minCol + 2;
    int minRow = (x/3) * 3;
    int maxRow = minRow + 2;

    //allows us to see if there is more than one instance of the same value
    //in the same 3x3 grid. if there is, return false
    for(int i = minCol; i <= maxCol; i++){
        for(int j = minRow; j <= maxRow; j++){
            if(a[i][j]==val){
                counter++;
            }
        }
    }
    return counter < 2;
}

/********************************************************************
 * @name findNext
 * @brief function checks for the next available space in the puzzle
 * @param *x
 * @param *y
 * @retval 1 or 0
 *******************************************************************/
int Sudoku::findNext(int *x, int *y) {
    //checks each space in array b to find a valid space to start
    //iterating on
    while(*x<=8) {
        while(*y<=8) {
            if (b[*x][*y] == 0) {
                return 1;
            }
            //increases the row position
            *y+=1;
        }
        //increases the column position
        *x+=1;
        *y=0;
    }
    return 0;
}

/********************************************************************
 * @name findPrevious
 * @brief function checks for the previous legal space in the puzzle
 * @param *x
 * @param *y
 * @retval 1 or 0
 *******************************************************************/
int Sudoku::findPrevious(int *x, int *y) {
    while(*x>=0) {
        while(*y>=0) {
            //increases the row position
            *y-=1;
            if (b[*x][*y] == 2) {
                return 1;
            }

        }
        //increases to column position
        *x-=1;
        *y=8;
    }
    return 0;
}