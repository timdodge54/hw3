#include "sudoku.h" 
#include <iostream> 
 
using namespace std; 
 /********************************************************************* 
* @name main 
* @brief creates a sudoku board with n squares filled and solves it
* @param none 
* @retval  none 
*********************************************************************/ 
int main() 
{ 
	//variable for amount of squares to be filled
	int n; 
	//ask for the amount of squares to prepopulate and welcome user
	cout << "Welcome to SudokuSolver!!" << endl; 
	cout << "Enter number of squares to prepopulate: "; 
	//get number
	cin >> n; 
	//create sudoku board and print it to screen
	Sudoku s(n); 
	//check to see if the board was solved sucessfully
	if (s.solve()) { 
		//if so, print message
		cout << "Solved!" << endl;
		//print grid
		s.printGrid(); 
	} 
	//if grid was not solved
	else { 
		//print message stating that board was unsolvable
		cout << "Sorry, unsolvable..." << endl; 
	} 
	//return 0 for normal termination
  	return 0; 
} 