#include "sudoku.h"
#include <iostream>
#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;


/********************************************************************* 
* @name Sudoku
* @brief class constructor-builds object of class sudoku
* @param int n of how many squares to populate
* @retval  none 
*********************************************************************/ 
Sudoku::Sudoku(int n) {
	int i=0;
	int num = n;
	//seed value for rand
	srand(time(NULL));
	//loop untill correct number of squares are allocated
	while (i < num) {
		//temp variables created and set to random values
		int tempr, tempc, temp;
		temp = rand() % 9 + 1;
		tempr = rand() % 9;
		tempc = rand() % 9;
		//if spot is empty and it is a valid position
		if ((grid[tempr][tempc] == 0)) {
			if ((Sudoku::isvalid(tempr, tempc, temp))==1) {
				//set position equal to temp value
				grid[tempr][tempc] = temp;
				//increment counter
				i++;
			}
		}
		
	}
	//print starting grid
	Sudoku::printGrid();
	//print extra whitespace for better readability
	cout << endl << endl;
}
/********************************************************************* 
* @name isvalid
* @brief checks if the space is a valid location for number
* @param int row, column, and n
* @retval  1 if valid location, 0 if invalid
*********************************************************************/ 
int Sudoku::isvalid(int row, int col, int n) {
	//create a temp value
	int t;
	//check to see if n is valid by calling respective functions
	t=!(inrow(row, n) || incol(col, n) || inbloc(row, col, n));
	return t;
	
}
/********************************************************************* 
* @name inrow
* @brief checks to see if int n is in the specified row
* @param int row, n
* @retval  1 if n is in row, 0 if not
*********************************************************************/ 
int Sudoku::inrow(int row, int n) {
	//loop through every position in row
	for (int i = 0; i < 9; i++) {
		//check each position in row for n
		if (grid[row][i] == n) {
			//return 1 if n is found in row
			return 1;
		}
	}
	//return 0 if n is not found in row
	return 0;
}
/********************************************************************* 
* @name incol
* @brief checks to see if int n is in the specified col
* @param int col, n
* @retval  1 if n is in col, 0 if not
*********************************************************************/ 
int Sudoku::incol(int col, int n) {
	//loop through every position in row
	for (int i = 0; i < 9; i++) {
		//check each position in row for n
		if (grid[i][col] == n) {
			//return 1 if n is found in row
			return 1;
		}
	}
	//return 0 if n is not found in row
	return 0;
}
/********************************************************************* 
* @name inbloc
* @brief checks if n is in 3x3 block
* @param int row, col, and n
* @retval  1 if it is in the block, 0 else
*********************************************************************/ 
int Sudoku::inbloc(int row, int col, int n) {
	int br, bc;
	//find what block position is in
	br = row - (row % 3);
	bc = col - (col % 3);
	//loop through 3 rows in block
	for (int i = 0; i < 3; i++) {
		//loop through 3 columns in block
		for (int j = 0; j < 3; j++) {
			//check each position for n
			if (grid[(br + i)][(bc + j)] == n)
				//return 1 if found
				return 1;
		}
	}
	//return 0 by default indicating n is not in block
	return 0;
}
/********************************************************************* 
* @name printGrid
* @brief Prints grid to screen
* @param none 
* @retval  none 
*********************************************************************/ 
void Sudoku::printGrid() {
	//loop through rows
	for (int i = 0; i < 9; i++) {
		//loop through columns
		for (int j = 0; j < 9; j++) {
			//if at row 3 or 6, add dividing bars
			if (j == 3 || j == 6)
				cout << " | ";
			//change 0 to *
			if (grid[i][j] == 0)
				cout << "* ";
			//if the number is not 0 print it
			else {
				//print number
				cout << grid[i][j] << " ";
			}
		}
		//handle horizontal dividers
			if (i == 2 || i == 5) {
				//next line
				cout << endl;
				//print horizontal dividers
				for (int k = 0; k < 9; k++) {
					cout << "---";
				}
			}
		cout << endl;
	}
}
/********************************************************************* 
* @name solve
* @brief solves sudoku grid
* @param none 
* @retval  1 if success, 0 if failure
*********************************************************************/ 
int Sudoku::solve() {
	//create a temp variable to hold location of row and column
	int row, col;
	//if statement that checks to see if every space is full
	//also uses pointers to get the location of empty space
	if (!Sudoku::nextplace(row, col)) {
		//return 1 if grid is solved
		return 1;
	}
	//counter variable
	int n = 1;
	//loop through numbers
	for (n; n <= 9; n++) {
		//check if number is valid in that spot
		if (isvalid(row, col, n)) {
			//set place equal to counter number
			grid[row][col] = n;
			//check for recursive sucess and initiate new member
			if (Sudoku::solve()) {
				//returns 1 if offshoot is solved
				return 1;
			}
			//if the grid was not solved, set position back to 0
			grid[row][col] = 0;
		}
		
	}
	//return 0 if all 9 numbers fail check to cause recursion
	//to cascade back to a new valid position
	return 0;

}

/********************************************************************* 
* @name nextplace
* @brief finds the next place that is free
* @param pointer to row and column
* @retval  1 if new location found, 0 if all squares are complete
*********************************************************************/ 
int Sudoku::nextplace(int& r, int& c) {
	//loop through rows
	for (r=0; r < 9; r++) {
		//loop through columns
		for (c=0; c < 9; c++) {
			//if a space is free, return 1
			if (grid[r][c] == 0) {
				return 1;
			}
		}
	}
	//return 0 if every space is full
	return 0;
}