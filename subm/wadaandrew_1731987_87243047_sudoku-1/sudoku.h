#pragma once
/********************************************************************* 
* @brief class sudoku
*********************************************************************/ 
class Sudoku {
	private:
		int grid[9][9] = {0};
		//done
		int isvalid(int row, int col, int n);
		//done
		int inrow(int row, int n);
		//done
		int incol(int col, int n);
		//done
		int inbloc(int row, int col, int n);
		//done
		int nextplace(int &row, int &col);
	public:
		//done
		Sudoku(int n);
		//done
		int solve();
		//done
		void printGrid();
};