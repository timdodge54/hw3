
#ifndef sudoku
#define sudoku

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
class Sudoku
{
private:
	int preset[9][9], grid[9][9];

	/**
	@name checkRow (private function)
	@brief checks the given Row to see if input is valid
	@param row, number
	@retval  1 (valid) or 0 (invalid)
	**/

	int checkRow(int r, int n)
	{	// begin
		int i;
		for (i = 0; i < 9; i++)
		{	// on given row, check every column
			if (grid[r][i] == n)
			{	// if numbers match, return 0 (invalid)
				return 0;
			}
		}	// if all numbers checked, return 1 (valid)
		return 1;
	}

	/**
	@name checkCol (private function)
	@brief checks the given column to see if input is valid
	@param column, number
	@retval  1 (valid) or 0 (invalid)
	**/

	int checkCol(int c, int n)
	{	// Begin
		int i;
		for (i = 0; i < 9; i++)
		{	// on given column, check every row
			if (grid[i][c] == n)
			{	// if numbers match, return 0 (invalid)
				return 0;
			}
		}	// if all numbers checked, return 1 (valid)
		return 1;
	}

	/**
	@name checkBox (private function)
	@brief checks the given box to see if input is valid
	@param row, column, number
	@retval  1 (valid) or 0 (invalid)
	**/

	int checkBox(int r, int c, int n)
	{	// Begin
		int r1, c1;
			// set r1 and c1 to top left box of square
		r1 = (r / 3) * 3;
		c1 = (c / 3) * 3;
			// loop while within box
		for (r = r1; r <= (r1 + 2); r++)
		{
			for (c = c1; c <= (c1 + 2); c++)
			{		// if numbers match, return 0 (invalid)
				if (grid[r][c] == n)
				{
					return 0;
				}
			}
		}	// if all numbers checked, return 1 (valid)
		return 1;
	}

	/**
	@name nextBox (private function)
	@brief finds the next alterable box
	@param row, column
	@retval  1 (found box) or 0 (hit end of array)
	**/

	int nextBox(int& r, int& c)
	{	// Begin
			// loop row by collumn
		for (;r < 9; r++)
		{
			for (; c < 9; c++)
			{		// if value in box is 0, return 1 (box found)
				if (grid[r][c] == 0)
				{
					return 1;
				}
			}
			c = 0;
		}	// if end of array is hit, return 0
		return 0;
	}

	/**
	@name prevBox (private function)
	@brief finds the previous alterable box
	@param row, column
	@retval  1 (found box) or 0 (hit end of array)
	**/

	int prevBox(int& r, int& c)
	{	// Begin
		c--;
			// loop row by collumn
		for (; r >= 0; r--)
		{
			for (; c >= 0; c--)
			{		// if value not preset, return 1 (box found)
				if (preset[r][c] == 0)
				{
					return 1;
				}
			}
			c = 8;
		}	// if end of array is hit, return 0 
		return 0;
	}


public:
	Sudoku(int n);
	int solve();
	void printGrid();
};

#endif
