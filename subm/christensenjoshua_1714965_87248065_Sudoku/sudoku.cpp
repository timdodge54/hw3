#include"sudoku.h"

/**
@name Sudoku::Sudoku
@brief Constructor to generate Sudoku puzzle with n prededermined squares
@param number of squares to prepopulate
@retval  none
**/

Sudoku::Sudoku(int n)
{	// Begin
	int row, col, i = 0, num;
		// initialize grid and preset arrays to 0
	for (row = 0; row < 9; row++)
	{
		for (col = 0; col < 9; col++)
		{
			grid[row][col] = 0;
			preset[row][col] = 0;
		}
	}
		// seed randomizer
	srand(time(NULL));
		// loop until i hits number of squares to prepopulate
	while (i < n)
	{		// Generate a random row, column, and number
		row = rand() % 9;
		col = rand() % 9;
		num = (rand() % 9) + 1;
			// Check if row column and number combination is valid
		if (checkRow(row, num) && checkCol(col, num) 
			&& checkBox(row, col, num) && (preset[row][col] == 0))
		{	// if valid, set values for grid, preset, and increment i
			grid[row][col] = num;
			preset[row][col] = 1;
			i++;
		}
	}
		// print the prepopulated grid to the screen
	printGrid();
	cout << endl << endl << endl;
}	// End

/**
@name Sudoku::solve
@brief attempts to solve a given sudoku puzzle
@param none
@retval  1 (solved) or 0 (unsolvable)
**/

int Sudoku::solve()
{	// Begin
	int r1 = 0, c1 = 0, r, c, flag = 0, n, check;
		// find the first box to work with
	nextBox(r1, c1);
	r = r1;
	c = c1;
	n = 1;
		// loop indefinately
	while (r < 9)
	{		// loop while our guess is less than 10
		for (; n < 10; n++)
		{		// Check if guess is valid
			if (checkRow(r, n) && checkCol(c, n)
				&& checkBox(r, c, n))
			{
				// if valid, set value and try to find next box
				grid[r][c] = n;
				if (nextBox(r, c))
				{		// if next box is found, set guess back to 1
					n = 0;
				}
				else
				{		// if no next box, puzzle is solved
					return 1;
				}
			}
		}
		// if guess hits 10, set number back to 0 and find previous box
		grid[r][c] = 0;
		if (prevBox(r, c))
		{	// if previous box found, set guess to previous guess + 1
			n = grid[r][c] + 1;
		}
		else
		{	// if no previous box found, puzzle unsolvable
			return 0;
		}
	}
	return 0;
}	// End

/**
@name Sudoku::printGrid
@brief prints the sudoku grid to the screen
@param none
@retval  none
**/

void Sudoku::printGrid()
{	// Begin
	int r, c, i;
		// loop while row is less than 9
	for (r = 0;r < 9;r++)
	{		// loop while column is less than 9
		for (c = 0;c < 9;)
		{		// print 3 numbers
			for (i = 0; i < 3; i++, c++)
			{		// if number is 0, print a *
				if (grid[r][c] == 0)
				{
					cout << "*   ";
				}
				else
				{
					cout << grid[r][c] << "   ";
				}
			}	// after 3 numbers, print vertical bar if column is < 9
			if (c < 9)
			{
				cout << "|   ";
			}
			else
			{	// else, go to next line
				cout << endl;
			}
		}	// once we hit the 3rd and 5th row, print a row of - and +
		if ((r % 3 == 2) && r < 8)
		{
			cout << "- - - - - - + - - - - - - - + ";
			cout << "- - - - - -" << endl;
		}
	}
}	// End