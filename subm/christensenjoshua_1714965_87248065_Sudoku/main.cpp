
/**
@name main
@brief generates and attempts to solve random sudoku puzzles
@param none
@retval  none
**/

// Code for main copied from example on canvas

#include "sudoku.h" 

int main()
{
	int n;
		// Recieve initail number of squares from user
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;
		// Generate puzzle with constructor
	Sudoku s(n);
		// Attempt to solve the puzzle
	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printGrid();
	}
		// Print if solved or say if unsolvable
	else {
		cout << "Sorry, unsolvable..." << endl;
	}
		// End
	return 0;
}

