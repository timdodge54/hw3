#ifndef SUDOKU_H
#define SUDOKU_H 

class Sudoku {
public:
	Sudoku(int n);
	int solve();
	void printGrid();
private:
	 int grid[9][9];
	 int base[9][9];
	 int r, c, value;
	 int* x = &r, * y = &c;
	 int columns(int value, int c);
	 int rows(int value, int r);
	 int squares(int value, int r, int c);
	 int findfirstspot(int* x, int* y);
	 int findnextspot(int* x, int* y);
	 int findlastspot(int* x, int* y);


};
#endif
