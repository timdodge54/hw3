#include "sudoku.h"
#include<iostream>

using namespace std;
/**
@name	main
@brief	asks user for a number and calles the sudoku function to create/solve 
@param
@retval	returns a 0
*/

int main(void)
{
	int num;
	// tell user to put in a number
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> num;
	// call sudoku and solve the sudoku
	Sudoku s(num);
	if (s.solve())
	{
		// if the sudoku is solvable print the solution
		cout << "Solved!" << endl;
		s.printGrid();
	}
	// if the sudoku is unsolvable then print sorry unsolvable
	else
	{
		cout << "Sorry, unsolvable..." << endl;
	}
	return 0;

}