#include "sudoku.h"
#include <iostream>
#include <cstdlib>
using namespace std;

/**
@name	findfirstspot
@brief	goes in the base grid and finds the first spot avaliable
@param	the pointers x and y are the location of the current value
@retval	returns a one if the spot was found and returns a zero if it 
		didn't find anything
*/
int Sudoku::findfirstspot(int* x, int* y)
{
	// start at 0,0
	int row = 0, col = 0;
	// loop
	for (col = 0; col < 9; col++)
	{
		// loop
		for (row = 0; row < 9; row++)
		{
			// check if the spot is valid on the base and if so return 1
			if (base[row][col]==0)
			{
				*x = row;
				*y = col;
				return 1;
			}
			// endloop when the row reaches 8
		}
		//endloop when the column reaches 8
	}
}

/**
@name	findnextspot
@brief	finds the next spot avaliable in the base 
@param	the pointers x and y are the location of the current value
@retval	returns a one if the spot was found and returns a zero if it 
		didn't find anything
*/
int Sudoku::findnextspot(int* x, int* y)
{
	int row = *x, col= *y;
	
	row++;
	//loop
	while (col < 9)
	{
		
		//loop
		while (row < 9)
		{
			
			// if the spot is not avaliable
			if (base[row][col]==1)
			{
				// add a row
				row++;
			}
			// else return 1
			else
			{
				*x = row;
				*y = col;
				return 1;
			}
			// endloop when row gets to 8
		}
		row = 0;
		col++;
		// endloop when col gets to 8
	}
	// return 0
	return 0;

}

/**
@name	findlastspot
@brief	takes the current spot and finds the last generated one
@param	the pointers x and y are the location of the current value
@retval	returns a one if the spot was found and returns a zero if it 
		didn't find anything
*/
int Sudoku::findlastspot(int* x, int* y)
{
	int row = *x, col = *y;
	
	//loop
	while (col >= 0)
	{
		
		//loop
		while (row >= 0)
		{
			
			// go back one row
			row--;
			if (row >= 0)
			{
				// if the spot is avaliable return 1
				if (base[row][col] != 1)
				{
					// check to make sure the prev isn't 9
					if (grid[row][col] != 9)
					{
						*x = row;
						*y = col;
						return 1;
					}
					// if it is 9 reset it to zero and keep looping
					else
					{
						
						
						grid[row][col] = 0;

					}
				}
				//endloop when row gets to 0
			}
		}
		row = 8;
		col--;
			// endloop when col gets to 0
	
	}

	return 0;
	
}




/**
@name	solve
@brief	solves the sudoku problem that has been generated
@param	none
@retval	 an one if it is solvable and a zero ifit is not
*/
int Sudoku::solve()
{
	int button=1, r, c, checker1, checker2;
	// get the first spot avaliable
	findfirstspot(&r, &c);
	
	// loop
	for (;;)
	{
		// if the button is not a duplicate
		if (columns(button, c) && rows(button, r)&& squares(button, r, c)
			&& grid[r][c]!=10)
		{
				//set the grid to button and reset the button to one
				grid[r][c] = button;
				button = 1;
				// get to the next spot and check if your are done
				checker1 = findnextspot(&r, &c);
				if (checker1 == 0)
				{
					
					// we've reached the end!!
					return 1;
				}
		}
		else// if its not all good
		{
			
			button++;
			// up the button and check if it is not 10
			if (button == 10)
			{
				grid[r][c] = 0;
				checker2 = findlastspot(&r, &c);
				//if we went back to before the array declare it unsolvable
				if (checker2 == 0)
				{
					
					return 0;
				}
				// make the button the new grid spot value
				else
				{
					button = grid[r][c];
					
				}
			}
		}
			

	}
	
}

/**
@name	printGrid
@brief	prints the array with separation lines where needed
@param	none
@retval	there is none its void
*/
void Sudoku::printGrid()
{
	int a = 0, b = 0, potatoe;
	//loop
	for (b=0;b < 9;b++)
	{
		//check if we need to add our nice horz lines
		if (b == 3 || b == 6)
		{
			for (potatoe = 0; potatoe < 11; potatoe++)
			{
				cout << "--";
			}
			cout << endl;
		}
		// print one of the spots at a time till you reach the end of the row
		for (a = 0; a < 9; a++)
		{
			if (grid[a][b] == 0)
			{
				cout << "* ";
			}
			// the if and elses are for whether it is a taken or not
			else
			{
				cout << grid[a][b] << " ";
			}
			// check if we need to put a vertical line
			if (a == 2 || a == 5)
			{
				cout << "| ";
			}
			
		}
		cout << endl;

	}


}

/**
@name	sudoku
@brief	makes a sudoku board with n numbers filled out
@param	int n is the number of numbers on the starting board
@retval	 
*/
Sudoku::Sudoku(int n)
{
	int holder, lr, ud, v;
	// set all the grid and base array nums to zero
	base[0][0] = { 0 };
	grid[0][0] = { 0 };
	srand(time(NULL));
	// loop 
	for (holder = 0; holder<n; )
	{
		// generate a rand spot and value
		lr = rand() % 9;
		ud = rand() % 9;
		v = (rand() % 9) + 1;
		// check if it is valid
		if (columns(v, ud)== 1 && rows(v, lr)==1&& squares(v, lr, ud) == 1 
			&& base[lr][ud] != 1)
		{
			
				//place it in the spot in both arrays and add one to the holder
				grid[lr][ud] = v;
				base[lr][ud] = 1;
				holder++;
			
		}
		//endloop when holder is equivalent to n
	}
	Sudoku::printGrid();
}

/**
@name	columns
@brief	checks through the column for any duplicates
@param	value which is the value generated c which is the location
@retval	returns a zero if there is a duplicate or a one if not
*/
int Sudoku::columns(int value, int c)
{
	// check the columns for duplicates by going through a for loop for c
	int tracker;
	for (tracker = 0; tracker < 9; tracker++)
	{
		if (grid[tracker][c] == value)
		{
			return 0;
		}
	}
	return 1;
}

/**
@name	rows
@brief	checks if anything in the row is that number
@param	value which is the value generated r which is the location
@retval	returns a 0 if there is a duplicate or a 1 if its all good to go
*/
int Sudoku::rows(int value, int r)
{
	//check the rows for duplicates by going through a loop for r
	int tracker;
	for (tracker = 0; tracker < 9; tracker++)
	{
		if (value == grid[r][tracker])
		{
			return 0;
		}
	}
	return 1;
}

/**
@name	squares
@brief	checks if the squares in the box share a number 
@param	value which is the value generated r and c which is the location
@retval	0 if there is a duplicate 1 if the all clear is given
*/
int Sudoku::squares(int value, int r, int c)
{
	int i, j, spotlr, spotud;
	i = (r / 3) * 3;
	j = (c / 3) * 3;

	// Loop and each time add one to the spotud
	for (spotud = 0; spotud < 3; spotud++)
	{
		// loop and each time add one to the spotlr
		for (spotlr = 0; spotlr < 3; spotlr++)
		{
			// check the array at spotlr + i*4 by spotud + j*4
			if (grid[spotlr + i][spotud + j] == value)
			{
				return 0;
			}
			// end the loop when spotlr + i*4 gets to three
		}
			// end the loop when the spots gets to three
	}
	return 1;
}
