#include "sudoku.h" 
#include <iostream> 

using namespace std;
/******************************************************************************
@name main
@brief Entry point of the program
@param none
@retval 0 upon successful completion of program
******************************************************************************/
int main()
{
	// variable to store number of squares to prepopulate
	int n;

	// welcome message
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	// create a sudoku object with n squares prepopulated
	Sudoku s(n);

	// print the initial grid
	s.printGrid();

	// solve the sudoku puzzle
	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printGrid();
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}

	// return 0 indicating successful completion of program
	return 0;
}