#pragma once
#include <cstdlib>
#include <ctime>
#include <iostream>

/*********************************************************************
@class Sudoku
@brief Represents a sudoku puzzle and its operations
This class holds the implementation of a sudoku puzzle and its operations.
The operations include generating a puzzle, checking if a number is safe,
finding an unassigned location, solving the puzzle, and printing the grid.
@author None
@date None
@version None
*********************************************************************/

class Sudoku {
public:
	Sudoku(int n);
	int solve();
	void printGrid();

private:
	int grid[9][9];
	int n;
	bool isSafe(int row, int col, int num);
	bool findUnassignedLocation(int& row, int& col);
};