#include "sudoku.h"
#include <iostream>

using namespace std;

/*********************************************************************
@name Sudoku
@brief Constructor for the class
This constructor initializes the sudoku puzzle by filling the grid with
*s, generates a random puzzle with n numbers and stores it in the grid.
@param[in] n - the number of random values to be generated in the grid
@retval None
*********************************************************************/
Sudoku::Sudoku(int n) : n(n) {

    srand(time(NULL));
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            grid[i][j] = 0;
        }
    }
    int count = 0;
    while (count < n) {
        int row = rand() % 9;
        int col = rand() % 9;
        int num = (rand() % 9) + 1;
        if (isSafe(row, col, num)) {
            grid[row][col] = num;
            count++;
        }
    }
}

/******************************************************************************
@name isSafe
@brief Check if a number is safe to place in the grid
This function checks if a number is safe to place in the grid by
checking if it is not in the same row, column or 3x3 sub-grid.
@param[in] row - the row index of the number in the grid
@param[in] col - the column index of the number in the grid
@param[in] num - the number to be checked
@retval True if the number is safe to place, False otherwise
******************************************************************************/

bool Sudoku::isSafe(int row, int col, int num) {
    for (int i = 0; i < 9; i++) {
        if (grid[row][i] == num || grid[i][col] == num) {
            return false;
        }
    }
    int rowStart = row - row % 3;
    int colStart = col - col % 3;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (grid[i + rowStart][j + colStart] == num) {
                return false;
            }
        }
    }
    return true;
}

/******************************************************************************
@name findUnassignedLocation
@brief Finds an unassigned location in the grid
This function finds an unassigned location in the grid by checking
for cells with value 0 and returns the row and column indices.
@param[out] row - the row index of the unassigned location
@param[out] col - the column index of the unassigned location
@retval True if an unassigned location is found, False otherwise
******************************************************************************/

bool Sudoku::findUnassignedLocation(int& row, int& col) {
    for (row = 0; row < 9; row++) {
        for (col = 0; col < 9; col++) {
            if (grid[row][col] == 0) {
                return true;
            }
        }
    }
    return false;
}

/******************************************************************************
@name solve
@brief Solves the sudoku puzzle
This function solves the sudoku puzzle by finding an unassigned location,
trying all possible numbers in that location, checking if it's safe to place,
and repeating this process until a solution is found or there is no solution.
@retval True if the puzzle is solvable, False otherwise
******************************************************************************/

int Sudoku::solve() {
    int row, col;
    if (!findUnassignedLocation(row, col)) {
        return true;
    }
    for (int num = 1; num <= 9; num++) {
        if (isSafe(row, col, num)) {
            grid[row][col] = num;
            if (solve()) {
                return true;
            }
            grid[row][col] = 0;
        }
    }
    return false;
}

/*********************************************************************
@name printGrid
@brief Prints the sudoku puzzle grid
This function prints the sudoku puzzle grid to the standard output.
@retval None
*********************************************************************/

void Sudoku::printGrid() {
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            if (grid[i][j] == 0) {
                cout << "*";
            }
            else
            {
                cout << grid[i][j];
            }
            cout << " ";
            if (j % 3 == 2 && j < 8) {
                cout << "| ";
            }
        }
        cout << endl;
        if (i % 3 == 2 && i < 8) {
            cout << "------+-------+-------" << endl;
        }
    }
}