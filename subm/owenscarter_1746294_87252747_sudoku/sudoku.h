#include <iostream>
#include <cstdlib>
#include <ctime>


class Sudoku {
	int values[9][9];
	int lock[9][9];
	int checkrow(int, int);
	int checkcol(int, int);
	int checkbox(int, int, int);
	int tryval(int, int, int);
public:
	Sudoku(int n);
	void printGrid();
	int solve();
};