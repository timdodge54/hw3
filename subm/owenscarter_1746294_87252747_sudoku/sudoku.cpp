#include "sudoku.h"
using namespace std;
/*************
@name checkrow
@brief checks if a number is valid on a given row
@param val, x
@retval valid or not
**************/
int Sudoku::checkrow(int val, int x) {
	for (int i = 0; i < 9; i++) {
//compare whole row agains input val
		if (Sudoku::values[i][x] == val) {
			return 0;
		}
	}
	return 1;
}

/*************
@name checkcol
@brief checks if a number is valid on a given collumn
@param val, y
@retval valid or not
**************/
int Sudoku::checkcol(int val, int y) {
	for (int i = 0; i < 9; i++) {
//compare whole collumn against input val
		if (Sudoku::values[y][i] == val) {
			return 0;
		}
	}
	return 1;
}/*************
@name checkbox
@brief checks if a number is valid in a given box
@param val, x, y
@retval valid or not
**************/
int Sudoku::checkbox(int x, int y, int val) {
		int boxx;
		int boxy;
//find top right of box
		boxx = (x / 3) * 3;
		boxy = (y / 3) * 3;
//check input val against whole box
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
//return 0 if valid
				if (Sudoku::values[boxy][boxx] == val) 
					return 0;
				boxy++;
			}
			boxx++;
			boxy = (y / 3) * 3;
		}
		return 1;
}
/*************
@name tryval
@brief checks if a number is valid in a given coordinate
@param val, x
@retval valid or not
**************/
int Sudoku::tryval(int val, int y, int x) {
//check number in its, collumn, row, and box
	if (checkrow(val, x) && checkcol(val, y) && checkbox(x, y, val) && (val<10))
		return 1;
	else
		return 0;
}

/*************
@name Sudoku
@brief Generates sudoku puzzles
@param n
@retval none
**************/
Sudoku::Sudoku(int n) {
//initialize number grid
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			Sudoku::values[i][j] = 0;
		};
	};
//initialize lock grid
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			Sudoku::lock[i][j] = 0;
		};
	};
	int x=0, y=0, val=0;
//initialize random number generator
	srand(time(NULL));
	for (int i = 1; i <= n;) {
//generate number
		x = rand() % 9;
		y = rand() % 9;
		val = (rand() % 9) + 1;
//check validity
		if (tryval(val, y, x)) {
			if (!Sudoku::lock[y][x]) {
				Sudoku::values[y][x] = val;
				Sudoku::lock[y][x] = 1;
				i++;
			}
		}
	}
}

/*************
@name printGrid
@brief Prints the sudoku grid
@param none
@retval none
**************/
void Sudoku::printGrid() {
	int cx=0, cy = 0;
	for (int i = 0; i < 3; i++) {
		for (int l = 0; l < 3; l++) {

			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) 
				{
//if value is 0 print an astersk
					if (Sudoku::values[cy][cx] == 0)
						cout << "  *";
					else
//otherwise print the value
						cout << "  " << Sudoku::values[cy][cx];
					cy++;
				}
//every three collumns print a line
				cout << " |";
			}

			cout << "\n";
			cy = 0;
			cx++;
		}
//every three rows print a line
		cout << "---------------------------------\n";
	}
}

/*************
@name solve
@brief solves the grid
@param none
@retval success or failure
**************/
int Sudoku::solve() {
	int x = 0, y = 0, val = 1;
	int minx = 0, miny = 0, minflag = 0;
	int maxx = 8, maxy = 8, maxflag = 0;
	int solved = 0;

//find the lowest unlocked cell
	while (!minflag) 
	{
		if (lock[y][x]) 
		{
			if (y < 9) 
			{
				y++;
			}
			else 
			{
				y = 0;
				x++;
			}
		}
//save lowest
		else {
			minx = x;
			miny = y;
			minflag = 1;
		}
	}
	x = 8;
	y = 8;
//find highest unlocked cell
	while (!maxflag)
	{
		if (lock[y][x])
		{
			if (y > 0)
			{
				y--;
			}
			else
			{
				y = 8;
				x--;
			}
		}
		else {
//save highest
			maxx = x;
			maxy = y;
			maxflag = 1;
		}
	}
//start at minimum
	x = minx;
	y = miny;
	while (!solved) {
//try a value at the current position
		if (tryval(val, y, x)) {
//if succesful place in grid
			values[y][x] = val;
			do 
			{
//increment along the grid until an unlocked spcae
				if (y < 9)
				{
					y++;
				}
				else
				{
					y = 0;
					x++;
				}
			} while (lock[y][x]);
//set value to 1
			val = 1;
//if at the end return solved and exit
			if (((maxy==8)&&(maxx+1)==x)||(y==maxy+1)&&(x==maxx+1)){
				return 1;
				solved = 1;
			}
		}
		else
//if invalid number check if at nine
			if (val >= 9) {
//if at nine set value to 1
				val = 1;
//if at beginning return unsolvable
				if ((x == minx) && (y == miny)) {
					return(0);
					solved = 1;
				}
//if not at begining step back to last open space
				else {
					values[y][x] = 0;
					do
					{
						if (y > 0)
						{
							y--;
						}
						else
						{
							y = 8;
							x--;
						}
//increase value and try again
						val = values[y][x] + 1;
					} while (lock[y][x]);
				}
			}
//increment value
			else {
				val++;
			}
	}
}