#include "sudoku.h"

using namespace std;
/*************
@name main
@brief generates and solves sudoku puzzles
@param none
@retval none
**************/
int main()
{
	int n;
//generate puzzle
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;
	Sudoku s(n);
//pritn the grid
	s.printGrid();

//attempt to solve
	if (s.solve()) {
//if succesful print solved grid
		cout << "Solved!" << endl;
		s.printGrid();
	}
//if failed print unsolvable
	else {
		cout << "Sorry, unsolvable..." << endl;
	}

	return 0;
}
