#include "sudoku.h" 
#include <iostream> 

using namespace std;
/********************************************
* @name main
* @brief Functions to begin and end the sudoku
* @param none
* @retval none
********************************************/

int main()
{
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	Sudoku s;
	s.starter(n);

	if (s.solve()) {
		cout << "Solved!" << endl;
		s.print();
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}
	return 0;
}