#pragma once
#ifndef SUDOKU_H
#define SUDOKU_H
class Sudoku {
private:
    int grid[9][9];
    int gridlock[9][9];
    bool check(int row, int col, int num);
   

public:
    Sudoku();
    void print();
    void starter(int num);
    bool solve();
};

#endif