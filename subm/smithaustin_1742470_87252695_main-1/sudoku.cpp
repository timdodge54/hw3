#include <iostream>
#include <cstdlib>
#include "sudoku.h"

using namespace std;
/********************************************
* @name Sudoku
* @brief Functions to set up the board
* @param none
* @retval none
********************************************/

Sudoku::Sudoku() {
    //sets up grid
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            grid[i][j] = 0;
        }
    }
}

/********************************************
* @name print
* @brief Functions to print the board
* @param none
* @retval none
********************************************/


void Sudoku::print() {
    int timescol = 0;
    for (int i = 0; i < 9; i++) {
        int timesrow = 0;
        cout << " ";
        for (int j = 0; j < 9; j++) {
            if (grid[i][j] == 0) {
                cout << "* "; //Sets 0 to *'s
            }
            else {
                cout << grid[i][j] << " ";
            }
            if ((j + 1) % 3 == 0 && timesrow < 2) {
                //prints 3x3
                cout << "| ";
                timesrow++;
            }
            if ((i + 1) % 3 == 0 && (j + 1) % 9 == 0 && timescol < 2) {
                //prints line for 3x3
                cout << endl << "-------+-------+--------";
                timescol++;

            }
        }
        cout << endl;
    }
}

/********************************************
* @name Solve
* @brief Functions to solve sudoku
* @param none
* @retval none
********************************************/

bool Sudoku::solve() {
    for (int row = 0; row < 9; row++) {
        for (int col = 0; col < 9; col++) {
            //if there is no preset number from starter
            if (grid[row][col] == 0) {
                //adds number
                for (int num = 1; num <= 9; num++) {
                    //testes number
                    if (check(row, col, num)) {
                        //if true sets value
                        grid[row][col] = num;
                        //Moves onto next zero (resets function)
                        if (solve()) {
                            return true;
                        }
                        //if failed resets to zero and contines number testing
                        grid[row][col] = 0;
                    }
                }
                //Ran out of testable numbers
                return false;
            }
        }
    }
    //reaches the end of grid succesfully
    return true;
} //Returns soves through the links


/********************************************
* @name check
* @brief checks the number in row, colums, and squares
* @param none
* @retval none
********************************************/
bool Sudoku::check(int row, int col, int num) {
    // checks row
    for (int i = 0; i < 9; i++) {
        if (grid[i][col] == num) {
            return false;
        }
    } //checks collums
        for (int j = 0; j < 9; j++) {
            if (grid[row][j] == num) {
                return false;
            }
        }
        //finds 3x3
    int row3 = row - (row % 3);
    int col3 = col - (col % 3);
    //checks 3x3
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (grid[i + row3][j + col3] == num) {
                return false;
            }
        }
    }

    return true;
}


/********************************************
* @name start
* @brief Functions to set up the board
* @param none
* @retval none
********************************************/

void Sudoku::starter(int start) {
    int entered = 0;
   
    // While the entered amout is less than wanted
    while (entered < start) {
        int row = rand() % 9; //Rand row
        int col = rand() % 9; //Rand col
        int n1 = rand() % 9 + 1; //Rand Numeber betweer 1-10
        //Checks if can input
        if (grid[row][col] == 0 && check(row, col, n1)){
            grid[row][col] = n1;
            gridlock[row][col] = 1;
            entered++;
        }
    }
    print();
}

