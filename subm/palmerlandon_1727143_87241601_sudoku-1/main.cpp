#include "sudoku.h" 
#include <iostream> 

using namespace std;

int main()
{	//Begin
	int n;
	//user is notified and prompted to enter a value of squares to 
	//prepopulate in the grid
	cout << "Welcome to the program that both creates and solves Sudoku" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;
	//if the user tries to be cheeky, an error is thrown as a warning to 
	//enter a valid number of squares to populate.
	if (n > 81)
	{
		cout << "That is not a valid number squares." << endl;
		cout << "Please enter a valid number (1 - 81 inclusive)." << endl;
	}
	//a function of class Sudoku named "s" is created
	Sudoku s(n);
	//if s.solve returns a true value, aka no error is thrown, the grid
	//is printed with the user being notified through "solved" being printed
	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printGrid();
	}
	//if an error is thrown, the grid is not printed, and the user is notified
	else 
	{
		cout << "Sorry, this is unsolvable." << endl;
	}

	return 0;
} //End