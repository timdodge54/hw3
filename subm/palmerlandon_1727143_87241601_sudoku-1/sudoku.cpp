#include "sudoku.h"
#include <stdlib.h>
#include <ctime>
#include <iostream>

	Sudoku::Sudoku(int n)
	{
		srand(time(NULL));

		int firstSquareInPlay = rand() % 9 + 1; 
		int x, y, row, col, r, c, counter = 0;
		int rand1, rand2, rand3;
		rand1 = rand() % 2;//random numbers to choose placement of fixed vals
		rand2 = rand() % 2;
		rand3 = rand() % 2;

		char grid[8][8] = {'*'}; //array to hold placed values
		int fixed[8][8] = { 0 }; //array to hold fixed values

		while (counter < n)
		{//loop to randomly place fixed values within array "fixed"
			fixed[rand1][rand2] = rand3;

			if (fixed[rand1][rand2] != 0)
			{
				rand1 = rand() % 2;
				rand2 = rand() % 2;
			}
		}
	}

	
	int Sudoku::checkRow(int val, int row)
	{
			int r, validation; //funciton to check if value to be placed is
							   //already present in the row
			validation = 0;
			r = 0;

			for (r <= 9; grid[r][row] == '*'; r++)
			{
				if (grid[r][row] != '*')
				{
					validation = 0; //change the value for validation
									//based on result of check
				}
				else
				{
					validation = 1;
				}
			}
			if (validation == 0)
			{
				return 1;
			}
			else //throw error if validation turns up incorrect or invalid
			{
				return 0;
			}
	}
	//function to check for a given value within the current column
	int Sudoku::checkCol(int val, int col)
	{
			int c, validation;
			validation = 0;
			c = 0;
			//as before, the set is searched to check if a given value
			//is already present
			for (c <= 9; grid[col][c] == '*'; c++)
			{
				//change the value of validation accordingly
				if (grid[col][c] != '*')
				{
					validation = 0;
				}
				else
				{
					validation = 1;
				}
			}
			//throw error accordingly if value is already present
			if (validation == 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
	}
	//function to check the 3x3 box for a given value
	int Sudoku::checkBox(int val, int col, int row)
	{
		int mincol, maxcol, minrow, maxrow, i, j;
		maxrow = 0;
		i = 0;
		j = 0;
		//arithmetic that serves to set the bounds for which
		//the funtion can search
		mincol = (col / 3) * 3;
		maxcol = mincol + 2;

		minrow = (row / 3) * 3;
		maxrow = maxrow + 2;

		//loop through each value of the 3x3 searching for the given
		//value. If it is found, throw error. Otherwise, proceed.
		for (i <= mincol; i <= maxcol; i++)
		{
			for (j <= minrow; j <= maxrow; j++)
			{
				if (grid[i][j] == val || fixed[i][j] == val)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
		}
	}
	//function to find the first square in play, 
	//and the next square that is to be in play
	int Sudoku::findFirstSquareInPlay(int* x, int* y)
	{
		int n, success, prevSquareInPlay;
		n = 1;
	

		for (n == 1, grid[*x][*y] = n; n++;)
		{
			grid[*x][*y] = n;  //place n at grid[x, y]

			//If no violations
			if (grid[*x][*y] == '*' && *x <= 9 && *y <= 9)
			{
				n = 1;
					if (*x > 9 || *y > 9) //if off grid
					{ 
						success = true; //then success
						return 0;
					}
					else
					{
						n = n++; //else n = n + 1
					}
					if (n == 10)
					{
						grid[*x][*y] = 0; //place 0 at current posi
										  //if n is 10
						 //(x, y) = prevSquareInPlay
						if (*x < 0 || *y < 0) //if off edge of grid
											  //backtracking, it is unsolvable
						{
							return 1;
						}
						else
						{
							int findPrevSquareInPlay(int *x, int *y);
						}/*if space to move back, incriment value at
						 that position by 1 before moving on by calling
						 findPrevSquareInPlay*/
					}
			}
		} //EndLoop
	}
	//function that serves to find the previous square played,
	//given the case that the current square is not a legal location for
	//the given value
	int Sudoku::findPrevSquareInPlay(int* x, int* y)
	{
		//loop through the array to find the previous legal location
		//for placing a value
		for (int i = *x; i < 9; i++)
		{
			char xcoord, ycoord = 0;
			xcoord = *x;
			ycoord += 1;
			//restart the y-coordinate if the last column is reached
			//otherwise, move back in the row by one space.
			if (ycoord == 9)
			{
				i++;
				ycoord = 0;
			}
			else
			{
				xcoord = *x - 1;
			}
			grid[xcoord][ycoord];
			//if the array backs up beyond what is a legal value for x or y,
			//throw error. Otherwise, return as normal.
			if (xcoord < 0 || ycoord < 0)
			{
				return 1;
			}
			else 
			{
				return 0;
			}
		}
	}
	//solving function that calls upon previous functions to solve the puzzle
	int Sudoku::solve()
	{
		//if any errors are thrown, aka "true" in any of the functions used,
		//solve will return an error back to the main function.
		if (!findFirstSquareInPlay || !findPrevSquareInPlay || !checkRow ||
			!checkCol || !checkBox)
		{
			return 1;
		}
		else
		{
			int findFirstSquareInPlay(int* x, int* y);
			int findPrevSquareInPlay(int* x, int* y);
			int checkRow(int val, int row);
			int checkCol(int val, int col);
			int checkBox(int val, int row, int col);
			return 0;
		}
		
	}
	//function that serves to print the grid, utilizing nested 'for' loops.
	void Sudoku::printGrid()
	{
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				cout << " " << grid[i][j] << " ";
			}

		}
	}