/******************************************************************************
* Class Sudoku
*
* Summary:
* A class for performing the necessary fuctions to create and solve sudoku 
* puzzles. This includes the backtracking to overwrite incorrect placement,
* placing random numbers, and printing the grid.
******************************************************************************/

#ifndef SUDOKU_H
#define SUKOKU_H
using namespace std;

class Sudoku {
	public:
		Sudoku(int n);
		int solve(); //solves the sudoku puzzle that is generated
		void printGrid(); //prints the 9x9 grid

	private:
		int checkRow(int val, int y); //function to check if a value
									//that is to be placed is present in a row
		int checkCol(int val, int x);
		//function to check if a value
		//that is to be placed is present in a column
		int checkBox(int val, int x, int y); //checks 3x3 for given value
		int findFirstSquareInPlay(int* x, int* y);
		//finds first square to place value
		int findPrevSquareInPlay(int* x, int* y);
		//finds previous square to alter value given that next value
		//has no legal placement in the current location

		char grid[8][8] = {'*'};
		int fixed[8][8] = {0};

};

#endif