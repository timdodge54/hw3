class Sudoku
{
private:
	int board[9][9];
	bool prepop[9][9];

	/****************************************************
*	@name		colcheck
*	@brief		check column to see if you can place a number there
*	@param		column location (x), number to be checked (number)
*	@retval		1 if legal, 0 in not legal
********************************************************/
	int colcheck(int x, int number);
	/****************************************************
*	@name		rowcheck
*	@brief		check row to see if you can place a number there
*	@param		row location (y), number to be checked (number)
*	@retval		1 if legal, 0 in not legal
********************************************************/
	int rowcheck(int y, int number);
	/****************************************************
*	@name		boxcheck
*	@brief		check box to see if you can place a number there
*	@param		coordinates of number (x, y) number to be checked (number)
*	@retval		1 if legal, 0 in not legal
********************************************************/
	int boxcheck(int x, int y, int number);
public:
	/****************************************************
*	@name		Sudoku
*	@brief		bouild a board from random number that doesn't break
*					break any sudoku rules
*	@param		amount of spaces to prepopulate (num)
*	@retval		none
********************************************************/
	Sudoku(int n);
	/****************************************************
*	@name		printgrid
*	@brief		print the grid with the numbers that are currently filled in
*	@param		none
*	@retval		none
********************************************************/
	void printgrid();
	/****************************************************
*	@name		solve
*	@brief		solve the sudoku puzzle with backwards tracking
*	@param		none
*	@retval		1 if solvable, 0 if unsolvable
********************************************************/
	int solve();
};