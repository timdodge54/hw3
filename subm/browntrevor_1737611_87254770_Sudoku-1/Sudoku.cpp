#include "sudoku.h"
#include <iostream>
#include <cstdlib>

using namespace std;

Sudoku::Sudoku(int n)
{
	int num, x, y, i = 0;
	bool legal = false;
		//seed a time for random nums
	srand(time(NULL));
		//initiate board to everything 0 and flase
	for (y = 0; y < 9; y++)
	{
		for (x = 0; x < 9; x++)
		{
			board[x][y] = 0;
			prepop[x][y] = false;
		}
	};

	while (i < n)
	{
			//get random numbers
		x = rand() % 9;
		y = rand() % 9;
		num = (rand() % 9) + 1;
				//make sure the number can be placed in a legal spot
		if (Sudoku::colcheck(x, num) && Sudoku::rowcheck(y, num) 
			&& Sudoku::boxcheck(x, y, num))
		{
			legal = true;
		}
		else
		{
			legal = false;
		}

				//if theres not already a number there and placement is legal
		if (!prepop[x][y] && legal)
		{
			board[x][y] = num;
			prepop[x][y] = true;
			i++;		//if number gets placed, increment i
		}
		legal = false;	//prep legal for next iteration
	}

}

void Sudoku::printgrid()
{
	int x, y;
	//normal for loop to print 2d array
	for (y = 0; y < 9; y++)
	{
		for (x = 0; x < 9; x++)
		{		//if there's a 0 in there print '*'
			if (board[x][y] == 0)
			{
				cout << "*";
			}
			else
			{	//if there is number print the number
				cout << board[x][y];
			}
			if (x == 2 || x == 5)
			{		//draw lines between collums to make board look pretty
				cout << " | ";
			}
			else
			{
				cout << " ";
			}
		}
		if (y == 2 || y == 5)
		{			//draw lines between rows to make board look pretty
			cout << endl << "------+-------+-------" << endl;
		}
		else
		{
			cout << endl;
		}
	}
}

int Sudoku::colcheck(int x, int number)
{
	bool not_in_col = true;
	int y = 0;
	int count = 0;
	
			//exit loop if we find the number or go past the collumn
	while (not_in_col == true && y < 9)
	{
		if (number == board[x][y])
		{
			not_in_col = false;
		}
		y++;
	}
	if (not_in_col)		//return values if the placement is legal or not
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int Sudoku::rowcheck(int y, int number)
{
	bool not_in_row = true;
	int x = 0;
	int count = 0;
				//exit loop if we find the number or go past the row
	while (not_in_row == true && x < 9)
	{
		if (number == board[x][y])
		{
			not_in_row = false;
		}

		x++;
	}
	if (not_in_row)			//return values if the placement is legal or not
	{
		return 1;
	}
	else
	{

		return 0;
	}
}

int Sudoku::boxcheck(int x, int y, int number)
{
	int i, j, x_hold;
	bool not_in_box = true;
		//find left edge of box
	x /= 3;
	x *= 3;
		//store the left edge of the box
	x_hold = x;
		//find the top edge of the box
	y /= 3;
	y *= 3;
			//only loop 3 times because box is 3x3
	for (j = 0; not_in_box && j < 3; j++)
	{
		for (i = 0; not_in_box && i < 3; i++)
		{
			if (number == board[x][y])	//look for number in box
			{
				not_in_box = false;
			}
			x++;		//go one spot to the right
		}
		x = x_hold;		//go back to the left edge of box
		y++;			//move down a row in box
	}
	if (not_in_box)		//return values if the placement is legal or not
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int Sudoku::solve()
{
	int x = 0, y = 0, num, temp = 0;
	bool num_reverse = false;

		//weird for loops so I only increment x and y when I want to
	for (y = 0; y < 9;)
	{
		for (x = 0; x < 9;)
		{
			for (num = 1; num < 10; num++)
			{
					//if we are going backward then we resume at previous number
				if (num_reverse)
				{
					num += temp;
				}
					//if a prepopulated number then proceed directly to next space
				if (prepop[x][y])
				{
					x++;
					break;
				}
				else
				{
					num_reverse = false;	//we are no longer going back
					if (colcheck(x, num) && rowcheck(y, num) && boxcheck(x, y, num) 
						&& num < 10)	//if valid placement and number
					{
						board[x][y] = num;	//assign value to board
						x++;				//go to next space
						break;				//restart loop
					}
				}
				
				if (num >= 9)			//if no number fit
				{
					if (x <= 0)			//if we aren't at the start of a row
					{
						if (y > 0)		//if we aren't on the first row
						{
								//go back to the last collumn on previous row
							y--;
							x = 8;
						}
						else
						{		//we are on first row first slot
							//and no number fits so the puzzle is unsolvable
							return 0;
						}
					}
					else
					{	//everything is kinda normal so just go back a space
						x--;
					}

					while (prepop[x][y])	//if back a space is prepopulated
					{
						if (x == 0)			//if we are at the start of a row
						{
							if (y > 0)		//if not the first row
							{
								y--;	//go back a row to last collumn
								x = 8;
							}
							else
							{		//we were on first row first collumn
									//and no numbers were valid
								return 0;
							}
						}
						else
						{			//go back one space
							x--;
						}
						
					}
					temp = board[x][y];	//store the value to resume counting
					board[x][y] = 0;	//clear board
					num_reverse = true;	//activate backwards logic
					
				}
			}
		}
		y++;		//gow down a row
	}
	return 1;		//everything went well and the puzzle is solved!
}