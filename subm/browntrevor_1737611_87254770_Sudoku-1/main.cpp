#include "sudoku.h" 
#include <iostream> 

using namespace std;

/****************************************************
*	@name		main
*	@brief		communicate with user
*	@param		none
*	@retval		none
********************************************************/
int main()
{
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	//i should try/catch my cin...

	Sudoku s(n);
	s.printgrid();

  	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printgrid();
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}
	return 0;
}