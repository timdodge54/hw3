#ifndef SUDOKU_H
#define SUDOKU_H

class Sudoku
{
	public:
		Sudoku(int n);
		int solve();
		void printStar();
	private:
		int s[9][9] = {0};
		int fixed[9][9] = {0};
		int firstSquare(int &y, int &x);
		int nextSquare(int &y, int &x);
		int prevSquare(int &y, int &x, int &num);
		int checkRow(int y, int num);
		int checkCol(int x, int num);
		int checkBox(int y, int x, int num);
};

#endif