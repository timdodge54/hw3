#include "sudoku.h"
#include <iostream>

using namespace std;

/********************************************************************* 
* @name Sudoku
* @brief Constructor which creates the sudoku puzzle and prints to screen
* @param integer n for number of prepopulated values
* @retval  none
*********************************************************************/

Sudoku::Sudoku(int n)
{
	int i = 0;
	//input legal numbers as often as user input dictates
	while(i < n)
	{
		//get new numbers
		int y = rand() % 9;
		int x = rand() % 9;
		int num = rand() % 9 + 1;
		
		//check row
		if (checkRow (y, num))
		{
			//check col
			if (checkCol(x, num))
			{
				//check box
				if(checkBox(y, x, num))
				{
					if(s[y][x] == 0)
					{
						//insert if legal
						s[y][x] = num;
						fixed[y][x] = 1;
						i++;
					}
				}
			}
		}
	}
	//print grid
	printStar();
	cout << endl << endl;
}

/********************************************************************* 
* @name checkRow
* @brief Checks if the placement of a number is valid for a row
* @param y coordinate and desired input number
* @retval 0 if illegal move or 1 if legal move
*********************************************************************/

int Sudoku::checkRow(int y, int num)
{
	int i = 0;
	
	//Loop entire row
	while (i < 9)
	{
		//if illegal move
		if (s[y][i] == num)
		{
			return 0;
		}
		i++;
	}
	
	return 1;
}

/********************************************************************* 
* @name checkCol
* @brief Checks if the placement of a number is valid for a column
* @param x coordinate and desired input number
* @retval  0 if illegal move or 1 if a legal move
*********************************************************************/

int Sudoku::checkCol(int x, int num)
{
int i = 0;
	
	//loop entire column
	while (i < 9)
	{
		//if illegal move
		if (s[i][x] == num)
		{
			return 0;
		}
		i++;
	}
	
	return 1;
}

/********************************************************************* 
* @name checkBox
* @brief Checks if the placement of a number is valid for a box
* @param y coordinate, x coordinate, and desired input number
* @retval  0 if an illegal move or 1 if a legal move
*********************************************************************/

int Sudoku::checkBox(int y, int x, int num)
{
	//find box x coordinate start
	int mincol = (x/3) * 3;
	int maxcol = mincol + 2;
		
	//find box y coordinate start
	int minrow = (y/3) * 3;
	int maxrow = minrow + 2;
		
	//Loop for all values
	for (int c = mincol; c <= maxcol; c++)
	{
		for (int r = minrow; r <= maxrow; r++)
		{
			//if illegal move
			if (s[r][c] == num)
			{
				return 0;
			}
		}
	}
	
	return 1;
}

/********************************************************************* 
* @name firstSquare
* @brief Seeks for a valid starting position in the puzzle
* @param x and y coordinates passed by reference
* @retval  1 if available space or 0 if no space available
*********************************************************************/

int Sudoku::firstSquare(int& y, int& x)
{	
	//check for a valid square
	while (1 == 1)
	{
		if (fixed[y][x] == 0)
		{
			//return available space
			return 1;
		}
		
		//move through until a valid space is found
		if (fixed[y][x] == 1)
		{
			x++;
			
			//move to next row when end of column reached
			if (y == 9)
			{
				x = 0;
				y++;
			}
		}
		
		//return if there are no availables spaces
		if (fixed[y][x] == 1 && x == 8 && y == 8)
		{
			return 0;
		}
	}
}

/********************************************************************* 
* @name nextSquare
* @brief Finds next square to input a number
* @param x and y coordinate by reference
* @retval  0 if puzzle complete or 1 if next square found
*********************************************************************/

int Sudoku::nextSquare(int& y, int& x)
{
	//move to next available square
	while (1 == 1)
	{
		x++;
		
		//if next spot goes off the row
		if (x == 9)
		{
			x = 0;
			y++;
		}
		
		//if next position available
		if (fixed[y][x] == 0)
		{
			//if it flies of the end of the whole puzzle
			if (y > 8)
			{
				return 0;
			}
			//success return;
			return 1;
		}
	}
}

/********************************************************************* 
* @name prevSquare
* @brief Moves to previous square while resetting original square to 0
* @param x and y coordinates and input value passed by reference
* @retval  0 if unsolvable puzzle or 1 if next position found
*********************************************************************/

int Sudoku::prevSquare(int& y, int& x, int &num)
{
	while (2 == 2)
	{
		//move to last position
		x--;
		
		//if moves too far back in a col
		if (x < 0)
		{
			//move up one row and at end of col
			x = 8;
			y--;
			
			//if unsolvable by having no possible previous position
			if (y < 0)
			{
				return 0;
			}
		}
		
		//if found the previous position
		if (fixed[y][x] == 0)
		{
			//correct for values of 9
			if (s[y][x] == 9)
			{
				s[y][x] = 0;
			}
			else
			{
				//increment number
				num = s[y][x] + 1;
				return 1;
			}
		}
	}
}

/********************************************************************* 
* @name solve
* @brief gives process to solve sudoku puzzle utilizing multiple functions
* @param none
* @retval  1 if puzzle completed or 0 if puzzle is impossible
*********************************************************************/

int Sudoku::solve()
{
	int x=0, y=0, checkCounter = 0;
	
	//find first position
	firstSquare(y, x);
	
	//have number 1 to insert
	int num=1;
	
	//Loop
	while(1 == 1)
	{
		//reset checkCounter
		checkCounter = 0;
		
		//check if legal row
		if (checkRow(y, num))
		{
			checkCounter++;
		}
		
		//check if legal col
		if (checkCol(x, num))
		{
			checkCounter++;
		}
		
		//check if legal box
		if (checkBox(y, x, num))
		{
			checkCounter++;
		}
		
		if (checkCounter == 3)
		{
			//input number
			s[y][x] = num;
			
			//move to next square
			if (!nextSquare(y, x))
			{
				//winning puzzle return
				return 1;
			}
			
			else
			{
				num = 1;
			}
		}
		//else
		if (checkCounter < 3)
		{
			//add one to number
			num += 1;
			
			//if number is now 10
			if (num >= 10)
			{
				//put 0 at current position
				s[y][x] = 0;
				
				//if goes too far back
				if (!prevSquare(y, x, num))
				{
					//can't solve it
					return 0;
				}
			}
		}
	}
		
	
	return 0;
}

/********************************************************************* 
* @name printStar
* @brief prints the sudoku grid to the screen
* @param none
* @retval  none 
*********************************************************************/

void Sudoku::printStar()
{
	//loop around for all values
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			cout << "   ";
			
			if (j == 3)
			{
				cout << " | ";
			}
			if (j == 6)
			{
				cout << " | ";
			}
			if (j == 3)
			{
				cout << "\b\b\b";
			}
			if (j == 6)
			{
				cout << "\b\b\b";
			}
			if (s[i][j] == 0)
			{
				cout << "*";
			}
			else
			{
				cout << s[i][j];
			}
			if (j == 2)
			{
				cout << "|";
			}
			if (j == 5)
			{
				cout << "|";
			}
			if (j == 8)
			{
				cout << endl << "            |            |           " << endl;
			}
			if (i == 2 && j == 8)
			{
				cout << "------------+------------+-----------" << endl;
			}
			if (i == 5 && j == 8)
			{
				cout << "------------+------------+-----------" << endl;
			}
		}
	}
}