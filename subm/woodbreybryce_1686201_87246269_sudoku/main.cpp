#include "sudoku.h"
#include <cstdlib>
#include <iostream>

using namespace std;

/********************************************************************* 
* @name main
* @brief Solves a randomly generated sudoku puzzle
* @param none 
* @retval  none 
*********************************************************************/

int main()
{
	int n;
	
	//ask for number of squares to prepopulate
	cout << "Welcome to SudokuSolver!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;
	
	//seed number generator
	srand(time(NULL));	
	
	//construct puzzle
	Sudoku s(n);
	
	//solve
	if (s.solve())
	{
		cout << "Solved!" << endl;
		//s.printGrid();
		s.printStar();
	}
	else
	{
		cout << "Sorry, unsolvable..." << endl;
	}
	
	return 0;
}