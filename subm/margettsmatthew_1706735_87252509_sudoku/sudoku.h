/********************************************************************* 
* @name Sudoku
* @brief Class that contains two 9x9 integer arrays that are sudoku puzzles
* @param int n, number of spots to set in arrays
* @retval  none
*********************************************************************/
class Sudoku{
	private:
		int grid[9][9];
		int gridcmp[9][9];
		int checkRow(int row, int value);
		int checkCol(int col, int value);
		int checkSqr(int row, int col, int value);
	public:
	Sudoku(int n);
	int solve();
	void printGrid();
};