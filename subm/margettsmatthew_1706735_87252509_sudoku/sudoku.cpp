#include "sudoku.h"
#include <cstdlib>
#include <iostream>

using namespace std;
/********************************************************************* 
* @name checkRow
* @brief Checks if there are any row violations in the puzzle
* @param int row, int value
* @retval  int 1 if success or 0 if failure
*********************************************************************/
int Sudoku::checkRow(int row, int value){
	//for each item in the row
	for(int i = 0; i < 9; i++){
		//cout << "checks row" << endl;
		//does it have the same value?
		if(grid[row][i] == value){
			return 0;
		}
	}
	return 1;
}
/********************************************************************* 
* @name checkCol
* @brief Checks if there are any column violations in the puzzle
* @param int col, int value
* @retval  int 1 if success or 0 if failure
*********************************************************************/
int Sudoku::checkCol(int col, int value){
	//for each item in the column
	for(int i = 0; i < 9; i++){
		//cout << "checks col" << endl;
		//does it have the same value?
		if(grid[i][col] == value){
			return 0;
		}
	}
	return 1;
}
/********************************************************************* 
* @name checkSqr
* @brief Checks if there are any square violations in the puzzle
* @param int row, int col, int value
* @retval  int 1 if success or 0 if failure
*********************************************************************/
int Sudoku::checkSqr(int row, int col, int value){
	//divide row and col by 3 to determine which 9x9 square it's in
	int sqrRow = row/3;
	int sqrCol = col/3;
	int rowCheck, colCheck;
	int success = 0;
	rowCheck = sqrRow * 3;
	colCheck = sqrCol * 3;
	//check each member of the 9x9 starting with (row/3)x3 and (col/3)x3
	while(!success){
		//cout << "checks grid" << endl;
		if(grid[rowCheck][colCheck] == value){
			return 0;
		}
		else{
			if (rowCheck < (sqrRow*3)+3){
				rowCheck ++;
			}
			else if(rowCheck == (sqrRow*3)+3 && colCheck < (sqrCol*3)+3){
				rowCheck = sqrRow * 3;
				colCheck ++;
			}
			else{
				success = 1;
			}
		}
	}
	return 1;
}
/********************************************************************* 
* @name Sudoku
* @brief Constructor for Sudoku class,sets base to 0 and randomly fills
* @param int n
* @retval  none 
*********************************************************************/
Sudoku::Sudoku(int n){//Sudoku constructor
int success = 0;
for(int a = 0; a < 9; a++){//initialize sudoku grid with *
	for(int b = 0; b < 9; b++){
		grid[a][b] = 0;
		gridcmp[a][b] = 0;
	}
}
srand(time(NULL));
//loop that iterates n times
	for(int i = 0; i < n;){
		success = 0;
		//generate 3 random ints: row, col, val
		int row = rand() % 9;
		int col = rand() % 9;
		int val = (rand() % 9) + 1;
		//if there's not a value already there
		if(grid[row][col] == 0){
			//run checkRow
			//run checCol
			//run checkSqr
			success = checkRow(row,val) * checkCol(col,val) * checkSqr(col,row,val);
		}
		//if success, set the value and increment loop
		if (success){
			grid[row][col] = val;
			gridcmp[row][col] = val;
			i++;
		}
	}
	printGrid();
}
/********************************************************************* 
* @name solve
* @brief Is supposed to solve the sudoku puzzle
* @param none
* @retval  none 
*********************************************************************/
int Sudoku::solve(){
	char test;
	/*
	initialize grid to all zero
   n=1, (x,y)=firstSquareInPlay*/
   	int n = 1;
	int x=0;//current positions
	int y=0;
	while(grid[x][y] != 0){//making starting position the first blank spot
		x++;
		if(x==9){
			x=0;
			y++;
			if(y==9){
				return 1;//if it's already filled, then success!
			}
		}
	}
	int success = 0;
	int trigger = 0;//setting variables to fill squares
	while(true){
		if(checkRow(x,n) * checkCol(y,n) * checkSqr(x,y,n)){//if legal number placement
		grid[x][y] = n;
		//cout << "criteria success" << endl;
			n=1;
			while(!success){
				//cout << "success loop" << endl;
				if(x==8){
					if(y==8){
						return 1;//returns success
					}
					x = 0;
					y++;
				}
				else{
					x++;//otherwise increments
				}
				//check if next square is filled
				if(gridcmp[x][y] == 0){
					success = 1;
				}
				else{
					success = 0;
				}
			}
		}
		else{//failed criterea
		//cout << "failed criteria" << endl;
			n+=1;//increments n
			if(n==10){
				grid[x][y] = 0;//resets the grid space
				while(!trigger){
					//cout << "fail loop" << endl;
					if(x==0){//backtracking algorithm
						if(y==0){
							//cout << "actual failure";
							return 0;//returns failure
						}
						x = 8;
						y--;
					}
					else{
						x--;
					}
					//check if previous square is locked
					if(gridcmp[x][y] == 0){
						trigger = 1;
					}
					else{
						trigger = 0;
					}
				}
				n = grid[x][y]+1;//increments current grid value
			}
		}
		//cout << endl << "n= " << n << endl;
		//cin >> test;
		//if(test == 'p'){
		//	printGrid();
		//}
	}
   /*Loop
      place n at grid[x,y]
      If no violations
      Then
         n=1, (x,y)=nextSquareInPlay
         If off grid
         Then
            SUCCESS
         EndIf
      Else
         n=n+1
         If n is 10
         Then
            place 0 at grid[x,y]
            (x,y)=prevSquareInPlay
            If off grid
            Then
               FAIL
            Else
               n=grid[x,y]+1
            EndIf
         EndIf
      EndIf
   EndLoop
	*/
	//cout << "Emergency problem";
	return 0;//if something goes wrong
}
/********************************************************************* 
* @name printGrid
* @brief prints the Sudoku grid
* @param none
* @retval  none 
*********************************************************************/
void Sudoku::printGrid(){
	int alt = 1;
	int gridRow = 0;
	int gridCol = 0;
	for(int line = 0; line < 11; line ++){
		if(line == 3 || line ==7){
			for(int divider = 0; divider < 36; divider ++){
				if (divider == 12 || divider == 24){
					cout << "+";
				}
				else{
					cout << "-";
				}
			}
			cout << endl;
		}
		else{
			for(int column = 0; column < 19;column++){
				if(column == 0 || column == 18){
					cout << "  ";
					if(column == 18){
						cout << endl;
					}
					alt = 1;
				}
				else if(column == 12 || column == 6){
					cout << " | ";
					alt = 1;
				}
				else{
					if(alt){
						if(grid[gridRow][gridCol] == 0){
							cout << "*";
						}
						else{
							cout << grid[gridRow][gridCol];
						}
						if(gridRow < 8){
							gridRow ++;
						}
						else{
							gridRow = 0;
							gridCol ++;
						}
						alt = 0;
					}
					else{
						cout << "   ";
						alt = 1;
					}
				}
			}
		}
	}
}