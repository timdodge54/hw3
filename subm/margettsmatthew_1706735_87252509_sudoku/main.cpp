#include "sudoku.h" 
#include <iostream> 
using namespace std; 
 /********************************************************************* 
* @name main
* @brief Provides the interface for the sudoku solver
* @param none
* @retval none
*********************************************************************/
int main() 
{ 

 int n; 
  
 cout << "Welcome to SudokuSolver!!" << endl; 
 cout << "Enter number of squares to prepopulate: "; 
 cin >> n; 
  
 Sudoku s(n); 
  
 if (s.solve()) { 
  cout << "Solved!" << endl; 
  s.printGrid(); 
 } 
 else { 
  cout << "Sorry, unsolvable..." << endl; 
 } 
  
 return 0; 
} 