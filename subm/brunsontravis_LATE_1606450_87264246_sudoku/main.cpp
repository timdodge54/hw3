#include <iostream>
#include <ctime>
#include"sudoku.h"

using namespace std;
/******************************************************************************
@name	main
@brief	to run a program that builds and solves a sudoku puzzle
@praam	none	
@retval	none
******************************************************************************/
int main (void){
	int iter;	//integer for amount of numbers entered
	cout << "Enter number of squares to prepopulate: ";
	cin >> iter;//recieve input
	
	sudoku poopy(iter);//build a class named poopy

	poopy.printGrid();//prints poopy
	poopy.solve();		//"solves" poopy
	cout<<endl<<endl<<endl;//Gap
	cout<<"We can call this solved, but we both know...";
	poopy.printGrid();//Prints poopy results
	
	return 0;
}