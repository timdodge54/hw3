#include "sudoku.h"
#include <iostream>
#include <ctime>
using namespace std;
/******************************************************************************
@name	solve
@brief	adjust the poopy class's values to solve sudoku puzzle
@praam	none	
@retval	none
******************************************************************************/
int sudoku::solve(){
	char value = '1';
	int row = 0;
	int col = 0;
	
	for (int i = 0; i<9; i++){	//start loop
		if(is_valid(row,col,value))//if its valid. place one.
		{
			grid[row][col] = value;//I played around with this for a while
			//i was just trying to replace * with a number
			//on the entire top row, but for some reason, every time it worked
			//then it would leave my loop. I'm honestly exhausted right now
			//but I'm very interested in the solution. I will finish this
			//program someday. 
		}
		col++;//move to the next collum and place value if valid
		i++; //until 9
	}
	return 0;
}
/******************************************************************************
@name	printGrid
@brief	to print the grid from class
@praam	none	
@retval	none
******************************************************************************/
void sudoku::printGrid(){
	for (int i = 0; i < 9; i++)
    {
		if (i==3 | i ==6){cout<<endl<< "------+------+------";}
		cout <<endl;
		for (int j = 0; j < 9; j++)
		{
			if (j==3|j==6|j==9){cout<<'|';}
			cout<< grid[i][j]<< ' ';
		}
	}	
}
/******************************************************************************
@name	is_valid
@brief	to decide if the given location and value coordinate with sudoku rules
@praam	row, col (the position), n(value)
@retval	true or false (1 or 0)
******************************************************************************/
int sudoku::is_valid(int row, int col, char n){
	int t = 0;
	int i = 0;
	int j;

	if (grid[row][col] != '*'){return 0;}	//if its already full, return false
	for (grid[row][t]; t < 11; t++){//move horizontally
		if ((grid[row][t]) == n)
		{
			return 0;	//return false if the same value is found in row
		}
	
		for (grid[i][col]; i < 11; i++){//move vertically
			if ((grid[i][col]) == n)
			{
				return 0;	//return false if the same value is found in row
			}
		}
	}
	int minrow = (row/3)*3;
	int maxrow = minrow + 2;	//sets integers up to check 3x3 boxes
	int mincol = (col/3)*3;
	int maxcol = mincol + 2;
	for(i = minrow;i<=maxrow;i++){//for the box going right
		for(j = mincol;j<=maxcol;j++){//for the box going down
			if(grid[i][j] == n){//if found
				return 0;		//return false
			}
		}
	}
	return 1;//if not false, return true
}	
/******************************************************************************
@name	sudoku
@brief	a constructor for the sudoku program
@praam	n (amount of iterations or values to place	
@retval	a built sudoku class
******************************************************************************/
sudoku::sudoku(int n){	
	srand(time(NULL));	//activates time / random number generator
	char num;
	int xax, yax, recieve;
	for(int i = 0; i < n;)	//for loop to run through iterations
	{	
		recieve = rand() %9 + 1;//get random value integer
		xax = rand() %9;		//get random x axis
		yax = rand() %9;		//get random y axis
		
		switch (recieve)		//This is the only way I could figure out 
		{						//how to change my random integer into
		case 1: num = '1';		//a char value
			break;				//You know i'm sure there is some way on cplusplus
		case 2: num = '2';		//but i'm out of it right now. I'll look later
			break;
		case 3:	num = '3';
			break;
		case 4: num = '4';
			break;
		case 5: num = '5';
			break;
		case 6: num = '6';
			break;
		case 7: num = '7';
			break;
		case 8: num = '8';
			break;
		case 9: num = '9';
			break;
		}
		int answer = is_valid(yax, xax, num);//checks if its valid by running
			//it through the is_valid function
		if (answer == 1){		//if it is true
			grid[yax][xax] = num;	//set the value
			i++;					//iterate i. Only when it is true.
		}
	}
}

