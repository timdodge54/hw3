#ifndef sudoku_hpp
#define sudoku_hpp

class Sudoku {
	private:
		int prearray [9][9];
		int postarray [9][9];
		bool checkrow(int i, int v);
		bool checkcolumn( int j, int v);
		bool checkbox(int i, int j, int v);
		bool valid(int i, int j, int v);
		bool first(int *x, int *y);
		bool prev(int *x, int *y);
		bool next(int *x, int *y);
		
	public:
	Sudoku(int n);
	bool solver();
	void printGrid();
	
};

#endif