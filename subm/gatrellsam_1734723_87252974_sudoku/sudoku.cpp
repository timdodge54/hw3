#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include <ctime>
#include <cstdbool>
#include "sudoku.h"


#define sp "  "
#define bsp "           "
#define und "----------"
using namespace std;

/*************************
*	@name Sudoku
*	@brief constructor, sets up the pre and post arrays
*	@param int n
*	@retval none
******************/
Sudoku::Sudoku(int n){
	int i, j, c, v, r;
	for(i = 0; i < 9; i++)
		for(j = 0; j < 9; j++)
		{prearray[i][j] = 0;
		postarray[i][j]=0;
		}

srand( time(NULL));
v = 0;
	r = 1;
	c = 1;
while( n > 0)
{if(valid(r, c, v)==true)
	{prearray[r][c]=1;
		postarray[r][c] = v;
	n = n-1;
	}
	v = (rand() % 9) + 1;
	r = rand() % 9;
	c = rand() % 9;
	
	while(valid(r, c, v)==false)
	{v = (rand() % 9) + 1;
	r = rand() % 9;
	c = rand() % 9;

	}
	
	
}
}
/*************************
*	@name print grid
*	@brief was dumb so it configures the array one box at a time
*	@param none
*	@retvalnone
******************/
void Sudoku::printGrid()
{int i, j;

for(i = 0; i < 3; i++)//checking to see if it should print a real number or '*'
{cout << sp;
	for(j = 0; j < 3; j++)
	{if(postarray[i][j] == 0)
		{cout << "*" << sp;
		}else{
			cout << postarray[i][j] << sp;
		}
	}

cout << "|" << sp;
for( j=3 ; j < 6; j++)//checking to see if it should print a real number or '*'
{if(postarray[i][j] == 0){
	cout << "*" << sp;
	}else{
		cout << postarray[i][j] << sp;
	}
}
cout << "|";
for( j=6 ; j < 9; j++)//checking to see if it should print a real number or '*'
{if(postarray[i][j] == 0){
	cout << "*" << sp;
	}else{
		cout << postarray[i][j] << sp;
	}
}
cout << endl;

}
cout << und << "-+" << und << "-+" << und << endl;//sets up the border things
for(i = 3; i < 6; i++)//checking to see if it should print a real number or '*'
{cout << sp;
	for(j = 0; j < 3; j++)
	{if(postarray[i][j] == 0)
		{cout << "*" << sp;
		}else{
			cout << postarray[i][j] << sp;
		}
	}

cout << "|" << sp;
for( j=3 ; j < 6; j++)//checking to see if it should print a real number or '*'
{if(postarray[i][j] == 0){
	cout << "*" << sp;
	}else{
		cout << postarray[i][j] << sp;
	}
}
cout << "|";
for( j=6 ; j < 9; j++)//checking to see if it should print a real number or '*'
{if(postarray[i][j] == 0){
	cout << "*" << sp;
	}else{
		cout << postarray[i][j] << sp;
	}
}
cout << endl;

}
cout << und << "-+" << und << "-+" << und << endl;//sets up the border thing
for(i = 6; i < 9; i++)
{cout << sp;//checking to see if it should print a real number or '*'
	for(j = 0; j < 3; j++)
	{if(postarray[i][j] == 0)
		{cout << "*" << sp;
		}else{
			cout << postarray[i][j] << sp;
		}
	}

cout << "|" << sp;
for( j=3 ; j < 6; j++)//checking to see if it should print a real number or '*'
{if(postarray[i][j] == 0){
	cout << "*" << sp;
	}else{
		cout << postarray[i][j] << sp;
	}
}
cout << "|";
for( j=6 ; j < 9; j++)//checking to see if it should print a real number or '*'
{if(postarray[i][j] == 0){
	cout << "*" << sp;
	}else{
		cout << postarray[i][j] << sp;
	}
}
cout << endl;

}

}
/*************************
*	@name check column
*	@brief makes sure no duplicate number in column
*	@param int j, int v
*	@retval bbol
******************/
bool Sudoku::checkcolumn(int j,int  v)
{
	for (int row = 0; row < 9; row++)
		if(postarray[row][j] == v)	//checks rows
		{
			return true;}
		return false;
}
/*************************
*	@name check row
*	@brief checks that no duplicate number in row
*	@param int i, v
*	@retval bool
******************/
bool Sudoku::checkrow(int i,int v)
{
	for (int row = 0; row < 9; row++)
	{if(postarray[i][row]== v)
		{//checks all the rows
	return true;}}
		return false;
}
/*************************
*	@name checkbox
*	@brief checks to make sure move is legal
*	@param int j, int i, int v
*	@retval bool
******************/
bool Sudoku::checkbox(int j,int i,int v)
{
	for (j = 0; j < 9; j++){
	{for (i =0; i < 9; i++)	//checks the box
		{if(postarray[i][j] == v)
			{
	return true;}}}}
			return false;
}

/*************************
*	@name first
*	@brief finds the first open spot
*	@param int *x, *y;
*	@retval bool
******************/
bool Sudoku::first(int *x,int *y)
{int i, j;

for (i =0; i<9; i++){
		for (j=0; j<9; j++){//finds the first 0 in the prearray
				if(prearray[i][j] == 0)
				{
					*x=i;
					*y=j;
				return true;}
}
}
				return false;
				
}
/*************************
*	@name next
*	@brief finds the next open spot by looking for zeroes in prearray
*	@param int *x, *y
*	@retval bool
******************/
bool Sudoku::next(int *x, int *y)
{int i, j;
i=*x;
j=*y;
for (; i<9; i++){//finding the next open spot
		for (; j<9; j++){
			{if(prearray[i][j] == 0 && i!=*x||j!=*y)
				{
					*x=i;
					*y=j;
				return true;}
			}
		}
		if(i == 9){
		i=0;}
				

}
				return false;
				
}
/*************************
*	@name prev
*	@brief	goes back one spot for backtracking
*	@param int *x *y
*	@retval bool
******************/
bool Sudoku::prev(int *x, int *y)
{int i, j;
i=*x;
j=*y;
for (; j>0; j--){
		for (; i<0; i--){//moves everything back and resets
			{if(prearray[i][j] == 0 && (i!=*x||j!=*y))
				{
					*x=i;
					*y=j;
				return true;}
			}
		}
		if(i == 0){
		i=8;}
				

}
				return false;
				
}
/*************************
*	@name valid
*	@brief test if spot is valid
*	@param int i, j, and v
*	@retval bool
******************/
bool Sudoku::valid(int i,int j,int v)
{
	return (!checkrow(i, v) && !checkcolumn(j, v) 
	&& !checkbox(i-i%3, j-j%3, v));//checks if valid
}
/*************************
*	@name solver 
*	@brief solves the puzzle
*	@param none
*	@retval bool
******************/
bool Sudoku::solver(){
	int i, j, v, backup;
	int *x ,*y;
	x=&i;
	y=&j;
	v=1;
	first(x,y);
	for(;;)
	{
		backup=0;
		if(valid(i, j, v)&& v<=9)
		{cout<<"is valid" <<endl;
			postarray[i][j]=v;
			v++;
		if(next(&i, &j) == 0)
			{if(valid(i, j, v)&& v<=9)//checking to see vaild
				{postarray[i][j] = v;//osting it
				}
				if(i==9)
				{return 1;}
			}
			return 1;
		}
		
		
		else{
			v++;
			if(v>9){
				postarray[i][j]=0;//back tracking
			if(prev(&i, &j)==1){
			v = postarray[i][j]+1;}
		}
		
		
			else{
			return false;}
			
				
	}
			
	
		return false;
	
}
	
}

	
