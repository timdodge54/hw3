#include "sudoku.h"
#include <iostream>

using namespace std;

/*************************
*	@name main
*	@brief gets number of spots to be filled
*	@param none
*	@retval none
******************/
int main()
{
	int n;
	
	cout << "Welcome to Sudoku Solver!!" << endl;//gets number of spots to fill
	cout << "Enter numer of squares to preopulate: ";
	
	cin >> n;
	
	Sudoku s(n);
	s.printGrid();//names and prints grid
	if (s.solver()==true) {
		cout << "Solved!" << endl;//if solvable
		s.printGrid();
	}
	else{
		cout << "Sorry, unsolvable..." << endl;//not solvable
	}
	
	return 0;
}