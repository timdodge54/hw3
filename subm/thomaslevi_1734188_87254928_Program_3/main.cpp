#include "sudoku.h"
#include <iostream>
using namespace std;

/*********************************************************************
* @name		main
* @brief	Creates a sudoku puzzle with a user-determined number of
*			pre-populated squares and solves the puzzle if possible.
* @param	none
* @retval	none
*********************************************************************/

int main(void) {
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	Sudoku s(n);
	s.printGrid();

	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printGrid();
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}

	return 0;
}