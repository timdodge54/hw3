#ifndef SUDOKU_H
#define SUDOKU_H

/*********************************************************************
* @name		Sudoku
* @brief	Contains a randomly generated sudoku puzzle, with
*			functions to solve it and print it
*********************************************************************/

class Sudoku {
public:
	//class constructor prototype
	Sudoku(int n);

	//public member function prototypes
	int solve();
	void printGrid();
	
private:
	//sudoku grid and fixed index
	int grid[9][9] = {0};
	int fixed[9][9] = {0};
	
	//private member function prototypes
	int checkRow(int n, int y);
	int checkCol(int n, int x);
	int checkBox(int n, int x, int y);
	int firstSquareInPlay(int& x, int& y);
	int nextSquareInPlay(int& x, int& y);
	int prevSquareInPlay(int& x, int& y);
};

#endif