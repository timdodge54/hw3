#include <iostream>
#include "sudoku.h"

/*********************************************************************
* @name		Sudoku
* @brief	A constructor for the Sudoku class that initializes a
*			random sudoku puzzle with a given number of pre-populated
*			squares
* @param	number of pre-populated squares
* @retval	none
*********************************************************************/

Sudoku::Sudoku(int n) {
	int v, x, y, spotFound;

	//for each number to be generated
	for (int i = 0; i < n; i++) {
		spotFound = 0;

		//while a possible solution hasn't been found
		while (!spotFound) {
			//generate a random number and position
			v = rand() % 9 + 1;
			x = rand() % 9;
			y = rand() % 9;

			//if it's possible
			if (fixed[x][y] == 0) {
				if (checkRow(v, y) == 0 && checkCol(v, x) == 0 && checkBox(v, x, y) == 0) {
					//mark the spot as fixed and continue
					fixed[x][y] = 1;
					spotFound = 1;
				}
			}
		}
		
		//assign the solution
		grid[x][y] = v;
	}
}

/*********************************************************************
* @name		printGrid
* @brief	Prints the sudoku grid to the screen
* @param	none
* @retval	none
*********************************************************************/

void Sudoku::printGrid() {
	int x;
	int y;

	//for each row
	for (y = 0; y < 9; y++) {
		std::cout << "  ";

		//for each number in the row
		for (x = 0; x < 9; x++) {
			//if the number is 0, print an asterisk
			if (grid[x][y] == 0) {
				std::cout << "*";
			}
			//otherwise, print the number
			else {
				std::cout << grid[x][y];
			}

			//print vertical lines after the 3rd and 6th numbers
			if (x == 2 || x == 5) {
				std::cout << " | ";
			}
			else {
				std::cout << "   ";
			}
		}

		//new line
		std::cout << std::endl;

		//print horizontal lines after the 3rd and 6th rows
		if (y == 2 || y == 5) {
			std::cout << "------------+-----------+------------" << std::endl;
		}
		//fill in vertical line gaps
		else if (y != 8) {
			std::cout << "            |           |            " << std::endl;
		}
	}

	//blank line after
	std::cout << std::endl;
}

/*********************************************************************
* @name		checkRow
* @brief	Checks if a value already exists on a given row and
*			returns a binary yes or no.
* @param	value
* @param	row number
* @retval	answer
*********************************************************************/

int Sudoku::checkRow(int n, int y) {
	int x;

	//for each number in the row
	for (x = 0; x < 9; x++) {
		//if the number in question is found
		if (grid[x][y] == n) {
			//return yes
			return 1;
		}
	}

	//return no
	return 0;
}

/*********************************************************************
* @name		checkCol
* @brief	Checks if a value already exists on a given column and
*			returns a binary yes or no.
* @param	value
* @param	column number
* @retval	answer
*********************************************************************/

int Sudoku::checkCol(int n, int x) {
	int y;

	//for each number in the column
	for (y = 0; y < 9; y++) {
		//if the number in question is found
		if (grid[x][y] == n) {
			//return yes
			return 1;
		}
	}

	//return no
	return 0;
}

/*********************************************************************
* @name		checkBox
* @brief	Checks if a value already exists in a given box and
*			returns a binary yes or no.
* @param	value
* @param	column number
* @param	row number
* @retval	answer
*********************************************************************/

int Sudoku::checkBox(int n, int x, int y) {
	int xl, xr, yt, yb;

	//if the value is in the 1st column of boxes
	if (x >= 0 && x <= 2) {
		//set the x boundaries to the 1st column of boxes
		xl = 0;
		xr = 2;
	}
	//if the value is in the 2nd column of boxes
	else if (x >= 3 && x <= 5) {
		//set the x boundaries to the 2nd column of boxes
		xl = 3;
		xr = 5;
	}
	//if the value is in the 3rd column of boxes
	else if (x >= 6 && x <= 8) {
		//set the x boundaries to the 3rd column of boxes
		xl = 6;
		xr = 8;
	}

	//if the value is in the 1st row of boxes
	if (y >= 0 && y <= 2) {
		//set the y boundaries to the 1st row of boxes
		yt = 0;
		yb = 2;
	}
	//if the value is in the 2nd row of boxes
	else if (y >= 3 && y <= 5) {
		//set the y boundaries to the 2nd row of boxes
		yt = 3;
		yb = 5;
	}
	//if the value is in the 3rd row of boxes
	else if (y >= 6 && y <= 8) {
		//set the y boundaries to the 3rd row of boxes
		yt = 6;
		yb = 8;
	}
	
	//for each row of the value's box
	for (y = yt; y <= yb; y++) {
		//for each value in the row
		for (x = xl; x <= xr; x++) {
			//if the number in question is found
			if (grid[x][y] == n) {
				//return yes
				return 1;
			}
		}
	}

	//return no
	return 0;
}

/*********************************************************************
* @name		firstSquareInPlay
* @brief	Finds the first non-fixed square if said square exists
* @param	column number
* @param	row number
* @retval	whether or not a free square exists
*********************************************************************/

int Sudoku::firstSquareInPlay(int& x, int& y) {
	//for each row
	for (y = 0; y < 9; y++) {
		//for each number in the row
		for (x = 0; x < 9; x++) {
			//if the square is free
			if (fixed[x][y] == 0) {
				//return successfully
				return 1;
			}
		}
	}

	//return unsuccessfully
	return 0;
}

/*********************************************************************
* @name		nextSquareInPlay
* @brief	Finds the next non-fixed square if said square exists
* @param	column number
* @param	row number
* @retval	whether or not there is a next square available
*********************************************************************/

int Sudoku::nextSquareInPlay(int& x, int& y) {
	int startingY = y;

	//for each row, starting on the given one
	for (; y < 9; y++) {
		//for each number in the row, starting after the given x index if
		//it's on the initial row
		for (x = (y == startingY) ? x + 1 : 0; x < 9; x++) {
			//if the square is free 
			if (fixed[x][y] == 0) {
				//return successfully
				return 1;
			}
		}
	}

	//return unsuccessfully
	return 0;
}

/*********************************************************************
* @name		prevSquareInPlay
* @brief	Finds the previous non-fixed square if said square exists
* @param	column number
* @param	row number
* @retval	whether or not there is a previous square available
*********************************************************************/

int Sudoku::prevSquareInPlay(int& x, int& y) {
	int startingY = y;

	//for each row working backwards from the given one
	for (; y >= 0; y--) {
		//for each number in the row working backwards, starting on
		//the given x index if it's on the initial row
		for (x = (y == startingY) ? x - 1 : 8; x >= 0; x--) {
			//if the square is free 
			if (fixed[x][y] == 0) {
				//if the square is a 9
				if (grid[x][y] == 9) {
					//set it to zero and keep going
					grid[x][y] = 0;
				}
				else {
					//otherwise, return successfully
					return 1;
				}
				
			}
		}
	}

	//return unsuccessfully
	return 0;
}

/*********************************************************************
* @name		solve
* @brief	solves the sudoku puzzle if possible
* @param	none
* @retval	whether or not the puzzle was solvable
*********************************************************************/

int Sudoku::solve() {
	int n = 1;
	int x = 0;
	int y = 0;

	//if there is no first square available
	if (!firstSquareInPlay(x, y)) {
		//the puzzle is already solved
		return 1;
	}

	//loop until a value is returned
	while (true) {
		

		//if this is a valid placement
		if (checkRow(n, y) == 0 && checkCol(n, x) == 0 && checkBox(n, x, y) == 0) {
			//set the current square to the current number
			grid[x][y] = n;

			//reset the current number's value
			n = 1;

			//if the next square is off the board
			if (!nextSquareInPlay(x, y)) {
				//the puzzle is solved
				return 1;
			}
		}
		//otherwise
		else {
			//set the current square to the current number
			grid[x][y] = n;

			//increment the current number
			n++;

			//if the current number exceeds possible values
			if (n > 9) {
				//reset the current square to 0
				grid[x][y] = 0;

				//if the previous square is off the board
				if (!prevSquareInPlay(x, y)) {
					//the puzzle is unsolvable
					return 0;
				}
				//otherwise
				else {
					//set the current number to the next increment of the
					//previous square's value
					n = grid[x][y] + 1;
				}
			}
		}
	}
}