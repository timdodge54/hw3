#include "sudoku.h"
#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;


/*****************************************************************************
* @name Sudoku
* @brief receives inputted number from user and generates a randomized legal
*		 sudoku board with that many starting positions filled
* @param receives an integer from main function
* @retval none (sends generated sudoku board to the printGrid function)
*****************************************************************************/


//constructor function to create random pre-filled board
Sudoku::Sudoku(int n)
{
	char random;
	int xplace, yplace;

	//set the random seed
	srand(time(NULL));

	//for loop assigns 'n' number of numbers randomly in the sudoku board
	for (int i = 1; i <= n; i++)
	{
		//gets random number and converts it to a char
		random = (rand() % 9 + 1) + '0';

		//sets x cord random position
		xplace = rand() % 9;

		//sets y cord random position
		yplace = rand() % 9;

		//if it's legal to place the number at the position
		if (noviolation(xplace, yplace, random)) {

			//assign that spot as taken on seperate hidden board
			invisigrid[xplace][yplace] = '1';

			//place that number on the main board
			grid[xplace][yplace] = random;
		}
		else {

			//go back a step in the for loop if it can't place the number
			i -= 1;
		}		
	}

	//print the random generated board to the screen
	printGrid();
}


/*****************************************************************************
* @name solve
* @brief looks at the randomly generated sudoku board and attempts to solve
*		 it using backtracking methods
* @param none (looks at current sudoku board in the class)
* @retval returns a 1 if the board was solved or returns a 0 if not
*****************************************************************************/


//tries to solve the board
int Sudoku::solve() {
	char btrack = '1';
	int solving, placefail = 0, startingx, startingy;
	int firstposset = 1, reassigned = 0;
	int continuefirst = 0, findinglegalspot = 1;

	//these 2 for loops cycle through every position of sudoku board
	for (int xsolve = 0; xsolve < 9; xsolve++) {
		for (int ysolve = 0; ysolve < 9; ysolve++) {
			
			//restart while loop since position has moved
			solving = 1;

			////while loop stops once the current spot gets a legal number
			while (solving) {

				//check if the position has a preset number in it
				if (invisigrid[xsolve][ysolve] == 0) {

					//if there's no sudoku violations in current spot
					if (noviolation(xsolve, ysolve, btrack)) {

						//place the number in the spot
						grid[xsolve][ysolve] = btrack;

						//reset variables tracking placement success
						solving = 0;
						placefail = 0;
						reassigned = 0;

						//if it's the first time placing a number then
						//assign these variables to track what the first
						//position and number are
						if (firstposset == 1) {
							startingx = xsolve;
							startingy = ysolve;
							firstposset = 0;
						}
					}

					//increase number placing by 1
					btrack += 1;					

					//if it's coming back from backtracking make sure btrack
					//variable doesn't go past 9
					if (reassigned == 1) {
						if (btrack == ':') {
							btrack = '1';
							//variable that tracks if placing any of the numbers
							//0-9 was unsuccessful
							placefail = 2;
						}
					}

					//set number placing back to 1 after it gets to 9
					if (btrack == ':') {
						btrack = '1';

						//variable that tracks if placing any of the numbers
						//0-9 was unsuccessful
						placefail += 1;
					}

					//check if there's been a number placed yet
					if (firstposset == 0) {

						//if it's at the starting position and it couldn't
						//place the current number
						if (xsolve == startingx && ysolve == startingy && 
							placefail == 2) {

							//cycle through remaining numbers and see if
							//they're legal
							for (char testbtrack = btrack; 
								testbtrack <= '9'; testbtrack++) {
								if (noviolation(xsolve, ysolve, testbtrack))
								{
									//a legal option remaining was found
									continuefirst = 1;
								}
							}
							if (continuefirst == 0) {
								
								//no more legal options for the first 
								//starting position
								return 0;
							}
						}
					}

					//if it can't place numbers 1-9 in current position
					if (placefail == 2) {

						//if never was able to place the first number
						if (firstposset == 1) {
							
							//first open spot didn't have a solution
							return 0;
						}

						//set a * at the failed position
						grid[xsolve][ysolve] = '*';

						//reset variables
						placefail = 0;
						reassigned = 0;

						//enter while loop
						findinglegalspot = 1;

						//loop backtracks to the next legal position
						while (findinglegalspot == 1) {

							//move position back 1
							ysolve -= 1;

							//move position up grid if off grid left
							if (ysolve < 0) {
								ysolve = 8;
								xsolve -= 1;

								//check if it tries to go above grid
								if (xsolve < startingx) {
									
									//this means there were no legal spots
									//to backtrack to
									return 0;
								}
							}

							//if the spot wasn't prefilled
							if (invisigrid[xsolve][ysolve] != '1') {

								//if the empty spot already has a 9 in it
								if (grid[xsolve][ysolve] == '9') {

									//replace it with a *
									grid[xsolve][ysolve] = '*';
								}
								else {

									//a legal spot was found, endloop
									findinglegalspot = 0;

									//increase number in found position
									btrack = grid[xsolve][ysolve] + 1;

									//set reassigned flag to 1
									reassigned = 1;
								}
							}
						}
					}
				}
				// this means that the spot was pre filled
				else {
					solving = 0;
				}
			}
		}
	}
	
	//if it goes through whole for loop without failing then it was a success
	return 1;
}


/*****************************************************************************
* @name printGrid
* @brief takes the current sudoku board and prints it to the screen
* @param none (looks at current board)
* @retval none (prints the current sudoku board)
*****************************************************************************/


//function to print the sudoku board
void Sudoku::printGrid()
{

	//for loop cycles through every position
	for (int gridi = 0; gridi < 9; gridi++) {
		for (int gridj = 0; gridj < 9; gridj++) {

			//place a | every 3 numbers
			if (gridj == 3 || gridj == 6) {
				cout << "| ";
			}
			else {

				//put a space between the numbers
				cout << "  ";
			}

			//if to put * in unpredetermined places
			if (grid[gridi][gridj] == 0) {
				grid[gridi][gridj] = '*';
			}

			//put space between the numbers
			cout << grid[gridi][gridj] << " ";
		}

		//put large lines every 3 on the x cord to make grid
		if (gridi == 2 || gridi == 5) {
			cout << endl << "------------+-----------+------------" << endl;
		}
		
		//else put | again
		else if (gridi != 8) {
			cout << endl << "            |           |" << endl;
		}
	}

	//formatting
	cout << endl << endl << endl;
}


/*****************************************************************************
* @name noviolation
* @brief checks if the given number can legally be placed in the given 
		 position as defined by sudoku rules
* @param an integer representing the x position in the sudoku board, an 
*		 integer representing the y position in the sudoku board, and a char
*		 that represents the number that is trying to be placed
* @retval returns a 1 if the number can be legally placed and a 0 if it can't
*****************************************************************************/


//function to check if it can place a number on the board
int Sudoku::noviolation(int xplace, int yplace, char random)
{
	int squarenum;

	//if it's trying to place something that isn't a number then fail
	if (random > '9' || random < '1') {
		return 0;
	}

	//if the spot was predetermined then fail
	if (invisigrid[xplace][yplace] == '1') {
		return 0;
	}


	//all of these tell the program which 3x3 grid it's in

	//in top row
	if (xplace == 0 || xplace == 1 || xplace == 2) {

		//in top left 3x3 (square 1)
		if (yplace == 0 || yplace == 1 || yplace == 2) {
			squarenum = 1;
		}

		//in top middle 3x3 (square 2)
		if (yplace == 3 || yplace == 4 || yplace == 5) {
			squarenum = 2;
		}

		//in top right 3x3 (square 3)
		if (yplace == 6 || yplace == 7 || yplace == 8) {
			squarenum = 3;
		}
	}

	//in middle row
	if (xplace == 3 || xplace == 4 || xplace == 5) {

		//in middle left 3x3 (square 4)
		if (yplace == 0 || yplace == 1 || yplace == 2) {
			squarenum = 4;
		}

		//in middle 3x3 (square 5)
		if (yplace == 3 || yplace == 4 || yplace == 5) {
			squarenum = 5;
		}

		//in middle right 3x3 (square 6)
		if (yplace == 6 || yplace == 7 || yplace == 8) {
			squarenum = 6;
		}
	}

	//in bottom row
	if (xplace == 6 || xplace == 7 || xplace == 8) {

		//in bottom left 3x3 (square 7)
		if (yplace == 0 || yplace == 1 || yplace == 2) {
			squarenum = 7;
		}

		//in bottom middle 3x3 (square 8)
		if (yplace == 3 || yplace == 4 || yplace == 5) {
			squarenum = 8;
		}

		//in bottom right 3x3 (square 9)
		if (yplace == 6 || yplace == 7 || yplace == 8) {
			squarenum = 9;
		}
	}

	//if it breaks sudoku rules in the row then fail
	for (int ypos = 0; ypos < 9; ypos++) {
		if (grid[xplace][ypos] == random) {
			return 0;
		}
	}

	//if it breaks sudoku rules in the column then fail
	for (int xpos = 0; xpos < 9; xpos++) {
		if (grid[xpos][yplace] == random) {
			return 0;
		}
	}

	
	//these all check if it breaks sudoku rules in the 3x3

	//square 1
	if (squarenum == 1) {

		//loops through all positions of the 3x3 grid
		for (int smallx = 0; smallx < 3; smallx++) {
			for (int smally = 0; smally < 3; smally++) {

				//if number trying to place equals any of them then fail
				if (random == grid[smallx][smally]) {
					return 0;
				}
			}
		}
	}

	//square 2
	else if (squarenum == 2) {

		//loops through all positions of the 3x3 grid
		for (int smallx = 0; smallx < 3; smallx++) {
			for (int smally = 3; smally < 6; smally++) {

				//if number trying to place equals any of them then fail
				if (random == grid[smallx][smally]) {
					return 0;
				}
			}
		}
	}

	//square 3
	else if (squarenum == 3) {

		//loops through all positions of the 3x3 grid
		for (int smallx = 0; smallx < 3; smallx++) {
			for (int smally = 6; smally < 9; smally++) {

				//if number trying to place equals any of them then fail
				if (random == grid[smallx][smally]) {
					return 0;
				}
			}
		}
	}

	//square 4
	else if (squarenum == 4) {

		//loops through all positions of the 3x3 grid
		for (int smallx = 3; smallx < 6; smallx++) {
			for (int smally = 0; smally < 3; smally++) {

				//if number trying to place equals any of them then fail
				if (random == grid[smallx][smally]) {
					return 0;
				}
			}
		}
	}

	//square 5
	else if (squarenum == 5) {

		//loops through all positions of the 3x3 grid
		for (int smallx = 3; smallx < 6; smallx++) {
			for (int smally = 3; smally < 6; smally++) {

				//if number trying to place equals any of them then fail
				if (random == grid[smallx][smally]) {
					return 0;
				}
			}
		}
	}

	//square 6
	else if (squarenum == 6) {

		//loops through all positions of the 3x3 grid
		for (int smallx = 3; smallx < 6; smallx++) {
			for (int smally = 6; smally < 9; smally++) {

				//if number trying to place equals any of them then fail
				if (random == grid[smallx][smally]) {
					return 0;
				}
			}
		}
	}

	//square 7
	else if (squarenum == 7) {

		//loops through all positions of the 3x3 grid
		for (int smallx = 6; smallx < 9; smallx++) {
			for (int smally = 0; smally < 3; smally++) {

				//if number trying to place equals any of them then fail
				if (random == grid[smallx][smally]) {
					return 0;
				}
			}
		}
	}

	//square 8
	else if (squarenum == 8) {

		//loops through all positions of the 3x3 grid
		for (int smallx = 6; smallx < 9; smallx++) {
			for (int smally = 3; smally < 6; smally++) {

				//if number trying to place equals any of them then fail
				if (random == grid[smallx][smally]) {
					return 0;
				}
			}
		}
	}

	//square 9
	else if (squarenum == 9) {

		//loops through all positions of the 3x3 grid
		for (int smallx = 6; smallx < 9; smallx++) {
			for (int smally = 6; smally < 9; smally++) {

				//if number trying to place equals any of them then fail
				if (random == grid[smallx][smally]) {
					return 0;
				}
			}
		}
	}

	//no violations, can place number at position safely
	return 1;
}