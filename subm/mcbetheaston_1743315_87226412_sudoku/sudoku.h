#pragma once

//Soduku class
class Sudoku {
private:
	//main sudoku board
	char grid[9][9] = {};

	//invisible board set when random generation happens
	char invisigrid[9][9] = {};

public:
	//required functions
	Sudoku(int n);
	int solve();
	void printGrid();

private:
	//function to check for violations
	int noviolation(int xplace, int yplace, char random);
};