/*****************************************************************************
* @name main
* @brief The user enters a number, then the program sends this number to a
*		 different system which generates a legal sudoku board with that
*        many numbers filled in already
* @param a number between 0 and 81
* @retval outputs a printed solved sudoku board if it's possible to solve it
*		  and outputs "Sorry, unsolvable..." if not
*****************************************************************************/

#include "sudoku.h" 
#include <iostream> 

using namespace std;

//given main function as seen in Sudoku rubric
int main()
{
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";

	//get a number from the user, this how many boxes will be prefilled
	cin >> n;

	//create a new member of the class called 's'
	Sudoku s(n);

	//if else statement to check if the random board was able to be solved
	if (s.solve())
	{
		cout << "Solved!" << endl;

		//print the grid to show user if it was solved successfully
		s.printGrid();
	}
	else
	{
		cout << "Sorry, unsolvable..." << endl;
	}

	return 0;
}