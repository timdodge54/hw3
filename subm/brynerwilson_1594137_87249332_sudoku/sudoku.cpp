#include "sudoku.h"
#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;

/*****************************************************************************
* @name 	Sudoku
* @brief	Generates a puzzle with n elements predefined legally
* @param	number of elements to predefine
* @retval	none
*****************************************************************************/
Sudoku::Sudoku(int n)
{//first fill our grid with zeros
	for (int i = 0; i < 9; i++)
	{
		for (int ii = 0; ii < 9; ii++)
		{
			grid[ii][i] = 0;
			untouchable[ii][i] = false;
		}
	}
	//init random # generator
	srand(time(NULL));
	int myNum, r, c;
	//loop to fill grid with n numbers
	for (int j = 0; j < n;)
	{//choose a random number and location
		myNum = rand() % 9 + 1;
		r = rand() % 9;
		c = rand() % 9;
		if (!untouchable[c][r] && !Sudoku::test3x3(c, r, myNum))
		{
			if(!Sudoku::testCol(c, myNum) && !Sudoku::testRow(r, myNum))
			{//if no rules broken, put the number at location
				grid[c][r] = myNum;
				untouchable[c][r] = true;//don't overwrite when we solve
				j++;
			}
		}
	}
}

/*****************************************************************************
* @name 	Sudoku::solve
* @brief	Solves a sudoku puzzle
* @param	none
* @retval	1 on success, 0 on failure
*****************************************************************************/
int Sudoku::solve()
{
	clock_t time = clock();
	int iterations = 0;
	int index = 0;//index of grid location
	int r, c, val;//row, column, value at location
	int is_solved = 0;
	bool flag = false;//true when puzzle solved (or unsolveable)
	bool flag2 = false;//true once we've found a valid number at location
	while (!flag)
	{
		r = index / 9;
		c = index % 9;
		//move to next available location
		while (untouchable[c][r])
		{
			index++;
			r = index / 9;
			c = index % 9;
		}
		val = grid[c][r];

		flag2 = false;
		while (!flag2)
		{
			//increase number to next legal value
			val++;
			if (val == 10)
			{//if no valid number
				//set val to zero
				grid[c][r] = 0;
				do {//move index back to next available spot
					if (grid[c][r] == 9) grid[c][r] = 0;
					index--;
					r = index / 9;
					c = index % 9;
				} while (untouchable[c][r] || grid[c][r] == 9);
				flag2 = true;
				//if we went backwards off grid, the puzzle is unsolveable
				if (index == -1)
				{
					flag = true;
					is_solved = 0;
				}
			}
			else if (!Sudoku::testCol(c, val) && !Sudoku::testRow(r, val) && !Sudoku::test3x3(c, r, val))
			{//if we found a valid number, save and move on
				grid[c][r] = val;
				index++;
				flag2 = true;
			}
		}
		if (index == 81)
		{//if we went off the end of the grid, we won!
			flag = true;
			is_solved = 1;
		}
		iterations++;
		//"if it takes too long, just give up" - Cp. Brant, Boredom Patrol
		if ((float)(clock() - time) / CLOCKS_PER_SEC > 60)
		{
			flag = true;
			is_solved = 0;
		}
		/*if (iterations > 100000) {
			Sudoku::printGrid();
			cout << iterations << '\t' << index << endl;
			getchar();
		}*/
	}
	return is_solved;
}

/*****************************************************************************
* @name 	Sudoku::printGrid
* @brief	prints the sudoku puzzle to the console
* @param	none
* @retval	none
*****************************************************************************/
void Sudoku::printGrid()
{
	for (int i = 0; i < 9; i++)
	{
		cout << '\t';
		if (i > 1 && i % 3 == 0)
		{//divide rows into thirds
			cout << " -  -  -  +  -  -  -  +  -  -  -" << endl << '\t';
		}
		for (int ii = 0; ii < 9; ii++)
		{//ii is x coord, i is y coord
			if (ii > 1 && ii % 3 == 0) cout << " | ";//divide cols into thirds
			if (untouchable[ii][i]) cout << "[" << grid[ii][i] << "]";
			else if (grid[ii][i] == 0) cout << " * ";//if val is zero, print a star
			else cout << " " << grid[ii][i] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

/*****************************************************************************
* @name 	Sudoku::testRow
* @brief	checks to see if number num is found on row
* @param	row number, number to check for
* @retval	1 if found, 0 if not
*****************************************************************************/
bool Sudoku::testRow(int row, int num)
{
	//loop through each value in the row
	for (int i = 0; i < 9; i++)
	{//if location contains value, return true
		if (grid[i][row] == num) return true;
	}//else, return false
	return false;
}

/*****************************************************************************
* @name 	Sudoku::testCol
* @brief	checks to see if number num is found in column
* @param	column number, number to check for
* @retval	1 if found, 0 if not
*****************************************************************************/
bool Sudoku::testCol(int col, int num)
{
	//loop through each value in the column
	for (int i = 0; i < 9; i++)
	{//if location contains value, return true
		if (grid[col][i] == num) return true;
	}//else, return false
	return false;
}

/*****************************************************************************
* @name 	Sudoku::test3x3
* @brief	checks to see if number num is found in one of the 3x3 boxes
* @param	row number, column number, number to check for
* @retval	1 if found, 0 if not
*****************************************************************************/
bool Sudoku::test3x3(int col, int row, int num)
{
	//figure out which box we're in
	int c_min = ((col) / 3) * 3;
	int r_min = ((row) / 3) * 3;
	//loop through values in box
	for (int i = 0; i < 3; i++)
	{
		for (int ii = 0; ii < 3; ii++)
		{//if found, return true
			if (grid[c_min + ii][r_min + i] == num) return true;
		}
	}//else, return false
	return false;
}
