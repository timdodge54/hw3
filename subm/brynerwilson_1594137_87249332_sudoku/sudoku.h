#ifndef SUDOKU_H
#define SUDOKU_H
class Sudoku {
public:
	Sudoku(int n);
	int solve();
	void printGrid();
private:
	int grid[9][9];
	bool untouchable[9][9]; //shows which values are part of original puzzle
	bool testRow(int row, int num);
	bool testCol(int col, int num);
	bool test3x3(int col, int row, int num);
};

#endif

/*************************************

	0 1 2   3 4 5   6 7 8 - column

0	* * * | * * * | * * *
1	* * * | * * * | * * *
2	* * * | * * * | * * *
	- - - + - - - + - - -
3	* * * | * * * | * * *
4	* * * | * * * | * * *
5	* * * | * * * | * * *
	- - - + - - - + - - -
6	* * * | * * * | * * *
7	* * * | * * * | * * *
8	* * * | * * * | * * *
|
r
o
w

*************************************/
