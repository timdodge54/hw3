/*****************************************************************************
* @name 	main
* @brief	Generates and solves sudoku puzzles
* @param	none
* @retval	none
*****************************************************************************/

#include <iostream>
#include "sudoku.h"
using namespace std;

int main(void)
{
	int n;
	//Get number of squares to prepopulate from user
	cout << "How many squares would you like to prepopulate?" << endl;
	cout << "Number of squares: ";
	cin >> n;

	//Generate  the puzzle and print to screen
	Sudoku s(n);
	s.printGrid();

	//Try to solve the puzzle!
	if (s.solve())
	{//If solveable, celebrate!
		cout << "Solved!" << endl;
		s.printGrid();
	}
	else
	{//else if unsolveable, apologize for not meeting expectations
		cout << "Oops, that one's unsolveable. So embarrassing..." << endl;
	}
	return 0;
}