/**
* @name main
* @brief asks for user to fill sudoku puzzle and shows user filled in puzzle
* @param none
* @retval none
**/

#include <iostream>

#include "sudoku.h"

using namespace std;

int main(void)
{
	int n;
	// ask for number of squares to fill in
	cout << "Welcome to Sudoku Solver!" << endl;
	cout << "Enter number of squares you want already filled in: ";
	// get number
	cin >> n;

	// create puzzle and print it
	Sudoku s(n);
	s.printGrid();

	// if solvable
	if (s.solve())
	{
		// show solved puzzle
		cout << "The solved puzzle looks like:" << endl;
		s.printGrid();
	}
	// else
	else
	{
		// say puzzle is unsolvable
		cout << "Sorry, Unsolvable" << endl;
	}

	return 0;
}