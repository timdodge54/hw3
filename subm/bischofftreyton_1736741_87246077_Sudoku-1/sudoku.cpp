#include <iostream>
#include <cstdlib>
#include <ctime>

#include "sudoku.h"

using namespace std;

/**
* @name Sudoku
* @brief create sudoku puzzle with n amount of square filled in
* @param n amount of squares filled in
* @retval none
**/

Sudoku::Sudoku(int n)
{
	int count, i, j, num;
	srand(time(0));
	// loop
	for (count = 0; count != n; count++)
	{
		// generate random number 0-8
		i = rand() % 9;
		j = rand() % 9;
		// if there is an open space
		if (grid[i][j] == 0)
		{
			// generate random number 1-9
			num = rand() % 9 + 1;
			// if that number works
			if (isValid(i, j, num))
			{
				// put number into that spot
				grid[i][j] = num;
			}
			// else
			else
			{
				// lower the count by one
				count--;
			}
		}
		// else
		else
		{
			// lower the count by one
			count--;
		}
	}
	// endloop
}

/**
* @name isValid
* @brief checks to see if a specific number can be put into the puzzle
* @param row number, column number, and number to be put in
* @retval true or false
**/

bool Sudoku::isValid(int row, int col, int num)
{
	int subRow, subCol;
	// loop
	for (int i = 0; i < 9; i++)
	{
		// if number is anywhere else in the same row or column
		if (grid[row][i] == num || grid[i][col] == num)
		{
			// return false
			return false;
		}
	}
	// endloop
	// define 3x3 grid number would be put into
	subRow = row - row % 3;
	subCol = col - col % 3;
	// loop
	for (int i = subRow; i < subRow + 3; i++)
	{
		// loop
		for (int j = subCol; j < subCol + 3; j++)
		{
			// if any spot in that 3x3 grid already has stated number
			if (grid[i][j] == num)
			{
				// return false
				return false;
			}
		}
	}
	// return true
	return true;
}

/**
* @name solve
* @brief uses backtracking to solve puzzle if solving is possible
* @param none
* @retval 1 or a 0. 1 if true and 0 if false
**/

int Sudoku::solve()
{
	int i, j, num;
	// loop
	for (i = 0; i < 9; i++)
	{
		// loop
		for (j = 0; j < 9; j++)
		{
			// if spot it empty
			if (grid[i][j] == 0)
			{
				// loop
				for (num = 0; num < 10; num++)
				{
					// if number is valid
					if (isValid(i, j, num))
					{
						// spot in grid = that number
						grid[i][j] = num;
						// if solve returns true
						if (solve())
						{
							// return true
							return true;
						}
						// else
						else
						{
							// set spot in grid back to zero
							grid[i][j] = 0;
						}
					}
				}
				// endloop
				// return false
				return false;
			}
		}
		// endloop
	}
	// endloop
	// return false
	return true;
}

/**
* @name printGrid
* @brief prints sudoku puzzle
* @param none
* @retval none
**/

void Sudoku::printGrid()
{
	cout << endl;
	// loop
	for (int i = 0; i < 9; i++)
	{
		// loop
		for (int j = 0; j < 9; j++)
		{
			// if spot = 0
			if (grid[i][j] == 0)
			{
				// print '*' instead
				cout << " " << "*" << " " << " ";
			}
			// else
			else
			{
				// print number
				cout << " " << grid[i][j] << " " << " ";
			}
			// if 3x3 grid
			if (j == 2 || j == 5)
			{
				// print '|'
				cout << " | ";
			}
		}
		cout << endl << endl;
		// if 3x3 grid
		if (i == 2 || i == 5)
		{
			// print spaces
			cout << "-------------+--------------+------------" << endl;
			cout << endl;
		}
	}
}