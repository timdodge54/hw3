#ifndef _SUDOKU_
#define _SUDOKU_

class Sudoku {
		int grid[9][9] = { 0 };
	public:
		Sudoku(int n);
		bool isValid(int row, int col, int num);
		int solve();
		void printGrid();
};

#endif