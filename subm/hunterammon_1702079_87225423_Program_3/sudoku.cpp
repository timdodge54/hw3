#include "sudoku.h"
#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>
using namespace std;


Sudoku::Sudoku(int n) {
	for (int x = 0; x < 9; x++) {
		for (int y = 0; y < 9; y++) {
			grid[x][y] = 0; //fill grid with zeros
		}
	}
	
	for (int x = 0; x < n; x++) {//populates grid with n random numbers
		int randx = rand() % 9;
		int randy = rand() % 9;
		int randn = (rand() % 9)+1;
		while (!isValidPlacement(randn, randx, randy) || grid[randx][randy] != 0) {
			randx = rand() % 9;
			randy = rand() % 9;
			randn = (rand() % 9) + 1;
		}
		grid[randx][randy] = -randn;//set values are negative
		
		
	}

}
int Sudoku::solve() {
	int a = 0; 
	int n = 1;
	int count = 0;
	while (a != 81 && a>=0) {

		int x = a - int(a / 9) * 9;//uses single variable to run through grid
		int y = a / 9;
		if (grid[x][y] < 0) {
			a++;
		}
		else if (!isValidPlacement(n, x, y)) {
			if (n == 9) {
				grid[x][y] = 0;
				a -= 1;
				
				while (grid[a - int(a / 9) * 9][int(a / 9)]<0 ||//finds prev valid place
					grid[a - int(a / 9) * 9][int(a / 9)] == 9) {
					if (grid[a - int(a / 9) * 9][int(a / 9)] == 9) {
						grid[a - int(a / 9) * 9][int(a / 9)] = 0;
					}
					a -= 1;
				}
				n = grid[a - int(a / 9) * 9][int(a / 9)]+1;//defines n as prev valid value
			}
			else {
				n++;
			}
		}
		else {
			grid[x][y] = n;
			n = 1;
			a++;
		}
		
		
	}
	if (a < 0) {
		return 0;
	}
	else {
		return 1;
		
	}
}
void Sudoku::printGrid() {
	cout << endl;
	for (int a = 0; a < 3; a++) {
		for (int b = 0; b < 3; b++) {
			cout << "     ";
			for (int c = 0; c < 3; c++) {
				for (int d = 0; d < 3; d++) {
					int num = grid[c * 3 + d][a * 3 + b];
					if (!num) {
						cout << "* ";
					}
					else {
						cout << abs(num) << " ";
					}
				}
				if (c != 2) {
					cout << "| ";
				}
			}
			cout << "\n";
		}
		if (a != 2) {
			cout << "     - - - + - - - + - - - " << endl;
		}
	}
	cout << "\n\n\n";
}
bool Sudoku::isValidPlacement(int n, int x, int y) {
	int good = true;
	int c = x / 3;
		c *= 3;
	int r = y / 3;
		r *= 3;
	for (int xa = 0; xa < 9; xa++) {
		int xpos = xa - (int(xa / 3) * 3);
		int ypos = int(xa / 3);
		if (n == abs(grid[xa][y]) || //Checks columns
			n == abs(grid[x][xa]) || //Checks rows
			n == abs(grid[c+xpos][r+ypos])) { //checks boxes
			good = false;
		}
		
	}
	return good;
}