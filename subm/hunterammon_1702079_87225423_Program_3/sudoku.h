
#ifndef SUDUKU_H
#define SUDOKU_H
class Sudoku {
public:
	Sudoku(int n);
	int solve();
	void printGrid();
private:
	bool isValidPlacement(int n, int x, int y);
	int grid[9][9];
};
#endif