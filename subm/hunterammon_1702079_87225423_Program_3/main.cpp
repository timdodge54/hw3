#include <iostream>
#include <cstdlib>
#include <ctime>
#include "sudoku.h"


using namespace std;


int main(void) {
	srand(time(NULL));
	int preNum;
	
		cout << "Enter number of prepoulated squares: ";
		cin >> preNum;
		if (preNum > 81 || preNum < 0) {
			cout << "Invalid entry" << endl;
			return 0;
		}
		Sudoku s(preNum);
		s.printGrid();
		if (s.solve()) {
			cout << "Solved!" << endl;
			s.printGrid();
		}
		else {
			cout << "Sorry, unsolvable..." << endl;
		}
	
	return 0;
}