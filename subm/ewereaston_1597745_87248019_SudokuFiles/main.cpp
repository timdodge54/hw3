/*************************************************************************
* @name main
* @brief generates and solves a sudoku puzzle
* @param none
* @retval none
*************************************************************************/

#include "sudoku.h"



int main(void)
{
	int n;

	//print welcome message and ask for input
	cout << "Welcome to SudokuSolver!!" << endl; 
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;
	try //throw exception if number entered is too big
	{
		if (n > 81)
		{
			throw (1);
		}
	}
	catch (int e)
	{
		cout << "There are only 81 squares in this puzzle. ";
		cout << "Enter a lower number." << endl;
		return 0;
	}

	Sudoku puzzle(n); //construct object puzzle
	puzzle.printGrid(); //print prepopulated grid
	cout << endl << endl;

	if (puzzle.solve()) //try to solve.. 
	{
		cout << "Solved!" << endl; //if it can be solved print it
		puzzle.printGrid();
		cout << endl;
	}
	else
	{	//if it can't be solved print sorry
		cout << "Sorry, unsolvable..." << endl;
	}

	return 0;
}
