#include "sudoku.h"

/*************************************************************************
* @name Sudoku
* @brief Constructor for Sudoku class
* @param number of squares to prepopulate
* @retval  none
*************************************************************************/
Sudoku::Sudoku(int n)
{
	srand((unsigned)time(NULL));

	int x, y, value, i = 0;

	
		for (i = 0; i < n;)
		{
			x = rand() % 9;	//get random digit for x position
			y = rand() % 9; // get random digit for y position
			value = ((rand() % 9) + 1); // get random digit 1-9 for value

			if (a[x][y] != 0) //check to make sure box isn't already filled
			{
				//  if box is already filled do nothing
			}
			else
			{
				if (checkbox(x, y, value))	//check 3x3 boxes
				{
					if (checkcols(x, y, value)) //check columns 
					{
						if (checkrows(x, y, value)) //check rows
						{
							a[x][y] = value; //if no duplicates found, fill it
							b[x][y] = 1;
							i++;			// count how many boxes have been filled
						}
					}
				}
				
			}
		}
}




/*************************************************************************
* @name printGrid
* @brief prints the sudoko puzzle to screen
* @param 
* @retval none
*************************************************************************/
void Sudoku::printGrid() 
{
	int i, row;

	for (row = 0; row <= 8; row++) //print every row and column 
	{
		cout << endl;
		if (row == 3) //print dashes
		{
			cout << "---------+";
			cout << "---------+";
			cout << "---------";
			cout << endl;
		}

		if (row == 6) //print dashes
		{
			cout << "---------+";
			cout << "---------+";
			cout << "---------";
			cout << endl;
		}

		for (i = 0; i <= 8; i++) 
		{
			if (a[row][i] == 0)
			{
				cout << " " << "*" << " "; //print zeros as *
			}
			else
			{
				cout << " " << a[row][i] << " ";
			}
			if (i == 2) { cout << "|"; }
			if (i == 5) { cout << "|"; } //print vertical bars
		}
	}
}




/*************************************************************************
* @name printtoscreenB
* @brief prints secondary binary array to screen
* @param
* @retval none
*************************************************************************/
void Sudoku::printtoscreenB()
{
	int i, row;

	for (row = 0; row <= 8; row++) //loop to print every row and column
	{
		cout << endl;
		if (row == 3)
		{
			cout << "---------+"; //print dashes
			cout << "---------+";
			cout << "---------";
			cout << endl;
		}

		if (row == 6)
		{
			cout << "---------+"; //print dashes
			cout << "---------+";
			cout << "---------";
			cout << endl;
		}

		for (i = 0; i <= 8; i++)
		{
			cout << " " << b[row][i] << " ";
			if (i == 2) { cout << "|"; }
			if (i == 5) { cout << "|"; }
		}
	}
}


/*************************************************************************
* @name checkbox
* @brief checks the 3x3 boxes for duplicate numbers
* @param x coordinate, y coordinate, value to be placed
* @retval true or false
*************************************************************************/
int Sudoku::checkbox(int xpos, int ypos, int val)
{
	int col, row, mincol, maxcol, minrow, maxrow;

	row = xpos;		//get row position input
	col = ypos;		//get col position input

	mincol = ((col / 3) * 3);	//determine which box to check
	maxcol = mincol + 2;
	minrow = ((row / 3) * 3);
	maxrow = minrow + 2;
	
	for (col = mincol; col <= maxcol; col++)
	{
		for (row = minrow; row <= maxrow; row++)
		{
			if (a[row][col] == val)		//check each position in the box
			{
				return 0; //return 0 if that number already exists in the box
			}
		}
	}

	return 1; //return 1 if that number isn't already in the box
}





/*************************************************************************
* @name checkcols
* @brief checks each column in the row for duplicate numbers
* @param x coordinate, y coordinate, value to be placed
* @retval true or false
*************************************************************************/
int Sudoku::checkcols(int xpos, int ypos, int val)
{
	int col;

	for (col = 0; col <= 8; col++) //walk through each column
	{
		if (a[xpos][col] == val)	// check for duplicate
		{
			return 0;	//if duplicate return 0
		}
	}
	return 1; //if no duplicate return 1
}



/*************************************************************************
* @name checkrows
* @brief checks each row in the column for duplicate numbers
* @param x coordinate, y coordinate, value to be placed
* @retval true or false
*************************************************************************/
int Sudoku::checkrows(int xpos, int ypos, int val)
{
	int row;

	for (row = 0; row <= 8; row++)	//walk through each row
	{
		if (a[row][ypos] == val) // check for duplicate
		{
			return 0; // if duplicate return 0
		}
	}
	return 1; //if no duplicate return 1
}



/*************************************************************************
* @name solve
* @brief attempts to solve the Sudoku puzzle
* @param none
* @retval 1 if solvable, 0 if not solvable
*************************************************************************/
int Sudoku::solve()
{
	int n, x, y; // n is the number that we will test out

	x = 0;
	y = 0;
	n = 1; //start testing with 1

	if (b[x][y] != 0) //check if first spot isn't open
	{
		if (findnextzero(x, y)) //if it isn't, find next spot that is
		{
			return 1; //if we go off the end of the grid, success!
		}
	}

	while (n < 10)
	{
		//check for violations in the current position
		if ((checkbox(x, y, n)) && (checkcols(x, y, n)) && checkrows(x, y, n))
		{
			a[x][y] = n; //if no violations, place the number
			n = 1;
			if (findnextzero(x, y)) //find next spot 
			{
				return 1; //if off grid, success!
			}
		}
		else //if violations occur..
		{
			n++; //try the next number up 
			if (n == 10) //if the next number up is 10
			{
				a[x][y] = 0; //put a zero in the current spot
				if (findprevzero(x, y) == 0) //backtrack to previous box
				{
					return 0; //if off grid, fail.
				}
				else
				{
					while (a[x][y] == 9) //if findprevzero finds a nine...
					{
						a[x][y] = 0; //set it to zero also
						if (findprevzero(x, y) == 0) //try again to find prev
						{
							return 0; //if off grid, fail.
						}
					}
					n = a[x][y] + 1; //increase previous box by 1
				}
			}
		}
	}
}




/*************************************************************************
* @name findnextzero
* @brief find the next open square for editing
* @param the current x and y values 
* @retval 1 if off the grid(success), 0 not off the grid
*************************************************************************/
int Sudoku::findnextzero(int& xpos, int& ypos)
{
	
	while ((xpos < 9) && (ypos < 9))
	{
		if (ypos == 8) //if y is at the end of the grid...
		{
			xpos++; //move down a row...
			ypos = 0; //reset y to zero.
		}
		else //if y isnt at the end of the grid...
		{
			ypos++; //move y one spot to the right. 
		}

		if (xpos > 8) //if x pos is now off the grid...
		{
			return 1; //success!
		}

		if (b[xpos][ypos] == 0) //if the new spot is a zero...
		{
			return 0; //quit
		}
	}
}



/*************************************************************************
* @name findprevzero
* @brief find the previous square to edit
* @param the current x and y values
* @retval 1 if not of the grid, 0 if off the grid(fail)
*************************************************************************/
int Sudoku::findprevzero(int& xpos, int& ypos)
{

	while ((xpos < 9) && (ypos < 9))
	{

		if (ypos == 0) //if y is at the beginning of the grid...
		{
			xpos--; //move up a row
			ypos = 8; //reset y to 8.
		}
		else //if y isn't at the beginning of the grid...
		{
			ypos--; //move y one spot to the left. 
		}

		if (xpos < 0) //if x pos is now less than zero...
		{
			return 0; //we went off the grid(FAIL)
		}

		if (b[xpos][ypos] == 0) //if the new spot is a zero...
		{
			return 1; //quit
		}
	}
}
