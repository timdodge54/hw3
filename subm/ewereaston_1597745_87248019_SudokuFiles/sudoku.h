#ifndef SUDOKU_H
#define SUDOKU_H

#include <ctime>
#include <cstdlib>
#include <iostream>

using namespace std;

class Sudoku {
private:
	int a[9][9] = { 0 };
	int b[9][9] = { 0 };

public:
	Sudoku(int n);
	int solve();
	void printGrid();
	void printtoscreenB();
	int checkbox(int xpos, int ypos, int val);
	int checkcols(int xpos, int ypos, int val);
	int checkrows(int xpos, int ypos, int val);
	int findnextzero(int &xpos, int &ypos);
	int findprevzero(int& xpos, int& ypos);
};

#endif
