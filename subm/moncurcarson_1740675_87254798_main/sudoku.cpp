/*********************************************************************
* @name sudoku.ccp
* @brief location of functions
* @param none
* @retval  none
*********************************************************************/
#include <iostream>
#include <fstream>
#include <cmath>
#include "sudoku.h"

using namespace std;
int matrix[9][9];

/*********************************************************************
* @name sudoku
* @brief populates n squares of the 9x9 matrix with legal values
* @param none
* @retval  none
*********************************************************************/
	Sudoku::Sudoku(int start_num)
	{
		srand(time(NULL));
		int r, c, k, num, num2, value;
		

		for (r = 0; r < 9; r++)				// initalize matrix
		{
			for (c = 0; c < 9; c++)
			{								// set each spot to zero
				matrix[r][c] = 0;
						
			}
		}							// for number of inputs requested,
		for (k = 0; k < start_num; k++)		
		{					// generate random coordinates and numbers 
			value = (rand() % 9) + 1;	
			num = (rand() % 9);			// between 1 and 9 
			num2 = (rand() % 9);
			if (open_spot(r, c, value) && matrix[num][num2] == 0) 
			{	// if choosen spot is legal("0") and the value passes the 
						// row, column, and box tests, assign value to spot
				matrix[num][num2] = value;		
				
			}
			else
				k--;		// if chosen spot fails, don't iterate int k
									// and retry with new random values
		}
	}
/*********************************************************************
* @name solve
* @brief produces a valid solution to the generated puzzle
* @param none
* @retval  none
*********************************************************************/
	
	bool Sudoku::solve() 
	{
		int r = 0, c = 0, value;
		if (!firstspot(r, c))
			return true;     //when all places are filled
		for (int value = 1; value <= 9; value++) {
			//valid numbers are 1 - 9
			if (open_spot(r, c, value)) {
				//check validation, if yes, put the number in the grid
				matrix[r][c] = value;
				if (solve())
					//recursively go for other rooms in the grid
					return true;
				// the core of backtracking
				matrix[r][c] = 0;
			//turn to unassigned space when conditions are not satisfied
			}
		}
		return false;
	}
/*********************************************************************
* @name print_grid
* @brief prints the 9 x 9 grid 
* @param none
* @retval  none
*********************************************************************/
	 void Sudoku::print_grid()
	 {
									 //print the sudoku grid after solve
	
			 for (int r = 0; r < 9; r++) 
			 {
				 for (int c = 0; c < 9; c++)
				 {					// any O value becomes an astrix *
					 if (matrix[r][c] == 0)		
					 {
						 cout << "*";
					 }
					 else
					 {
						 cout << matrix[r][c];
					 }
					 cout << " ";
					 if (c % 3 == 2 && c < 8) {
						 cout << "| ";
					 }
				 }
				 cout << endl;

				 if (r % 3 == 2 && r < 8)
				 {
					 cout << "------+-------+-------" << endl;
				 }
			 }

	 }

/*********************************************************************
* @name check_row
* @brief 
* @param none
* @retval  none
*********************************************************************/
	bool Sudoku::check_row(int r, int value)
	{					//if the passed in value already exists, return true
						//if not, return false to open_spot function
		for (int c = 0; c < 9; c++)		
			if (matrix[r][c] == value)
				return true;
		return false;
	}

/*********************************************************************
* @name check_col
* @brief
* @param none
* @retval  none
*********************************************************************/
	bool Sudoku::check_col(int c, int value)
	{					//if the passed in value already exists, return true
						//if not, return false to open_spot function
		for (int r = 0; r < 9; r++)		
			if (matrix[r][c] == value)
				return true;
		return false;
	}

/*********************************************************************
* @name check_box
* @brief
* @param none
* @retval  none
*********************************************************************/
	bool Sudoku::check_box(int initial_row, int initial_column, int value)
	{					//if the passed in value already exists, return true
								//if not, return false to open_spot function
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 3; c++)
				if (matrix[r + initial_row][c + initial_column] == value)
					return true;
		return false;
	}

/*********************************************************************
* @name avalible_spot
* @brief
* @param none
* @retval  none
*********************************************************************/

	bool Sudoku::open_spot(int r, int c, int value)
	{			//checks to see if the spot and value is legal (no conflicts)
		return !check_row(r, value) && !check_col(c, value) 
			&& !check_box(r - r % 3, c - c % 3, value);
	}

/*********************************************************************
* @name nextspot
* @brief
* @param none
* @retval  none
*********************************************************************/
	bool Sudoku::firstspot(int &r, int &c)
	{
									//hunts down free/legal spot
		for (r = 0; r < 9; r++)
			for (c = 0; c < 9; c++)
				if (matrix[r][c] == 0)
					return true;
		return false;
	}

