#ifndef SUDOKU_H
#define SUDOKU_H

class Sudoku {
public:
	Sudoku(int start_num); // constructor
	bool solve();
	void print_grid();

private:
	bool check_row(int r, int value);
	bool check_col(int c, int value);
	bool check_box(int initial_row, int initial_column, int value);
	bool open_spot(int r, int c, int value);
	

	bool firstspot(int &r, int &c);
	

	int matrix[9][9];
	
};
#endif
