/*********************************************************************
* @name main
* @brief generates a sudoku puzzle
* @param none
* @retval  none
*********************************************************************/
#include "sudoku.h" 
#include <iostream> 

using namespace std;

int main()
{
	int start_num;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> start_num;

	Sudoku s(start_num);
	s.print_grid();

	if (s.solve())
	{
		cout << "Solved!" << endl;
		s.print_grid();
	}
	else
	{
		cout << "Sorry, unsolvable..." << endl;
	}
	
	
	return 0;
}