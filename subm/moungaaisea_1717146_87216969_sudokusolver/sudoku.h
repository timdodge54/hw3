class Sudoku
{
	private:
	char game[9][9];
	int isVaccant(int x, int y);
	int xCheck(char c, int y);
	int yCheck(char c, int x);
	int GridCheck(char c, int x, int y);
	
	public:
	Sudoku(int n);
	int solve(int x, int y);
	void printGrid();
	
};