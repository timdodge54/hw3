#include "sudoku.h"
#include <iostream>
#include <cstdlib>

using namespace std;

/********************************************************************* 
* @name Sudoku::xCheck
* @brief checks validity of x axis
* @param char c, int y
* @retval  int 
*********************************************************************/

int Sudoku::xCheck(char c, int y)
{
	//set variables
	int flag = 1, i;
	//going through the y axis to check validity
	for (i = 0; i < 9; i++)
	{
		if(c == game[y][i])
		{
			flag = 0;
		}
	}
	//returns t/f
	return flag;
}

/********************************************************************* 
* @name Sudoku::yCheck
* @brief checks validity of y axis
* @param char c, int x
* @retval  int 
*********************************************************************/

int Sudoku::yCheck(char c, int x)
{
	//set variables
	int flag = 1, i;
	//going through the y axis to check validity
	for (i = 0; i < 9; i++)
	{
		if(c == game[i][x])
		{
			flag = 0;
		}
	}
	//returns t/f
	return flag;
}

/********************************************************************* 
* @name Sudoku::GridCheck
* @brief checks validity of the grid
* @param char c, int x, int y
* @retval  int 
*********************************************************************/

int Sudoku::GridCheck(char c, int x, int y)
{
	int flag = 1, i, j, g[9];
	//setting test statements for each of the 9 subgrids
	//Grid 1
	if(x >= 0 && x <= 2)
	{
		if(y >= 0 && y <= 2)
		{
			g[0] = 1;
			for (i = 0; i <= 2; i++)
			{
				for (j = 0; j <= 2; j++)
				{
					if(c == game[i][j])
					{
						flag = 0;
					}
				}
			}
		}
	}
	
	//Grid 2
	if(x >= 3 && x <= 5)
	{
		if(y >= 0 && y <= 2)
		{
			g[1] = 1;
			for (i = 3; i <= 5; i++)
			{
				for (j = 0; j <= 2; j++)
				{
					if(c == game[i][j])
					{
						flag = 0;
					}
				}
			}
		}
	}	

	//Grid 3
	if(x >= 6 && x <= 8)
	{
		if(y >= 0 && y <= 2)
		{
			g[2] = 1;
			for (i = 6; i <= 8; i++)
			{
				for (j = 0; j <= 2; j++)
				{
					if(c == game[i][j])
					{
						flag = 0;
					}
				}
			}
		}
	}

	//Grid 4
	if(x >= 0 && x <= 2)
	{
		if(y >= 3 && y <= 5)
		{
			g[3] = 1;
			for (i = 0; i <= 2; i++)
			{
				for (j = 3; j <= 5; j++)
				{
					if(c == game[i][j])
					{
						flag = 0;
					}
				}
			}
		}
	}	
	
	//Grid 5
	if(x >= 3 && x <= 5)
	{
		if(y >= 3 && y <= 5)
		{
			g[4] = 1;
			for (i = 3; i <= 5; i++)
			{
				for (j = 3; j <= 5; j++)
				{
					if(c == game[i][j])
					{
						flag = 0;
					}
				}
			}
		}
	}	
	
	//Grid 6
	if(x >= 6 && x <= 8)
	{
		if(y >= 3 && y <= 5)
		{
			g[5] = 1;
			for (i = 6; i <= 8; i++)
			{
				for (j = 3; j <= 5; j++)
				{
					if(c == game[i][j])
					{
						flag = 0;
					}
				}
			}
		}
	}	
	
	//Grid 7
	if(x >= 0 && x <= 2)
	{
		if(y >= 6 && y <= 8)
		{
			g[6] = 1;
			for (i = 0; i <= 2; i++)
			{
				for (j = 6; j <= 8; j++)
				{
					if(c == game[i][j])
					{
						flag = 0;
					}
				}
			}
		}
	}	
	
	//Grid 8
	if(x >= 3 && x <= 5)
	{
		if(y >= 6 && y <= 8)
		{
			g[7] = 1;
			for (i = 3; i <= 5; i++)
			{
				for (j = 6; j <= 8; j++)
				{
					if(c == game[i][j])
					{
						flag = 0;
					}
				}
			}
		}
	}
	
	//Grid 9
	if(x >= 6 && x <= 8)
	{
		if(y >= 6 && y <= 8)
		{
			g[8] = 1;
			for (i = 6; i <= 8; i++)
			{
				for (j = 6; j <= 8; j++)
				{
					if(c == game[i][j])
					{
						flag = 0;
					}
				}
			}
		}
	}
	return flag;
}

/********************************************************************* 
* @name Sudoku::Sudoku
* @brief creates the grid with n random legal values
* @param int n
* @retval none 
*********************************************************************/

Sudoku::Sudoku(int n)
{
	int i, j, xc, yc, gc, vc;
	//initializing  to *
	for(i=0;i<9;i++)
	{
		for(j=0;j<9;j++)
		{
			game[i][j] = '*';
		}
	}
	
	//setting random generator
	srand((unsigned) time(NULL));
	int randomxy = rand() % 9;
	char randomnum = 48 + (rand() % 9);
	
	//for n times, set random values to random coordinates
	for(i=0;i<n;)
	{
		int randomx = rand() % 9;
		int randomy = rand() % 9;
		char randomnum = 49 + (rand() % 9);
		
		//assigning cheking values for if statements
		xc = xCheck(randomnum, randomx);
		yc = yCheck(randomnum, randomy);
		gc = GridCheck(randomnum, randomx, randomy);
		
		//checks if the place is vaccant
		vc = isVaccant(randomx, randomy);
		
		//if the value passes all x,y,grid,vaccant checks, place the value
		if(xc==1)
		{
			if(yc==1)
			{
				if(gc==1)
				{
					if(vc==1)
					{
						//passes all tests, number placed
						game[randomx][randomy] = randomnum;
						//cout << endl << "++++++++++++++++++" << endl;
						//printGrid();
						i++;
					}
				}
			}
		}
	}
}

/********************************************************************* 
* @name Sudoku::printGrid
* @brief prints the grid of sudoku
* @param none
* @retval none 
*********************************************************************/

void Sudoku::printGrid()
{
	int i, j;
	//prints top part of game
	for(j=0;j<3;j++)
	{
		for(i=0;i<9;i++)
		{
			cout << game[i][j];
			if(i==2)
			{
				cout << '|';
			}
			if(i==5)
			{
				cout << '|';
			}
		}
		cout << endl;
	}
	//separates into middle
	cout << "-----------" << endl;
	//prints middle part of game
	for(j=3;j<6;j++)
	{
		for(i=0;i<9;i++)
		{
			cout << game[i][j];
			if(i==2)
			{
				cout << '|';
			}
			if(i==5)
			{
				cout << '|';
			}
		}
		cout << endl;
	}
	
	//separates into bottom
	cout << "-----------" << endl;
	//prints bottom part of game
	for(j=6;j<9;j++)
	{
		for(i=0;i<9;i++)
		{
			cout << game[i][j];
			if(i==2)
			{
				cout << '|';
			}
			if(i==5)
			{
				cout << '|';
			}
		}
		cout << endl;
	}
	
}
/********************************************************************* 
* @name Sudoku::isVaccant
* @brief checks if the place is vaccant
* @param int x, int y
* @retval int torf
*********************************************************************/

int Sudoku::isVaccant(int x, int y)
{
	int torf = 0;
	if(game[x][y] == '*')
	{
		torf = 1;
	}
	
	return torf;
}

/********************************************************************* 
* @name Sudoku::solve
* @brief solves the puzzle if possible
* @param int x, int y
* @retval int
*********************************************************************/

int Sudoku::solve(int x, int y)
{
	
	//if solved return true
	if(y==9)
	{
		return 1;
	}
	//setting variables
	int i = (x + 1) % 9;
	int j;
	//if x = 8 move to next position
	if(x==8)
	{
		j=(y+1);
	}
	else
	{
		j=y;
	}
	int xc, yc, gc;
	char test;
	
	//check for valid nodes
	if(game[x][y]!='*')
	{
		return solve(i,j);
	}
	
	
	
	//running through numbers 1-9 and checking them
	for(test=49;test<=57;test++)
	{
		xc = xCheck(test, x);
		yc = yCheck(test, y);
		gc = GridCheck(test, x, y);
		//if tests fail, continue
		if((xc + yc + gc)!=3)
		{
			continue;
		}
		//if succeeds, place value and call again to see if solved
			game[x][y] = test;
			if(solve(i,j))
			{
				return 1;
			}
			//return values to default if soln did not work
			game[x][y] = '*';
	}
	//return false if not solvable
	return 0;
}