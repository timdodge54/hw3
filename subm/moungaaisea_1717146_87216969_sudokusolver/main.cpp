/********************************************************************* 
* @name main
* @brief prints and solves (if possible) a sudoku game
* @param none 
* @retval  none 
*********************************************************************/
#include "sudoku.h" 
#include <iostream> 
 
using namespace std; 
 
int main() 
{ 	
	//begin
	//sets variable of the number for the prepopulation constructor
	int n; 
 
	//asks user for number
	cout << "Welcome to SudokuSolver!!" << endl; 
	cout << "Enter number of squares to prepopulate: "; 
	cin >> n; 
  
	//sets random sudoku prepopulation
	Sudoku s(n); 
	
	//prints the sudoku before its solved
	s.printGrid();

	//if solvable, prints solution
	if (s.solve(0,0))
	{ 
		cout << "Solved!" << endl; 
		s.printGrid(); 
	} 
	else //else it will print unsolvable
	{ 
		cout << "Sorry, unsolvable..." << endl; 
	} 
	
	//end
	return 0; 
} 