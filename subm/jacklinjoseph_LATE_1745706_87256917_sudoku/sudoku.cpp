#include "sudoku.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

/******************************************************************************
* @name		Sudoku
* @brief	constructs sudoku puzzle.
* @param	number of prepopulated squares.
* @retval	none
******************************************************************************/
Sudoku::Sudoku(int start_num)
{
	int x, y, n;
	srand(time(NULL));

	//Loops until it has generated the number of random values user asked for.
	for (int i = 0; i < start_num;)
	{
		//Generate random position and value.
		x = rand() % 9;
		y = rand() % 9;
		n = rand() % 9+1;

		//Checks to make sure that random spot hasn't already been used.
		if (fixed[x][y] == 0)
		{
			//Checks to make sure it is valid
			if (checkRow(n, y) == 0)
			{
				if (checkCol(n, x) == 0)
				{
					if (checkBox(n, x, y) == 0)
					{
						//Places value and marks it fixed.
						grid[x][y] = n;
						fixed[x][y] = 1;
						i++;
					}
				}
			}
		}
	}
	//Print prepopulated Grid
	printGrid();
}
/******************************************************************************
* @name		printGrid
* @brief	prints the sudoku puzzle.
* @param	none
* @retval	none
******************************************************************************/
void Sudoku::printGrid()
{
	int i, j;

	std::cout << std::endl;

	//Loops until entire grid has been printed.
	for (i = 0; i < 9; i++)
	{
		std::cout << std::endl;

		//Prints horizontal bars after the third & sixth values in each column.
		if ((i == 3) || (i == 6))
		{
			for (int k = 0; k < 31; k++)
			{
				//Prints cross sections when needed.
				if (k == 9 || k == 21)
				{
					std::cout << "+";
				}
				else
				{
					std::cout << "-";
				}
			}
			std::cout << std::endl;
		}
		//Prints values in according location.
		for (j = 0; j < 9; j++)
		{
			//If the value is zero prints * in place.
			if (grid[i][j] == 0)
			{
				std::cout << "*  ";
			}
			//Else prints the value.
			else
			{
				std::cout << grid[i][j] << "  ";
			}
			//If after the third and sixth value in the row print vertical bar.
			if ((j == 2) || (j == 5))
			{
				std::cout << "|  ";
			}
		}
	}
	std::cout << std::endl << std::endl;

}
/******************************************************************************
* @name		checkRow
* @brief	checks if check value is already in row.
* @param	int val, y
* @retval	True or False int 1 or 0
******************************************************************************/
int Sudoku::checkRow(int val, int y)
{
	//Loops until entire row has been checked.
	for (int x = 0; x < 9; x++)
	{
		//If the value already exists in the row throw error.
		if (grid[x][y] == val)
		{
			return 1;
		}
	}
	//If the value doesnt exist in row exit without error.
	return 0;
}
/******************************************************************************
* @name		checkCol
* @brief	checks if check value is already in column.
* @param	int val, x
* @retval	True or False int 1 or 0
******************************************************************************/
int Sudoku::checkCol(int val, int x)
{
	//Loops until entire column has been checked.
	for (int y = 0; y < 9; y++)
	{
		// If the value already exists in the column throw error.
		if (grid[x][y] == val)
		{
			return 1;
		}
	}
	//If the value doesnt exist in column exit without error.
	return 0;
}
/******************************************************************************
* @name		checkBox
* @brief	checks if check value is already in box.
* @param	int val, x , y
* @retval	True or False int 1 or 0
******************************************************************************/
int Sudoku::checkBox(int val, int x, int y)
{
	int col, row;
	int mincol = (x / 3) * 3;
	int maxcol = mincol + 2;
	int minrow = (y / 3) * 3;
	int maxrow = minrow + 2;

	//Loops until entire box has been checked.
	for (col = mincol; col <= maxcol; col++)
	{
		//Loops until rows in box have been checked.
		for (row = minrow; row <= maxrow; row++)
		{
			//If the value exists in the rows throw error.
			if (grid[col][row] == val)
			{
				return 1;
			}
		}
	}
	//If the vlaue doesnt exist in the box exit without error.
	return 0;
}
/******************************************************************************
* @name		solve
* @brief	solves sudoku puzzle
* @param	none
* @retval	True or False int 1 or 0
******************************************************************************/
int Sudoku::solve()
{
	int n = 1;
	char f;
	//Find first playable square.
	if (findFirstSquareInPlay() == 0)
	{
		return 1;
	}

	//Loop until puzzle is solved
	while (col <= 8)
	{
		//Check if checking value has gotten to 10.
		if (n == 10)
		{
			//Set current square to 0 and find the previous sqaure in play.
			grid[col][row] = 0;
			row = row - 1;
			//If previous square is off the board solving failed.
			if (findPrevSquareInPlay() == 0)
			{
				return 0;
			}
			//Else incriment the previous square.
			else
			{
				n = (grid[col][row] + 1);
			}
		}
		else
		{
			//If check value doesnt work in row incriment check value.
			if (checkRow(n, row) == 1)
			{
				n++;
			}
			else
			{
				//If check value doesnt work in column incriment check value.
				if (checkCol(n, col) == 1)
				{
					n++;
				}
				else
					//If check value doesnt work in box incriment check value.
				{
					if (checkBox(n, col, row) == 1)
					{
						n++;
					}
					//Else place the value in square and set check value to 1.
					else
					{
						grid[col][row] = n;
						n = 1;
						//Find the next quare in play.
						if (findNextSquareInPlay() == 0)
						{
							//If next square is off the board puzzle solved.
							return 1;
						}
					}
				}
			}
		}
	}
}
/******************************************************************************
* @name		findFirstSquareInPlay
* @brief	finds first square in play.
* @param	none
* @retval	True or False int 1 or 0
******************************************************************************/
int Sudoku::findFirstSquareInPlay()
{
	//Loops until first square has been found.
	for (int i = 0; i < 9; i++)
	{
		//Loops until all squares in row have been checked.
		for (int j = 0; j < 9; j++)
		{
			//If square isnt fixed mark it as the first square.
			if (fixed[i][j] == 0)
			{
				col = i;
				row = j;
				return 1;
			}
		}
	}
	return 0;
}
/******************************************************************************
* @name		findNextSquareInPlay
* @brief	finds Next square in play.
* @param	none
* @retval	True or False int 1 or 0
******************************************************************************/
int Sudoku::findNextSquareInPlay()
{
	int j = (row + 1);
	//Loop until next square has been found
	for (int i = col; i < 9; i++)
	{
		//Loop until all squares in row have been checked.
		for (; j < 9; j++)
		{
			//If square isnt fixed mark as next square
			if (fixed[i][j] == 0)
			{
				col = i;
				row = j;
				return 1;
			}
		}	
		j = 0;
	}
	return 0;
}
/******************************************************************************
* @name		findPrevSquareInPlay
* @brief	finds previous square in play.
* @param	none
* @retval	True or False int 1 or 0
******************************************************************************/
int Sudoku::findPrevSquareInPlay()
{
	int j = row;
	//Loop until prev square has been found
	for (int i = col; i >= 0; i--)
	{
		//Loop until all squares in row have been checked.
		for (; j >= 0; j--)
		{
			//If square isnt fixed mark as prev square
			if (fixed[i][j] == 0)
			{
				col = i;
				row = j;
				return 1;
			}
		}
		j = 9;
	}
	return 0;
}