#ifndef SUDOKU_H
#define SUDOKU_H


class Sudoku {
	public:
		Sudoku(int start_num);
		int solve();
		void printGrid();

	private:
		int checkRow(int val, int y);
		int checkCol(int val, int x);
		int checkBox(int val, int x, int y);
		int findFirstSquareInPlay(void);
		int findNextSquareInPlay(void);
		int findPrevSquareInPlay(void);

		int row, col;
		int grid[9][9]={0};
		int fixed[9][9]={0};

};

#endif