#include "sudoku.h" 
#include <iostream> 

using namespace std;
/******************************************************************************
* @name		main
* @brief	prints and solves sudoku puzzle
* @param	none
* @retval	int
******************************************************************************/
int main()
{
	int n;

	//Print welcome message to screen and prompt user for n.
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";

	//Get number of prepopulated squares.
	cin >> n;

	//Construct Sudoku with n prepopulate squares
	Sudoku s(n);

	//If solved print solved!
	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printGrid();
	}
	//Else print sorry, unsolvable...
	else {
		cout << "Sorry, unsolvable..." << endl;
	}

	return 0;
}