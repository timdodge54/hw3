#include <iostream>

#include "sudoku.h"

using namespace std;

/// @name main
/// @brief the main entry point for the program
/// @param None
/// @retval 0 on success

int main(void) {
    //The main function
    int squaresToPopulate = 0;
    cout << "Welcome to the Sudoku solver!" << endl;
    cout << "Enter the number of squares to prepopulate: ";
    cin >> squaresToPopulate;
    //Create object and solve if possible
    Sudoku s(squaresToPopulate);
    s.printGrid();
    cout <<endl << endl;
    if (s.solve()) {
        cout << "Solved!";
        s.printGrid();
    } else {
        cout << "Sorry,  unsolvable..." << endl << endl;
    }
    return 0;
}