#pragma once

class Sudoku
{
private:
    char grid[9][9];
    void resetGrid();
    int checkMove(int row, int col, int value);
public:
    Sudoku(int numberOfSquaresToPrePopulate);
    int solve();
    void printGrid();
};

struct Position {
    int row, col;
    Position(int row, int col) : row(row), col(col) {}
};