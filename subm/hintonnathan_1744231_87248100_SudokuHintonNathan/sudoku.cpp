#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>

#include "sudoku.h"

using namespace std;

/// @name Sudoku::Sudoku
/// @brief Overrides the default constructor to include a number of squares
/// randomly populate
/// @param The number of squares to be populated
/// @retval None
Sudoku::Sudoku(int numberOfSquaresToPrePopulate){
    //Init random generator
    srand(time(NULL));
    Sudoku::resetGrid();
    //Pre-populate the squares randomly
    for (int xx = 0; xx < numberOfSquaresToPrePopulate; xx ++) {
        int col = rand()%9;
        int row = rand()%9;
        int value = (rand()%9)+49;
        if (('*' == Sudoku::grid[row][col]) && (Sudoku::checkMove(row, col, value))) {
            Sudoku::grid[row][col] = value;
        } else {
            xx --;
        }
    }
}

/// @name Sudoku::checkMove
/// @brief Checks if a move at a row, col and value are valid
/// @param A row column and value to test
/// @retval int: returns 1 if valid
int Sudoku::checkMove(int testRow, int testCol, int testValue) {
    //check the row and col for matching values:
    for (int row = 0; row < 9; row ++ ) {
        for (int col = 0; col < 9; col ++ ) {
            if ((col == testCol) || (row == testRow)) {
                if (testValue == Sudoku::grid[row][col]) {
                    return 0;
                }
            }
        }
    }
    //Check inside of the box. This is the tricky one
    for (int row = 0; row < 3; row ++ ) {
        for (int col = 0; col < 3; col ++ ) {
            if (testValue == Sudoku::grid[row+((testRow/3)*3)][col+((testCol/3)*3)]) {
                // Sudoku::print();
                return 0;
            }
        }
    }
    return 1;
}

/// @name Sudoku::resetGrid
/// @brief Sets all the values in the grid to be a *
/// @param None
/// @retval None
void Sudoku::resetGrid() {
    //Reset the grid to empty
    for (int row = 0; row < 9; row ++ ) {
        for (int col = 0; col < 9; col ++ ) {
            Sudoku::grid[row][col] = '*';
        }
    }
}

/// @name Sudoku::solve
/// @brief Tries to solve the current grid
/// @param None
/// @retval int: Returns 1 if solvable and 0 if not solvable
int Sudoku::solve() {
    //Setup a flag for exiting and to know if grid[row][col] should be edited
    bool changePosition = false;
    //Use a vector to store positions that have been tried
    vector<Position> triedPositions;
    //Loop through rows and cols.
    //By manipulating row and col we can check different spots
    for (int row = 0; row < 9; row ++) {
        for (int col = 0; col < 9; col ++) {
            //If it is a * treat as untouched square
            if ('*' == Sudoku::grid[row][col] || changePosition) {
                int x = Sudoku::grid[row][col];//1 in ascii
                if (42 == x) {
                    x = 49;
                }
                while (x < 58) {
                    //While x between 1 and 9
                    if (Sudoku::checkMove(row, col, x)) {
                        triedPositions.push_back(Position(row, col));
                        Sudoku::grid[row][col] = x;
                        changePosition = false;
                        x = 100; //Set past 58 to exit loop
                    } else {
                        x ++;
                    }
                }
                if (100 != x) { //Because we set x to 100 when we change
                    //Initiate a back step...
                    if (0 == triedPositions.size()) {
                        return 0;
                    }
                    Sudoku::grid[row][col] = '*';
                    row = triedPositions.back().row;
                    col = triedPositions.back().col-1;
                    triedPositions.pop_back();
                    changePosition = true;
                }
            }
        }
    }
    return 1;
}

/// @name Sudoku::printGrid
/// @brief Prints the current sudoku grid to the screen
/// @param None
/// @retval None
void Sudoku::printGrid() {
    cout << endl;
    for (int row = 0; row < 9; row ++) {
        for (int col = 0; col < 9; col ++) {
            if ((0 == (col) % 3) && 0 != col) {
                cout << "|";
            }
            cout << " " << Sudoku::grid[row][col] << " ";
        }
        cout << endl;
        if ((0 == (row+1) % 3) && 8 != row) {
            cout << "---------+---------+---------" << endl;
        }
    }
}