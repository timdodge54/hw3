#include <iostream>
#include "sudoku.h"
using namespace std;

/****
* @name		main
* @brief	generates a sudoku puzzle and attempts to solve it
* @param	none
* @retval	none
****/
int main()
{
	int n;

	srand(time(NULL));

	Sudoku s;

	cout << "Enter number of squares to prepopulate: ";
	cin >> n;
	
	s.sudoku(n);
	s.printGrid();

	if (s.solve())
	{
		cout << "Solved" << endl;
		s.printGrid();
	}
	else
	{
		cout << "This sudoku grid is unsolvable." << endl;
	}

	return 0;
}