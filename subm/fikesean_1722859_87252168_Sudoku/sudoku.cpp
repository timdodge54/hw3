#include <iostream>
#include <cstdlib>
#include "sudoku.h"
using namespace std;

/****
* @name		sudoku
* @brief	populates n squares of the 9x9 matrix with legal values
* @param	# of prepopulated squares
* @retval	none
****/
void Sudoku::sudoku(int n)
{
	int i, j, k, x, y, num, invalid, xcheck, ycheck, count;

	Sudoku s;
	
	count = 0;

	for (i = 0; i < n; i++)
	{
		try 
		{
			if (count > 10000000)
			{
				throw (count);
			}
		}
		catch (int fail)
		{
			cout << "\nPuzzle creation failed: timeout" << endl;
			exit(EXIT_FAILURE);
		}

		//get random values
		num = rand() % 9 + 1;
		x = rand() % 9;
		y = rand() % 9;
		//cout << i << num <<	x << y << endl;
		
		//check for existing value
		if (!grid[x][y])
		{
			//check for valid input
			invalid = 0;

			//check within 3x3 grid
			//finds which 3x3 grid to check
			for (ycheck = 3; ycheck <= 9; ycheck += 3)
			{
				for (xcheck = 3; xcheck <= 9; xcheck += 3)
				{
					//start check within 3x3
					if (x < xcheck && y < ycheck)
					{
						//iterate y position in 3x3
						for (j = ycheck - 3; j < ycheck; j++)
						{
							//iterate x position in 3x3
							for (k = xcheck - 3; k < xcheck; k++)
							{
								//check for invalid numbers
								if (num == grid[k][j])
								{
									invalid = 1;
								}
							}
						}
					}
				}
			}

			for (j = 0; j < 9; j++)
			{
				//skip checking current square
				if (j == x || j == y)
				{
					;
				}
				//check for horizontal values and vertical values
				else if (grid[x][j] == num || grid[j][y] == num)
				{
					invalid = 1;
				}
				else
				{
					grid[x][y] = num;
				}
			}

			if (invalid)
			{
				grid[x][y] = 0;
				i--;
				//cout << "invalid" << endl;
			}
		}
		else
		{
			//cout << "invalid" << endl;
			i--;
		}
		count++;
	}
}


/****
* @name		solve
* @brief	Produces a valid solution to the generated puzzle
* @param	none
* @retval	1 on success, 0 on failure
****/
int Sudoku::solve()
{
	int x, y, i, j, num, invalid, xcheck, ycheck;

	num = 1;

	for (y = 0; y < 9; y++)
	{
		for (x = 0; x < 9; x++)
		{
			//check if target square is prepopulated
			if (!grid[x][y])
			{
				for (; num < 10; num++)
				{
					invalid = 0;

					//check within 3x3 grid
					//finds which 3x3 grid to check
					for (ycheck = 3; ycheck <= 9; ycheck += 3)
					{
						for (xcheck = 3; xcheck <= 9; xcheck += 3)
						{
							//start check within 3x3
							if (x < xcheck && y < ycheck)
							{
								//iterate y position in 3x3
								for (j = ycheck - 3; j < ycheck; j++)
								{
									//iterate x position in 3x3
									for (i = xcheck - 3; i < xcheck; i++)
									{
										//check for invalid numbers
										if (num == grid[i][j])
										{
											invalid = 1;
										}
									}
								}
							}
						}
					}

					//check row and column
					for (i = 0; i < 9; i++)
					{
						//skip checking current square
						if (i == x || i == y)
						{
							;
						}
						//check for horizontal values and vertical values
						else if (grid[x][i] == num || grid[i][y] == num)
						{
							invalid = 1;
						}
						else
						{
							grid[x][y] = num;
						}
					}

					//if no valid inputs, go back
					if (num == 9 && !grid[x][y])
					{
						if (x > 0)
						{
							x -= 1;
						}
						else
						{
							y -= 1;
							x = 8;
						}
						num = grid[x][y] + 1;
					}
				}
			}
			//end if unsolvable
			if (grid[0][0] > 9)
			{
				return 0;
			}
		} 
	}
	
	return 1;
}


/****
* @name		printGrid
* @brief	prints the 9x9 grid
* @param	none
* @retval	none
****/
void Sudoku::printGrid()
{
	int x, y, i;

	for (y = 0; y < 9; y++)
	{
		for (x = 0; x < 9; x++)
		{
			//replace 0 values with *
			if (grid[x][y] == 0)
				cout << "*    ";
			else
				cout << grid[x][y] << "    ";
		}
		cout << "\n\n";
	}
}