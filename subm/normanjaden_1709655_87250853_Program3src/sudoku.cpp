#include "sudoku.h"
#include <cstdlib>
#include <iostream>
using namespace std;

/**
* @name Sudoku
* @breif Constructor for Sudoku Class
* @param int n
* @retval none
*/

Sudoku::Sudoku(int n) {
	srand((unsigned)time(NULL));

	for (int i = 0; i < n; i++) {	// loops till the amount of
		int row = rand() % 9;		// numbers is set
		int col = rand() % 9;

		if (puzzle[row][col].fixed == 0) {
			puzzle[row][col].fixed = 1;

			int validInput = 0;
			while (validInput != 1) {
				int n = 1 + (rand() % 9);
				puzzle[row][col].num = n;

						// Will run infinitely if 1-9 doesnt work in one spot
						// Need to fix
				if (completeCheck()) {
					validInput = 1;
				}
			}
		}
		else {
			i--;		// that place was already taken
		}				// have it run again
	}
}



/**
* @name solve
* @breif solves the puzzle using recursion
* @param row and column
* @retval returns a 1 or a 0 depending on whether it is possible to solve
*/

int Sudoku::solve(int row, int col) {
		if (row == 9) {		// recursion made it to the end of the array
			return 1;		// which means that it has a solved puzzle
		}

		int newCol = 0;
		int newRow = 0;
		values element = puzzle[row][col];

		if (element.fixed == 1) {	// dont change a fixed number
			newCol = col + 1;
			newRow = row;
			
			if (newCol >= 9) {
				newRow++;
				newCol = 0;
			}
			return solve(newRow, newCol);
		}

								// This is the backtracking step
		for (int number = 1; number < 10; number++) {
			puzzle[row][col].num = number;

			if (completeCheck() == 1) {

				newCol = col + 1;
				newRow = row;

				if (newCol >= 9) {
					newRow = row + 1;
					newCol = 0;
				}
				
				if (solve(newRow, newCol) == 1) {
					return 1;
				}				
				
			}
		
		}
					// ran through 1-9 and none of them work and return that
		puzzle[row][col].num = 0;
		return 0;
	}

/**
* @name printGrid
* @breif prints the puzzle in Sudoku fashion
* @param none
* @retval none
*/

void Sudoku::printGrid() {
	for (int i = 0; i < 9; i++) {	// Rows
		for (int j = 0; j < 9; j++) {	// Columns
			if (i == 3 && j == 0|| i == 6 && j == 0 ) {
				cout << "-------+--------+-------" << endl;
			}
			
			else if ((j) % 3 == 0 && j != 0) {
				cout << " | ";
			}

			int number = puzzle[i][j].num;
			if (number == 0) {
				cout << "*";
			}
			else {
				cout << number;
			}
			cout << " ";
		}
		cout << endl;
	}
}

/**
* @name checkRow
* @breif checks all nine rows if they are not violating rules for Sudoku
* @param none
* @retval returns 1 if all rows aren't violating the rules, else returns 0
*/

int Sudoku::checkRow() {
	int numCheck;

	for (int i = 0; i < 9; i++) {	// Checks 1-9 are used once 
		int number[9] = { 0 };		// one each row

		for (int j = 0; j < 9; j++) {
			
			numCheck = puzzle[i][j].num;
			if (numCheck == 0) {
				continue;
			}
			else if (number[numCheck - 1] == 1) {
				return 0;
			}
			else {
				number[numCheck - 1] = 1;
			}
		}
	}
	return 1;
}

/**
* @name checkCol
* @breif checks all 9 columns of the puzzle to see if any are voilating Sudoku rules
* @param none
* @retval returns 1 if all columns aren't violating the rules, else returns 0
*/

int Sudoku::checkCol() {
	int numCheck;

	for (int i = 0; i < 9; i++) {	// checks 1-9 are used once
		int number[9] = { 0 };		// one each column

		for (int j = 0; j < 9; j++) {

			numCheck = puzzle[j][i].num;
			if (numCheck == 0) {
				continue;
			}
			else if (number[numCheck - 1] == 1) {
				return 0;
			}
			else {
				number[numCheck - 1] = 1;
			}
		}
	}
	return 1;
}

/**
* @name checkBox
* @breif checks all nine boxes if they are not violating rules for Sudoku
* @param none
* @retval returns 1 if all boxes aren't violating the rules, else returns 0
*/

int Sudoku::checkBox() {
	int numCheck;

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			int number[9] = { 0 };
								
								// checks the numbers 1-9 are 
								// used once in each box
			for (int x = i * 3; x < (i + 1) * 3; x++) {
				for (int y = j * 3; y < (j + 1) * 3; y++) {

					numCheck = puzzle[x][y].num;

					if (numCheck == 0) {
						continue;
					}
					else if (number[numCheck - 1] == 1) {
						return 0;
					}
					else {
						number[numCheck - 1] = 1;
					}
				}
			}
		}
	}
	return 1;
}

/**
* @name completeCheck
* @breif combines checkRow, checkCol, checkBox into one
* @param none
* @retval returns 1 if aren't violating the rules, else returns 0
*/

int Sudoku::completeCheck() {
					// checks all three checks are right in one spot
	if (checkRow() == 1 && checkCol() == 1 && checkBox() == 1) {
		return 1;
	}

	return 0;
}