#include "sudoku.h"
#include <iostream>
using namespace std;

/**
* @name main
* @breif generates a random puzzle, and sees if it solves
* @param none
* @retval none
*/

int main(void) {
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	Sudoku s(n);
	cout << "Created a puzzle" << endl;
	s.printGrid();
	cout << endl;

	if (s.solve(0, 0) == 1) {
		cout << "Solved!" << endl;
		s.printGrid();
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}
	
	return 0;
}