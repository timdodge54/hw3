class Sudoku {
	struct values {
		int fixed;
		int num;
	};
	
	values puzzle[9][9] = { 0 };

public:
	Sudoku(int n);
	int solve(int x, int y);
	void printGrid();
private:
	int checkRow();
	int checkCol();
	int checkBox();
	int completeCheck();
};
