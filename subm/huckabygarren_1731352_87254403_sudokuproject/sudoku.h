/*************************************************************************
*	@name	Sudoku
*	@brief Defines class Sudoku
*	@param	none
*	@retval	none
*************************************************************************/

#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>

#ifndef SUDOKU_H
#define SUDOKU_H
using namespace std;

class Sudoku {
public:
	Sudoku(int n);
	int solve();
	void printGrid();
private:
	int checkrow(int r, int v);
	int checkcolumn(int c, int v);
	int checksquare(int r, int c, int v);
	int findfirst(int *x, int *y);
	int findnext(int *x, int *y);
	int findprev(int *x, int *y);
	
	int arrcheck[9][9]={0};
	int arr[9][9] = {0};
};
//define public and private sections of class
#endif