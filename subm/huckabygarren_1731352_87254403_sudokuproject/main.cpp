/*************************************************************************
*	@name	main
*	@brief calls functions in class sudoku to determine if there is a
* solution for a randomly generated sudoku puzzle.
*	@param	none
*	@retval	none
*************************************************************************/

#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>
#include "sudoku.h" 

using namespace std;
 
int main() 
{ 
 int n;
 cout << "Welcome to SudokuSolver!!" << endl; 
 cout << "Enter number of squares to prepopulate: "; 
 //prompt user for input
 cin >> n;
//get input for Sudoku class 
 
 Sudoku sudoku(n);
//call sudoku 
 sudoku.printGrid();	
//print initial grid
 sudoku.solve();
//solve
 if (sudoku.solve()) { 
  cout << "Solved!" << endl;
//if solved print solved, and solution  
  sudoku.printGrid(); 
 } 
 else { 
  cout << "Sorry, unsolvable..." << endl;  
 //if 0, no solution
 }
 return 0; 
} 