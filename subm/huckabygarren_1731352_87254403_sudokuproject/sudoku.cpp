
#include "sudoku.h"

/*************************************************************************
*	@name	Sudoku
*	@brief  Constructor for class Sudoku
*	@param	n
*	@retval	none
*************************************************************************/
Sudoku::Sudoku(int n)
{
	srand(time(NULL));
	int r, c, v, it, t;
	it = 0;
//initialize variables
	while (it < n)
	{
//while it is less than n
	r = (rand() % 9);
	c = (rand() % 9);
	v = (rand() % 9) + 1;
//get random numbers to populate grid

if(arr[r][c] == 0 && checkrow(c,v) && checkcolumn(r,v) && checksquare(r,c,v)){
//check for valid inputs	
			arr[r][c] = v;
			cout << endl;
			arrcheck[r][c] = 1;
//create static array to check values later
			it++;
		}
	}	
}
/*************************************************************************
*	@name	printGrid
*	@brief  Private function called to print members of class Sudoku
*	@param	
*	@retval	none
*************************************************************************/
void Sudoku::printGrid()
{
	for(int row = 0; row < 9; row++) {
//loop in function
		cout << " " << endl;
		if (row == 3)
			cout << "- - - - - - - - - - - -" << endl;
		if (row == 6)
			cout << "- - - - - - - - - - - -" << endl;
		
	for(int col = 0; col < 9; col++) {	
	
		cout << " ";
	
		if (col == 3)
			cout << "| ";
		if (col == 6)
			cout << "| ";
		
	if (arr[row][col] == 0)
			
			cout << '*';
//set formatting to create 3 by 3 square
			else
				cout << arr[row][col];
//print grid		
		}
		
	}
	cout << endl;
	
	for(int row = 0; row < 9; row++) {
		cout << " " << endl;
		if (row == 3)
			cout << "- - - - - - - - - - - -" << endl;
		if (row == 6)
			cout << "- - - - - - - - - - - -" << endl;
		
	for(int col = 0; col < 9; col++) {	
	
		cout << " ";
	
		if (col == 3)
			cout << "| ";
		if (col == 6)
			cout << "| ";
//format static array the same as array		
//cout << arrcheck[row][col];		
		}
	}
	cout << endl;
}
/*************************************************************************
*	@name	solve
*	@brief  function called backtrack through the empty spots in the
* puzzle and supply legal values
*	@param	
*	@retval	1, 0
*************************************************************************/
int Sudoku::solve()
{
	int row, col; 
	int x = 0;
	int y = 0;
	int v = 1;
	int forever = 1;
//initialize variables
	findfirst(&x,&y);
//get to first spot in grid
//cout << "solving1" <<  x << y << endl;
	while (forever == 1)
//infinite loop
	{

	if (checkrow(y,v) && checkcolumn(x,v) && checksquare(x,y,v) && v <= 9)
//check for valid inputs
		{
			//cout << "solving2" <<  x << y << endl;
			arr[x][y] = v;
			//cout << "solving3" <<  x << y << endl;
				if (x == 9 && y == 9)
//if at the end of grid
				{
					cout << "solving4" << endl;
					return 1;
				}
//printGrid();
			findnext(&x, &y);
			v = 1;
//set v equal to 1
		}
		else
		{
			//cout << "solving6" <<  x << y << endl;
		arr[x][y] = v;
		v++;
//increment v		
			if (v == 10)
			{
			//cout << "solving7" <<  x << y << endl;
							
				arr[x][y] = 0;
				findprev(&x, &y);
//find previous value
					if (x == 0 && y == 0)
//if to the first of array
					{
			//cout << "solving8" <<  x << y << endl;
						return 0;
//quit the program
					}
					else
					{
//if not
			//cout << "solving9" <<  x << y << endl;
						findprev(&x, &y);
//decrement to previous value
						v = arr[x][y] + 1;
//make array equal to v + 1
						//printGrid();
					}	
				
			}
			
		}
		
		
	}	
		
	return 1;	
//return		
	}
	
/*************************************************************************
*	@name	checkrow
*	@brief  function called to check and see if the row determines
* invalid inputs
*	@param	c, v
*	@retval	1, 0
*************************************************************************/
int Sudoku::checkrow(int c, int v)
{
	for(int row = 0; row < 9; row++){
//loop through row
		
		if (arr[row][c] == v){
//if array already equals v, cancel
			return 0;	
		}
	}
	return 1;
//success
}
/*************************************************************************
*	@name	checkcolumn
*	@brief  function called to check and see if the column determines
* invalid inputs
*	@param r, v
*	@retval	1, 0
*************************************************************************/
int Sudoku::checkcolumn(int r, int v)
{
	for(int col = 0; col < 9; col++){
//loop through column
		if (arr[r][col] == v){
//if array already equals v, cancel
		return 0;
		}		
	}
	return 1;
//success
}
/*************************************************************************
*	@name	checksquare
*	@brief  function called to check and see if the square determines
* invalid inputs
*	@param	c, r, v
*	@retval	1, 0
*************************************************************************/
int Sudoku::checksquare(int c, int r, int v)
{
int mincol = (c / 3) * 3;
int maxcol = mincol + 2;
int minrow = (r / 3) * 3;
int maxrow = minrow + 2;
int i, j;
//initialize variables
for (i = mincol; i <= maxcol; i++)
{
	for(j = minrow; j <= maxrow; j++)
	{
//nested for loop
		if (arr[i][j] == v)
	
	return 0;
//if array already equals v, cancel
	}

}
return 1;
//success
}

/*************************************************************************
*	@name	findfirst
*	@brief  function called to increment to the first location in array
*	@param	*x, *y
*	@retval	1, 0
*************************************************************************/

int Sudoku::findfirst(int *x, int *y)
{
	int row, col;
//initialize variables
	for (row = 0; row < 9; row++){
		for (col = 0; col < 9; col++){
//nested for loop
			if (arrcheck[col][row] == 0){
//if static array equals 0
			*x = row;
			*y = col;
//make column and row equal to pointers
			return 1;
			}
		}
	}
	return 0;	
//failure
}
/*************************************************************************
*	@name	findnext
*	@brief  function called to increment to the next location
*	@param	*x, *y
*	@retval	1, 0
*************************************************************************/

int Sudoku::findnext(int *x, int *y)
{
	int row, col;
	col = *y;
	row = *x;
	col++;
//initialize variables
	while (col < 10 && row < 10)
//while less than 10
	{
	if (col == 9)
	{
		row++;
		col = 0;
//increment row, col equals 0
	}
		if (row == 9)
//if final row reached
		{
			return 0;
//failure
		}
	
			if (arrcheck[row][col] == 0)
//if static array equals 0
			{
			*x = row;
			*y = col;
//set row and column equal to pointers
			return 1;
//success
			}
			else
			{
				col++;
//increment column
			}
	}
	return 0;
//failure
}		


/*************************************************************************
*	@name	findprev
*	@brief  function called to backtrack to the previous location
*	@param	*x, *y
*	@retval	1, 0
*************************************************************************/

int Sudoku::findprev(int *x, int *y)
{	
	int row, col;
	col = *y;
	row = *x;
	col--;
//initialize variables
	while (1)
//infinite loop
	{
	if (col == 0)
//decrement row, set column equal to 8
	{
		row--;
		col = 8;
	}
		if (row == 0)
//if row equals 0
		{
			return 0;
//failure
		}
	
			if (arrcheck[row][col] == 0)
//if static array equals 0
			{
			*x = row;
			*y = col;
//set row and column equal to pointers
			return 1;
//success
			}
			else
			{
				col--;
//decrement column
			}
	}
	return 0;
//failure
}