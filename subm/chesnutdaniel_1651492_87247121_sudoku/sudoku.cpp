#include "sudoku.h"

/*********************************************************************
* @name Sudoku
* @brief Constructor creates a random number generator to randomly 
*		 generate numbers 1-9 on the sudoku board
* @param input integer n
* @retval  none
*********************************************************************/

Sudoku::Sudoku(int n)
{
	//seed random number generator
	srand((unsigned)time(NULL));
	int i = 0, slotVal, row, column;

	//loop through random number values for row, column, and slotVal
	for (int i = 0; i < n; i++)
	{
		row = rand() % 9;
		column = rand() % 9;
		slotVal = rand() % 9+1;

		//if the slot to be placed is valid
		if (validSlot(row, column, slotVal))
		{
			//if the slot to be placed is uninhabited
			if (grid[column][row] == 0)
			{
				//print slotVal to those coordinates
				grid[column][row] = slotVal;
			}
			//else try again
			else
			{
				n++;
			}
		}
		//else try again
		else
		{
			n++;
		}
	}

	//print grid to screen
	printGrid();
}

/*********************************************************************
* @name checkRow
* @brief Function checks if any values in row are equal to the
*		 value being added
* @param input integer for row and slot value
* @retval  output 1 (true) or 0 (false)
*********************************************************************/

int Sudoku::checkRow(int row, int slotVal)
{
	int i;

	//loop through row
	for (i = 0; i < 9; i++)
	{
		//check if the slotVal is equal to any values in row
		if (slotVal == grid[i][row])
		{
			//if yes, return 0
			return 0;
		}
	}

	//if no values are found to be equal, return 1
	return 1;
}

/*********************************************************************
* @name checkCol
* @brief Function checks if any values in column are equal to the
*		 value being added
* @param input integer for column and slot value
* @retval  output 1 (true) or 0 (false)
*********************************************************************/

int Sudoku::checkCol(int column, int slotVal)
{
	int j;

	//loop through column
	for (j = 0; j < 9; j++)
	{
		//check if the slotVal is equal to any values in column 
		if (slotVal == grid[column][j])
		{
			//if yes, return 0
			return 0;
		}
	}

	//if no values are found to be equal, return 1
	return 1;
}

/*********************************************************************
* @name checkBox
* @brief Function checks if any values in 3x3 box are equal to the
*		 value being added. This is done by checking the inputted
*		 row and column, setting them equal to the top left corner
*		 of the box being checked, and loop through that box
* @param input integer for column, row, and slot value
* @retval  output 1 (true) or 0 (false)
*********************************************************************/

int Sudoku::checkBox(int column, int row, int slotVal)
{
	
	int i, j;

	//initialize row to base box row
	//if row is less than 2
	if (row <= 2)
	{
		//set row equal to 0
		row = 0;
	} 
	//else if row is greater than 3, but less than 5
	else if (row >= 3 && row <= 5)
	{
		//set row equal to 3
		row = 3;
	}
	//else if row is greater than 6
	else if (row >= 6)
	{
		//set row equal to 6
		row = 6;
	}

	//initialize column to base box column
	//if column is less than 2
	if (column <= 2)
	{
		//set column to 0
		column = 0;
	}
	//else if column is greater than 3 but less than 5
	else if (column >= 3 && column <= 5)
	{
		//set column to 3
		column = 3;
	}
	//else if column is greater than 6
	else if (column >= 6)
	{
		//set column equal to 6
		column = 6;
	}

	//check row by row and column bu column for any duplicates in box
	for (i = row; i <= row+2; i++)
	{
		for (j = column; j <= column+2; j++)
		{
			//check if slotVal is equal to any values in box
			if (slotVal == grid[j][i])
			{
				//if yes, return 0
				return 0;
			}
		}
	}

	//if no values are found to be equal, return 1
	return 1;
}

/*********************************************************************
* @name findEmptySlot
* @brief Finds any slots in the grid that are uninhabited
* @param input address of row, and address of column
* @retval output 1 (true) or 0 (false)
*********************************************************************/

int Sudoku::findEmptySlot(int &row, int &column)
{
	for (row = 0; row < 9; row++)
	{
		for (column = 0; column < 9; column++)
		{
			if (grid[column][row] == 0)
			{
				return 1;
			}
		}
	}

	return 0;
}

/*********************************************************************
* @name validSlot
* @brief Checks if the slot being selected has any violations in the
*		 corresponding rows, columns, or box.
* @param input row, column, slot value
* @retval output 1 (true) or 0 (false)
*********************************************************************/

int Sudoku::validSlot(int row, int column, int slotVal)
{
	//if any violations in row, column or slot
	if (checkRow(row, slotVal) != 0 &&
		checkCol(column, slotVal) != 0 &&
		checkBox(column, row, slotVal) != 0)
	{
		//return 1
		return 1;
	}

	//else return 0
	return 0;
}

/*********************************************************************
* @name solve
* @brief Recursively solves sudoku board by checking for empty slots
*		 until all have been filled
* @param none
* @retval output 1 (true) or 0 (false) 
*********************************************************************/

int Sudoku::solve()
{
	int i, row, column;

	//if there are no empty slots found in array
	if (!findEmptySlot(row, column))
	{
		//return 1
		return 1;
	}

	//loop checking valid slot by counting 1-9 
	for (i = 1; i <= 9; i++)
	{
		//check if slot is valid
		if (validSlot(row, column, i))
		{
			//set grid location equal to i (1-9)
			grid[column][row] = i;

			//recursively call solve again until true
			if (solve())
			{
				return 1;
			}

			//set grid location back to 0 if backtracking
			grid[column][row] = 0;
		}
	}

	//return 0 if unsolved
	return 0;

}

/*********************************************************************
* @name printGrid
* @brief Prints grid to the screen
* @param none
* @retval  none
*********************************************************************/

void Sudoku::printGrid()
{
	int i, j;

	//loop through rows and columns of array
	for (i = 0; i < 9; i++)
	{
		for (j = 0; j < 9; j++)
		{
			//if any spots in the array are equal to 0
			if (grid[j][i] == 0)
			{
				//set 0 to be a star
				cout << " *";
			}
			//else spacing for sudoku board
			else
			{
				cout << " " << grid[j][i];
			}

			//whenever column 2 or 5 is passed, print a line
			if (j == 2 || j == 5)
			{
				cout << " |";
			}
			//else print spacing
			else
			{
				cout << "  ";
			}
		}

		//if you reach row 2 or 5, print sudoku board lines
		if (i == 2 || i == 5)
		{
			cout << endl;
			cout << "-----------+-----------+-----------";
		}

		//print end line
		cout << endl;
	}
	
	//print end line
	cout << endl;
}