#ifndef SUDOKU_H
#define SUDOKU_H

#include <iostream>

using namespace std;

class Sudoku
{
private:
	int grid[9][9] = {0};
	int checkRow(int row, int slotVal);
	int checkCol(int column, int slotVal);
	int checkBox(int row, int column, int slotVal);
	int findEmptySlot(int &row, int &column);
	int validSlot(int row, int column, int slot);

public: 
	Sudoku(int n);
	int solve();
	void printGrid();
};

#endif
