#include "sudoku.h"

/*********************************************************************
* @name main
* @brief Prints the instructions for user to interact with sudoku
* @param none
* @retval  none
*********************************************************************/

int main()
{
	int n;

	//prints welcome to the user, and prompts for number of squares to prepopulate
	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	//calls constuctor and inputs the inputted number
	Sudoku s(n);

	//if sudoku is solved, print solved to screen and grid
	if (s.solve()) 
	{
		cout << "Solved!" << endl;
		s.printGrid();
	}

	//else, print unsolvable
	else 
	{
		cout << "Sorry, unsolvable..." << endl;
	}

	return 0;
}