#include "sudoku.h" 
#include <iostream> 
using namespace std;
/*********************************************************************
* @name main
* @brief prompts user for # of prepopulated spots, and generates then solves.
* @param none
* @retval  none
*********************************************************************/
int main()
{ // Begin
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n; // introduce the game and store value in n

	Sudoku s(n); // populate the puzzle and print to the screen
	cout << endl << endl;

	if (s.solve() == 0) { // solve the puzzle, if it solved
		cout << "Solved!" << endl;
		s.printGrid(); //  state that it is solved and print to the screen
	}
	else { // else
		cout << "Sorry, unsolvable..." << endl; //print there is an error
	}

	return 0;
} // End