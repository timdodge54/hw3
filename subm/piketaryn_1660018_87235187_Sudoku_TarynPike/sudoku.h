#include <string>
#ifndef SUDOKU_H
#define SUDOKU_H
class Sudoku {
public:
	Sudoku(int n);
	int solve();
	void printGrid(); 
private:
	void initialize();
	int isvalid(int x, int y, int value);
};

#endif