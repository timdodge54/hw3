#include "sudoku.h"
#include <iostream>
#include <cstdlib>
using namespace std;
int grid[9][9] = { 0 };
int untouch[9][9] = { 0 };
int x=0;
int y=0;
int success = 0;
int count1 = 0;
/*********************************************************************
* @name Sudoku::Sudoku
* @brief populates a sudoku puzzle, and populates the number of spaces inputed
* @param integer n, how many spaces the user would like prepopulated
* @retval  none
*********************************************************************/
Sudoku::Sudoku(int n)
{	// Begin
	int oil = 0;
	int water = 0;
	int filler = 0;
	int guessvalue = 0;
	int gg = 1;
	initialize(); // fill the puzzle with zeros

	srand(time(0)); // generate random numbers
	for (int i = 0; i < n; i++) { // generate n amount of random spots
		success = 0;
		while (success != 1) {
			oil = rand() % 9; // generate random rows and columns
			water = rand() % 9;
			x = oil;
			y = water;
			if (untouch[y][x] == 0) { // set the spot as fixed
				untouch[y][x] = 1;
				success = 1;
			}
		}
		gg = 1;
		while (gg != 0) { // while the guess for the given spot isn't valid
			guessvalue = rand() % 9 + 1;
			gg = isvalid(x, y, guessvalue);
			if (gg == 1) { // keep generating 
				filler++;
			}
		}
		if (gg == 0) { // if the random value is valid in the spot, set it
			grid[x][y] = guessvalue;
			count1++; // count up the generated values -- for problem solving.
		}
	}		
	printGrid();
} // End
/*********************************************************************
* @name Sudoku::isvalid
* @brief Checks if the value at the given slot is valid or not
* @param ints xx, yy, value
* @retval  0, or 1. stating whether it is valid or not.
*********************************************************************/
int Sudoku::isvalid(int xx, int yy, int value) { // Begin
	int isgood = 1; // assume it is invalid unless proven otherwise
	for (int i = 0; i <= 8; i++) {
		if (grid[xx][i] != value) { // check the row
			isgood = 0; // if its valid set to zero
		}
		else {
			if (i == yy) { // don't check the given position
				isgood = 0;
			}
			else {
				isgood = 1;
				return 1; // return 1 if it failed
			}
		}
	}
	for (int i = 0; i <= 8; i++) { // check the column
		if (grid[i][yy] != value) {
			isgood = 0; // if it is valid set zero
		}
		else { 
			if (i == xx) { // don't check the given position
				isgood = 0; 
			}
			else {
				isgood = 1;
				return 1; // return 1 if it failed
			}
		}
	}
	if (xx >= 6) { // last column
		if (yy >= 6) { // bottom cube
			for (int g = 6; g <= 8; g++) { // check if this box is valid
				for (int h = 6; h <= 8; h++) {
					if (grid[g][h] != value) {
						isgood = 0; // set value to zero if it is good

					}
					else {
						isgood = 1;
						return 1; // return 1 if it failed
					}

				}
			}
		}
		else if (yy <= 5 && yy >= 3) { // middle cube
			for (int g = 6; g <= 8; g++) { // check if this box is valid
				for (int h = 3; h <= 5; h++) {
					if (grid[g][h] != value) {
						isgood = 0; // set value to zero if it is good
					}
					else {
						isgood = 1;
						return 1; // return 1 if it failed
					}
				}
			}
		}
		else { // top cube
			for (int g = 6; g <= 8; g++) { // check if this box is valid
				for (int h = 0; h <= 2; h++) {
					if (grid[g][h] != value) {
						isgood = 0; // set value to zero if it is good
					}
					else {
						isgood = 1;
						return 1; // return 1 if it failed
					}
				}
			}
		}
	}
	else if (xx <= 5 && xx >= 3) { // middle column
		if (yy >= 6) { // bottom cube
			for (int g = 3; g <= 5; g++) { // check if this box is valid
				for (int h = 6; h <= 8; h++) {
					if (grid[g][h] != value) {
						isgood = 0; // set value to zero if it is good
					}
					else {
						isgood = 1;
						return 1; // return 1 if it failed
					}
				}
			}
		}
		else if (yy <= 5 && yy >= 3) { // middle cube
			for (int g = 3; g <= 5; g++) { // check if this box is valid
				for (int h = 3; h <= 5; h++) {
					if (grid[g][h] != value) {
						isgood = 0; // set value to zero if it is good
					}
					else {
						isgood = 1;
						return 1; // return 1 if it failed
					}
				}
			}
		}
		else { // top cube
			for (int g = 3; g <= 5; g++) { // check if this box is valid
				for (int h = 0; h <= 2; h++) {
					if (grid[g][h] != value) {
						isgood = 0; // set value to zero if it is good
					}
					else {
						isgood = 1;
						return 1; // return 1 if it failed
					}
				}
			}
		}
	}
	else { // first column
		if (yy >= 6) { // bottom cube
			for (int g = 0; g <= 2; g++) { // check if this box is valid
				for (int h = 6; h <= 8; h++) {
					if (grid[g][h] != value) {
						isgood = 0; // set value to zero if it is good
					}
					else {
						isgood = 1;
						return 1; // return 1 if it failed
					}
				}
			}
		}
		else if (yy <= 5 && yy >= 3) { // middle cube
			for (int g = 0; g <= 2; g++) { // check if this box is valid
				for (int h = 3; h <= 5; h++) {
					if (grid[g][h] != value) {
						isgood = 0; // set value to zero if it is good
					}
					else {
						isgood = 1;
						return 1; // return 1 if it failed
					} 
				}
			}
		}
		else { // top cube
			for (int g = 0; g <= 2; g++) { // check if this box is valid
				for (int h = 0; h <= 2; h++) {
					if (grid[g][h] != value) {
						isgood = 0; // set value to zero if it is good
					} 
					else { 
						isgood = 1;
						return 1; // return 1 if it failed
					}
				}
			}
		}
	}
	if (isgood == 0) {
		return 0;
	}
	return 1;
} // End
/*********************************************************************
* @name Sudoku::printGrid
* @brief Prints the sudoku puzzle to the screen
* @param none
* @retval  none
*********************************************************************/
void Sudoku::printGrid()
{ // Begin
	int o1;
	for (int y = 0; y <= 8; y++) {
		cout << "  "; // print two spaces to start each row
		int x = 0;
		for (int z1 = 0; z1 <= 2; z1++) {
			for (int z2 = 0; z2 <= 2; z2++) {
				if (grid[y][x] == 0) {
					cout << "*"; // Print "*" in place of each zero
				}
				else {
					cout << grid[y][x]; // print value if it is non-zero
				}
				cout << "  "; // print two spaces in between 
				x++;
			} 
			if (z1 != 2) { // print the lines ever three printed
				cout << "|  ";
			}
			else { // enter every 9 printed
				cout << "\n";

				if ((y+1)%3 ==0 && y != 8) { // every three rows print lines.
					for (int tn = 0; tn <= 2; tn++) {
						if (tn == 2) {
							cout << "-----------";
						}
						else { // add "+" after the first and second lines.
							cout << "-----------+";
						}
						o1 = tn;
					}
					if (o1 == 2) { // dont print the "+" at the end of the second line
						cout << "\n"; 
					}
				}
			}
		}
	}
} // End
/*********************************************************************
* @name Sudoku::solve
* @brief Solves the Sudoku Puzzle
* @param none
* @retval  0, or 1. depending on if it is solvable or not
*********************************************************************/
int Sudoku::solve()
{ // Begin
	int attempts = 0; // for debugging - count attempts
	for (int t = 0; t <= 8; t++) {
		for (int r = 0; r <= 8; r++) { // go through every available spot
			// cout << "|"; for debugging
			if (untouch[r][t] == 1) {
				attempts++; // for debugging
			}
			else {
				int cg = 1;
				int tries = 0;
				int trigger = 0;
				while (cg != 0 && grid[t][r] <= 8) { // while guess not valid
					tries = isvalid(t, r, (grid[t][r] + 1)); 
					grid[t][r] = grid[t][r] + 1; //see if next number is valid
					// cout << "."; for debugging
					if (tries == 0) { // don't exit until it is valid or over
						cg = 0;
					}
				}
				//if (grid[t][r] == 9 && isvalid(t, r, grid[t][r]) == 1) {
				if (cg == 1) { // if it reached 9 and not successful
					grid[t][r] = 0; // set current one to zero
					int irp = 1;
					while (irp != 0) { // dont exit until we backtrack
						if (r < 0 || t < 0) {
							return 1; //exit if it tries to backtrack from 0,0
						}
						if (r == 0) {
							t--; // if it is on end of row, go up one row.
							r = 7; // start at the next end
						}
						else { // otherwise go back one
							r--;
							if (untouch[r][t] == 1) {
								attempts++; //if it is a fixed number, skip it
							}
							else {
								r--; // otherwise go back one and try again.
								irp = 0;
							}
						}

					}
					

				}
				else { // this was for debugging
				}
			}
			if (t == 8 && r == 8) { // if you reach the end, return successful
				return 0; // exit
			}
		}
	}
	return 0; // return 0 if successful
} // End
/*********************************************************************
* @name Sudoku::initialize
* @brief fills the arrays with zeros
* @param none
* @retval  none
*********************************************************************/
void Sudoku::initialize()
{ // Begin
	for (int y = 0; y < 9; y++) { 
		for (int x = 0; x < 9; x++) { // fill arrays with zero
			if (x % 2 == 0) { 
				grid[x][y] = 0;
				untouch[x][y] = 0; // fill with zeros
			}
			else { // fill with zeros
				grid[x][y] = 0;
				untouch[x][y] = 0;
			}
		}
	}
} // End
