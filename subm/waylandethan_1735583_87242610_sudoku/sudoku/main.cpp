#include "sudoku.h"
using namespace std;

/******************************************************************************
@name	main
@breif	asks for number of squares to prepopulate sudoku grid with, then 
		generates grid and attempts to solve.
@param	none
@retval	int
******************************************************************************/
int main()
{
    int n;
	//Asks for input n
    cout << "Welcome to SudokuSolver!!" << endl;
    cout << "Enter number of squares to prepopulate: ";
    cin >> n;
	
	//generates s of class Sudoku with n prepopulated squares
    Sudoku s(n);
    
	//prints grid
    s.printGrid();

	//if solvable, print solution
    if (s.solve()) {
        cout << "\n\nSolved!\n\n";
        s.printGrid();
    }
    else {	//if unsolvable, print message
        cout << "Sorry, unsolvable" << endl;
    }
	
}

