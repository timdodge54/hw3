#include <iostream>
#include <cstdlib>
#include <ctime>

class Sudoku {
	int a[9][9];
	int b[9][9];
	int c[2];
public:
	Sudoku(int n);
	int solve();
	void printGrid();
	char printchar(int x, int y);
	int checkvalid(int x, int y, int val);
	int backtrack(int x, int y);



};

