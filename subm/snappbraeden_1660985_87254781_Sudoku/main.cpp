/******************************************************************************
    @name main
    @brief have user enter numbers to prepopulate the sudoku
    @param none
    @retval none 
 *****************************************************************************/
#include "sudoku.hpp"
#include <cstdlib>
#include <iostream>
#define sp "  "
#define bsp "           "
#define und "----------"
using namespace std;
 
int main(){
    int n;
    //int board[9][9];
    //int fixed[9][9];
    //declare varibles and arrays
    
    cout << "Welcome to SudokuSolver!!" << endl;
    //from program param
    cout << "Enter number of squares to prepopulate: ";
    //from program param
    cin >> n;
    //from program param
    if (n > 81){//if user is dumb and wants more than 81 values
        cout << "Too many squares, try again" << endl;//wont let them
        cout << "Welcome to SudokuSolver!!" << endl;//makes them try again
        cout << "Enter number of squares to prepopulate: ";//try again
        cin >> n;//enter value
        Sudoku s(n);//runs program
    } //if entered a good number
        
    Sudoku s(n);//prints prepopulated gird
    
    if (s.solve() == 1) {//if solve function worked
        cout << "Solved!" << endl; //print solved
        s.printGrid(); //print solved grid
    }
    else {//doesnt work
        cout << "Sorry, unsolvable..." << endl;//pirnt unsolvable
    }
    
    return 0; //return nothing
}
