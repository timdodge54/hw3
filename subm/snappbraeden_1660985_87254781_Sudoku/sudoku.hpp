/******************************************************************************
    @name sukoku header
    @brief store the class and functions
    @param none
    @retval none
 *****************************************************************************/
#ifndef sudoku_hpp
#define sudoku_hpp

class Sudoku {
    private:
        int board[9][9]; //row then column
        int fixed[9][9]; //for fixed numbers we get from prepopulation
        int checkcolumn (int c, int v); //check the column
        int checkrow (int r, int v); //check the row
        int checkbox (int r, int c, int v); //check the 3x3 box
        int findFirst (int *r, int *c); //First Square in play
        int findNext (int *r, int *c); //Next Square in play
        int findPrev (int *r, int *c); //Previous Square in play
        
    
    public:
        Sudoku (int n); //constructr
        int solve (); //to solve the soduko
        void printGrid ();//to print the gird
        void printfixed (); //REMOVE B4 SUBMISSION
};

#endif
