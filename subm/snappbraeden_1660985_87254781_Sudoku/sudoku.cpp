/******************************************************************************
    @name Sudoku.cpp
    @brief file to store all class funcitons
    @param abunch
    @retval abunch
 *****************************************************************************/
#include "sudoku.hpp"
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <ctime>
#include <cstdbool>
#define sp "  "
#define bsp "           "
#define und "----------"
using namespace std;
/******************************************************************************
    @name Sudoku
    @brief constructor for sudoku
    @param n
    @retval 1 or 0
 *****************************************************************************/
Sudoku::Sudoku (int n)
{
    int i, j, i1, j1, r, c, v; //declare varibles
    i = 0;
    i1 = 0;
    j = 0;
    j1 = 0;
    //set varibles equal to something
    
    while (i < 9){
        j = 0;
        while(j < 9){
            board[i][j] = 0;
            j++;
        }
        i++;
    }
    //set board array all equal to 0
    
    while (i1 < 9){
        j1 = 0;
        while(j1 < 9){
            fixed[i1][j1] = 1;
            j1++;
        }
        i1++;
    }
    //set fixed array all equal to 1
    srand( time(NULL));
    //used for random number generator
    while(n > 0){
        v = rand() % 9 + 1; //generates 1-9
        r = rand() % 9; //generates 0-8
        c = rand() % 9; //generates 0-8
        if(checkcolumn(c,v)&&checkrow(r,v)&&checkbox(r,c,v)&&fixed[r][c]==1){
            //check to make sure that its all good and nothing was placed b4
            board[r][c] = v;
            fixed[r][c] = 0;
            //set board and fixed @rc equal to value and 0
        } else{
            n = n+1; //adds one to try again
        }
        n = n-1; //removes one when works
    }
    printGrid(); //prints my prepopulated grid
    cout << endl; //for space
};
/******************************************************************************
    @name printGrid
    @brief have user enter numbers to prepopulate the sudoku
    @param none
    @retval none
 *****************************************************************************/
void Sudoku::printGrid ()
{
    int i, j; //i is row j is column
    i = 0; //give varibles values
    j = 0;
    
    while (i < 3){ //top 3 boxes
        j = 0;
        cout << sp; //gives some space in between varibles
        while (j < 3){ //in the first box
            if(board[i][j] == 0){ //if position is 0
                cout << "*" << sp; //place * and add some space
            } else{
                cout << board[i][j] << sp; //if not =0 cout the value there
            }
            j++; //move to next column
        }
        cout << "|" << sp; //cout divider inbetween boxes
        while (j < 6){ //2nd box
            if(board[i][j] == 0){ //if position is 0
                cout << "*" << sp; //place * and add some space
            } else{
                cout << board[i][j] << sp;//if not =0 cout the value there
            }
            j++;//move to next column
        }
        cout << "|" << sp;//cout divider inbetween boxes
        while (j < 9){//3rd box
            if(board[i][j] == 0){//if position is 0
                cout << "*" << sp;//place * and add some space
            } else{
                cout << board[i][j] << sp;//if not =0 cout the value there
            }
            j++;//move to next column
        }
        cout << endl;//add some space
        i++;//move to next row
    }
    cout << und << "-+" << und << "-+" << und << endl; //add dividers
    while (i < 6){ // middle 3 boxes
        j = 0;
        cout << sp; //gives some space in between varibles
        while (j < 3){ //in the first box
            if(board[i][j] == 0){ //if position is 0
                cout << "*" << sp; //place * and add some space
            } else{
                cout << board[i][j] << sp;//if not =0 cout the value there
            }
            j++;//next column
        }
        cout << "|" << sp;//cout divider inbetween boxes
        while (j < 6){//middle box
            if(board[i][j] == 0){//if position is 0
                cout << "*" << sp;//place * and add some space
            } else{
                cout << board[i][j] << sp;//if not =0 cout the value there
            }
            j++;//next column
        }
        cout << "|" << sp;//cout divider inbetween boxes
        while (j < 9){//last box
            if(board[i][j] == 0){//if position is 0
                cout << "*" << sp;//place * and add some space
            } else{
                cout << board[i][j] << sp;//if not =0 cout the value there
            }
            j++;//next column
        }
        cout << endl;//add space
        i++;//next row
    }
    cout << und << "-+" << und << "-+" << und << endl;//add dividers
    while (i < 9){//last 3 boxes
        j = 0;
        cout << sp;//add some space
        while (j < 3){//1st box
            if(board[i][j] == 0){//if equal to 0
                cout << "*" << sp;//place *
            } else{
                cout << board[i][j] << sp;//if not print value already there
            }
            j++;//next column
        }
        cout << "|" << sp;//divider in between boxes
        while (j < 6){//2nd box
            if(board[i][j] == 0){//if equal to 0
                cout << "*" << sp;//place a *
            } else{
                cout << board[i][j] << sp;//if not print value already there
            }
            j++;//next column
        }
        cout << "|" << sp;//space between boxes
        while (j < 9){//last box
            if(board[i][j] == 0){//if equal to 0
                cout << "*" << sp;//place a *
            } else{
                cout << board[i][j] << sp;//if not print value there
            }
            j++;//next column
        }
        cout << endl;//space
        i++;//next row
    }
    cout << endl;//more space
};
/******************************************************************************
    @name checkcolumn
    @brief check the values in each column to see if they are repeated
    @param c&v
    @retval none
 *****************************************************************************/
int Sudoku::checkcolumn (int c, int v){
    int i; //delcare varibles
    i = 0;
    
    while(i !=9){ //while not eqaul to 9
        if (board[i][c] == v) //check to see if value already exists in row
            return 0; //returns false if already exists
        else
            i++; //continue along
    }
    return 1; //returns true if doesnt exist in row
};
/******************************************************************************
    @name checkrow
    @brief check the row to see if any numbers already exsit there
    @param rc&v
    @retval none
 *****************************************************************************/
int Sudoku::checkrow (int r, int v){
    int j;//declare varibles
    j = 0;

    while(j !=9){//while not eqaul to 9
        if (board[r][j] == v) //check to see if value already exists in row
            return 0; //returns false if already exists
        else
            j++;//continue along
    }
    return 1;//returns true if doesnt exist in row
};
/******************************************************************************
    @name checkbox
    @brief have user enter numbers to prepopulate the sudoku
    @param none
    @retval none
 *****************************************************************************/
int Sudoku::checkbox (int r, int c, int v){
    int i, j, minc, maxc, minr, maxr;
    minc = (c / 3) * 3;//math for the smallest column
    maxc = minc + 2;//math for the biggest column
    minr = (r / 3) * 3;//math for the smallest row
    maxr = minr + 2;//math for the biggest row
    i = 0;
    j = 0;

    for(i = minr; i<=maxr; i++){//for loop to go throughout the box
        for(j = minc; j<=maxc; j++){//for loop to go throught the box
            if (board[i][j] == v){//check box for value
                return 0;//return false if there is one there
            }
        }
    }
    return 1;//return true if number doesnt exist in box
};
/******************************************************************************
    @name solve
    @brief see if we can solve the sudoku
    @param none
    @retval 1 or 0;
 
 Begin
 initialize grid to all zero
    n=1, (r,c)=firstSquareInPlay
    Loop
       place n at grid[x,y]
       If no violations
       Then
          n=1, (x,y)=nextSquareInPlay
          If off grid
          Then
             SUCCESS
          EndIf
       Else
          n=n+1
          If n is 10
          Then
             place 0 at grid[x,y]
             (x,y)=prevSquareInPlay
             If off grid
             Then
                FAIL
             Else
                n=grid[x,y]+1
             EndIf
          EndIf
       EndIf
    EndLoop
 if the first one becomes a 9 means its unsolvable
 *****************************************************************************/
int Sudoku::solve (){
    int v, r, c, w;
    r = 0;
    c = 0;
    v = 1;
    //declare and set varibles
    return 0; //stop before begin bc dont work
    if(findFirst(&r, &c) == 1){
        while(1){ //if doesnt work try an infinite loop
            if(checkcolumn(c,v)&&checkrow(r,v)&&checkbox(r,c,v)&&v<=9){
                //checks to see if its good
                board[r][c] = v; //set value at board location
                v = 1; //add 1 to value
                if(findNext(&r, &c) == 0){//find next available location
                    return 1; //if none found end and return true
                    }
                } else { //if not
                    v++; //add 1 to value
                    if(v == 10){//if v is greater than 9
                        board[r][c] = 0; //set board back to 0
                        if(findPrev(&r, &c) == 1){ //look for previous placement
                                v = board[r][c] + 1; //adds 1 to previous spot
                            if(v > 9){
                                board[r][c] = 0;
                                findPrev(&r, &c);
                            }
                            } else {
                                return 0;//if it doesnt work stop program
                        }
                }
            }
        }
    } else{
        return 0; //if not working stop program
    }
};
/******************************************************************************
    @name findFirst
    @brief find the first square in play
    @param *r*c
    @retval 1 or 0
 *****************************************************************************/
int Sudoku::findFirst (int* r, int* c){
    int i, j; //declare varibles
        i = 0;
        j = 0;
            
        for(i = 0; i < 9; i++){//go throught the grid and search
            for(j = 0; j < 9; j++){//go throught the grid and search
                if(fixed[i][j] == 1){//check to see if its an open spot
                    *r = i;//set value = to open spot value
                    *c = j;//set value = to open spot value
                    return 1;//return true when it happens
                }
            }
        }
        return 0;//return false if no open spot values
};
/******************************************************************************
    @name findNext
    @brief find the next square in play
    @param *r*c
    @retval 1 or 0
 *****************************************************************************/
int Sudoku::findNext (int *r, int *c){ //DEBUG THIS
    int i, j; //declare varibles
    i = *r;//set = to pointer
    j = *c;//set = to pointer

        while(i < 9){//to go throught the array
            j++;//add to column
            if(j == 9){//make sure we stayin in the array
                j = 0; //reset
                i++; //add row
                if(fixed[i][j] == 1){//check to make sure its an open spot
                    *r = i; //set pointer = value of open spot
                    *c = j;//set pointer = value of open spot
                    return 1;//return true when done
                }
            }
            if(i == 9){//make sure we stay inside array
                return 0;//end if we leave array
            }
            if(fixed[i][j] == 1){//check to make sure its an open spot
                *r = i; //set pointer = value of open spot
                *c = j;//set pointer = value of open spot
                return 1;//return true when done
            }
        }
    return 1; //return true
};
/******************************************************************************
    @name findPrev
    @brief find the previous square that was in play
    @param rc
    @retval 1 or 0
 *****************************************************************************/
int Sudoku::findPrev (int *r, int *c){ //DEBUG THIS
    int i, j; //declare varibles
    i = *r;//set value = pointer passed in
    j = *c;//set value = pointer passed in
    while(i < 9){ //go throughout the array
        j--;//move column
        if(j == 9){//make sure we stayin inside the array
            j = 0;//reset
            i--;//move row
        }
        if(i < 0){//make sure we stayin inside the array
            return 0;//return false if we outside the array
        }
        if(fixed[i][j] == 1){//check for an open spot
            *r = i;//set pointer = value of open spot
            *c = j;//set pointer = value of open spot
            return 1;//return true if works
        }
    }

    
    return 1; //return true if breaks free
};
