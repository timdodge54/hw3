#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include "sudoku.h"
using namespace std;

/******************************************************************************
*	@name		Sudoku::Sudoku
*	@brief		generates a sudoku board to be solved
*	@param		an integer supplied by the user
*	@retval		N/A
******************************************************************************/	

Sudoku::Sudoku(int n)
{
	//Set random number generator and declare variables
	srand(time(NULL));
	int i = 0;
	int x;
	int y;
	int num;
	
	//Initiate all start values on the board to 0
	for(int i = 0; i < 9; ++i)
	{
		for(int j = 0; j < 9; ++j)
		{
			board[i][j] = 0;                                                                                                                                           
		}
	}
	
	//loop until the user input number squares are filled
	i = 0;
 	for (int i = 0; i < n; ++i)
	{
		//loop until x and y are at a blank location
		for (int a = 0; a != 1;)
		{
			x = rand() % 9;	
			y = rand() % 9;
			if (board[x][y] == 0)
			{
				a = 1;
			}
			else {a = 0;}
		}
		
		//declare flagging variables that will help the
		//function not get stuck
		int i1 = 0, i2 = 0, i3 = 0, i4 = 0, i5 = 0;
		int i6 = 0, i7 = 0, i8 = 0, i9 = 0;
		//loop until a number can be placed on the board
		for (int b = 0; b != 1;)
		{
			int col, row, cell;
			num = rand() % 9 + 1;
			//check that the number placement is valid
			col = checkcolumn(y, num);
			row = checkrow(x, num);
			cell = checkcell(x, y, num);
			// if the number placement is valid, then place it and exit
			if (col == 1 && row == 1 && cell == 1)
			{
				board[x][y] = num;
				filled[x][y] = 1;
				b = 1;
			}
			//else continue looping
			else {b = 0;}
			//set corresponding flag to 1 when num is a value
			if (num == 1) {i1 = 1;}
			if (num == 2) {i2 = 1;}
			if (num == 3) {i3 = 1;}
			if (num == 4) {i4 = 1;}
			if (num == 5) {i5 = 1;}
			if (num == 6) {i6 = 1;}
			if (num == 7) {i7 = 1;}
			if (num == 8) {i8 = 1;}
			if (num == 9) {i9 = 1;}
			//if all numbers 1-9 have been tried, then get new x & y values
			if (i1 == 1 && i2 == 1 && i3 == 1
			&& i4 == 1 && i5 == 1 && i6 == 1
			&& i7 == 1 && i8 ==1 && i9 == 1)
			{
				//loop to find new location
				for (int a = 0; a != 1;)
				{
					x = rand() % 9;
					y = rand() % 9;
					if (board[x][y] == 0)
					{
						a = 1;
					}
					else {a = 0;}
				}
			}
		}
	}
}


/******************************************************************************
*	@name		Sudoku::solve
*	@brief		solves the sudoku board or marks that it is unsolvable
*	@param		N/A
*	@retval		1 if the board is solvable 0 if the board isn't solvable
******************************************************************************/	
int solve();


/******************************************************************************
*	@name		Sudoku::printline
*	@brief		prints a line of the sudoku puzzle to screen
*	@param		N/A
*	@retval		N/A
******************************************************************************/	

void Sudoku::printline(int x)
{
	//loop to print the first three numbers of row
	cout << " ";
	for(int i = 0; i < 3; ++i)
	{
		if (board[x][i] == 0)
		{
			cout << "*   ";
		}
		else
		{
			cout << board[x][i] << "   ";
		}
	}
	
	//loop to print the middle three numbers of row
	cout << "| ";
	for(int i = 3; i < 6; ++i)
	{
		if (board[x][i] == 0)
		{
			cout << "*   ";
		}
		else
		{
			cout << board[x][i] << "   ";
		}
	}
	
	//loop to print the last three numbers of row
	cout << "| ";
	for(int i = 6; i < 9; ++i)
	{
		if (board[x][i] == 0)
		{
			cout << "*   ";
		}
		else
		{
			cout << board[x][i] << "   ";
		}
	}
	
	cout << endl;

}

/******************************************************************************
*	@name		Sudoku::printGrid
*	@brief		prints a sudoku grid to the screen
*	@param		N/A
*	@retval		N/A
******************************************************************************/	

void Sudoku::printGrid()
{
	//call print line to print the board
	printline(0);
	printline(1);
	printline(2);
	cout << "-------------+-------------+------------" << endl;
	printline(3);
	printline(4);
	printline(5);
	cout << "-------------+-------------+------------" << endl;
	printline(6);
	printline(7);
	printline(8);
	cout << endl;
}

/******************************************************************************
*	@name		Sudoku::checkrow
*	@brief		checks to see if number placement is valid in current row
*	@param		x coordinate and the number value
*	@retval		0 if false (don't place number) and 1 if true (place number)
******************************************************************************/	

int Sudoku::checkrow(int x, int num)
{
	//loop until value on row matches num or 9 iterations
	for(int i = 0; i < 9; ++i)
	{
		if (board[x][i] == num)
		{
			//return false
			return 0;
		}
	}
	
	//return true
	return 1;
}

/******************************************************************************
*	@name		Sudoku::checkcolumn
*	@brief		checks to see if number placement is valid in current column
*	@param		y coordinate and the number value
*	@retval		0 if false (don't place number) and 1 if true (place number)
******************************************************************************/	

int Sudoku::checkcolumn(int y, int num)
{
	//loop until value in column matches num or 9 iterations
	for(int i = 0; i < 9; ++i)
	{
		if (board[i][y] == num)
		{
			//return false
			return 0;
		}
	}
	
	//return true
	return 1;
}


/******************************************************************************
*	@name		Sudoku::checkcell
*	@brief		checks to see if number placement is valid in current column
*	@param		y coordinate and the number value
*	@retval		0 if false (don't place number) and 1 if true (place number)
******************************************************************************/	

int Sudoku::checkcell(int x, int y, int num)
{
	//loop to check first cell values don't match num
	if (x >= 0 && x <= 2 && y >= 0 && y <= 2)
	{
		for(int i = 0; i < 3; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				if (board[i][j] == num)
				{
					return 0;
				}
			}
		}
	}
	
	//loop to check second cell values don't match num
	if (x >= 0 && x <= 2 && y >= 3 && y <= 5)
	{
		for(int i = 0; i < 3; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				if (board[i][j+3] == num)
				{
					return 0;
				}
			}
		}
	}
	
	//loop to check third cell values don't match num
	if (x >= 0 && x <= 2 && y >= 6 && y <= 8)
	{
		for(int i = 0; i < 3; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				if (board[i][j+6] == num)
				{
					return 0;
				}
			}
		}
	}
	
	//loop to check fourth cell values don't match num
	if (x >= 3 && x <= 5 && y >= 0 && y <= 2)
	{
		for(int i = 0; i < 3; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				if (board[i+3][j] == num)
				{
					return 0;
				}
			}
		}
	}
	
	//loop to check fifth cell values don't match num
	if (x >= 3 && x <= 5 && y >= 3 && y <= 5)
	{
		for(int i = 0; i < 3; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				if (board[i+3][j+3] == num)
				{
					return 0;
				}
			}
		}
	}
	
	//loop to check sixth cell values don't match num
	if (x >= 3 && x <= 5 && y >= 6 && y <= 8)
	{
		for(int i = 0; i < 3; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				if (board[i+3][j+6] == num)
				{
					return 0;
				}
			}
		}
	}
	
	//loop to check seventh cell values don't match num
	if (x >= 6 && x <= 8 && y >= 0 && y <= 2)
	{
		for(int i = 0; i < 3; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				if (board[i+6][j] == num)
				{
					return 0;
				}
			}
		}
	}
	
	//loop to check eighth cell values don't match num
	if (x >= 6 && x <= 8 && y >= 3 && y <= 5)
	{
		for(int i = 0; i < 3; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				if (board[i+6][j+3] == num)
				{
					return 0;
				}
			}
		}
	}
	
	//loop to check last cell values don't match num
	if (x >= 6 && x <= 8 && y >= 6 && y <= 8)
	{
		for(int i = 0; i < 3; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				if (board[i+6][j+6] == num)
				{
					return 0;
				}
			}
		}
	}
	
	//return true if the cell values don't match num
	return 1;
}

