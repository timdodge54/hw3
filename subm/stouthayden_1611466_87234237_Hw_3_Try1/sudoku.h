#ifndef SUDOKU_H
#define SUDOKU_H

class Sudoku {
	private:
		int checkrow(int x, int num);
		int checkcolumn(int y, int num);
		int checkcell(int x, int y, int num);
		void printline(int x);
		int findfirst(int *x, int *y);
		int findnext(int *x, int *y);
		int findprev(int *x, int *y);
		
		int board[9][9];
		int filled[9][9];
		
	public:
		Sudoku(int n); 
		int solve();
		void printGrid();
		
};

#endif