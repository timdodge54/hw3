#include "sudoku.h"
#include <iostream>
using namespace std;

/******************************************************************************
*	@name		main
*	@brief		Generates a sudoku board and then returns the solution to the
				board or an error message if the board is unsolvable
*	@param		none
*	@retval		none
******************************************************************************/
int main()
{
int n;
cout << "Welcome to SudokuSolver!!" << endl;
cout << "Enter number of squares to prepopulate: ";
cin >> n;
Sudoku s(n);

s.printGrid();
/* if (s.solve()) {
cout << "Solved!" << endl;
s.printGrid();
}
else {
cout << "Sorry, unsolvable..." << endl;
} */
return 0;
}