/***********************************************************************************************************************************
* Class Sudoku
*
* Summary: A class for generating and solving and printing a randomly prepopulated sudoku puzzle.
*
************************************************************************************************************************************/

#pragma once

class Sudoku {

private:
	
	//private function to check rows.
	//int checkRow(int val, int y);
	//int checkCol(int val, int x);
	// int checkBox(int val, int x, int y);
	// int findFirstSquareInPlay(int *x, int *y);
	// int findNextSquareInPlay(int *x, int *y);
	// int findPrevSquareInPlay(int *x, int *y);
	// 
	// 
	//private function to check columns.

	//private function to check 9, 3x3 grids.

	//int grid[9][9];
	//int fixed[9][9];



public:
	//constructor verifies the prepopulated inputs, uses 3 private class functions to verify
	//constructor also generates 3 random numbers, 2 for random locations within the array and
	// 1 to put in that location
	Sudoku(int n);

	//class function to solve the prepopulated array 
	//contain backtracking algorithm
	int solve();
	
	//print the solved puzzle
	void printGrid();
















};
