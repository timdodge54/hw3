/************************************************************************************************************************************
* @name     Sudoku class functions
* @brief    constructor to prepopulate and verify values, solve function and print function 
* @param    none
* @retval   function solve returns a value to tell main if the puzzle was solved or failed
*************************************************************************************************************************************/

#include <iostream>
#include <cmath>
#include <iomanip>
#include "sudoku.h"

using namespace std;
int s[9][9];
//constructor definition, should prepopulate the grid with n values and check that the values are
//valid in their rows, columns and smaller 3x3 grid
//contains srand function to get 3 random values, two for location in the array and 1 for the value
Sudoku::Sudoku(int n)
{
	//generate n random numbers and put into random locations in the array
	
		srand(time(NULL));

		int r = 0, c = 0;
		int i = 0;
		while (i < n)
		{
			r = 0, c = 0;
			s[r = (rand() % 9)][c = (rand() % 9)] = (rand() % 9) + 1;                    //put random number in random location in the array

			i++;
		}




	//verify numbers in locations 

	
	//call print function to display with prepopulated values
	printGrid();


}

//class function definition for solving the sudoku puzzle
//contains backtracking algorithm
int Sudoku::solve()
{
	int test = 0;


	//2nd array	1 keeps the fixed values 




	//tricky part of backtracking algorithm is checking the 3x3 grid
	 
	//check row: int i,j;


	/*
	for (i=mincol; i <= maxcol; i++)
	{

	for (j=minrow; j<= maxrow; j++)
		{
		if(grid[i][j]==val)

		columns and rows for 3x3 grid
		0-2  3-5  6-8

		checkgrid(int col,int row, int val)

		col==0,1 or 2 look in column 0

		mincol=(col/3) * 3   int divition will truncate remainder if column coming in is 0-2 == 0 if 3-5 ==3 6-8 == 6

		maxcol = mincol + 2;
	*/


	if (test > 20)
		return 1;
	else
		return 0;

	//if it was solved return 1
	//if it couldnt be solved return 0
}

//class function definition for printing the solved puzzle
void Sudoku::printGrid()
{
	for (int r = 0; r < 9; r++) 
		{
		if (r == 3)
			cout << endl << "------------+--------------+--------------";
		else
			cout << endl;
		if (r == 6)
			cout << "------------+--------------+--------------" << endl;
		else
			cout << endl;
		for (int c = 0; c < 9; c++) 
		{
			if (c == 3)
				cout  << "|" << " " << " ";
			if (c == 6)
				cout << "|" << " " << " ";
			if (s[r][c] == 0)
				cout  << "*" << " " << " " << " ";
			else
			cout  << s[r][c] << " " << " " << " ";

		}
	}
}