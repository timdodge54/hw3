/*************************************************************************************************************************************
* @name    main
* @brief   prompt user to choose number of squares to prepopulate, work with class sudoku to prepopulate, verify, solve and display.
*          report to the user.
* @param   none
* @retval  none
***************************************************************************************************************************************/

#include "sudoku.h" 
#include <iostream> 

using namespace std;

int main()
{
	int n;

	cout << "Welcome to SudokuSolver!!" << endl;
	cout << "Enter number of squares to prepopulate: ";
	cin >> n;

	//constructor
	Sudoku s(n);

	//output to the user if the puzzle is solved or not
	if (s.solve()) {
		cout << "Solved!" << endl;
		s.printGrid();
	}
	else {
		cout << "Sorry, unsolvable..." << endl;
	}

	return 0;
}






















//3 private functions for checking rows column and 3x3 grid
//array int a[9][9] index 0-8
//print original grid and final solution
//while debugging print the grid over and over watch backtracking algorithm
//keep track of prepopulated values somehow.
//srand seed the random function
